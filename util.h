#ifndef _UTIL_H_
#define _UTIL_H_

class Util {

public:
        static void Reboot();
        static bool CalibrateTime(const char *time);
        static bool From_sys_clock();
        static int SetBaseParams(const char *filename, const char *sec,
                                 const char *key, const char *val);
        static int GetBaseParams(const char *filename, const char *sec,
                                 const char *key, const char *def, char *val);
        static int GetVersionParams(const char *filename, const char *sec,
                                const char *key, const char *def, char *val);
};

#endif
