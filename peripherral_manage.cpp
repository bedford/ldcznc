/**
 * @file	peripherral_manage.cpp
 * @brief	外设管理模块实现
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2011-12-20
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 *
 * @revise	取用内存队列代替原来的new/delete
 * @date	2012-3-13
 * @version	1.1.0
 *
 */

#include <list>
#include <sys/time.h>
#include "mutex.h"
#include "peripherral_manage.h"
#include "debug.h"
#include "mem_pool.h"

static std::list < TriggerInfo * >trigger_list;
static Mutex list_mutex;

static bool disable_recv        = false;
static MemPool *_mem_pool       = NULL;
static const int kListLength    = 10;

PeripherralManage::PeripherralManage()
{

}

PeripherralManage::~PeripherralManage()
{

}

void PeripherralManage::Init()
{
        _mem_pool = new MemPool(sizeof(TriggerInfo), kListLength);
}

void PeripherralManage::Deinit()
{
        ClearList();
        _mem_pool->Release();
        delete _mem_pool;
        _mem_pool = NULL;
}

void PeripherralManage::EnableRecv()
{
        disable_recv = false;
}

void PeripherralManage::DisableRecv()
{
        disable_recv = true;
        ClearList();
}

/**
 *
 * @fn		bool GetTriggerInfo(trigger_info *info)
 * @brief	从触发数据队列获取触发数据
 *
 * @param	[out] info	从队列中获取触发数据
 *
 * @return	是否有触发数据
 * @retval	true | false
 */
bool PeripherralManage::GetTriggerInfo(TriggerInfo ** info)
{
        if (list_mutex.Lock()) {
                if (trigger_list.empty()) {
                        return false;
                }

                *info = trigger_list.front();
                trigger_list.pop_front();
                list_mutex.Unlock();
                return true;
        } else {
                return false;
        }
}

static inline bool IsTimeout(const TriggerInfo *list_info, const TriggerInfo *new_info)
{
        long delta = (new_info->passing_time.tv_sec - list_info->passing_time.tv_sec) * 1000 +
            (new_info->passing_time.tv_usec - list_info->passing_time.tv_usec) / 1000;
        Debug(INFO, "trigger info time delta is %ld", delta);
        if (delta > 500) {
                return true;
        }

        return false;
}

/**
 * @fn		bool PutTriggerInfo(const trigger_info *info)
 * @brief	将触发数据放入队列中
 *
 * @param	[in] info	传入触发数据
 *
 * @return	是否传入触发数据成功
 * @retval	true | false
 */
bool PeripherralManage::PutTriggerInfo(const TriggerInfo * info)
{
        if (disable_recv) {
                return false;
        }

        if (list_mutex.Lock()) {
                std::list < TriggerInfo * >::iterator iter = trigger_list.begin();
                while (iter != trigger_list.end()) {
                        if (IsTimeout((*iter), info)) {
                                ReleaseTriggerInfo(*iter);
                                iter = trigger_list.erase(iter);
                                Debug(DEBUG, "delete timeout trigger info");
                        } else {
                                break;
                        }
                }
                
                if (GetTriggerListSize() == kListLength) {
                        ClearListFront();
                }

                PushListBack(info);
                list_mutex.Unlock();
                return true;

        } else {

                return false;
        }
}

/**
 * @fn		int GetTriggerListSize()
 * @brief	获取触发数据队列的长度
 *
 * @return	队列的长度
 */
int PeripherralManage::GetTriggerListSize()
{
        return (int)trigger_list.size();
}

/**
 * @fn		bool ReleaseTriggerInfo(TriggerInfo *info)
 * @brief	释放触发数据内存
 *
 * @param	info
 *
 * @return	
 */
bool PeripherralManage::ReleaseTriggerInfo(TriggerInfo * info)
{
        _mem_pool->DeAlloc((void *)info);
        info = NULL;
        return true;
}

/**
 * @fn		void ClearList()
 * @brief	清除队列中的数据
 *
 * @param	null
 * @return	
 */
void PeripherralManage::ClearList()
{
        if (list_mutex.Lock()) {
                TriggerInfo *del_info = NULL;
                while (!trigger_list.empty()) {
                        del_info = trigger_list.front();
                        trigger_list.pop_front();
                        ReleaseTriggerInfo(del_info);
                }
                list_mutex.Unlock();
        }
        Debug(INFO, "Clear trigger info list");
}

/**
 * @fn		void ClearListFront()
 * @brief	删除队列第一个数据
 *
 * @param	null
 * @return
 */
void PeripherralManage::ClearListFront()
{
        TriggerInfo *del_info = NULL;
        del_info = trigger_list.front();
        trigger_list.pop_front();
        ReleaseTriggerInfo(del_info);
}

/**
 * @fn		bool PushListBack(const TriggerInfo *info)
 * @brief	将触发数据放到队尾
 *
 * @param	[in] info 触发数据
 *
 * @return	
 */
bool PeripherralManage::PushListBack(const TriggerInfo * info)
{
        TriggerInfo *tmpInfo    = (TriggerInfo *) _mem_pool->Alloc();
        if (tmpInfo == NULL) {
                return false;
        }

        tmpInfo->lane           = info->lane;
        tmpInfo->vehicle_speed  = info->vehicle_speed;
        tmpInfo->vehicle_length = info->vehicle_length;
        tmpInfo->snap_verify_ID = info->snap_verify_ID;
        tmpInfo->drive_mode     = info->drive_mode;
        tmpInfo->offence_type   = info->offence_type;
        tmpInfo->passing_time   = info->passing_time;
        tmpInfo->redlight_time  = info->redlight_time;

        memcpy(tmpInfo->snap_nbr, info->snap_nbr, sizeof(info->snap_nbr));
        trigger_list.push_back(tmpInfo);

        return true;
}

/**
 * @fn       int GetTriggerListAvailableSize()
 *
 * @brief    获取触发信息有效长度,避免同一车道的触发信息使用相同的图片
 *           在FPGA曝光前一张图像的后半段触发时,FPGA会将连拍的两张图片往后传,这时已经收到两次触发信息,
 *           如果将同一车道的信息都跟图像合并,则会出现违法抓拍图片中有两相机图片相同(车辆无位移的情况)
 *           现实使用中,也不可能同时有两辆车在那么短的时间间隔内经过同一个车道同一位置
 *
 * @return      
 */
int PeripherralManage::GetTriggerListAvailableSize()
{
        int list_size = GetTriggerListSize();
        if (list_size < 2) {
                return list_size;
        }

        if (!list_mutex.Lock()) {
                return list_size;
        }

        int available_size = 1;
        TriggerInfo *first_info = trigger_list.front();
        std::list < TriggerInfo * >::iterator iter = trigger_list.begin();
        ++iter;
        while (iter != trigger_list.end()) {
                if (first_info->lane == (*iter)->lane) {
                        break;
                } else {
                        ++iter;
                        ++available_size;
                }
        }

        first_info = NULL;
        list_mutex.Unlock();

        return available_size;
}

