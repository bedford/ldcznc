/**
 * @file        survey.cpp
 * @brief       车流量统计及心跳包和车流量上传功能
 * @author      hrh <huangrh@landuntec.com>
 * @version     1.0.0
 * @date        2012-02-06
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011-2012
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include "parameters.h"
#include "debug.h"
#include "snap_type.h"
#include "survey.h"
#include "Adaptor.h"

typedef struct {
        int passing_number;
        int red_light_violation_number;
        int reverse_violation_number;
        int total_number;
} PerLaneStatics;

/**
 * @brief       单个相机支持的最大车道数,以实际的device.lane的车道个数来统计
 */
static const int kMaxLane = 5;
static PerLaneStatics per_lane_statics[kMaxLane];

static int LastMin;
static bool bInit = false;

void Survey::Init()
{
        char tmp[128];
        struct timeval tv;
        struct tm *pTm;
        gettimeofday(&tv, NULL);
        pTm = localtime(&(tv.tv_sec));
        strftime(tmp, 128, "%Y-%m-%d %H:%M:%S", pTm);
        char start_time[32];
        sprintf(start_time, "%s.%03d", tmp, (int)(tv.tv_usec / 1000));

        LastMin = pTm->tm_min;

        Adaptor::InitAdaptor();
        bInit = true;
}

void Survey::DeInit()
{
        if (bInit) {
                Adaptor::ReleaseAdaptor();
                bInit = false;
        }
}

int Survey::TrafficStaticsUpload()
{
        char tmp[128];
        struct tm *pTm;
        struct timeval tv;
        gettimeofday(&tv, NULL);
        pTm = localtime(&(tv.tv_sec));

        DeviceParam params;
        TrafficParam traffic_param      = Parameters::GetInstance()->GetTrafficParam();
        DeviceInfo info                 = Parameters::GetInstance()->GetDeviceInfo();
        params.device_info              = &info;
        params.traffic_params           = &traffic_param;

        int period = traffic_param.period;
        bool timeout = ((period == 1) && (LastMin != pTm->tm_min)) ||
            ((LastMin % period) && (!(pTm->tm_min % period)));

        if (timeout) {
                TrafficStatus traffic_status;
                memset(&traffic_status, 0, sizeof(TrafficStatus));
                traffic_status.period = period;
                strftime(tmp, 128, "%Y-%m-%d %H:%M:%S", pTm);
                sprintf(traffic_status.end_time, "%s.%03d", tmp, (int)(tv.tv_usec / 1000));
                int max_lane = info.lane;

                for (int i = 0; i < max_lane; i++) {
                        traffic_status.total_num = per_lane_statics[i].total_number;
                        traffic_status.lane = i + 1;

                        if (Adaptor::SendTrafficStatus(&params, &traffic_status)) {
                                printf("success to send traffice data\n");
                        }
                }
                TrafficStaticsReset();
        }
        LastMin = pTm->tm_min;

        return 0;
}

int Survey::PassingStaticsUpdate(PassingStatus * data)
{
        switch (data->event_type) {
        case DeviceSnapType_RedLightPass:
                per_lane_statics[data->lane - 1].red_light_violation_number++;
                break;
        case DeviceSnapType_ReversePass:
                per_lane_statics[data->lane - 1].reverse_violation_number++;
                break;
        case DeviceSnapType_NormalPass:
                per_lane_statics[data->lane - 1].passing_number++;
                break;
        default:
                break;
        }
        per_lane_statics[data->lane - 1].total_number++;

        return 0;
}

void Survey::TrafficStaticsReset()
{
        memset(per_lane_statics, 0, sizeof(per_lane_statics));
}

int Survey::DeviceStatusUpload()
{
        if (bInit) {
                DeviceInfo info                 = Parameters::GetInstance()->GetDeviceInfo();
                TrafficParam traffic_param      = Parameters::GetInstance()->GetTrafficParam();
                NetworkParam network_param      = Parameters::GetInstance()->GetNetworkParam();
                UploadParam upload_param        = Parameters::GetInstance()->GetUploadParam();
                DeviceStatus status             = Parameters::GetInstance()->GetDeviceStatus();

                DeviceParam params;
                params.device_info              = &info;
                params.traffic_params           = &traffic_param;
                params.network_params           = &network_param;
                params.upload_params            = &upload_param;

                Adaptor::SendHeartBeat(&status, &params);
                TrafficStaticsUpload();
        } else {
                Debug(DEBUG, "Not init survey module");
        }

        return 0;
}
