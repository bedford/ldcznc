/**
 * @file ce_buffer.h
 * @brief	CMem缓存管理接口
 * @author hrh
 * @version 1.0.0
 * @date 2011-10-16
 *
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef CE_BUFFER_H_
#define CE_BUFFER_H_

#ifdef __cplusplus
extern "C" {
#endif

enum ImageFormat {

	Gray,
	Bayer8GBRG,
	Bayer8GRBG,
	Bayer8BGGR,
	Bayer8RGGB,
	RGB24RGB,
	RGB24BGR,
	YUV444,
	UYVY,
	YUYV,
	YUV411,
	YUV420,
	JPEG,
	JPEG2000,
};

	
struct cmemBuffer {

        unsigned int	phy_addr;		/* physical addr 		*/
        unsigned int	vir_addr;		/* virtual addr 		*/
        int		size;			/* image data size 		*/
        int		width;			/* image width 			*/
        int		height;			/* image height 		*/
        int		stride;			/* actual pixels per line	*/
	int		line;			/* actual bytes per line	*/
        int		extra_size;		/* extra size			*/
        int		index;			/* index of cmem 		*/
	int		bpp;			/* bits per pixel		*/
	enum		ImageFormat format;	/* image format			*/
	int		offset;			/* first byte start offset	*/

} ;


int buffer_create(struct cmemBuffer *buffer);
int buffer_delete(struct cmemBuffer *buffer);
int buffer_createEx(void **buf, unsigned int size);
int buffer_deleteEx(void *buf, unsigned int size);


#ifdef __cplusplus
}
#endif

#endif
