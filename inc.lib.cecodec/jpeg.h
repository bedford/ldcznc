/**
 * @file jpeg.h
 * @brief	
 * @author hrh
 * @version 1.0.0
 * @date 2011-10-16
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef LIB_CECODEC_JPEG_H_
#define LIB_CECODEC_JPEG_H_

#ifdef __cplusplus
extern "C" {
#endif

struct jpeg_params {
        int width;
        int height;
        int stride;
        int qvalue;
        int rotation;
};

int jpeg_init(struct jpeg_params *params);
int jpeg_exit(void);

/**
 * @fn     int jpeg_encode(const void *uyvy_buf, int uyvy_len,
 *                        void *jpeg_buf, int *jpeg_len);
 * @brief  jpgenc演示
 * @return 程序是否成功退出
 */
int jpeg_encode(const void *uyvy_buf, int uyvy_len,
               void *jpeg_buf, int *jpeg_len);


/**
 * @fn		void set_jpegenc_param (struct jpeg_params *params)
 * @brief	设置jpeg encoder的动态参数
 * @param	params
 */
void set_jpegenc_param(struct jpeg_params *params);


const char *jpeg_get_version(void);

#ifdef __cplusplus
}
#endif

/**
 * @page LIB_CECODEC_JPEG_HOW 如何使用JPEG编解码接口?
 * @section LIB_CECODEC_JPEG_HOW_STEP 使用方法
 * <pre>
 * 1. jpeg_init()
 * 2. jpegencode() & jpegdecode()
 * 3. jpeg_exit()
 * </pre>
 * @section LIB_CECODE_JPEG_HOW_LIMIT 使用限制
 * <pre>
 * 1. 编码
 *    (1)图像格式 : 只支持UYVY排列方式
 * 2. 解码
 *    (1)图像格式 : 只支持UYVY输出
 * </pre>
 * @section LIB_CECODEC_JPEG_HOW_EXAMPLE 示例代码
 *
 * @code
 * #include "jpeg.h"
 *
 * int main()
 * {
 *         jpeg_init();
 *         jpeg_decode_file(
 *         jpeg_exit();
 *
 *         return 0;
 * }
 * @endcode
 */

#endif /* LIB_CECODEC_JPEG_H_ */
