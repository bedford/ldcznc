/**
 * @file        image_process.cpp
 * @brief       图像处理类
 * @author      hrh <huangrh@landuntec.com>
 * @version     1.0.0
 * @date        2012-05-16
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <iostream>
#include <unistd.h>
#include <time.h>

#include "image_buffer.h"
#include "image_process.h"
#include "overlay.h"
#include "SHA1.h"
#include "debug.h"
#include "snap_type.h"

#include "parameters.h"
#include "property.h"

static const char *kFontName    = "wqy-zenhei.pcf";
static const int kFontSize      = 32;

static const int kMaxJpegLength         = 1024 * 1024;
static const int kDefaultJpegQvalue     = 70;

static const int kMaxTextNumber = 13;
static const int kOverlayTextLength = 128;
static struct overlay_text text[kMaxTextNumber];
static char overlay_array[kMaxTextNumber][kOverlayTextLength];

static char AuthorityCode[20] = {0};

static const int kMaxLightZoneNumber    = 6;
static Rectangle kSignalLightZone[kMaxLightZoneNumber];

static const int kBaseAngle             = 90;
static const enum OverlayAngle kOverlayAngle[] = {
                        OverlayAngle0,
                        OverlayAngle90,
                        OverlayAngle180,
                        OverlayAngle270
};

static const char *kEventDescriptionText[] = {
        "违章通行",
        "闯红灯",
        "正常通行",
        "超高速",
        "逆行",
        "手动抓拍"
};

static const char *kDeviceNumberLabel = "设备编号:";
static const char *kSiteLabel = "地点:";
static const char *kRedLightTimingLabel = "红灯开始时间:";
static const char *kDirectionLabel = "方向:";
static const char *kPassingTyepLabel = "通行方式:";
static const char *kLaneLabel = "车道:";
static const char *kCaptureTimingLabel = "抓拍时间:";

static inline void time2str(char *time, const struct timeval *tv)
{
        struct tm *tm = localtime(&(tv->tv_sec));
        sprintf(time, "%d-%02d-%02d %02d:%02d:%02d.%03d",
                tm->tm_year + 1900,
                tm->tm_mon + 1,
                tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
                (int)(tv->tv_usec / 1000));
}

ImageProcess::ImageProcess(int type)
{
        _zone_number    = 0;
        _status         = CAPTURE;

        InitProcessor(type);
}

void ImageProcess::InitProcessor(int type)
{
        DeviceInfo info = Parameters::GetInstance()->GetDeviceInfo();
        GetAuthentication(info.serial_number, AuthorityCode);

        Debug(DEBUG, "snap mode is %d", info.snap_mode);
        char font_name[32];
        const char *path = getenv("PATH_APPFS_FONT");
        sprintf(font_name, "%s/", path);
        strcat(font_name, kFontName);

        struct overlay_font overlay_font;
        overlay_font.name       = font_name;
        overlay_font.size       = kFontSize;
        overlay_font.angle      = kOverlayAngle[info.snap_mode];
        overlay_font.mode       = AUTO_SETTING;
        overlay_init(&overlay_font);

        CameraProperty property = Property::GetDeviceProperty(type);
        _param.width    = property.width;
        _param.height   = property.height_useful;
        _param.stride   = property.stride;
        _param.qvalue   = kDefaultJpegQvalue;
        _param.rotation = info.snap_mode * kBaseAngle;
        jpeg_init(&_param);

        RedLightZone redlight_zone =
                Parameters::GetInstance()->GetRedlightZone();
        SetRedlightZoneProcess(&redlight_zone);
}

ImageProcess::~ImageProcess()
{
        jpeg_exit();
        overlay_deinit();
        Debug(DEBUG, "deconstruct ImageProcess");
}

void ImageProcess::SetJpegQvalue(int value)
{
        _param.qvalue = value;
        set_jpegenc_param(&_param);
}

void ImageProcess::GetWatermark(char *pWmcode)
{
        char *pcode             = pWmcode;
        int WmCodelength        = 16;
        int iFlag               = 62;
        int i, k;

        clock_t start_time;
        clock_t cur_time;

        char allArray[63] = "abcdefghijklmnopqrstuvwxyzABCDEFG" \
                "HIJKLMNOPQRSTUVWXYZ1234567890";

        start_time = clock();
        do {
                cur_time = clock();
        } while ((cur_time - start_time) < 3);

        srand((unsigned)time(NULL) + clock());

        for (i = 0; i < WmCodelength; i++) {
                k = rand();
                k = k % iFlag;
                pcode[i] = allArray[k];
        }

        pcode[WmCodelength] = '\0';
}

int ImageProcess::GetAuthentication(char *serialNumber, char *pAucode)
{
        int AuCodeLength = 20;
        char *pcode = pAucode;
        char *p;
        int ia = 1;
        int ib = 0;
        double dc = 0;
        double dseed = 0;

        char upperArray[37] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        for (p = serialNumber; p < serialNumber + strlen(serialNumber); p++) {
                ia *= 10;
                ib = toascii(*p);
                dc = ia * ib;
                dseed += dc;
        }

        srand((unsigned)dseed);
        int iFlag = 36;

        int i, k;
        for (i = 0; i < AuCodeLength; i++) {
                k = rand();
                k = k % iFlag;
                pcode[i] = upperArray[k];
        }

        pcode[AuCodeLength] = '\0';

        return 0;
}

int ImageProcess::EmbedAntiCounterfeitInfo(const unsigned char *au_code,
                                           unsigned int au_code_len,
                                           const unsigned char *wm_code,
                                           unsigned int wm_code_len,
                                           unsigned char *jpeg_buf,
                                           unsigned int *jpeg_buf_len)
{
        unsigned int i;
        int len = *jpeg_buf_len;

        if ((au_code == NULL) || (wm_code == NULL)) {
                return -1;
        }
        if (jpeg_buf == NULL) {
                return -1;
        }

        /* au code offset is 150 */
        for (i = 0; i < au_code_len; ++i) {
                jpeg_buf[len++] = au_code[i] + 150;
        }

        /* wm code offset is 100 */
        for (i = 0; i < wm_code_len; ++i) {
                jpeg_buf[len++] = wm_code[i] + 100;
        }

        *jpeg_buf_len = len;

        return 0;
}

int ImageProcess::CreateJPGFileHash(unsigned char *jpeg_buf,
                                    unsigned int *jpeg_buf_len)
{
        //压缩数据加密
        CSHA1 *pSha1 = NULL;
        int bCreatOk = 0;

        pSha1 = new CSHA1();
        if (pSha1 == NULL) {
                return -1;
        }

        bCreatOk = pSha1->CreateJPGBufferHash(jpeg_buf, jpeg_buf_len);
        delete pSha1;

        return bCreatOk;
}

bool ImageProcess::OverlayText(struct cmemBuffer & imageInfo,
                               const struct overlay_text * text,
                               int len)
{
        overlay_multi(&imageInfo, text, len);
        return true;
}

bool ImageProcess::JpegEncodeProcess(struct cmemBuffer & imageInfo,
                                     struct cmemBuffer * outBuffer)
{
        jpeg_encode((void *)imageInfo.vir_addr, imageInfo.size,
                    (void *)outBuffer->vir_addr, &outBuffer->size);

        outBuffer->width        = imageInfo.width;
        outBuffer->height       = imageInfo.height;
        outBuffer->line         = imageInfo.line;

        return true;
}

int ImageProcess::VideoProcess(struct cmemBuffer *buffer,
                               struct image_buffer_description *image)
{
        if (_status == CAPTURE) {
                _status = VIDEO;
                usleep(2000);
        }

        for (int index = 0; index < _zone_number; index++) {
                ReviseRedlight(*buffer, kSignalLightZone[index]);
        }

        JpegEncodeProcess(*buffer, &image->info);

        return 0;
}

int ImageProcess::TextPrepare(TriggerInfo *info)
{
        DeviceInfo device_info = Parameters::GetInstance()->GetDeviceInfo();
        OverlaySetting overlay_setting = Parameters::GetInstance()->GetOverlaySetting();
        int overlay_text_number = 0;

        /* 设备号 */
        if (overlay_setting.enable_device_number) {
                sprintf(overlay_array[overlay_text_number], "%s%s",
                                kDeviceNumberLabel, device_info.device_number);
                text[overlay_text_number].text = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        /* 抓拍时间 */
        if (overlay_setting.enable_timestamp) {
                char tmp[128];
                time2str(tmp, &info->passing_time);
                sprintf(overlay_array[overlay_text_number], "%s%s",
                                kCaptureTimingLabel, tmp);
                text[overlay_text_number].text  = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        /* 安装地点 */
        if (overlay_setting.enable_site) {
                sprintf(overlay_array[overlay_text_number], "%s%s",
                                kSiteLabel, device_info.site);
                text[overlay_text_number].text  = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        /* 车道 */
        if (overlay_setting.enable_lane && (info->lane > 0)) {
                sprintf(overlay_array[overlay_text_number], "%s%d",
                                kLaneLabel, info->lane);
                text[overlay_text_number].text  = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        /* 方向 */
        if (overlay_setting.enable_direction) {
                sprintf(overlay_array[overlay_text_number], "%s%s",
                                kDirectionLabel, device_info.direction_text);
                text[overlay_text_number].text  = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        /* 红灯开始时间 */
        if ((overlay_setting.enable_redlight_time) &&
                        (info->offence_type == DeviceSnapType_RedLightPass)) {
                sprintf(overlay_array[overlay_text_number], "%s%d秒",
                                kRedLightTimingLabel, info->redlight_time);
                text[overlay_text_number].text  = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        /* 通行方式     */
        if (overlay_setting.enable_passing_type) {
                sprintf(overlay_array[overlay_text_number], "%s%s",
                                kPassingTyepLabel,
                                kEventDescriptionText[info->offence_type]);
                text[overlay_text_number].text  = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        /* 自定义信息1 */
        if (overlay_setting.enable_extra_info1) {
                sprintf(overlay_array[overlay_text_number], "%s",
                                overlay_setting.extra_overlay_info1);
                text[overlay_text_number].text  = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        /* 自定义信息2 */
        if (overlay_setting.enable_extra_info2) {
                sprintf(overlay_array[overlay_text_number], "%s",
                                overlay_setting.extra_overlay_info2);
                text[overlay_text_number].text  = overlay_array[overlay_text_number];
                overlay_text_number++;
        }

        return overlay_text_number;
}

int ImageProcess::CaptureProcess(struct cmemBuffer *buffer,
                                 struct image_buffer_description *image,
                                 TriggerInfo * info)
{
        if (_status == VIDEO) {
                _status = CAPTURE;
                usleep(2000);
        }

        for (int index = 0; index < _zone_number; index++) {
                ReviseRedlight(*buffer, kSignalLightZone[index]);
        }

        STATICS_START("Overlay\t\t");
        DeviceInfo device_info = Parameters::GetInstance()->GetDeviceInfo();
        bool bLimitedPassFlag = (info->offence_type == DeviceSnapType_NormalPass)
                        || (info->offence_type == DeviceSnapType_ReversePass);
        if ((device_info.device_mode == LimitedPassViolation)
                        && bLimitedPassFlag) {
                info->offence_type = DeviceSnapType_LimitedPass;
        }

        int overlay_text_number = TextPrepare(info);
        OverlayText(*buffer, text, overlay_text_number);
        STATICS_STOP();

        JpegEncodeProcess(*buffer, &image->info);
        if (image->info.size > kMaxJpegLength) {
                Debug(WARN, "image info size is %d, max length is %d",
                                image->info.size, kMaxJpegLength);
                return -1;
        }

        char watermark[32];
        GetWatermark(watermark);
        EmbedAntiCounterfeitInfo((unsigned char *)AuthorityCode,
                                 strlen(AuthorityCode),
                                 (unsigned char *)watermark,
                                 strlen(watermark),
                                 (unsigned char *)image->info.vir_addr,
                                 (unsigned int *)&image->info.size);
        CreateJPGFileHash((unsigned char *)image->info.vir_addr,
                        (unsigned int *)&image->info.size);

        return 0;
}

void ImageProcess::SetRedlightZoneProcess(RedLightZone *redlight_zone)
{
        _zone_number = redlight_zone->rect_num;
        if (_zone_number > kMaxLightZoneNumber) {
                _zone_number = kMaxLightZoneNumber;
        }

        for (int index = 0; index < _zone_number; index++) {
                kSignalLightZone[index].top_position    = redlight_zone->rect[index].top;
                kSignalLightZone[index].left_position   = redlight_zone->rect[index].left;
                kSignalLightZone[index].width           = redlight_zone->rect[index].width;
                kSignalLightZone[index].height          = redlight_zone->rect[index].height;
        }
}

bool ImageProcess::ReviseRedlight(struct cmemBuffer & imageInfo, Rectangle zone)
{
        int i = 0, j = 0, count = 0;
        int u = 0, v = 0, y = 0;
        bool bRedPoint = false;

        int redlight_top = zone.height;
        int redlight_left = zone.width;
        int redlight_bottom = 0, redlight_right = 0;
        int line = imageInfo.line;

        STATICS_START("revise redlight");
        int offset = line * zone.top_position + zone.left_position * 2;
        unsigned char *p = (unsigned char *)imageInfo.vir_addr + offset;

        for (i = 0; i < zone.height; i++) {
                for (j = 0; j < zone.width; j++) {
                        if (j % 2) {
                                y = *(p + j * 2 + i * line + 1);
                                u = *(p + j * 2 + i * line - 2);
                                v = *(p + j * 2 + i * line);
                        } else {
                                y = *(p + j * 2 + i * line + 1);
                                u = *(p + j * 2 + i * line);
                                v = *(p + j * 2 + i * line + 2);
                        }

                        bRedPoint = (y < 120) && (u > 60) && (v > 160);
                        if (bRedPoint) {
                                count++;
                                redlight_top = (redlight_top > i) ? i : redlight_top;
                                redlight_bottom = (redlight_bottom < i) ? i : redlight_bottom;
                                redlight_left = (redlight_left > j) ? j : redlight_left;
                                redlight_right = (redlight_right < j) ? j : redlight_right;
                        }
                }
        }

        Debug(INFO, "red point amount is %d", count);
        if ((count > 50) && (count < 2500)) {
                offset = (zone.top_position + redlight_top) * line +
                        (zone.left_position + redlight_left) * 2;
                p = (unsigned char *)imageInfo.vir_addr + offset;
                for (i = 0; i < redlight_bottom - redlight_top; i++) {
                        for (j = 0; j < redlight_right - redlight_left; j += 2) {
                                u = *(p + j * 2 + i * line);
                                v = *(p + j * 2 + i * line + 2);

                                if ((u < 80) && (v > 120)) {
                                        *(p + j * 2 + i * line + 1) = 90;
                                        *(p + j * 2 + i * line + 3) = 90;
                                        *(p + j * 2 + i * line) = 97;
                                        *(p + j * 2 + i * line + 2) = 232;
                                }
                        }
                }
        }
        STATICS_STOP();
        return true;
}
