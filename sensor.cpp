/**
 * @file	sensor.cpp
 * @brief	FPGA控制接口
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2011-10-24
 * @note	通过单体模式实现FPGA控制接口
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <iomanip>

/* 标准Linux头文件 */
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

/* DaVinci 特有的内核头文件 */
#include <media/davinci/videohd.h>

#include "sensor.h"
#include "debug.h"
#include "sensor_register.h"

const unsigned int MaxExposure = 2047;
const unsigned int MaxGain = 1023;
const unsigned int RegisterOffset = 8;
const unsigned int MaxDelayTime = 5;
const unsigned int MaxGray = 255;
const unsigned char RegisterMask = 0xFF;
const unsigned char CmdRegister = 0x66;

const int MAX_REGISTER_INDEX = 47;

typedef struct {
        unsigned char addr;
        char readable;
        char writeable;
} register_attribute;

register_attribute attribute[MAX_REGISTER_INDEX];

Sensor *Sensor::_Sensor = 0;

Sensor::Sensor()
:  _sensorHandle(-1)
{
        InitRegisterAttribute();
}

Sensor::~Sensor()
{
        _sensorHandle = -1;
}

void Sensor::InitRegisterAttribute()
{
        attribute[0].addr = EXPOSURE_HIGH;
        attribute[0].writeable = 1;
        attribute[1].addr = EXPOSURE_LOW;
        attribute[1].writeable = 1;
        attribute[2].addr = GAIN_HIGH;
        attribute[2].writeable = 1;
        attribute[3].addr = GAIN_LOW;
        attribute[3].writeable = 1;
        attribute[4].addr = RED_GAIN_HIGH;
        attribute[4].writeable = 1;
        attribute[5].addr = RED_GAIN_LOW;
        attribute[5].writeable = 1;
        attribute[6].addr = BLUE_GAIN_HIGH;
        attribute[6].writeable = 1;
        attribute[7].addr = BLUE_GAIN_LOW;
        attribute[7].writeable = 1;
        attribute[8].addr = MIN_GAIN_HIGH;
        attribute[8].writeable = 1;
        attribute[9].addr = MIN_GAIN_LOW;
        attribute[9].writeable = 1;
        attribute[10].addr = MAX_GAIN_HIGH;
        attribute[10].writeable = 1;
        attribute[11].addr = MAX_GAIN_LOW;
        attribute[11].writeable = 1;
        attribute[12].addr = MIN_EXPOSURE_HIGH;
        attribute[12].writeable = 1;
        attribute[13].addr = MIN_EXPOSURE_LOW;
        attribute[13].writeable = 1;
        attribute[14].addr = MAX_EXPOSURE_HIGH;
        attribute[14].writeable = 1;
        attribute[15].addr = MAX_EXPOSURE_LOW;
        attribute[15].writeable = 1;
        attribute[16].addr = WORK_MODE;
        attribute[16].writeable = 1;
        attribute[17].addr = AE_METHOD;
        attribute[17].writeable = 1;
        attribute[18].addr = AE_MODE;
        attribute[18].writeable = 1;
        attribute[19].addr = FLASH_DELAY;
        attribute[19].writeable = 1;
        attribute[20].addr = VIDEO_TARGET;
        attribute[20].writeable = 1;
        attribute[21].addr = LED_MULTIPLE;
        attribute[21].writeable = 1;
        attribute[22].addr = RED_LIGHT_SYNC_MODE;
        attribute[22].writeable = 1;
        attribute[23].addr = RED_LIGHT_EFFICIENT;
        attribute[23].writeable = 1;
        attribute[24].addr = CONSTANT_LIGHT_MODE;
        attribute[24].writeable = 1;
        attribute[25].addr = RED_LIGHT_DELAY;
        attribute[25].writeable = 1;
        attribute[26].addr = FLASH_MODE;
        attribute[26].writeable = 1;
        attribute[27].addr = LED_MODE;
        attribute[27].writeable = 1;
        attribute[28].addr = CAPTURE_NUMBER;
        attribute[28].writeable = 1;
        attribute[29].addr = CONTINUE_CAPTURE_DELAY_HIGH;
        attribute[29].writeable = 1;
        attribute[30].addr = CONTINUE_CAPTURE_DELAY_LOW;
        attribute[30].writeable = 1;
        attribute[31].addr = PREPROCESS_ENABLE;
        attribute[31].writeable = 1;
        attribute[32].addr = CAPTURE_NUMBER;
        attribute[32].writeable = 1;
        attribute[33].addr = CAPTURE_TARGET;
        attribute[33].writeable = 1;
        attribute[34].addr = AE_ZONE_INDEX1;
        attribute[34].writeable = 1;
        attribute[35].addr = AE_ZONE_INDEX2;
        attribute[35].writeable = 1;
        attribute[36].addr = AE_ZONE_INDEX3;
        attribute[36].writeable = 1;
        attribute[37].addr = AE_ZONE_INDEX4;
        attribute[37].writeable = 1;
        attribute[38].addr = RED_LIGHT_SYNC_SOURCE;
        attribute[38].writeable = 1;
        attribute[39].addr = AE_ADJUST_PICTURE_NUMBER;
        attribute[39].writeable = 1;
        attribute[40].addr = AD_SAMPLE_BITS;
        attribute[40].writeable = 1;
        attribute[41].addr = SHARPEN_FACTOR;
        attribute[41].writeable = 1;

        attribute[42].addr = CCD_TYPE;
        attribute[42].readable = 1;
        attribute[43].addr = PROJECT_ID;
        attribute[43].readable = 1;
        attribute[44].addr = FPGA_VERSION;
        attribute[44].readable = 1;
        attribute[45].addr = AVERAGE_LUMA;
        attribute[45].readable = 1;
}

bool Sensor::CheckRegisterWriteable(unsigned char addr)
{
        for (int i = 0; i < MAX_REGISTER_INDEX; i++) {
                if (addr == attribute[i].addr) {
                        if (attribute[i].writeable) {
                                return true;
                        } else {
                                return false;
                        }
                }
        }
        return false;
}

bool Sensor::CheckRegisterReadable(unsigned char addr)
{
        for (int i = 0; i < MAX_REGISTER_INDEX; i++) {
                if (addr == attribute[i].addr) {
                        if (attribute[i].readable) {
                                return true;
                        } else {
                                return false;
                        }
                }
        }
        return false;
}

bool Sensor::SetSensorHandle(int handle)
{
        _sensorHandle = handle;
        return true;
}

bool Sensor::GetVersion()
{
        if (_sensorHandle < 0) {
                return false;
        }

        char tmp[16];
        unsigned char val = 0xFF;
        struct v4l2_dbg_register reg;
        reg.match.type = V4L2_CHIP_MATCH_I2C_ADDR;
        reg.match.addr = CmdRegister;
        reg.reg = CCD_TYPE & RegisterMask;
        reg.val = val;

        if (CheckRegisterReadable(reg.reg)) {
                if (ioctl(_sensorHandle, VIDIOC_DBG_G_REGISTER, &reg) == -1) {
                        Debug(ERROR, "can't get addr: %02x, errno is %d, %s\n",
                              (unsigned char)reg.reg, errno, strerror(errno));
                        return false;
                }
                sprintf(tmp, "%x", (unsigned char)reg.val & RegisterMask);
        }

        reg.reg = PROJECT_ID & RegisterMask;
        if (CheckRegisterReadable(reg.reg)) {
                if (ioctl(_sensorHandle, VIDIOC_DBG_G_REGISTER, &reg) == -1) {
                        Debug(ERROR, "can't get addr: %02x, errno is %d, %s\n",
                              (unsigned char)reg.reg, errno, strerror(errno));
                        return false;
                }
                sprintf(tmp, "%s-%x", tmp, (unsigned char)reg.val & RegisterMask);
        }

        reg.reg = FPGA_VERSION & RegisterMask;
        if (CheckRegisterReadable(reg.reg)) {
                if (ioctl(_sensorHandle, VIDIOC_DBG_G_REGISTER, &reg) == -1) {
                        Debug(ERROR, "can't get addr: %02x, errno is %d, %s\n",
                              (unsigned char)reg.reg, errno, strerror(errno));
                        return false;
                }
                sprintf(tmp, "%s-%x", tmp, (unsigned char)reg.val & RegisterMask);
        }
        Debug(INFO, "FPGA version is %s", tmp);

        return true;
}

Sensor *Sensor::GetInstance()
{
        if (_Sensor == 0) {
                static Sensor instance;
                _Sensor = &instance;
        }

        return _Sensor;
}

int Sensor::SetSensorUchar(unsigned char addr, unsigned int setting)
{
        unsigned char val = setting & RegisterMask;
        return SetFPGAParam(addr, val);
}

int Sensor::SetExposure(unsigned int exposure, unsigned char low_addr, unsigned char high_addr)
{
        if (exposure > MaxExposure) {
                exposure = MaxExposure;
        }

        if (SetSensorUchar(low_addr, exposure)) {
                return -1;
        }

        exposure = exposure >> RegisterOffset;
        if (SetSensorUchar(high_addr, exposure)) {
                return -1;
        }

        return 0;
}

int Sensor::SetSensorExposure(unsigned int exposure)
{
        return SetExposure(exposure, EXPOSURE_LOW, EXPOSURE_HIGH);
}

int Sensor::SetSensorMaxExp(unsigned int exposure)
{
        return SetExposure(exposure, MAX_EXPOSURE_LOW, MAX_EXPOSURE_HIGH);
}

int Sensor::SetSensorMinExp(unsigned int exposure)
{
        return SetExposure(exposure, MIN_EXPOSURE_LOW, MIN_EXPOSURE_HIGH);
}

int Sensor::SetGain(unsigned int gain, unsigned char low_addr, unsigned char high_addr)
{
        if (gain > MaxGain) {
                gain = MaxGain;
        }

        if (SetSensorUchar(low_addr, gain)) {
                return -1;
        }

        gain = gain >> RegisterOffset;
        if (SetSensorUchar(high_addr, gain)) {
                return -1;
        }

        return 0;
}

int Sensor::SetSensorGain(unsigned int gain)
{
        return SetGain(gain, GAIN_LOW, GAIN_HIGH);
}

int Sensor::SetSensorMaxGain(unsigned int gain)
{
        return SetGain(gain, MAX_GAIN_LOW, MAX_GAIN_HIGH);
}

int Sensor::SetSensorMinGain(unsigned int gain)
{
        return SetGain(gain, MIN_GAIN_LOW, MIN_GAIN_HIGH);
}

int Sensor::SetSensorFlashOn()
{
        return SetFPGAParam(FLASH_MODE, FLASH_ENABLE);
}

int Sensor::SetSensorFlashOff()
{
        return SetFPGAParam(FLASH_MODE, FLASH_DISABLE);
}

int Sensor::SetSensorLedOn()
{
        return SetFPGAParam(LED_MODE, FLASH_ENABLE);
}

int Sensor::SetSensorLedOff()
{
        return SetFPGAParam(LED_MODE, FLASH_DISABLE);
}

int Sensor::SetSensorConstantLightOn()
{
        return SetFPGAParam(CONSTANT_LIGHT_MODE, FLASH_ENABLE);
}

int Sensor::SetSensorConstantLightOff()
{
        return SetFPGAParam(CONSTANT_LIGHT_MODE, FLASH_DISABLE);
}

int Sensor::SetSensorVideo()
{
        return SetFPGAParam(WORK_MODE, VIDEO);
}

int Sensor::SetSensorCapture()
{
        return SetFPGAParam(WORK_MODE, CAPTURE);
}

int Sensor::SetSensorRgain(unsigned int gain)
{
        return SetGain(gain, RED_GAIN_LOW, RED_GAIN_HIGH);
}

int Sensor::SetSensorBgain(unsigned int gain)
{
        return SetGain(gain, BLUE_GAIN_LOW, BLUE_GAIN_HIGH);
}

int Sensor::SetSensorAEAuto()
{
        return SetFPGAParam(AE_MODE, AUTO);
}

int Sensor::SetSensorAEManual()
{
        return SetFPGAParam(AE_MODE, MANNUAL);
}

int Sensor::SetSensorAEMethod(unsigned int method)
{
        if (method > 2) {
                method = 0;
        }

        return SetSensorUchar(AE_METHOD, method);
}

int Sensor::SetSensorFlashDelay(unsigned int time)
{
        if (time > MaxDelayTime) {
                time = MaxDelayTime;
        }

        return SetSensorUchar(FLASH_DELAY, time);
}

int Sensor::SetSensorVideoTargetGray(unsigned int gray)
{
        if (gray > MaxGray) {
                gray = MaxGray;
        }

        return SetSensorUchar(VIDEO_TARGET, gray);
}

int Sensor::SetSensorLEDMultiple(unsigned int time)
{
        return SetSensorUchar(LED_MULTIPLE, time);
}

int Sensor::SetSensorSyncOn()
{
        return SetFPGAParam(RED_LIGHT_SYNC_MODE, SYNC_ON);
}

int Sensor::SetSensorSyncOff()
{
        return SetFPGAParam(RED_LIGHT_SYNC_MODE, SYNC_OFF);
}

int Sensor::SetSensorRedLightEfficient(unsigned int time)
{
        return SetSensorUchar(RED_LIGHT_EFFICIENT, time);
}

int Sensor::SetSensorRedLightDelay(unsigned int time)
{
        return SetSensorUchar(RED_LIGHT_DELAY, time);
}

int Sensor::SetSensorContinousCapNum(unsigned int num)
{
        if (num > 10) {
                num = 10;
        }

        return SetSensorUchar(CAPTURE_NUMBER, num);
}

int Sensor::SetSensorContinousCapDuration(unsigned int time)
{
        return SetGain(time, CONTINUE_CAPTURE_DELAY_LOW, CONTINUE_CAPTURE_DELAY_HIGH);
}

int Sensor::SetSensorEnablePreProcess()
{
        return SetFPGAParam(PREPROCESS_ENABLE, ENABLE_MODE);
}

int Sensor::SetSensorDisablePreProcess()
{
        return SetFPGAParam(PREPROCESS_ENABLE, DISABLE_MODE);
}

int Sensor::SetSensorCaptureTargetGray(unsigned int gray)
{
        if (gray > MaxGray) {
                gray = MaxGray;
        }

        return SetSensorUchar(CAPTURE_TARGET, gray);
}

int Sensor::SetSensorAEZone(unsigned int zone)
{
        if (SetSensorUchar(AE_ZONE_INDEX1, zone)) {
                return -1;
        }

        zone = zone >> RegisterOffset;
        if (SetSensorUchar(AE_ZONE_INDEX2, zone)) {
                return -1;
        }

        zone = zone >> RegisterOffset;
        if (SetSensorUchar(AE_ZONE_INDEX3, zone)) {
                return -1;
        }

        zone = zone >> RegisterOffset;
        return SetSensorUchar(AE_ZONE_INDEX4, zone);
}

int Sensor::SetSensorRedLightSyncSource(unsigned int mode)
{
        return SetSensorUchar(RED_LIGHT_SYNC_SOURCE, mode);
}

int Sensor::SetSensorAEAdjustNumber(unsigned int number)
{
        return SetSensorUchar(AE_ADJUST_PICTURE_NUMBER, number);
}

int Sensor::SetSensorSharpenFactor(unsigned int factor)
{
        return SetSensorUchar(SHARPEN_FACTOR, factor);
}

int Sensor::SetSensorADSampleBits(unsigned int bit)
{
        if (bit == 0x02) {
                return SetFPGAParam(AD_SAMPLE_BITS, bit);
        }

        return SetFPGAParam(AD_SAMPLE_BITS, 0x01);
}

int Sensor::SetFPGAParam(unsigned char addr, unsigned char val)
{
        if (_sensorHandle < 0) {
                return -1;
        }

        if (!CheckRegisterWriteable(addr)) {
                Debug(WARN, "write to unwriteable register");
                return -1;
        }

        struct v4l2_dbg_register reg;
        reg.match.type = V4L2_CHIP_MATCH_I2C_ADDR;
        reg.match.addr = CmdRegister;
        reg.reg = addr;
        reg.val = val;

        if (ioctl(_sensorHandle, VIDIOC_DBG_S_REGISTER, &reg) == -1) {
                Debug(ERROR, "can't set addr: %02x with val: %02x, errno is %d, %s\n",
                      addr, val, errno, strerror(errno));
                return -1;
        }

        return 0;
}

int Sensor::GetCurrentAvgLuma()
{
        unsigned char val = 0xFF;
        struct v4l2_dbg_register reg;
        reg.match.type = V4L2_CHIP_MATCH_I2C_ADDR;
        reg.match.addr = CmdRegister;
        reg.reg = AVERAGE_LUMA & RegisterMask;
        reg.val = val;

        if (_sensorHandle < 0) {
                return -1;
        }

        if (!CheckRegisterReadable(reg.reg)) {
                Debug(WARN, "read unreadable register");
                return -1;
        }

        if (ioctl(_sensorHandle, VIDIOC_DBG_G_REGISTER, &reg) == -1) {
                Debug(ERROR, "can't get addr: %02x, errno is %d, %s\n",
                      (unsigned char)reg.reg, errno, strerror(errno));
                return -1;
        }

        return reg.val;
}
