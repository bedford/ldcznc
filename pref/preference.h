
#ifndef	_PREFERENCE_H_
#define _PREFERENCE_H_

#include "pref.h"

struct param_main {
        class pref *DeviceNumber;
        class pref *DeviceMode;
        class pref *DeviceSite;
        class pref *DirectionCode;
        class pref *DirectionText;
        class pref *RoadCode;
        class pref *DepartmentCode;
        class pref *Latitue;
        class pref *Longtitue;
        class pref *Lane;
        class pref *SnapMode;
};

struct param_traffic {
        class pref *StatsPeriod;
        class pref *MotorVehicleLength;
        class pref *SmallVehicleLength;
        class pref *LargeVehicleLength;
        class pref *LongVehicleLength;
};

struct param_upload {
        class pref *UploadMethod;
        class pref *UploadServer;
        class pref *UploadPort;
        class pref *UploadUser;
        class pref *UploadPasswd;
        class pref *UploadType;
        class pref *NtpServer;
        class pref *ClientAddr;
};

struct param_aew {
        class pref *DefaultGain;
        class pref *MinGain;
        class pref *MaxGain;
        class pref *DefaultExposure;
        class pref *MinExposure;
        class pref *MaxExposure;
        class pref *VideoTargetGray;
        class pref *TriggerTargetGray;
        class pref *RGain;
        class pref *BGain;
        class pref *AEZone;
        class pref *AEWMode;
        class pref *DaytimeStartMin;
        class pref *DaytimeStartHour;
        class pref *DaytimeEndMin;
        class pref *DaytimeEndHour;
        class pref *QValue;
        class pref *AdSampleBits;
        class pref *SharpenFactor;
};

struct param_peripherral {
        class pref *FlashMode;
        class pref *FlashDelay;
        class pref *LedMode;
        class pref *LedMutiple;
        class pref *ContinousLight;
        class pref *RedlightDelay;
        class pref *RedlightSyncMode;
        class pref *RedlightEfficient;
        class pref *LightOnThreshold;
};

struct param_overlay {
        class pref *OverlayDeviceNumber;
        class pref *OverlayTimeStamp;
        class pref *OverlaySite;
        class pref *OverlayDirection;
        class pref *OverlayLane;
        class pref *OverlayPlate;
        class pref *OverlayPassingType;
        class pref *OverlayRedlightTime;
        class pref *OverlaySpeed;
        class pref *OverlaySpeedLimit;
        class pref *OverlayOverspeedPercent;
        class pref *OverlayExtraInfo1;
        class pref *OverlayExtraInfo2;
        class pref *OverlayExtraInfo1Lable;
        class pref *OverlayExtraInfo2Lable;
};

struct param_redlight {
        class pref *RedlightNumber;
        class pref *Redlight1Left;
        class pref *Redlight1Top;
        class pref *Redlight1Width;
        class pref *Redlight1Height;
        class pref *Redlight2Left;
        class pref *Redlight2Top;
        class pref *Redlight2Width;
        class pref *Redlight2Height;
        class pref *Redlight3Left;
        class pref *Redlight3Top;
        class pref *Redlight3Width;
        class pref *Redlight3Height;
        class pref *Redlight4Left;
        class pref *Redlight4Top;
        class pref *Redlight4Width;
        class pref *Redlight4Height;
        class pref *Redlight5Left;
        class pref *Redlight5Top;
        class pref *Redlight5Width;
        class pref *Redlight5Height;
        class pref *Redlight6Left;
        class pref *Redlight6Top;
        class pref *Redlight6Width;
        class pref *Redlight6Height;
};

struct app_param {
        struct param_main main;
        struct param_upload upload;
        struct param_aew aew;
        struct param_peripherral peripherral;
        struct param_traffic traffic;
        struct param_redlight redlight;
        struct param_overlay overlay;
};

#ifdef __cplusplus
extern "C" {
#endif
        int preference_init(void);
        int preference_deinit(void);
        int preference_load(void);
        int preference_save(void);
        int preference_get(char *buf);
        int preference_set(char *buf, int len);

        char *preference_get_log_name(void);
        char *preference_get_inifile_name(void);

#ifdef __cplusplus
}
#endif
#endif
