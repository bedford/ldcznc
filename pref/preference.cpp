
#include <unistd.h>
#include "pref.h"
#include "preference_items.h"
#include "preference.h"
#include "xmlParser.h"

#define INI_FILE_NAME    "/more/param/param_app.xml"

#define SEC_MAIN		"MAIN"
#define KEY_DEVICE_NUM		"DeviceNumber"          /* 设备编号     */
#define KEY_DEVICE_MODE		"DeviceMode"            /* 设备工作模式 */
#define KEY_DEVICE_SITE		"DeviceSite"            /* 设备安装地点 */
#define KEY_DIRECTION_CODE	"DirectionCode"         /* 行驶方向代码 */
#define KEY_DIRECTION_TEXT	"DirectionText"         /* 行驶方向     */
#define KEY_ROAD_CODE		"RoadCode"              /* 路段代码     */
#define KEY_DEPARTMENT_CODE	"DepartmentCode"        /* 所属部门代码 */
#define KEY_LATITUE		"Latitue"               /* 经度         */
#define KEY_LONGTITUE		"Longtitue"             /* 纬度         */
#define KEY_LANE		"Lane"                  /* 车道         */
#define KEY_SNAP_MODE           "SnapMode"              /* 抓拍模式 0,0度;1,90度;2,180度;3,270度 */

static inline int preference_init_main(struct param_main *p)
{
        p->DeviceNumber         = new pref_string(SEC_MAIN, KEY_DEVICE_NUM, "LDCZNC001", 32);
        p->DeviceMode           = new pref_int_range(SEC_MAIN, KEY_DEVICE_MODE, 1, 0, 4);
        p->DeviceSite           = new pref_string(SEC_MAIN, KEY_DEVICE_SITE, "深南大道", 128);
        p->DirectionCode        = new pref_int_range(SEC_MAIN, KEY_DIRECTION_CODE, 1, 1, 8);
        p->DirectionText        = new pref_string(SEC_MAIN, KEY_DIRECTION_TEXT, "由东向西", 32);
        p->RoadCode             = new pref_string(SEC_MAIN, KEY_ROAD_CODE, "000001", 32);
        p->DepartmentCode       = new pref_string(SEC_MAIN, KEY_DEPARTMENT_CODE, "53000000", 32);
        p->Latitue              = new pref_string(SEC_MAIN, KEY_LATITUE, "104.000000", 32);
        p->Longtitue            = new pref_string(SEC_MAIN, KEY_LONGTITUE, "25.000000", 32);
        p->Lane                 = new pref_int_range(SEC_MAIN, KEY_LANE, 3, 1, 5);
        p->SnapMode             = new pref_int_range(SEC_MAIN, KEY_SNAP_MODE, 0, 0, 3);

        return 0;
}

static inline int preference_deinit_main(struct param_main *p)
{
        delete p->DeviceNumber;
        delete p->DeviceMode;
        delete p->DeviceSite;
        delete p->DirectionCode;
        delete p->DirectionText;
        delete p->RoadCode;
        delete p->DepartmentCode;
        delete p->Latitue;
        delete p->Longtitue;
        delete p->Lane;
        delete p->SnapMode;

        return 0;
}

static inline int preference_load_main(struct param_main *p, XMLNode & root_node)
{
        p->DeviceNumber->load(root_node);
        p->DeviceMode->load(root_node);
        p->DeviceSite->load(root_node);
        p->DirectionCode->load(root_node);
        p->DirectionText->load(root_node);
        p->RoadCode->load(root_node);
        p->DepartmentCode->load(root_node);
        p->Latitue->load(root_node);
        p->Longtitue->load(root_node);
        p->Lane->load(root_node);
        p->SnapMode->load(root_node);

        return 0;
}

static inline int preference_save_main(struct param_main *p, XMLNode & root_node)
{
        p->DeviceNumber->save(root_node);
        p->DeviceMode->save(root_node);
        p->DeviceSite->save(root_node);
        p->DirectionCode->save(root_node);
        p->DirectionText->save(root_node);
        p->RoadCode->save(root_node);
        p->DepartmentCode->save(root_node);
        p->Latitue->save(root_node);
        p->Longtitue->save(root_node);
        p->Lane->save(root_node);
        p->SnapMode->save(root_node);

        return 0;
}

#define SEC_UPLOAD		"UPLOAD"
#define KEY_UPLOAD_METHOD	"UploadMethod"  /* 数据上传方式         */
#define	KEY_UPLOAD_SERVER	"UploadServer"  /* 数据上传服务器       */
#define KEY_UPLOAD_PORT		"UploadPort"    /* ftp上传端口          */
#define KEY_UPLOAD_USER		"UploadUser"    /* ftp登陆用户名        */
#define KEY_UPLOAD_PASSWD	"UploadPasswd"  /* ftp登陆密码          */
#define KEY_UPLOAD_TYPE		"UploadType"    /* ftp上传类型          */
#define KEY_NTP_SERVER		"NtpServer"     /* NTP对时服务器地址    */
#define KEY_CLIENT_ADDR		"ClientAddr"    /* 客户端IP地址         */

static inline int preference_init_upload(struct param_upload *p)
{
        p->UploadMethod = new pref_int_range(SEC_UPLOAD, KEY_UPLOAD_METHOD, 0, 0, 1);
        p->UploadServer = new pref_string(SEC_UPLOAD, KEY_UPLOAD_SERVER, "192.168.0.222", 64);
        p->UploadPort   = new pref_int(SEC_UPLOAD, KEY_UPLOAD_PORT, 21);
        p->UploadUser   = new pref_string(SEC_UPLOAD, KEY_UPLOAD_USER, "", 32);
        p->UploadPasswd = new pref_string(SEC_UPLOAD, KEY_UPLOAD_PASSWD, "", 32);
        p->UploadType   = new pref_int_range(SEC_UPLOAD, KEY_UPLOAD_TYPE, 0, 0, 1);
        p->NtpServer    = new pref_string(SEC_UPLOAD, KEY_NTP_SERVER, "192.168.0.222", 64);
        p->ClientAddr   = new pref_string(SEC_UPLOAD, KEY_CLIENT_ADDR, "192.168.0.222", 64);

        return 0;
}

static inline int preference_deinit_upload(struct param_upload *p)
{
        delete p->UploadMethod;
        delete p->UploadServer;
        delete p->UploadPort;
        delete p->UploadUser;
        delete p->UploadPasswd;
        delete p->UploadType;
        delete p->NtpServer;
        delete p->ClientAddr;

        return 0;
}

static inline int preference_load_upload(struct param_upload *p, XMLNode & root_node)
{
        p->UploadMethod->load(root_node);
        p->UploadServer->load(root_node);
        p->UploadPort->load(root_node);
        p->UploadUser->load(root_node);
        p->UploadPasswd->load(root_node);
        p->UploadType->load(root_node);
        p->NtpServer->load(root_node);
        p->ClientAddr->load(root_node);

        return 0;
}

static inline int preference_save_upload(struct param_upload *p, XMLNode & root_node)
{
        p->UploadMethod->save(root_node);
        p->UploadServer->save(root_node);
        p->UploadPort->save(root_node);
        p->UploadUser->save(root_node);
        p->UploadPasswd->save(root_node);
        p->UploadType->save(root_node);
        p->NtpServer->save(root_node);
        p->ClientAddr->save(root_node);

        return 0;
}

#define SEC_AEW			"AEW"
#define KEY_DEFAULT_GAIN	"DefaultGain"           /* 默认增益             */
#define	KEY_MIN_GAIN		"MinGain"               /* 最小增益             */
#define KEY_MAX_GAIN		"MaxGain"               /* 最大增益             */
#define KEY_DEFAULT_EXP		"DefaultExposure"       /* 默认曝光             */
#define KEY_MIN_EXP		"MinExposure"           /* 最小曝光             */
#define KEY_MAX_EXP		"MaxExposure"           /* 最大曝光             */
#define KEY_V_TARGETGRAY	"VideoTargetGray"       /* 视频目标灰度         */
#define	KEY_T_TARGETGRAY	"TriggerTargetGray"     /* 抓拍目标灰度         */
#define KEY_R_GAIN		"RGain"                 /* 白平衡Rgain          */
#define KEY_B_GAIN		"BGain"                 /* 白平衡Bgain          */
#define KEY_AE_ZONE		"AEZone"                /* AE调整区域           */
#define KEY_AEW_MODE		"AEWMode"               /* AEW调整模式          */
#define KEY_START_MIN		"DaytimeStartMin"       /* 白天开始时间(分钟)   */
#define KEY_START_HOUR		"DaytimeStartHour"      /* 白天开始时间(小时)   */
#define KEY_END_MIN		"DaytimeEndMin"         /* 白天结束时间(分钟)   */
#define KEY_END_HOUR		"DaytimeEndHour"        /* 白天结束时间(小时)   */
#define KEY_QVALUE		"QValue"                /* Jpeg压缩比           */
#define KEY_AD_SAMPLE_BITS      "AdSampleBits"          /* AD采样位置           */
#define KEY_SHARPEN_FACTOR      "SharpenFactor"         /* 锐化因子             */

static inline int preference_init_aew(struct param_aew *p)
{
        p->DefaultGain          = new pref_int(SEC_AEW, KEY_DEFAULT_GAIN, 400);
        p->MinGain              = new pref_int(SEC_AEW, KEY_MIN_GAIN, 50);
        p->MaxGain              = new pref_int(SEC_AEW, KEY_MAX_GAIN, 450);
        p->DefaultExposure      = new pref_int(SEC_AEW, KEY_DEFAULT_EXP, 19);
        p->MinExposure          = new pref_int(SEC_AEW, KEY_MIN_EXP, 1);
        p->MaxExposure          = new pref_int(SEC_AEW, KEY_MAX_EXP, 40);
        p->VideoTargetGray      = new pref_int(SEC_AEW, KEY_V_TARGETGRAY, 80);
        p->TriggerTargetGray    = new pref_int(SEC_AEW, KEY_T_TARGETGRAY, 80);
        p->RGain                = new pref_int(SEC_AEW, KEY_R_GAIN, 400);
        p->BGain                = new pref_int(SEC_AEW, KEY_B_GAIN, 400);
        p->AEZone               = new pref_int(SEC_AEW, KEY_AE_ZONE, 0xFFFFFFFF);
        p->AEWMode              = new pref_int_range(SEC_AEW, KEY_AEW_MODE, 3, 0, 3);
        p->DaytimeStartMin      = new pref_int_range(SEC_AEW, KEY_START_MIN, 30, 0, 59);
        p->DaytimeStartHour     = new pref_int_range(SEC_AEW, KEY_START_HOUR, 6, 0, 23);
        p->DaytimeEndMin        = new pref_int_range(SEC_AEW, KEY_END_MIN, 30, 0, 59);
        p->DaytimeEndHour       = new pref_int_range(SEC_AEW, KEY_END_HOUR, 18, 0, 23);
        p->QValue               = new pref_int_range(SEC_AEW, KEY_QVALUE, 80, 1, 95);
        p->AdSampleBits         = new pref_int_range(SEC_AEW, KEY_AD_SAMPLE_BITS, 1, 1, 2);
        p->SharpenFactor        = new pref_int_range(SEC_AEW, KEY_SHARPEN_FACTOR, 1, 0, 255);

        return 0;
}

static inline int preference_deinit_aew(struct param_aew *p)
{
        delete p->DefaultGain;
        delete p->MinGain;
        delete p->MaxGain;
        delete p->DefaultExposure;
        delete p->MinExposure;
        delete p->MaxExposure;
        delete p->VideoTargetGray;
        delete p->TriggerTargetGray;
        delete p->RGain;
        delete p->BGain;
        delete p->AEZone;
        delete p->AEWMode;
        delete p->DaytimeStartMin;
        delete p->DaytimeStartHour;
        delete p->DaytimeEndMin;
        delete p->DaytimeEndHour;
        delete p->QValue;
        delete p->AdSampleBits;
        delete p->SharpenFactor;

        return 0;
}

static inline int preference_load_aew(struct param_aew *p, XMLNode & root_node)
{
        p->DefaultGain->load(root_node);
        p->MinGain->load(root_node);
        p->MaxGain->load(root_node);
        p->DefaultExposure->load(root_node);
        p->MinExposure->load(root_node);
        p->MaxExposure->load(root_node);
        p->VideoTargetGray->load(root_node);
        p->TriggerTargetGray->load(root_node);
        p->RGain->load(root_node);
        p->BGain->load(root_node);
        p->AEZone->load(root_node);
        p->AEWMode->load(root_node);
        p->DaytimeStartMin->load(root_node);
        p->DaytimeStartHour->load(root_node);
        p->DaytimeEndMin->load(root_node);
        p->DaytimeEndHour->load(root_node);
        p->QValue->load(root_node);
        p->AdSampleBits->load(root_node);
        p->SharpenFactor->load(root_node);

        return 0;
}

static inline int preference_save_aew(struct param_aew *p, XMLNode & root_node)
{
        p->DefaultGain->save(root_node);
        p->MinGain->save(root_node);
        p->MaxGain->save(root_node);
        p->DefaultExposure->save(root_node);
        p->MinExposure->save(root_node);
        p->MaxExposure->save(root_node);
        p->VideoTargetGray->save(root_node);
        p->TriggerTargetGray->save(root_node);
        p->RGain->save(root_node);
        p->BGain->save(root_node);
        p->AEZone->save(root_node);
        p->AEWMode->save(root_node);
        p->DaytimeStartMin->save(root_node);
        p->DaytimeStartHour->save(root_node);
        p->DaytimeEndMin->save(root_node);
        p->DaytimeEndHour->save(root_node);
        p->QValue->save(root_node);
        p->AdSampleBits->save(root_node);
        p->SharpenFactor->save(root_node);

        return 0;
}

#define SEC_PERIPHERRAL		"PERIPHERRAL"
#define KEY_FLASH_MODE		"FlashMode"             /* 闪光灯工作模式       */
#define KEY_FLASH_DELAY		"FlashDelay"            /* 闪光灯延时           */
#define KEY_LED_MODE		"LedMode"               /* LED灯工作模式        */
#define KEY_LED_MUTIPLE		"LedMutiple"            /* LED灯倍频            */
#define KEY_CONTINOUSLIGHT	"ContinousLight"        /* 常亮灯模式           */
#define KEY_REDLIGHT_DELAY	"RedlightDelay"         /* 红灯延时             */
#define KEY_REDLIGHT_SYNC_MODE	"RedlightSyncMode"      /* 红灯同步模式         */
#define KEY_REDLIGHT_EFFICIENT	"RedlightEfficient"     /* 红灯抓拍有效时间     */
#define KEY_LIGHTONTHRESHOLD	"LightOnThreshold"      /* 开灯阈值             */

static inline int preference_init_peripherral(struct param_peripherral *p)
{
        p->FlashMode = new pref_int_range(SEC_PERIPHERRAL, KEY_FLASH_MODE, 0, 0, 2);
        p->FlashDelay = new pref_int_range(SEC_PERIPHERRAL, KEY_FLASH_DELAY, 3, 0, 10);
        p->LedMode = new pref_int_range(SEC_PERIPHERRAL, KEY_LED_MODE, 0, 0, 2);
        p->LedMutiple = new pref_int(SEC_PERIPHERRAL, KEY_LED_MUTIPLE, 3);
        p->ContinousLight = new pref_int_range(SEC_PERIPHERRAL, KEY_CONTINOUSLIGHT, 0, 0, 2);
        p->RedlightDelay = new pref_int(SEC_PERIPHERRAL, KEY_REDLIGHT_DELAY, 0);
        p->RedlightSyncMode = new pref_int_range(SEC_PERIPHERRAL, KEY_REDLIGHT_SYNC_MODE, 1, 0, 1);
        p->RedlightEfficient = new pref_int(SEC_PERIPHERRAL, KEY_REDLIGHT_EFFICIENT, 19);
        p->LightOnThreshold = new pref_int_range(SEC_PERIPHERRAL, KEY_LIGHTONTHRESHOLD, 30, 0, 255);

        return 0;
}

static inline int preference_deinit_peripherral(struct param_peripherral *p)
{
        delete p->FlashMode;
        delete p->FlashDelay;
        delete p->LedMode;
        delete p->LedMutiple;
        delete p->ContinousLight;
        delete p->RedlightDelay;
        delete p->RedlightSyncMode;
        delete p->RedlightEfficient;
        delete p->LightOnThreshold;

        return 0;
}

static inline int preference_load_peripherral(struct param_peripherral *p, XMLNode & root_node)
{
        p->FlashMode->load(root_node);
        p->FlashDelay->load(root_node);
        p->LedMode->load(root_node);
        p->LedMutiple->load(root_node);
        p->ContinousLight->load(root_node);
        p->RedlightDelay->load(root_node);
        p->RedlightSyncMode->load(root_node);
        p->RedlightEfficient->load(root_node);
        p->LightOnThreshold->load(root_node);

        return 0;
}

static inline int preference_save_peripherral(struct param_peripherral *p, XMLNode & root_node)
{
        p->FlashMode->save(root_node);
        p->FlashDelay->save(root_node);
        p->LedMode->save(root_node);
        p->LedMutiple->save(root_node);
        p->ContinousLight->save(root_node);
        p->RedlightDelay->save(root_node);
        p->RedlightSyncMode->save(root_node);
        p->RedlightEfficient->save(root_node);
        p->LightOnThreshold->save(root_node);

        return 0;
}

#define SEC_TRAFFIC		"TRAFFIC"
#define KEY_STATS_PERIOD	"StatsPeriod"           /* 统计周期             */
#define KEY_MOTOR_LENGTH	"MotorVehicleLength"    /* 摩托车车长上限       */
#define KEY_SMALL_LENGTH	"SmallVehicleLength"    /* 小车车长上限         */
#define KEY_LARGE_LENGTH	"LargeVehicleLength"    /* 中型车车长上限       */
#define KEY_LONG_VEHICLE_LENGTH "LongVehicleLength"     /* 大车车长上限         */

static inline int preference_init_traffic(struct param_traffic *p)
{
        p->StatsPeriod          = new pref_int_range(SEC_TRAFFIC, KEY_STATS_PERIOD, 5, 1, 59);
        p->MotorVehicleLength   = new pref_int(SEC_TRAFFIC, KEY_MOTOR_LENGTH, 25);
        p->SmallVehicleLength   = new pref_int(SEC_TRAFFIC, KEY_SMALL_LENGTH, 50);
        p->LargeVehicleLength   = new pref_int(SEC_TRAFFIC, KEY_LARGE_LENGTH, 80);
        p->LongVehicleLength    = new pref_int(SEC_TRAFFIC, KEY_LONG_VEHICLE_LENGTH, 120);

        return 0;
}

static inline int preference_deinit_traffic(struct param_traffic *p)
{
        delete p->StatsPeriod;
        delete p->MotorVehicleLength;
        delete p->SmallVehicleLength;
        delete p->LargeVehicleLength;
        delete p->LongVehicleLength;

        return 0;
}

static inline int preference_load_traffic(struct param_traffic *p, XMLNode & root_node)
{
        p->StatsPeriod->load(root_node);
        p->MotorVehicleLength->load(root_node);
        p->SmallVehicleLength->load(root_node);
        p->LargeVehicleLength->load(root_node);
        p->LongVehicleLength->load(root_node);

        return 0;
}

static inline int preference_save_traffic(struct param_traffic *p, XMLNode & root_node)
{
        p->StatsPeriod->save(root_node);
        p->MotorVehicleLength->save(root_node);
        p->SmallVehicleLength->save(root_node);
        p->LargeVehicleLength->save(root_node);
        p->LongVehicleLength->save(root_node);

        return 0;
}

#define SEC_OVERLAY                     "OVERLAY"
#define KEY_OVERLAY_DEVICE_NUMBER       "OverlayDeviceNumber"
#define KEY_OVERLAY_TIMESTAMP           "OverlayTimeStamp"
#define KEY_OVERLAY_SITE                "OverlaySite"
#define KEY_OVERLAY_DIRECTION           "OverlayDirection"
#define KEY_OVERLAY_LANE                "OverlayLane"
#define KEY_OVERLAY_PLATE               "OverlayPlate"
#define KEY_OVERLAY_PASSING_TYPE        "OverlayPassingType"
#define KEY_OVERLAY_REDLIGHT_TIME       "OverlayRedlightTime"
#define KEY_OVERLAY_SPEED               "OverlaySpeed"
#define KEY_OVERLAY_SPEED_LIMIT         "OverlaySpeedLimit"
#define KEY_OVERLAY_OVERSPEED_PERCENT   "OverlayOverspeedPercent"
#define KEY_OVERLAY_EXTRA_INFO1         "OverlayExtraInfo1"
#define KEY_OVERLAY_EXTRA_INFO2         "OverlayExtraInfo2"
#define KEY_OVERLAY_EXTRA_INFO1_LABLE   "OverlayExtraInfo1Lable"
#define KEY_OVERLAY_EXTRA_INFO2_LABLE   "OVerlayExtraInfo2Lable"

static inline int preference_init_overlay(struct param_overlay *p)
{
        p->OverlayDeviceNumber     = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_DEVICE_NUMBER, 1, 0, 1);
        p->OverlayTimeStamp        = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_TIMESTAMP, 1, 0, 1);
        p->OverlaySite             = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_SITE, 1, 0, 1);
        p->OverlayDirection        = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_DIRECTION, 1, 0, 1);
        p->OverlayLane             = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_LANE, 1, 0, 1);
        p->OverlayPlate            = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_PLATE, 0, 0, 1);
        p->OverlayPassingType      = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_PASSING_TYPE, 1, 0, 1);
        p->OverlayRedlightTime     = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_REDLIGHT_TIME, 1, 0, 1);
        p->OverlaySpeed            = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_SPEED, 0, 0, 1);
        p->OverlaySpeedLimit       = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_SPEED_LIMIT, 0, 0, 1);
        p->OverlayOverspeedPercent = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_OVERSPEED_PERCENT, 0, 0, 1);
        p->OverlayExtraInfo1       = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_EXTRA_INFO1, 0, 0, 1);
        p->OverlayExtraInfo2       = new pref_int_range(SEC_OVERLAY, KEY_OVERLAY_EXTRA_INFO2, 0, 0, 1);
        p->OverlayExtraInfo1Lable  = new pref_string(SEC_OVERLAY, KEY_OVERLAY_EXTRA_INFO1_LABLE, "", 128);
        p->OverlayExtraInfo2Lable  = new pref_string(SEC_OVERLAY, KEY_OVERLAY_EXTRA_INFO2_LABLE, "", 128);

        return 0;
}

static inline int preference_deinit_overlay(struct param_overlay *p)
{
        delete p->OverlayDeviceNumber;
        delete p->OverlayTimeStamp;
        delete p->OverlaySite;
        delete p->OverlayDirection;
        delete p->OverlayLane;
        delete p->OverlayPlate;
        delete p->OverlayPassingType;
        delete p->OverlayRedlightTime;
        delete p->OverlaySpeed;
        delete p->OverlaySpeedLimit;
        delete p->OverlayOverspeedPercent;
        delete p->OverlayExtraInfo1;
        delete p->OverlayExtraInfo2;
        delete p->OverlayExtraInfo1Lable;
        delete p->OverlayExtraInfo2Lable;

        return 0;
}

static inline int preference_load_overlay(struct param_overlay *p, XMLNode &root_node)
{
        p->OverlayDeviceNumber->load(root_node);
        p->OverlayTimeStamp->load(root_node);
        p->OverlaySite->load(root_node);
        p->OverlayDirection->load(root_node);
        p->OverlayLane->load(root_node);
        p->OverlayPlate->load(root_node);
        p->OverlayPassingType->load(root_node);
        p->OverlayRedlightTime->load(root_node);
        p->OverlaySpeed->load(root_node);
        p->OverlaySpeedLimit->load(root_node);
        p->OverlayOverspeedPercent->load(root_node);
        p->OverlayExtraInfo1->load(root_node);
        p->OverlayExtraInfo2->load(root_node);
        p->OverlayExtraInfo1Lable->load(root_node);
        p->OverlayExtraInfo2Lable->load(root_node);

        return 0;
}

static inline int preference_save_overlay(struct param_overlay *p, XMLNode &root_node)
{
        p->OverlayDeviceNumber->save(root_node);
        p->OverlayTimeStamp->save(root_node);
        p->OverlaySite->save(root_node);
        p->OverlayDirection->save(root_node);
        p->OverlayLane->save(root_node);
        p->OverlayPlate->save(root_node);
        p->OverlayPassingType->save(root_node);
        p->OverlayRedlightTime->save(root_node);
        p->OverlaySpeed->save(root_node);
        p->OverlaySpeedLimit->save(root_node);
        p->OverlayOverspeedPercent->save(root_node);
        p->OverlayExtraInfo1->save(root_node);
        p->OverlayExtraInfo2->save(root_node);
        p->OverlayExtraInfo1Lable->save(root_node);
        p->OverlayExtraInfo2Lable->save(root_node);

        return 0;
}


#define SEC_REDLIGHT		"REDLIGHT"
#define KEY_REDLIGHT_NUMBER	"RedlightNumber"
#define KEY_REDLIGHT1_LEFT	"Redlight1Left"
#define KEY_REDLIGHT1_TOP	"Redlight1Top"
#define KEY_REDLIGHT1_WIDTH	"Redlight1Width"
#define KEY_REDLIGHT1_HEIGHT	"Redlight1Height"
#define KEY_REDLIGHT2_LEFT	"Redlight2Left"
#define KEY_REDLIGHT2_TOP	"Redlight2Top"
#define KEY_REDLIGHT2_WIDTH	"Redlight2Width"
#define KEY_REDLIGHT2_HEIGHT	"Redligth2Height"
#define KEY_REDLIGHT3_LEFT	"Redlight3Left"
#define KEY_REDLIGHT3_TOP	"Redlight3Top"
#define KEY_REDLIGHT3_WIDTH	"Redlight3Width"
#define KEY_REDLIGHT3_HEIGHT	"Redlight3Height"
#define KEY_REDLIGHT4_LEFT	"Redlight4Left"
#define KEY_REDLIGHT4_TOP	"Redlight4Top"
#define KEY_REDLIGHT4_WIDTH	"Redlight4Width"
#define KEY_REDLIGHT4_HEIGHT	"Redligth4Height"
#define KEY_REDLIGHT5_LEFT	"Redlight5Left"
#define KEY_REDLIGHT5_TOP	"Redlight5Top"
#define KEY_REDLIGHT5_WIDTH	"Redlight5Width"
#define KEY_REDLIGHT5_HEIGHT	"Redligth5Height"
#define KEY_REDLIGHT6_LEFT	"Redlight6Left"
#define KEY_REDLIGHT6_TOP	"Redlight6Top"
#define KEY_REDLIGHT6_WIDTH	"Redlight6Width"
#define KEY_REDLIGHT6_HEIGHT	"Redligth6Height"

static inline int preference_init_redlight(struct param_redlight *p)
{
        p->RedlightNumber = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT_NUMBER, 0, 0, 6);
        p->Redlight1Left = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT1_LEFT, 0, 0, 2432);
        p->Redlight1Top = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT1_TOP, 0, 0, 2048);
        p->Redlight1Width = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT1_WIDTH, 0, 0, 2432);
        p->Redlight1Height = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT1_HEIGHT, 0, 0, 2048);
        p->Redlight2Left = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT2_LEFT, 0, 0, 2432);
        p->Redlight2Top = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT2_TOP, 0, 0, 2048);
        p->Redlight2Width = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT2_WIDTH, 0, 0, 2432);
        p->Redlight2Height = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT2_HEIGHT, 0, 0, 2048);
        p->Redlight3Left = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT3_LEFT, 0, 0, 2432);
        p->Redlight3Top = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT3_TOP, 0, 0, 2048);
        p->Redlight3Width = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT3_WIDTH, 0, 0, 2432);
        p->Redlight3Height = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT3_HEIGHT, 0, 0, 2048);
        p->Redlight4Left = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT4_LEFT, 0, 0, 2432);
        p->Redlight4Top = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT4_TOP, 0, 0, 2048);
        p->Redlight4Width = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT4_WIDTH, 0, 0, 2432);
        p->Redlight4Height = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT4_HEIGHT, 0, 0, 2048);
        p->Redlight5Left = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT5_LEFT, 0, 0, 2432);
        p->Redlight5Top = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT5_TOP, 0, 0, 2048);
        p->Redlight5Width = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT5_WIDTH, 0, 0, 2432);
        p->Redlight5Height = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT5_HEIGHT, 0, 0, 2048);
        p->Redlight6Left = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT6_LEFT, 0, 0, 2432);
        p->Redlight6Top = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT6_TOP, 0, 0, 2048);
        p->Redlight6Width = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT6_WIDTH, 0, 0, 2432);
        p->Redlight6Height = new pref_int_range(SEC_REDLIGHT, KEY_REDLIGHT6_HEIGHT, 0, 0, 2048);

        return 0;
}

static inline int preference_deinit_redlight(struct param_redlight *p)
{
        delete p->RedlightNumber;
        delete p->Redlight1Left;
        delete p->Redlight1Top;
        delete p->Redlight1Width;
        delete p->Redlight1Height;
        delete p->Redlight2Left;
        delete p->Redlight2Top;
        delete p->Redlight2Width;
        delete p->Redlight2Height;
        delete p->Redlight3Left;
        delete p->Redlight3Top;
        delete p->Redlight3Width;
        delete p->Redlight3Height;
        delete p->Redlight4Left;
        delete p->Redlight4Top;
        delete p->Redlight4Width;
        delete p->Redlight4Height;
        delete p->Redlight5Left;
        delete p->Redlight5Top;
        delete p->Redlight5Width;
        delete p->Redlight5Height;
        delete p->Redlight6Left;
        delete p->Redlight6Top;
        delete p->Redlight6Width;
        delete p->Redlight6Height;

        return 0;
}

static inline int preference_load_redlight(struct param_redlight *p, XMLNode &root_node)
{
        p->RedlightNumber->load(root_node);
        p->Redlight1Left->load(root_node);
        p->Redlight1Top->load(root_node);
        p->Redlight1Width->load(root_node);
        p->Redlight1Height->load(root_node);
        p->Redlight2Left->load(root_node);
        p->Redlight2Top->load(root_node);
        p->Redlight2Width->load(root_node);
        p->Redlight2Height->load(root_node);
        p->Redlight3Left->load(root_node);
        p->Redlight3Top->load(root_node);
        p->Redlight3Width->load(root_node);
        p->Redlight3Height->load(root_node);
        p->Redlight4Left->load(root_node);
        p->Redlight4Top->load(root_node);
        p->Redlight4Width->load(root_node);
        p->Redlight4Height->load(root_node);
        p->Redlight5Left->load(root_node);
        p->Redlight5Top->load(root_node);
        p->Redlight5Width->load(root_node);
        p->Redlight5Height->load(root_node);
        p->Redlight6Left->load(root_node);
        p->Redlight6Top->load(root_node);
        p->Redlight6Width->load(root_node);
        p->Redlight6Height->load(root_node);

        return 0;
}

static inline int preference_save_redlight(struct param_redlight *p, XMLNode &root_node)
{
        p->RedlightNumber->save(root_node);
        p->Redlight1Left->save(root_node);
        p->Redlight1Top->save(root_node);
        p->Redlight1Width->save(root_node);
        p->Redlight1Height->save(root_node);
        p->Redlight2Left->save(root_node);
        p->Redlight2Top->save(root_node);
        p->Redlight2Width->save(root_node);
        p->Redlight2Height->save(root_node);
        p->Redlight3Left->save(root_node);
        p->Redlight3Top->save(root_node);
        p->Redlight3Width->save(root_node);
        p->Redlight3Height->save(root_node);
        p->Redlight4Left->save(root_node);
        p->Redlight4Top->save(root_node);
        p->Redlight4Width->save(root_node);
        p->Redlight4Height->save(root_node);
        p->Redlight5Left->save(root_node);
        p->Redlight5Top->save(root_node);
        p->Redlight5Width->save(root_node);
        p->Redlight5Height->save(root_node);
        p->Redlight6Left->save(root_node);
        p->Redlight6Top->save(root_node);
        p->Redlight6Width->save(root_node);
        p->Redlight6Height->save(root_node);

        return 0;
}

struct app_param app_param;

static char exe_path[32];
static char exe_name[32];
static char inifilename[256];

char *preference_get_inifile_name(void)
{
        sprintf(inifilename, "%s", INI_FILE_NAME);
        return inifilename;
}

int get_exe_path(char *buf, int size, char **path, char **name)
{
        memset(buf, 0, size);
        if (readlink("/proc/self/exe", buf, size) <= 0) {
                return -1;
        }

        char *temp = NULL;
        if ((temp = (char *)memrchr(buf, '/', size)) == NULL) {
                return -1;
        }
        *temp = '\0';
        ++temp;

        *path = buf;
        *name = temp;

        return 0;
}

int preference_init(void)
{
        struct app_param *p = &app_param;
        preference_init_main(&(p->main));       //初始化运行参数
        preference_init_upload(&(p->upload));   //初始化网络上传参数
        preference_init_aew(&(p->aew)); //初始化AEW参数
        preference_init_peripherral(&(p->peripherral)); //初始化外设参数
        preference_init_traffic(&p->traffic);   //初始化车流量统计参数
        preference_init_redlight(&p->redlight); //初始化红灯修改区域
        preference_init_overlay(&p->overlay);

        char exe_abs_path[256];
        char *tmp_path;
        char *tmp_name;
        if (get_exe_path(exe_abs_path, sizeof(exe_abs_path), &tmp_path, &tmp_name) < 0) {
        }
        memcpy(exe_path, exe_abs_path, strlen(tmp_path) + 1);
        memcpy(exe_name, exe_abs_path + strlen(tmp_path) + 1, strlen(tmp_name) + 1);
        sprintf(inifilename, "%s", INI_FILE_NAME);

        XMLNode::setGlobalOptions(1, 1, 1);
        preference_load();
        preference_save();

        return 0;
}

int preference_deinit(void)
{
        struct app_param *p = &app_param;
        preference_deinit_main(&(p->main));
        preference_deinit_upload(&(p->upload));
        preference_deinit_aew(&(p->aew));
        preference_deinit_peripherral(&(p->peripherral));
        preference_deinit_traffic(&(p->traffic));
        preference_deinit_redlight(&(p->redlight));
        preference_deinit_overlay(&(p->overlay));

        return 0;
}

int preference_load(void)
{
        struct app_param *p = &app_param;

        XMLNode root_node;
        XMLResults result;
        root_node = XMLNode::parseFile(inifilename, "xml", &result);
        if (root_node.getName() == NULL) {
                printf("error %d(%s)\n", result.error, XMLNode::getError(result.error));
                root_node = XMLNode::createXMLTopNode("xml", true);
                root_node.addAttribute("version", "1.0");
                root_node.addAttribute("encoding", "utf-8");
        }

        preference_load_main(&(p->main), root_node);
        preference_load_upload(&(p->upload), root_node);
        preference_load_aew(&(p->aew), root_node);
        preference_load_peripherral(&(p->peripherral), root_node);
        preference_load_traffic(&(p->traffic), root_node);
        preference_load_redlight(&(p->redlight), root_node);
        preference_load_overlay(&(p->overlay), root_node);

        root_node.writeToFile(inifilename);

        return 0;
}

int preference_save(void)
{
        struct app_param *p = &app_param;

        XMLNode root_node;
        XMLResults result;
        root_node = XMLNode::parseFile(inifilename, "xml", &result);
        if (root_node.getName() == NULL) {
                printf("error %d(%s)\n", result.error, XMLNode::getError(result.error));
                root_node = XMLNode::createXMLTopNode("xml", true);
                root_node.addAttribute("version", "1.0");
                root_node.addAttribute("encoding", "utf-8");
        }

        preference_save_main(&(p->main), root_node);
        preference_save_upload(&(p->upload), root_node);
        preference_save_aew(&(p->aew), root_node);
        preference_save_peripherral(&(p->peripherral), root_node);
        preference_save_traffic(&(p->traffic), root_node);
        preference_save_redlight(&(p->redlight), root_node);
        preference_save_overlay(&(p->overlay), root_node);

        root_node.writeToFile(inifilename);
        return 0;
}

int preference_set(char *buf, int len)
{
        FILE *fp = NULL;
        if ((fp = fopen(inifilename, "wb")) == NULL) {
                return -1;
        }
        if (fwrite(buf, len, 1, fp) < 1) {
                fclose(fp);
                return -1;
        }
        fclose(fp);
        preference_load();

        return 0;
}
