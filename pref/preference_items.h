
#ifndef	_PREFERENCE_ITEMS_H_
#define _PREFERENCE_ITEMS_H_

enum preference_item_index {

        min_id0 = 0,
        DeviceNumber,
        DeviceMode,
        DeviceSite,
        DirectionCode,
        DirectionText,
        RoadCode,
        DepartmentCode,
        Latitue,
        Longtitue,
        Lane,
        SnapMode,
        max_id0,

        min_id1 = 100,
        UploadMethod,
        UploadServer,
        UploadPort,
        UploadUser,
        UploadPasswd,
        UploadType,
        NtpServer,
        ClientAddr,
        max_id1,

        min_id2 = 200,
        DefaultGain,
        MinGain,
        MaxGain,
        DefaultExposure,
        MinExposure,
        MaxExposure,
        VideoTargetGray,
        TriggerTargetGray,
        RGain,
        BGain,
        AEZone,
        AEWMode,
        DaytimeStartMin,
        DaytimeStartHour,
        DaytimeEndMin,
        DaytimeEndHour,
        QValue,
        AdSampleBits,
        SharpenFactor,
        max_id2,

        min_id3 = 300,
        FlashMode,
        FlashDelay,
        LedMode,
        LedMutiple,
        ContinousLight,
        RedlightDelay,
        RedlightSyncMode,
        RedlightEfficient,
        LightOnThreshold,
        max_id3,

        min_id4 = 400,
        StatsPeriod,
        MotorVehicleLength,
        SmallVehicleLength,
        LargeVehicleLength,
        LongVehicleLength,
        max_id4,

        min_id5 = 500,
        RedlightNumber,
        Redlight1Left,
        Redlight1Top,
        Redlight1Width,
        Redlight1Height,
        Redlight2Left,
        Redlight2Top,
        Redlight2Width,
        Redlight2Height,
        Redlight3Left,
        Redlight3Top,
        Redlight3Width,
        Redlight3Height,
        Redlight4Left,
        Redlight4Top,
        Redlight4Width,
        Redlight4Height,
        Redlight5Left,
        Redlight5Top,
        Redlight5Width,
        Redlight5Height,
        Redlight6Left,
        Redlight6Top,
        Redlight6Width,
        Redlight6Height,
        max_id5,
        
        min_id6 = 600,
        OverlayDeviceNumber,
        OverlayTimeStamp,
        OverlaySite,
        OverlayDirection,
        OverlayLane,
        OverlayPlate,
        OverlayPassingType,
        OverlayRedlightTime,
        OverlaySpeed,
        OverlaySpeedLimit,
        OverlayOverspeedPercent,
        OverlayExtraInfo1,
        OverlayExtraInfo2,
        OverlayExtraInfo1Lable,
        OverlayExtraInfo2Lable,
        max_id6,

        Max_Idx
};

#ifdef __cplusplus
extern "C" {
#define ARRAY_SIZE(a)   (sizeof(a) / sizeof((a)[0]))
#define CLEAR(x)        MEMSET(&(x), 0, sizeof(x))
#endif

        int preference_items_init(void);
        int preference_items_get(int type, void *value);
        int preference_items_set(int type, void *value);
        int preference_items_get_string(int type, char *value);
        int preference_items_set_string(int type, char *value);

#ifdef __cplusplus
}
#endif
#endif
