
#include "pref.h"
#include "preference_items.h"
#include "preference.h"
#include <pthread.h>

static pthread_mutex_t Preference_mutex;

struct preference_item {
        enum preference_item_index idx;
        class pref *pref;
};

class preference_items {
        public:preference_items(struct preference_item *items, int len) {
                this->items = items;
                this->len = len;
        } ~preference_items(void) {
        }
      public:
        int get(int index, void *value) {
                int i = 0;
                for (i = 0; i < this->len; ++i) {
                        if (this->items[i].idx == index) {
                                return items[i].pref->get(value);
                        }
                }
                return -2;
        }
        int set(int index, void *value) {
                int i = 0;
                int tmp = 0;
                char IniFileName[256];
                char *pInifileName = IniFileName;

                for (i = 0; i < this->len; ++i) {
                        if (this->items[i].idx == index) {
                                XMLNode root_node;
                                XMLResults result;

                                tmp = items[i].pref->set(value);

                                pInifileName = preference_get_inifile_name();
                                root_node = XMLNode::parseFile(pInifileName, "xml", &result);
                                if (root_node.getName() == NULL) {
                                        return -1;
                                }
                                items[i].pref->save(root_node);
                                root_node.writeToFile(pInifileName);
                                return tmp;
                        }
                }
                return -2;
        }
        int get_string(int index, char *value) {
                int i = 0;
                for (i = 0; i < this->len; ++i) {
                        if (this->items[i].idx == index) {
                                return items[i].pref->get_string(value);
                        }
                }
                return -2;
        }
        int set_string(int index, char *value) {
                int i = 0;
                int tmp = 0;
                char IniFileName[256];
                char *pInifileName = IniFileName;

                for (i = 0; i < this->len; ++i) {
                        if (this->items[i].idx == index) {
                                XMLNode root_node;
                                XMLResults result;

                                tmp = items[i].pref->set_string(value);
                                pInifileName = preference_get_inifile_name();

                                root_node = XMLNode::parseFile(pInifileName, "xml", &result);
                                if (root_node.getName() == NULL) {
                                        return -1;
                                }

                                items[i].pref->save(root_node);
                                root_node.writeToFile(pInifileName);
                                return tmp;
                        }
                }
                return -2;
        }
      private:
        struct preference_item *items;
        int len;
};

static struct preference_item preference_items_main[max_id0];
static struct preference_item preference_items_upload[max_id1 - min_id1 - 1];
static struct preference_item preference_items_aew[max_id2 - min_id2 - 1];
static struct preference_item preference_items_peripherral[max_id3 - min_id3 - 1];
static struct preference_item preference_items_traffic[max_id4 - min_id4 - 1];
static struct preference_item preference_items_redlight[max_id5 - min_id5 - 1];
static struct preference_item preference_items_overlay[max_id6 - min_id6 -1];

int preference_items_get(int type, void *value)
{
        class preference_items items_main(preference_items_main,
                                ARRAY_SIZE(preference_items_main));
        class preference_items items_upload(preference_items_upload,
                                ARRAY_SIZE(preference_items_upload));
        class preference_items items_aew(preference_items_aew,
                                ARRAY_SIZE(preference_items_aew));
        class preference_items items_peripherral(preference_items_peripherral,
                                ARRAY_SIZE(preference_items_peripherral));
        class preference_items items_traffic(preference_items_traffic,
                                ARRAY_SIZE(preference_items_traffic));
        class preference_items items_redlight(preference_items_redlight,
                                ARRAY_SIZE(preference_items_redlight));
        class preference_items items_overlay(preference_items_overlay,
                                ARRAY_SIZE(preference_items_overlay));

        int ret;
        pthread_mutex_lock(&Preference_mutex);
        if ((ret = items_main.get(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }
        if ((ret = items_upload.get(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_aew.get(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_peripherral.get(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_traffic.get(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_redlight.get(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_overlay.get(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        pthread_mutex_unlock(&Preference_mutex);
        return ret;
}

int preference_items_set(int type, void *value)
{
        class preference_items items_main(preference_items_main,
                                ARRAY_SIZE(preference_items_main));
        class preference_items items_upload(preference_items_upload,
                                ARRAY_SIZE(preference_items_upload));
        class preference_items items_aew(preference_items_aew,
                                ARRAY_SIZE(preference_items_aew));
        class preference_items items_peripherral(preference_items_peripherral,
                                ARRAY_SIZE(preference_items_peripherral));
        class preference_items items_traffic(preference_items_traffic,
                                ARRAY_SIZE(preference_items_traffic));
        class preference_items items_redlight(preference_items_redlight,
                                ARRAY_SIZE(preference_items_redlight));
        class preference_items items_overlay(preference_items_overlay,
                                ARRAY_SIZE(preference_items_overlay));

        int ret;
        pthread_mutex_lock(&Preference_mutex);
        if ((ret = items_main.set(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_upload.set(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_aew.set(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_peripherral.set(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_traffic.set(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_redlight.set(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_overlay.set(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        pthread_mutex_unlock(&Preference_mutex);

        return ret;
}

int preference_items_get_string(int type, char *value)
{
        class preference_items items_main(preference_items_main,
                                ARRAY_SIZE(preference_items_main));
        class preference_items items_upload(preference_items_upload,
                                ARRAY_SIZE(preference_items_upload));
        class preference_items items_aew(preference_items_aew,
                                ARRAY_SIZE(preference_items_aew));
        class preference_items items_peripherral(preference_items_peripherral,
                                ARRAY_SIZE(preference_items_peripherral));
        class preference_items items_traffic(preference_items_traffic,
                                ARRAY_SIZE(preference_items_traffic));
        class preference_items items_redlight(preference_items_redlight,
                                ARRAY_SIZE(preference_items_redlight));
        class preference_items items_overlay(preference_items_overlay,
                                ARRAY_SIZE(preference_items_overlay));

        int ret;
        pthread_mutex_lock(&Preference_mutex);
        if ((ret = items_main.get_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_upload.get_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_aew.get_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_peripherral.get_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_traffic.get_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_redlight.get_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_overlay.get_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        pthread_mutex_unlock(&Preference_mutex);
        return ret;
}

int preference_items_set_string(int type, char *value)
{
        class preference_items items_main(preference_items_main,
                                ARRAY_SIZE(preference_items_main));
        class preference_items items_upload(preference_items_upload,
                                ARRAY_SIZE(preference_items_upload));
        class preference_items items_aew(preference_items_aew,
                                ARRAY_SIZE(preference_items_aew));
        class preference_items items_peripherral(preference_items_peripherral,
                                ARRAY_SIZE(preference_items_peripherral));
        class preference_items items_traffic(preference_items_traffic,
                                ARRAY_SIZE(preference_items_traffic));
        class preference_items items_redlight(preference_items_redlight,
                                ARRAY_SIZE(preference_items_redlight));
        class preference_items items_overlay(preference_items_overlay,
                                ARRAY_SIZE(preference_items_overlay));

        int ret;
        pthread_mutex_lock(&Preference_mutex);
        if ((ret = items_main.set_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }
        if ((ret = items_upload.set_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_aew.set_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }
        if ((ret = items_peripherral.set_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_traffic.set_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_redlight.set_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        if ((ret = items_overlay.set_string(type, value)) == 0) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        } else if (ret != -2) {
                pthread_mutex_unlock(&Preference_mutex);
                return ret;
        }

        pthread_mutex_unlock(&Preference_mutex);

        return ret;
}

extern struct app_param app_param;

static inline int preference_items_init_main(void)
{
        struct app_param *p = &app_param;
        preference_items_main[ 0].idx   = DeviceNumber;
        preference_items_main[ 0].pref  = p->main.DeviceNumber;
        preference_items_main[ 1].idx   = DeviceMode;
        preference_items_main[ 1].pref  = p->main.DeviceMode;
        preference_items_main[ 2].idx   = DeviceSite;
        preference_items_main[ 2].pref  = p->main.DeviceSite;
        preference_items_main[ 3].idx   = DirectionCode;
        preference_items_main[ 3].pref  = p->main.DirectionCode;
        preference_items_main[ 4].idx   = DirectionText;
        preference_items_main[ 4].pref  = p->main.DirectionText;
        preference_items_main[ 5].idx   = RoadCode;
        preference_items_main[ 5].pref  = p->main.RoadCode;
        preference_items_main[ 6].idx   = DepartmentCode;
        preference_items_main[ 6].pref  = p->main.DepartmentCode;
        preference_items_main[ 7].idx   = Latitue;
        preference_items_main[ 7].pref  = p->main.Latitue;
        preference_items_main[ 8].idx   = Longtitue;
        preference_items_main[ 8].pref  = p->main.Longtitue;
        preference_items_main[ 9].idx   = Lane;
        preference_items_main[ 9].pref  = p->main.Lane;
        preference_items_main[10].idx   = SnapMode;
        preference_items_main[10].pref  = p->main.SnapMode;

        return 0;
}

static inline int preference_items_init_traffic(void)
{
        struct app_param *p = &app_param;

        preference_items_traffic[0].idx         = StatsPeriod;
        preference_items_traffic[0].pref        = p->traffic.StatsPeriod;
        preference_items_traffic[1].idx         = MotorVehicleLength;
        preference_items_traffic[1].pref        = p->traffic.MotorVehicleLength;
        preference_items_traffic[2].idx         = SmallVehicleLength;
        preference_items_traffic[2].pref        = p->traffic.SmallVehicleLength;
        preference_items_traffic[3].idx         = LargeVehicleLength;
        preference_items_traffic[3].pref        = p->traffic.LargeVehicleLength;
        preference_items_traffic[4].idx         = LongVehicleLength;
        preference_items_traffic[4].pref        = p->traffic.LongVehicleLength;

        return 0;
}

static inline int preference_items_init_upload(void)
{
        struct app_param *p = &app_param;

        preference_items_upload[0].idx  = UploadMethod;
        preference_items_upload[0].pref = p->upload.UploadMethod;
        preference_items_upload[1].idx  = UploadServer;
        preference_items_upload[1].pref = p->upload.UploadServer;
        preference_items_upload[2].idx  = UploadPort;
        preference_items_upload[2].pref = p->upload.UploadPort;
        preference_items_upload[3].idx  = UploadUser;
        preference_items_upload[3].pref = p->upload.UploadUser;
        preference_items_upload[4].idx  = UploadPasswd;
        preference_items_upload[4].pref = p->upload.UploadPasswd;
        preference_items_upload[5].idx  = UploadType;
        preference_items_upload[5].pref = p->upload.UploadType;
        preference_items_upload[6].idx  = NtpServer;
        preference_items_upload[6].pref = p->upload.NtpServer;
        preference_items_upload[7].idx  = ClientAddr;
        preference_items_upload[7].pref = p->upload.ClientAddr;

        return 0;
}

static inline int preference_items_init_aew(void)
{
        struct app_param *p = &app_param;

        preference_items_aew[ 0].idx    = DefaultGain;
        preference_items_aew[ 0].pref   = p->aew.DefaultGain;
        preference_items_aew[ 1].idx    = MinGain;
        preference_items_aew[ 1].pref   = p->aew.MinGain;
        preference_items_aew[ 2].idx    = MaxGain;
        preference_items_aew[ 2].pref   = p->aew.MaxGain;
        preference_items_aew[ 3].idx    = DefaultExposure;
        preference_items_aew[ 3].pref   = p->aew.DefaultExposure;
        preference_items_aew[ 4].idx    = MinExposure;
        preference_items_aew[ 4].pref   = p->aew.MinExposure;
        preference_items_aew[ 5].idx    = MaxExposure;
        preference_items_aew[ 5].pref   = p->aew.MaxExposure;
        preference_items_aew[ 6].idx    = VideoTargetGray;
        preference_items_aew[ 6].pref   = p->aew.VideoTargetGray;
        preference_items_aew[ 7].idx    = TriggerTargetGray;
        preference_items_aew[ 7].pref   = p->aew.TriggerTargetGray;
        preference_items_aew[ 8].idx    = RGain;
        preference_items_aew[ 8].pref   = p->aew.RGain;
        preference_items_aew[ 9].idx    = BGain;
        preference_items_aew[ 9].pref   = p->aew.BGain;
        preference_items_aew[10].idx    = AEZone;
        preference_items_aew[10].pref   = p->aew.AEZone;
        preference_items_aew[11].idx    = AEWMode;
        preference_items_aew[11].pref   = p->aew.AEWMode;
        preference_items_aew[12].idx    = DaytimeStartMin;
        preference_items_aew[12].pref   = p->aew.DaytimeStartMin;
        preference_items_aew[13].idx    = DaytimeStartHour;
        preference_items_aew[13].pref   = p->aew.DaytimeStartHour;
        preference_items_aew[14].idx    = DaytimeEndMin;
        preference_items_aew[14].pref   = p->aew.DaytimeEndMin;
        preference_items_aew[15].idx    = DaytimeEndHour;
        preference_items_aew[15].pref   = p->aew.DaytimeEndHour;
        preference_items_aew[16].idx    = QValue;
        preference_items_aew[16].pref   = p->aew.QValue;
        preference_items_aew[17].idx    = AdSampleBits;
        preference_items_aew[17].pref   = p->aew.AdSampleBits;
        preference_items_aew[18].idx    = SharpenFactor;
        preference_items_aew[18].pref   = p->aew.SharpenFactor;

        return 0;
}

static inline int preference_items_init_peripherral(void)
{
        struct app_param *p = &app_param;

        preference_items_peripherral[0].idx     = FlashMode;
        preference_items_peripherral[0].pref    = p->peripherral.FlashMode;
        preference_items_peripherral[1].idx     = FlashDelay;
        preference_items_peripherral[1].pref    = p->peripherral.FlashDelay;
        preference_items_peripherral[2].idx     = LedMode;
        preference_items_peripherral[2].pref    = p->peripherral.LedMode;
        preference_items_peripherral[3].idx     = LedMutiple;
        preference_items_peripherral[3].pref    = p->peripherral.LedMutiple;
        preference_items_peripherral[4].idx     = ContinousLight;
        preference_items_peripherral[4].pref    = p->peripherral.ContinousLight;
        preference_items_peripherral[5].idx     = RedlightDelay;
        preference_items_peripherral[5].pref    = p->peripherral.RedlightDelay;
        preference_items_peripherral[6].idx     = RedlightSyncMode;
        preference_items_peripherral[6].pref    = p->peripherral.RedlightSyncMode;
        preference_items_peripherral[7].idx     = RedlightEfficient;
        preference_items_peripherral[7].pref    = p->peripherral.RedlightEfficient;
        preference_items_peripherral[8].idx     = LightOnThreshold;
        preference_items_peripherral[8].pref    = p->peripherral.LightOnThreshold;

        return 0;
}

static inline int preference_items_init_redlight(void)
{
        struct app_param *p = &app_param;

        preference_items_redlight[ 0].idx       = RedlightNumber;
        preference_items_redlight[ 0].pref      = p->redlight.RedlightNumber;
        preference_items_redlight[ 1].idx       = Redlight1Left;
        preference_items_redlight[ 1].pref      = p->redlight.Redlight1Left;
        preference_items_redlight[ 2].idx       = Redlight1Top;
        preference_items_redlight[ 2].pref      = p->redlight.Redlight1Top;
        preference_items_redlight[ 3].idx       = Redlight1Width;
        preference_items_redlight[ 3].pref      = p->redlight.Redlight1Width;
        preference_items_redlight[ 4].idx       = Redlight1Height;
        preference_items_redlight[ 4].pref      = p->redlight.Redlight1Height;
        preference_items_redlight[ 5].idx       = Redlight2Left;
        preference_items_redlight[ 5].pref      = p->redlight.Redlight2Left;
        preference_items_redlight[ 6].idx       = Redlight2Top;
        preference_items_redlight[ 6].pref      = p->redlight.Redlight2Top;
        preference_items_redlight[ 7].idx       = Redlight2Width;
        preference_items_redlight[ 7].pref      = p->redlight.Redlight2Width;
        preference_items_redlight[ 8].idx       = Redlight2Height;
        preference_items_redlight[ 8].pref      = p->redlight.Redlight2Height;
        preference_items_redlight[ 9].idx       = Redlight3Left;
        preference_items_redlight[ 9].pref      = p->redlight.Redlight3Left;
        preference_items_redlight[10].idx       = Redlight3Top;
        preference_items_redlight[10].pref      = p->redlight.Redlight3Top;
        preference_items_redlight[11].idx       = Redlight3Width;
        preference_items_redlight[11].pref      = p->redlight.Redlight3Width;
        preference_items_redlight[12].idx       = Redlight3Height;
        preference_items_redlight[12].pref      = p->redlight.Redlight3Height;
        preference_items_redlight[13].idx       = Redlight4Left;
        preference_items_redlight[13].pref      = p->redlight.Redlight4Left;
        preference_items_redlight[14].idx       = Redlight4Top;
        preference_items_redlight[14].pref      = p->redlight.Redlight4Top;
        preference_items_redlight[15].idx       = Redlight4Width;
        preference_items_redlight[15].pref      = p->redlight.Redlight4Width;
        preference_items_redlight[16].idx       = Redlight4Height;
        preference_items_redlight[16].pref      = p->redlight.Redlight4Height;
        preference_items_redlight[17].idx       = Redlight5Left;
        preference_items_redlight[17].pref      = p->redlight.Redlight5Left;
        preference_items_redlight[18].idx       = Redlight5Top;
        preference_items_redlight[18].pref      = p->redlight.Redlight5Top;
        preference_items_redlight[19].idx       = Redlight5Width;
        preference_items_redlight[19].pref      = p->redlight.Redlight5Width;
        preference_items_redlight[20].idx       = Redlight5Height;
        preference_items_redlight[20].pref      = p->redlight.Redlight5Height;
        preference_items_redlight[21].idx       = Redlight6Left;
        preference_items_redlight[21].pref      = p->redlight.Redlight6Left;
        preference_items_redlight[22].idx       = Redlight6Top;
        preference_items_redlight[22].pref      = p->redlight.Redlight6Top;
        preference_items_redlight[23].idx       = Redlight6Width;
        preference_items_redlight[23].pref      = p->redlight.Redlight6Width;
        preference_items_redlight[24].idx       = Redlight6Height;
        preference_items_redlight[24].pref      = p->redlight.Redlight6Height;

        return 0;
}

static inline int preference_items_init_overlay(void)
{
        struct app_param *p = &app_param;

        preference_items_overlay[ 0].idx        = OverlayDeviceNumber;
        preference_items_overlay[ 0].pref       = p->overlay.OverlayDeviceNumber;
        preference_items_overlay[ 1].idx        = OverlayTimeStamp;
        preference_items_overlay[ 1].pref       = p->overlay.OverlayTimeStamp;
        preference_items_overlay[ 2].idx        = OverlaySite;
        preference_items_overlay[ 2].pref       = p->overlay.OverlaySite;
        preference_items_overlay[ 3].idx        = OverlayDirection;
        preference_items_overlay[ 3].pref       = p->overlay.OverlayDirection;
        preference_items_overlay[ 4].idx        = OverlayLane;
        preference_items_overlay[ 4].pref       = p->overlay.OverlayLane;
        preference_items_overlay[ 5].idx        = OverlayPlate;
        preference_items_overlay[ 5].pref       = p->overlay.OverlayPlate;
        preference_items_overlay[ 6].idx        = OverlayPassingType;
        preference_items_overlay[ 6].pref       = p->overlay.OverlayPassingType;
        preference_items_overlay[ 7].idx        = OverlayRedlightTime;
        preference_items_overlay[ 7].pref       = p->overlay.OverlayRedlightTime;
        preference_items_overlay[ 8].idx        = OverlaySpeed;
        preference_items_overlay[ 8].pref       = p->overlay.OverlaySpeed;
        preference_items_overlay[ 9].idx        = OverlaySpeedLimit;
        preference_items_overlay[ 9].pref       = p->overlay.OverlaySpeedLimit;
        preference_items_overlay[10].idx        = OverlayOverspeedPercent;
        preference_items_overlay[10].pref       = p->overlay.OverlayOverspeedPercent;
        preference_items_overlay[11].idx        = OverlayExtraInfo1;
        preference_items_overlay[11].pref       = p->overlay.OverlayExtraInfo1;
        preference_items_overlay[12].idx        = OverlayExtraInfo2;
        preference_items_overlay[12].pref       = p->overlay.OverlayExtraInfo2;
        preference_items_overlay[13].idx        = OverlayExtraInfo1Lable;
        preference_items_overlay[13].pref       = p->overlay.OverlayExtraInfo1Lable;
        preference_items_overlay[14].idx        = OverlayExtraInfo2Lable;
        preference_items_overlay[14].pref       = p->overlay.OverlayExtraInfo2Lable;

        return 0;
}

int preference_items_init(void)
{
        preference_items_init_main();
        preference_items_init_upload();
        preference_items_init_aew();
        preference_items_init_peripherral();
        preference_items_init_traffic();
        preference_items_init_redlight();
        preference_items_init_overlay();
        pthread_mutex_init(&Preference_mutex, NULL);
        return 0;
}
