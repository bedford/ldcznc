#ifndef _UART_H_
#define _UART_H_

#include "ldczn_base.h"
#include "thread.h"

class Uart:public Thread {
      public:
        Uart();
        ~Uart();
        void SetSignalModuleTesting();

      protected:
        void Run();

      private:
        int SendMessage(char *buffer, int length);

        void ProcessPacket(char *buf, int len);
        void ProcessEvent(char *buf, int len);
        void UploadEventStatus(TriggerInfo * info);

        void EnableSend(void);
        void EnableRecv(void);

        void Init();
        void Deinit();
        int uart_fd;
        int uart_selector;
};

#endif
