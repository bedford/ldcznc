#ifndef _PROPERTY_H_
#define _PROPERTY_H_

struct CameraProperty {
        int width;
        int height;
        int height_useful;
        int stride;
        int line;
};

enum SensorType {
        LD_200W = 2,
        LD_500W = 5,
};

class Property {
public:
        static struct CameraProperty GetDeviceProperty(int ccd_type);
};

#endif
