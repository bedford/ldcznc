/**
 * @file        fix_capture.cpp
 * @brief       业务处理逻辑模块
 * @author      hrh <huangrh@landuntec.com>
 * @version     1.0.0
 * @date        2012-07-06
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011-2012
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <string.h>
#include <unistd.h>
#include "mutex.h"
#include "mem_pool.h"
#include "snap_type.h"
#include "fix_capture.h"
#include "debug.h"
#include "uploader.h"
#include "parameters.h"
#include "peripherral_manage.h"

static const int kMaxQueueSize = 5;
static const int kTimeoutDelta = 5000;
static CaptureInfo *Queue[kMaxQueueSize];

void FixCapture::Init()
{
        _mem_pool = new MemPool(FIRSET_FRAMT_OFFSET + 3 * MEGA_SIZE, 12);
        _uploader = new Uploader();
        _uploader->Open(_mem_pool);
        _uploader->Start();
}

FixCapture::FixCapture()
{
        memset(&Queue, 0, sizeof(Queue));
        _queue_empty    = true;
        _queue_size     = 0;

        Init();
}

void FixCapture::Deinit()
{
        _uploader->Terminate();
        _uploader->Join();
        delete _uploader;
        _uploader = NULL;
        Debug(DEBUG, "join uploader module");

        _mem_pool->Release();
        delete _mem_pool;
        _mem_pool = NULL;
        Debug(DEBUG, "deinit fixcapture");
}

FixCapture::~FixCapture()
{
        Debug(DEBUG, "fixcapture deconstruct");
}

CaptureInfo *FixCapture::CreateCaptureInfo(struct image_buffer_description *image,
                                           TriggerInfo * info)
{
        CaptureInfo *capture_info = (CaptureInfo *) _mem_pool->Alloc();
        if (capture_info == NULL) {
                Debug(INFO, "can't alloc memory from the pool");
                return capture_info;
        }

        Debug(DEBUG, "create capture info");
        capture_info->pTrigger_info = (TriggerInfo *) ((char *)capture_info + CAPTUREINFO_SIZE);
        memcpy(capture_info->pTrigger_info, info, TRIGGERINFO_SIZE);
        PeripherralManage::ReleaseTriggerInfo(info);

        capture_info->pDevice_info = (DeviceInfo *) ((char *)capture_info + CAPTUREINFO_SIZE +
                                                     TRIGGERINFO_SIZE + VEHICLEIMAGEINFO_SIZE);

        DeviceInfo device_info = Parameters::GetInstance()->GetDeviceInfo();
        memcpy(capture_info->pDevice_info, &device_info, DEVICEINFO_SIZE);

        capture_info->pVehicle_image_info = (VehicleImageInfo *) ((char *)capture_info +
                                                                  CAPTUREINFO_SIZE +
                                                                  TRIGGERINFO_SIZE);
        capture_info->pVehicle_image_info->frame = 1;
        capture_info->pVehicle_image_info->vehicle_image[0].image_width  = image->info.width;
        capture_info->pVehicle_image_info->vehicle_image[0].image_height = image->info.height;
        capture_info->pVehicle_image_info->vehicle_image[0].image_stride = image->info.stride;
        capture_info->pVehicle_image_info->vehicle_image[0].image_length = image->info.size;
        capture_info->pVehicle_image_info->vehicle_image[0].capture_time = image->tv;
        capture_info->pVehicle_image_info->vehicle_image[0].image_buffer =
            (char *)capture_info + FIRSET_FRAMT_OFFSET;

        memcpy(capture_info->pVehicle_image_info->vehicle_image[0].image_buffer,
               (char *)image->info.vir_addr, image->info.size);

        return capture_info;
}

bool FixCapture::PutSingleInfo(struct image_buffer_description * image, TriggerInfo * info)
{
        DeviceInfo device_info = Parameters::GetInstance()->GetDeviceInfo();

        if ((info->offence_type == DeviceSnapType_ReversePass) &&
                        (device_info.device_mode == Sitecap ||
                         device_info.device_mode == RedLight_Reverse)) {       //逆行
                Debug(INFO, "reverse drive");
                ViolationPassing(image, info);
        } else if ((info->offence_type == DeviceSnapType_NormalPass) &&
                        (device_info.device_mode == Sitecap)) {        //正常通行,卡口
                CaptureInfo *capture_info = CreateCaptureInfo(image, info);
                if (capture_info == NULL) {
                        Debug(WARN, "can't create captureinfo for passing vehicle");
                        return false;
                }
                _uploader->AddSampleToListFront(capture_info);  //推送到传输队列
                capture_info = NULL;
        } else if (info->offence_type == DeviceSnapType_RedLightPass) {        //闯红灯
                Debug(INFO, "red light pass");
                ViolationPassing(image, info);
        } else if (info->offence_type == DeviceSnapType_LimitedPass) { //违章通行
                Debug(INFO, "limited pass violation");
                ViolationPassing(image, info);
        } else {
                Debug(WARN, "Not supported offence type");
        }

        return true;
}

void FixCapture::ViolationPassing(struct image_buffer_description *image, TriggerInfo * info)
{
        int index = info->lane - 1;
        if (_queue_empty) {
                CaptureInfo *tmp_info = CreateCaptureInfo(image, info);
                if (tmp_info == NULL) {
                        Debug(WARN, "can't create captureinfo for violation vehicle");
                        return;
                }
                Queue[index] = tmp_info;
                _queue_empty = false;
                _queue_size++;
                return;
        }

        if (Queue[index]) {     //已经有数据的车道
                Enqueue(image, info);
        } else {                //之前未有数据的车道
                CaptureInfo *tmp_info = CreateCaptureInfo(image, info);
                if (tmp_info == NULL) {
                        Debug(WARN, "can't create captureinfo for violation vehicle");
                        return;
                }
                Queue[index] = tmp_info;
                _queue_empty = false;
                _queue_size++;
        }
        return;
}

void FixCapture::AddSampleToUploader(CaptureInfo * &info)
{
        Debug(DEBUG, "snapnbr is %s", info->pTrigger_info->snap_nbr);
        _uploader->AddSampleToListFront(info);
        info = 0;
        _queue_size--;
}

void FixCapture::Enqueue(struct image_buffer_description *image, TriggerInfo * info)
{
        int index = info->lane - 1;
        PeripherralManage::ReleaseTriggerInfo(info);    //对已经有数据的，不保存后来的两个触发数据

        _mutex.Lock();
        CaptureInfo *tmp_info = Queue[index];

        int frame = tmp_info->pVehicle_image_info->frame;
        tmp_info->pVehicle_image_info->frame++;

        tmp_info->pVehicle_image_info->vehicle_image[frame].image_width  = image->info.width;
        tmp_info->pVehicle_image_info->vehicle_image[frame].image_height = image->info.height;
        tmp_info->pVehicle_image_info->vehicle_image[frame].image_stride = image->info.stride;
        tmp_info->pVehicle_image_info->vehicle_image[frame].image_length = image->info.size;
        tmp_info->pVehicle_image_info->vehicle_image[frame].capture_time = image->tv;

        if (frame == 1) {
                tmp_info->pVehicle_image_info->vehicle_image[frame].image_buffer =
                    (char *)tmp_info + SECOND_FRAMT_OFFSET;
        } else {
                tmp_info->pVehicle_image_info->vehicle_image[frame].image_buffer =
                    (char *)tmp_info + THIRD_FRAMT_OFFSET;
        }

        memcpy(tmp_info->pVehicle_image_info->vehicle_image[frame].image_buffer,
               (char *)image->info.vir_addr, image->info.size);

        int offence_type = tmp_info->pTrigger_info->offence_type;

        switch (offence_type) {
        case DeviceSnapType_ReversePass:
        case DeviceSnapType_LimitedPass:
                AddSampleToUploader(Queue[index]);
                break;
        case DeviceSnapType_RedLightPass:
                if (frame == 2) {
                        AddSampleToUploader(Queue[index]);
                }
                break;
        default:
                Debug(WARN, "Unknow passing type");
                break;
        }

        tmp_info = NULL;
        _mutex.Unlock();
}

void FixCapture::Run()
{
        Debug(INFO, "start fix capture thread");
        while (!IsTerminated()) {
                if (!_queue_empty) {
                        ClearPool();
                }
                sleep(2);
        }

        if (!_queue_empty) {
                ClearPool();
        }
        Deinit();
}

void FixCapture::ClearPool()
{
        int index = 0;
        int frame = 0;
        CaptureInfo *tmp_info = NULL;
        _mutex.Lock();
        for (; index < 5; index++) {
                tmp_info = Queue[index];
                if (tmp_info == NULL) {
                        continue;
                }

                frame = tmp_info->pVehicle_image_info->frame;
                if (frame) {
                        if (IsTimeout
                            (&tmp_info->pVehicle_image_info->vehicle_image[0].capture_time)) {
                                _mem_pool->DeAlloc((void *)Queue[index]);
                                Queue[index] = 0;
                                _queue_size--;
                        }
                }
        }

        if (_queue_size == 0) {
                _queue_empty = true;
        }
        _mutex.Unlock();
        tmp_info = NULL;
}

bool FixCapture::IsTimeout(struct timeval *trigger_time)
{
        struct timeval current_time;
        gettimeofday(&current_time, NULL);

        long delta = (current_time.tv_sec - trigger_time->tv_sec) * 1000 +
            (current_time.tv_usec - trigger_time->tv_usec) / 1000;
        if (delta > kTimeoutDelta) {
                return true;
        }

        return false;
}

bool FixCapture::IsMatch(struct image_buffer_description * image, TriggerInfo * info)
{
        long delta = (image->tv.tv_sec - info->passing_time.tv_sec) * 1000 +
            (image->tv.tv_usec - info->passing_time.tv_usec) / 1000;
        Debug(INFO, "time delta is %ld", delta);
        if (delta < 0 || delta > 230) {
                return false;
        }

        return true;
}
