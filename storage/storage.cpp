#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/statfs.h>
#include <string.h>
#include <sys/time.h>
#include <vector>

#include <errno.h>

#include "storage.h"
#include "filesystem_util.h"
#include "debug.h"
#include "parameters.h"

static const char *kSaveHomePath        = "/snap";
static const int kMaxAvailablePercent   = 90;
static const int kDelMaxNumber          = 30;
static const size_t kSnapNbrLength      = 18;
static const int kShortFileName         = 32;
static const int kLongFileName          = 128;
static const long kExt3Magic            = 0xEF53;

static const int kInfoFileLenght = TRIGGERINFO_SIZE + VEHICLEIMAGEINFO_SIZE + DEVICEINFO_SIZE;

using namespace std;

Storage::Storage()
{
        const char *path = getenv("PATH_SNAP");
        if (path == NULL) {
                memcpy(m_szSaveHomePath, kSaveHomePath, sizeof(m_szSaveHomePath));
        } else {
                memcpy(m_szSaveHomePath, path, sizeof(m_szSaveHomePath));
        }

        bDiskFull = GetDiskStatus();
}

Storage::~Storage()
{

}

bool Storage::GetDiskStatus()
{
        struct statfs buf;
        int DiskPercentUsed;

        if (statfs(m_szSaveHomePath, &buf) < 0) {
                return false;
        }

        DiskPercentUsed = (int)((buf.f_blocks - buf.f_bfree) * 100ULL +
                                (buf.f_blocks - buf.f_bfree + buf.f_bavail) / 2) /
            (buf.f_blocks - buf.f_bfree + buf.f_bavail);
        Debug(DEBUG, "Disk-Percent-Used: %d", DiskPercentUsed);
        Debug(DEBUG, "file system type is %x", buf.f_type);
        if (buf.f_type != kExt3Magic) {
                DeviceStatus device_status = Parameters::GetInstance()->GetDeviceStatus();
                unsigned int status = device_status.status | NO_FLASH_DISK;
                Parameters::GetInstance()->ChangeDeviceStatus(status);
        }

        if (DiskPercentUsed >= kMaxAvailablePercent) {
                return true;
        } else {
                return false;
        }
}

vector < char *>Storage::GetSnapNbrLst(const char *fileName, int max_num)
{
        vector < char *>fileList;
        if (-1 == FileSystemUtil::ScanTree(m_szSaveHomePath, m_szSaveHomePath,
                                           fileName, fileList, max_num)) {

                vector < char *>file_tmp;
                return file_tmp;
        }

        char *snapNbr = NULL;
        vector < char *>snapNbrLst;

        for (vector < char *>::size_type i = 0; i < fileList.size(); i++) {
                char *p1 = strrchr(fileList[i], '.');
                char *p2 = strrchr(fileList[i], '/');
                int len  = p1 - p2 - 1;
                if (len > 0) {
                        snapNbr = new char[kShortFileName];
                        memset(snapNbr, 0, kShortFileName);
                        memcpy(snapNbr, p2 + 1, len);
                        snapNbrLst.push_back(snapNbr);
                        free(fileList[i]);
                }
        }

        return snapNbrLst;
}

bool Storage::RemoveOldInfo()
{
        vector < char *>snapNbr_list = GetSnapNbrLst("??????????????????.dat", kDelMaxNumber);
        for (vector < char *>::size_type i = 0; i < snapNbr_list.size(); i++) {
                RemoveVehicleInfo(snapNbr_list[i]);
                delete[] snapNbr_list[i];
                Debug(DEBUG, "RemoveOldInfo success");
        }

        return true;
}

//保存抓拍信息，传入参数含义可见关文件
bool Storage::SaveVehicleInfo(CaptureInfo * capture_info, bool bNeedUpload)
{
        if (capture_info == NULL) {
                Debug(ERROR, "capture info is NULL");
                return false;
        }

        if (capture_info->bSaved) {
                return true;
        }

        if (bDiskFull) {
                RemoveOldInfo();
        }

        int index = (bNeedUpload) ? 1 : 0;

        //生成存文件名
        char tmp[kLongFileName];
        char path[kLongFileName];
        char infoFileName[kLongFileName];
        char dataFileName[kLongFileName];

        struct tm *pTm = localtime(&(capture_info->pTrigger_info->passing_time.tv_sec));
        strftime(tmp, 128, "/%Y/%m/%d/", pTm);
        sprintf(path, "%s%s", m_szSaveHomePath, tmp);
        FileSystemUtil::CreateDir(path);

        sprintf(infoFileName, "%s%s%d.dat", path, capture_info->pTrigger_info->snap_nbr, index);
        FILE *fp = fopen(infoFileName, "wb");
        if (fp == NULL) {
                Debug(ERROR, "SaveVehicleInfo:Open %s error", infoFileName);
                return false;
        }

        if (1 != fwrite(capture_info->pTrigger_info, TRIGGERINFO_SIZE, 1, fp)) {
                Debug(ERROR, "SaveVehicleInfo:write %s error", infoFileName);
                fclose(fp);
                return false;
        }
        fwrite(capture_info->pVehicle_image_info, VEHICLEIMAGEINFO_SIZE, 1, fp);
        fwrite(capture_info->pDevice_info, DEVICEINFO_SIZE, 1, fp);
        fclose(fp);

        VehicleImageInfo *pVehicleImageInfo = capture_info->pVehicle_image_info;
        for (size_t i = 0; i < pVehicleImageInfo->frame; i++) {

                sprintf(dataFileName, "%s%s%d%s%d.jpg", path,
                        capture_info->pTrigger_info->snap_nbr, index, "_", i);
                fp = fopen(dataFileName, "wb");
                if (fp == NULL) {
                        Debug(ERROR, "Save CaptureInfo : Open %s error", dataFileName);
                        FileSystemUtil::Remove(infoFileName);
                        return false;
                }

                if (1 != fwrite(pVehicleImageInfo->vehicle_image[i].image_buffer,
                                pVehicleImageInfo->vehicle_image[i].image_length, 1, fp)) {

                        Debug(ERROR, "Save CaptureInfo : write %s error", dataFileName);
                        fclose(fp);
                        FileSystemUtil::Remove(infoFileName);
                        return false;
                }
                fclose(fp);
        }

        bDiskFull = GetDiskStatus();
        return true;
}

bool Storage::ReadVehicleInfo(CaptureInfo * capture_info, const char *snap_nbr)
{
        char path[kLongFileName];
        char infoFileName[kLongFileName];
        char dataFileName[kLongFileName];
        FILE *fp = NULL;
        
        VehicleImageInfo *pVehicleImageInfo = capture_info->pVehicle_image_info;
        char *first_pic_buffer  = pVehicleImageInfo->vehicle_image[0].image_buffer;
        char *second_pic_buffer = pVehicleImageInfo->vehicle_image[1].image_buffer;
        char *third_pic_buffer  = pVehicleImageInfo->vehicle_image[2].image_buffer;
        
        GetVehicleInfoPath(snap_nbr, path);
        sprintf(infoFileName, "%s%s.dat", path, snap_nbr);
        fp = fopen(infoFileName, "rb");
        if (fp == NULL) {
                Debug(ERROR, "can't read info file %s: %s", infoFileName, strerror(errno));
                FileSystemUtil::Remove(infoFileName);
                return false;
        }

        if (1 != fread((char *)capture_info + CAPTUREINFO_SIZE, kInfoFileLenght, 1, fp)) {
                Debug(ERROR, "read info file failed");
                fclose(fp);
                FileSystemUtil::Remove(infoFileName);
                return false;
        }
        fclose(fp);

        pVehicleImageInfo->vehicle_image[0].image_buffer = first_pic_buffer;
        pVehicleImageInfo->vehicle_image[1].image_buffer = second_pic_buffer;
        pVehicleImageInfo->vehicle_image[2].image_buffer = third_pic_buffer;

        int frame = pVehicleImageInfo->frame;
        if (frame < 0) {
                RemoveVehicleInfo(snap_nbr);
                pVehicleImageInfo->frame = 0;
                return false;
        }

        int ret = 0;
        for (int index = 0; index < frame; index++) {
                sprintf(dataFileName, "%s%s%s%d.jpg", path, snap_nbr, "_", index);
                fp = fopen(dataFileName, "rb");
                if (fp == NULL) {
                        Debug(ERROR, "open data file %s failed", dataFileName);
                        FileSystemUtil::Remove(infoFileName);
                        ret = -1;
                        break;
                }

                if (1 != fread(pVehicleImageInfo->vehicle_image[index].image_buffer,
                                        pVehicleImageInfo->vehicle_image[index].image_length,
                                        1, fp)) {
                        Debug(ERROR, "Read data file %s failed", dataFileName);
                        fclose(fp);
                        FileSystemUtil::Remove(infoFileName);
                        ret = -1;
                        break;
                }
                fclose(fp);
        }

        if (ret < 0) {
                return false;
        } else {
                return true;
        }
}

//根据抓拍编号从磁盘中读取抓拍信息与图片
bool Storage::GetVehicleInfo(CaptureInfo ** capture_info, int max_list_size)
{
        if (capture_info == NULL) {
                return false;
        }

        vector < char *>snapNbr_list = GetSnapNbrLst("?????????????????1.dat", max_list_size);

        CaptureInfo *tmp_info;
        for (vector < char *>::size_type i = 0; i < snapNbr_list.size(); i++) {

                tmp_info = capture_info[i];
                tmp_info->pVehicle_image_info->frame = 0;
                tmp_info->bSaved = true;

                ReadVehicleInfo(tmp_info, snapNbr_list[i]);
                delete[] snapNbr_list[i];
        }

        return true;
}

void Storage::GetVehicleInfoPath(const char *snapNbr, char *path)
{
        memset(path, 0, kLongFileName);
        strcpy(path, m_szSaveHomePath);
        if (path[strlen(path) - 1] != '/') {
                strcat(path, "/");
        }
        memcpy(&path[strlen(path)], snapNbr, 4);
        strcat(path, "/");
        memcpy(&path[strlen(path)], &(snapNbr[4]), 2);
        strcat(path, "/");
        memcpy(&path[strlen(path)], &(snapNbr[6]), 2);
        strcat(path, "/");
}

//根据抓编号从磁盘中删除拍抓信息文件与图片文件
bool Storage::RemoveVehicleInfo(const char *snapNbr)
{
        char tmpNbr[kShortFileName];
        char path[kLongFileName];
        char infoFileName[kLongFileName];
        char dataFileName[kLongFileName];
        GetVehicleInfoPath(snapNbr, path);

        if (strlen(snapNbr) < kSnapNbrLength) {
                sprintf(tmpNbr, "%s%d", snapNbr, 1);
        } else {
                sprintf(tmpNbr, "%s", snapNbr);
        }

        sprintf(infoFileName, "%s%s.dat", path, tmpNbr);

        int num = 3;
        for (int i = 0; i < num; i++) {
                sprintf(dataFileName, "%s%s%s%d.jpg", path, tmpNbr, "_", i);
                if (FileSystemUtil::FileExist(dataFileName)) {
                        FileSystemUtil::Remove(dataFileName);
                }
        }
        FileSystemUtil::Remove(infoFileName);

        bDiskFull = GetDiskStatus();
        return true;
}
