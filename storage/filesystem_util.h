#ifndef _FILESYSTEM_UTIL_H_
#define _FILESYSTEM_UTIL_H_

#include <vector>

class FileSystemUtil {
public:
        static int CreateDir(const char *sPathName);
        static long GetFileLength(char *filename);
        static int ScanTree(char *Homepath, char *path,
                            const char *file, std::vector < char *>&fileLst,
                            int length = 100, bool bScanSubDir = true);

        static bool Rename(const char *oldname, const char *newname);
        static bool DirExist(const char *Dir);
        static bool FileExist(const char *FName);
        static bool Mkdir(const char *Dir);
        static bool Rmdir(const char *Dir);
        static bool Remove(const char *FName);

private:
        FileSystemUtil();
        ~FileSystemUtil();
};

#endif
