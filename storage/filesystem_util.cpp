#include "filesystem_util.h"
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <dlfcn.h>
#include <dirent.h>
#include <fnmatch.h>

using namespace std;

FileSystemUtil::FileSystemUtil()
{

}

FileSystemUtil::~FileSystemUtil()
{

}

int FileSystemUtil::CreateDir(const char *sPathName)
{
        char DirName[256];

        if (sPathName == 0) {
                return -1;
        }
        if (access(sPathName, NULL) == 0) {
                return 0;
        }

        strcpy(DirName, sPathName);

        int i, len = strlen(DirName);
        if (DirName[len - 1] != '/') {
                strcat(DirName, "/");
        }

        len = strlen(DirName);

        for (i = 1; i < len; i++) {
                if (DirName[i] == '/') {
                        DirName[i] = 0;
                        if (access(DirName, NULL) != 0) {
                                if (mkdir(DirName, 0755) == -1) {
                                        perror("mkdir error");
                                        return -1;
                                }
                        }
                        DirName[i] = '/';
                }
        }
        return 0;
}

long FileSystemUtil::GetFileLength(char *filename)
{
        struct stat sb;
        if (filename == NULL) {
                return -1;
        }
        if (access(filename, NULL) != 0) {
                return -1;
        }
        if (stat(filename, &sb) == -1) {
                printf("fstat error! ");
                return -1;
        } else {
                return sb.st_size;
        }
}

int FileSystemUtil::ScanTree(char *Homepath, char *path,
                             const char *file, vector < char *>&fileLst,
                             int length, bool bScanSubDir)
{
        struct dirent **namelist;
        char *name, *path2;
        int i, n, r, rc = 0;
        struct stat buf;

        n = scandir(path, &namelist, 0, alphasort);
        if (n < 0) {
                perror("scandir");
                return -1;
        }

        for (i = 0; i < n; i++) {
                name = namelist[i]->d_name;

                if ((fnmatch(".", name, 0) == 0) || (fnmatch("..", name, 0) == 0)) { 
                        goto skip;
                }

                if (fileLst.size() >= (unsigned int)length) {
                        goto skip;
                }

                path2 = (char *)malloc(strlen(path) + strlen(name) + 3);
                strcpy(path2, path);
                strcat(path2, "/");
                strcat(path2, name);

                r = lstat(path2, &buf);
                if (r == 0 && false == S_ISDIR(buf.st_mode)) {
                        if (fnmatch(file, name, 0) == 0) {
                                fileLst.push_back(path2);
                        } else {
                                free(path2);
                        }
                } else {
                        if (bScanSubDir) {
                                rc = ScanTree(Homepath, path2, file, fileLst, length, bScanSubDir);
                        }
                        free(path2);
                        if (rc < 0) {
                                return rc;
                        }
                }
                //free(path2);
              skip:
                free(namelist[i]);
        }
        free(namelist);
        if (n == 2 && strcmp(path, Homepath) != 0) {
                remove(path);
        }
        return fileLst.size();
}

bool FileSystemUtil::DirExist(const char *Dir)
{
        if (access(Dir, F_OK) < 0) {
                return false;
        } else {
                return true;
        }
}

bool FileSystemUtil::FileExist(const char *FName)
{
        if (access(FName, F_OK) < 0) {
                return false;
        } else {
                return true;
        }
}

bool FileSystemUtil::Mkdir(const char *Dir)
{
        if (mkdir(Dir, 0755) < 0) {
                return false;
        } else {
                return true;
        }
}

bool FileSystemUtil::Rmdir(const char *Dir)
{
        if (remove(Dir) < 0) {
                return false;
        } else {
                return true;
        }
}

bool FileSystemUtil::Remove(const char *FName)
{
        if (remove(FName) < 0) {
                return false;
        } else {
                return true;
        }
}

bool FileSystemUtil::Rename(const char *oldname, const char *newname)
{
        if (strlen(oldname) <= 0 || strlen(newname) <= 0) {
                return false;
        }

        if (rename(oldname, newname) < 0) {
                return false;
        } else {
                return true;
        }
}
