#ifndef _STORAGE_H_
#define _STORAGE_H_

#include <vector>
#include "ldczn_base.h"
#include "storage.h"

class Storage {

public:
        Storage();
        ~Storage();
        bool SaveVehicleInfo(CaptureInfo * capture_info, bool bNeedUpload);
        bool GetVehicleInfo(CaptureInfo ** capture_info, int max_list_size);
        bool RemoveVehicleInfo(const char *snapNbr);
        bool GetDiskStatus(void);

private:
        void GetVehicleInfoPath(const char *snapNbr, char *path);
        std::vector < char *>GetSnapNbrLst(const char *fileName, int max_num);
        bool RemoveOldInfo(void);
        bool ReadVehicleInfo(CaptureInfo * capture_info, const char *snap_nbr);

        char m_szSaveHomePath[128];
        bool bDiskFull;
};

#endif
