#ifndef   _MEM_POOL_H_
#define   _MEM_POOL_H_

class Mutex;

class MemPool {

      public:
        MemPool(unsigned int block_size, int block_reserved);
        int Release();

        int Capacity();

        void *Alloc();
        void DeAlloc(void *p);

      private:
        unsigned int _block_size;       //每一个块的大小，即FixedAlloctor能分配的大小. 当m_blockSize不能为1.
        int _first_available_block;     //第一个可用块，一个可用块的大小是由blockSize指定的．
        int _blocks_amount;             //每个MemChunck中，Block的个数.
        int _blocks_available;          //是不是可用的空闲块

        char *_pData;                   //内存块的地址

        bool Is_ptr(void *p);
        bool Init(unsigned int block_size, int block_reserved);

        Mutex _mutex;
};

#endif
