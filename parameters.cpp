#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>
#include "parameters.h"
#include "preference.h"
#include "preference_items.h"
#include "debug.h"
#include "version.h"
#include "util.h"

static const char *kBaseParamsFilename  = "/more/param/param_base.xml";
static const char *kVersionFilename     = "/tmp/version";

Parameters *Parameters::_param = NULL;

Parameters::Parameters()
{
        Debug(INFO, "new member varies and init preference");
        InitParameters();
}

Parameters::~Parameters()
{
        Debug(DEBUG, "preference deinit");
        DeinitParameters();
}

Parameters *Parameters::GetInstance()
{
        if (_param == NULL) {
                static Parameters instance;
                _param = &instance;
        }

        return _param;
}

void Parameters::InitParameters()
{
        preference_init();
        preference_items_init();

        InitTrafficParam();
        InitNetworkParam();
        InitDeviceInfo();
        InitUploadParam();
        InitDeviceStatus();
        InitStorageInfo();
        InitProgramVersion();
        InitCameraParam();
        InitFlashParam();
        InitRedlightZone();
        InitOverlaySetting();
}

void Parameters::DeinitParameters()
{
        preference_deinit();
}

void Parameters::InitDeviceInfo()
{
        char value[32];
        preference_items_get_string(DeviceNumber, _device_info.device_number);
        preference_items_get_string(DeviceSite, _device_info.site);
        preference_items_get_string(DirectionText, _device_info.direction_text);
        preference_items_get_string(RoadCode, _device_info.road_code);
        preference_items_get_string(DepartmentCode, _device_info.department_code);
        preference_items_get_string(Latitue, _device_info.latitude);
        preference_items_get_string(Longtitue, _device_info.longtitude);
        preference_items_get_string(DeviceMode, value);
        _device_info.device_mode = atoi(value);
        preference_items_get_string(DirectionCode, value);
        _device_info.direction_code = atoi(value);
        preference_items_get_string(Lane, value);
        _device_info.lane = atoi(value);
        preference_items_get_string(SnapMode, value);
        _device_info.snap_mode = atoi(value);

        Util::GetBaseParams(kBaseParamsFilename, "manufacture", "serial_number",
                               "LDCZNC120601001", _device_info.serial_number);
}

void Parameters::InitUploadParam()
{
        char value[32];
        preference_items_get_string(UploadServer, _upload_params.upload_server);
        preference_items_get_string(UploadPort, value);
        _upload_params.upload_port = atoi(value);
        preference_items_get_string(NtpServer, _upload_params.ntp_server);
        preference_items_get_string(ClientAddr, _upload_params.client_addr);
}

void Parameters::InitNetworkParam()
{
        Util::GetBaseParams(kBaseParamsFilename, "net", "ethaddr",
                               "f0:00:00:00:00:00", _network_params.mac_addr);
        Util::GetBaseParams(kBaseParamsFilename, "net", "ipaddr",
                               "192.168.0.168", _network_params.device_ip);
        Util::GetBaseParams(kBaseParamsFilename, "net", "netmask",
                               "255.255.0.0", _network_params.netmask);
        Util::GetBaseParams(kBaseParamsFilename, "net", "gatewayip",
                               "192.168.0.1", _network_params.gateway);
        Util::GetBaseParams(kBaseParamsFilename, "net", "nameserver",
                               "0.0.0.0", _network_params.dns_server_main);
        Util::GetBaseParams(kBaseParamsFilename, "net", "nameserver2",
                               "0.0.0.0", _network_params.dns_server_bak);
}

void Parameters::InitTrafficParam()
{
        char value[32];
        preference_items_get_string(StatsPeriod, value);
        _traffic_params.period = atoi(value);
        preference_items_get_string(MotorVehicleLength, value);
        _traffic_params.motor_vehicle_length = atoi(value);
        preference_items_get_string(SmallVehicleLength, value);
        _traffic_params.small_vehicle_length = atoi(value);
        preference_items_get_string(LargeVehicleLength, value);
        _traffic_params.large_vehicle_length = atoi(value);
        preference_items_get_string(LongVehicleLength, value);
        _traffic_params.long_vehicle_length = atoi(value);
}

void Parameters::InitDeviceStatus()
{
        _device_status.status = NORMAL;
        _device_status.flash_status = 0;
        _device_status.total_passing = 0;
        _device_status.violation_passing = 0;
}

void Parameters::InitStorageInfo()
{
        _storage_info.total_capability = 4000;
        _storage_info.remain_capability = 4000;
        _storage_info.unuploaded_info_num = 0;
}

void Parameters::InitOverlaySetting()
{
        char value[32];
        preference_items_get_string(OverlayDeviceNumber, value);
        _overlay_setting.enable_device_number = atoi(value);
        preference_items_get_string(OverlayTimeStamp, value);
        _overlay_setting.enable_timestamp = atoi(value);
        preference_items_get_string(OverlaySite, value);
        _overlay_setting.enable_site = atoi(value);
        preference_items_get_string(OverlayDirection, value);
        _overlay_setting.enable_direction = atoi(value);
        preference_items_get_string(OverlayLane, value);
        _overlay_setting.enable_lane = atoi(value);
        preference_items_get_string(OverlayPlate, value);
        _overlay_setting.enable_plate = atoi(value);
        preference_items_get_string(OverlayPassingType, value);
        _overlay_setting.enable_passing_type = atoi(value);
        preference_items_get_string(OverlayRedlightTime, value);
        _overlay_setting.enable_redlight_time = atoi(value);
        preference_items_get_string(OverlaySpeed, value);
        _overlay_setting.enable_speed = atoi(value);
        preference_items_get_string(OverlaySpeedLimit, value);
        _overlay_setting.enable_speed_limit = atoi(value);
        preference_items_get_string(OverlayOverspeedPercent, value);
        _overlay_setting.enable_overspeed_percent = atoi(value);
        preference_items_get_string(OverlayExtraInfo1, value);
        _overlay_setting.enable_extra_info1 = atoi(value);
        preference_items_get_string(OverlayExtraInfo2, value);
        _overlay_setting.enable_extra_info2 = atoi(value);
        preference_items_get_string(OverlayExtraInfo1Lable, _overlay_setting.extra_overlay_info1);
        preference_items_get_string(OverlayExtraInfo2Lable, _overlay_setting.extra_overlay_info2);

        Debug(DEBUG, "Device number is %d", _overlay_setting.enable_device_number);
}

void Parameters::InitProgramVersion()
{
        memcpy(_program_version.dsp_version, "0", sizeof(_program_version.dsp_version));
        Util::GetVersionParams(kVersionFilename, "lowlevel", "kernel",
                               " ", _program_version.kernel_version);
        Util::GetVersionParams(kVersionFilename, "lowlevel", "appfs",
                               " ", _program_version.filesystem_version);
        Util::GetVersionParams(kVersionFilename, "lowlevel", "fpga",
                               " ", _program_version.fpga_version);
        Util::GetVersionParams(kVersionFilename, "lowlevel", "app",
                               " ", _program_version.app_version);
        Util::GetVersionParams(kVersionFilename, "lowlevel", "build_kernel",
                               " ", _program_version.build_kernel_version);
        Util::GetVersionParams(kVersionFilename, "lowlevel", "build_appfs",
                               " ", _program_version.build_appfs_version);
}

void Parameters::InitCameraParam()
{
        char value[32];
        preference_items_get_string(DefaultGain, value);
        _camera_params.default_gain = atoi(value);
        preference_items_get_string(MinGain, value);
        _camera_params.min_gain = atoi(value);
        preference_items_get_string(MaxGain, value);
        _camera_params.max_gain = atoi(value);

        preference_items_get_string(DefaultExposure, value);
        _camera_params.default_exposure = atoi(value);
        preference_items_get_string(MinExposure, value);
        _camera_params.min_exposure = atoi(value);
        preference_items_get_string(MaxExposure, value);
        _camera_params.max_exposure = atoi(value);

        preference_items_get_string(VideoTargetGray, value);
        _camera_params.video_target_gray = atoi(value);
        preference_items_get_string(TriggerTargetGray, value);
        _camera_params.trigger_target_gray = atoi(value);
        preference_items_get_string(RGain, value);
        _camera_params.red_gain = atoi(value);
        preference_items_get_string(BGain, value);
        _camera_params.blue_gain = atoi(value);

        preference_items_get_string(AEZone, value);
        _camera_params.ae_zone = (unsigned int)atoi(value);
        preference_items_get_string(AEWMode, value);
        _camera_params.aew_mode = atoi(value);

        preference_items_get_string(QValue, value);
        _camera_params.jpeg_quality_value = atoi(value);
        
        preference_items_get_string(AdSampleBits, value);
        _camera_params.ad_sample_bits = atoi(value);
        preference_items_get_string(SharpenFactor, value);
        _camera_params.sharpen_factor = atoi(value);
}

void Parameters::InitFlashParam()
{
        char value[32];
        preference_items_get_string(FlashMode, value);
        _flash_params.flash_mode = atoi(value);
        preference_items_get_string(FlashDelay, value);
        _flash_params.flash_delay = atoi(value);
        preference_items_get_string(LedMode, value);
        _flash_params.led_mode = atoi(value);
        preference_items_get_string(LedMutiple, value);
        _flash_params.led_mutiple = atoi(value);
        preference_items_get_string(ContinousLight, value);
        _flash_params.continuous_light = atoi(value);
        preference_items_get_string(RedlightDelay, value);
        _flash_params.redlight_delay = atoi(value);
        preference_items_get_string(RedlightSyncMode, value);
        _flash_params.redlight_sync_mode = atoi(value);
        preference_items_get_string(RedlightEfficient, value);
        _flash_params.redlight_efficient = atoi(value);
        preference_items_get_string(LightOnThreshold, value);
        _flash_params.flash_on_threshold = atoi(value);
}

void Parameters::InitRedlightZone()
{
        char value[32];
        preference_items_get_string(RedlightNumber, value);
        _redlight_zone.rect_num = atoi(value);
        preference_items_get_string(Redlight1Left, value);
        _redlight_zone.rect[0].left = atoi(value);
        preference_items_get_string(Redlight1Top, value);
        _redlight_zone.rect[0].top = atoi(value);
        preference_items_get_string(Redlight1Width, value);
        _redlight_zone.rect[0].width = atoi(value);
        preference_items_get_string(Redlight1Height, value);
        _redlight_zone.rect[0].height = atoi(value);
        preference_items_get_string(Redlight2Left, value);
        _redlight_zone.rect[1].left = atoi(value);
        preference_items_get_string(Redlight2Top, value);
        _redlight_zone.rect[1].top = atoi(value);
        preference_items_get_string(Redlight2Width, value);
        _redlight_zone.rect[1].width = atoi(value);
        preference_items_get_string(Redlight2Height, value);
        _redlight_zone.rect[1].height = atoi(value);
        preference_items_get_string(Redlight3Left, value);
        _redlight_zone.rect[2].left = atoi(value);
        preference_items_get_string(Redlight3Top, value);
        _redlight_zone.rect[2].top = atoi(value);
        preference_items_get_string(Redlight3Width, value);
        _redlight_zone.rect[2].width = atoi(value);
        preference_items_get_string(Redlight3Height, value);
        _redlight_zone.rect[2].height = atoi(value);
        preference_items_get_string(Redlight4Left, value);
        _redlight_zone.rect[3].left = atoi(value);
        preference_items_get_string(Redlight4Top, value);
        _redlight_zone.rect[3].top = atoi(value);
        preference_items_get_string(Redlight4Width, value);
        _redlight_zone.rect[3].width = atoi(value);
        preference_items_get_string(Redlight4Height, value);
        _redlight_zone.rect[3].height = atoi(value);
        preference_items_get_string(Redlight5Left, value);
        _redlight_zone.rect[4].left = atoi(value);
        preference_items_get_string(Redlight5Top, value);
        _redlight_zone.rect[4].top = atoi(value);
        preference_items_get_string(Redlight5Width, value);
        _redlight_zone.rect[4].width = atoi(value);
        preference_items_get_string(Redlight5Height, value);
        _redlight_zone.rect[4].height = atoi(value);
        preference_items_get_string(Redlight6Left, value);
        _redlight_zone.rect[5].left = atoi(value);
        preference_items_get_string(Redlight6Top, value);
        _redlight_zone.rect[5].top = atoi(value);
        preference_items_get_string(Redlight6Width, value);
        _redlight_zone.rect[5].width = atoi(value);
        preference_items_get_string(Redlight6Height, value);
        _redlight_zone.rect[5].height = atoi(value);
}

RedLightZone Parameters::GetRedlightZone()
{
        return _redlight_zone;
}

void Parameters::SetRedlightZone(RedLightZone * zone)
{
        _redlight_zone = *zone;
        preference_items_set(RedlightNumber, &_redlight_zone.rect_num);

        for (int index = 0; index < _redlight_zone.rect_num; index++) {
                preference_items_set(RedlightNumber + index * 4 + 1,
                                     &_redlight_zone.rect[index].left);
                preference_items_set(RedlightNumber + index * 4 + 2,
                                     &_redlight_zone.rect[index].top);
                preference_items_set(RedlightNumber + index * 4 + 3,
                                     &_redlight_zone.rect[index].width);
                preference_items_set(RedlightNumber + index * 4 + 4,
                                     &_redlight_zone.rect[index].height);
        }
}

CameraParam Parameters::GetCameraParam()
{
        return _camera_params;
}

void Parameters::SetCameraParam(CameraParam * param)
{
        _camera_params = *param;

        preference_items_set(DefaultGain, &_camera_params.default_gain);
        preference_items_set(MinGain, &_camera_params.min_gain);
        preference_items_set(MaxGain, &_camera_params.max_gain);

        preference_items_set(DefaultExposure, &_camera_params.default_exposure);
        preference_items_set(MinExposure, &_camera_params.min_exposure);
        preference_items_set(MaxExposure, &_camera_params.max_exposure);

        preference_items_set(VideoTargetGray, &_camera_params.video_target_gray);
        preference_items_set(TriggerTargetGray, &_camera_params.trigger_target_gray);
        preference_items_set(RGain, &_camera_params.red_gain);
        preference_items_set(BGain, &_camera_params.blue_gain);

        preference_items_set(AEZone, &_camera_params.ae_zone);
        preference_items_set(AEWMode, &_camera_params.aew_mode);

        preference_items_set(QValue, &_camera_params.jpeg_quality_value);
        preference_items_set(AdSampleBits, &_camera_params.ad_sample_bits);
        preference_items_set(SharpenFactor, &_camera_params.sharpen_factor);
}

NetworkParam Parameters::GetNetworkParam()
{
        return _network_params;
}

void Parameters::SetNetworkParam(NetworkParam * param)
{
        _network_params = *param;
        Util::SetBaseParams(kBaseParamsFilename, "net", "ipaddr", _network_params.device_ip);
        Util::SetBaseParams(kBaseParamsFilename, "net", "netmask", _network_params.netmask);
        Util::SetBaseParams(kBaseParamsFilename, "net", "gatewayip", _network_params.gateway);
        Util::SetBaseParams(kBaseParamsFilename, "net", "nameserver",
                               _network_params.dns_server_main);
        Util::SetBaseParams(kBaseParamsFilename, "net", "nameserver2",
                               _network_params.dns_server_bak);
}

UploadParam Parameters::GetUploadParam()
{
        return _upload_params;
}

void Parameters::SetUploadParam(UploadParam * param)
{
        _upload_params = *param;

        preference_items_set_string(UploadServer, _upload_params.upload_server);
        preference_items_set_string(NtpServer, _upload_params.ntp_server);
        preference_items_set_string(ClientAddr, _upload_params.client_addr);
}

DeviceStatus Parameters::GetDeviceStatus()
{
        return _device_status;
}

void Parameters::SetDeviceStatus(DeviceStatus * status)
{
        _device_status = *status;
}

void Parameters::ChangeDeviceStatus(unsigned int status)
{
        _device_status.status = status;
}

TrafficParam Parameters::GetTrafficParam()
{
        return _traffic_params;
}

void Parameters::SetTrafficParam(TrafficParam * param)
{
        _traffic_params = *param;
}

FlashParam Parameters::GetFlashParam()
{
        return _flash_params;
}

void Parameters::SetFlashParam(FlashParam * param)
{
        _flash_params = *param;
        preference_items_set(FlashMode, &_flash_params.flash_mode);
        preference_items_set(FlashDelay, &_flash_params.flash_delay);
        preference_items_set(RedlightDelay, &_flash_params.redlight_delay);
        preference_items_set(RedlightSyncMode, &_flash_params.redlight_sync_mode);
        preference_items_set(RedlightEfficient, &_flash_params.redlight_efficient);
        preference_items_set(LedMode, &_flash_params.led_mode);
        preference_items_set(LedMutiple, &_flash_params.led_mutiple);
        preference_items_set(ContinousLight, &_flash_params.continuous_light);
        preference_items_set(LightOnThreshold, &_flash_params.flash_on_threshold);
}

DeviceInfo Parameters::GetDeviceInfo()
{
        return _device_info;
}

void Parameters::SetDeviceInfo(DeviceInfo *info)
{
        _device_info = *info;
        preference_items_set_string(DeviceSite, _device_info.site);
        preference_items_set_string(DeviceNumber, _device_info.device_number);
        preference_items_set_string(DirectionText, _device_info.direction_text);
        preference_items_set_string(RoadCode, _device_info.road_code);
        preference_items_set_string(DepartmentCode, _device_info.department_code);
        preference_items_set_string(Latitue, _device_info.latitude);
        preference_items_set_string(Longtitue, _device_info.longtitude);
        preference_items_set(DeviceMode, &_device_info.device_mode);
        preference_items_set(Lane, &_device_info.lane);
        preference_items_set(DirectionCode, &_device_info.direction_code);
        preference_items_set(SnapMode, &_device_info.snap_mode);
        
        Util::SetBaseParams(kBaseParamsFilename, "manufacture", "serial_number",
                                _device_info.serial_number);
}

StorageInfo Parameters::GetStorageInfo()
{
        return _storage_info;
}

ProgramVersion Parameters::GetProgramVersion()
{
        return _program_version;
}

OverlaySetting Parameters::GetOverlaySetting()
{
        return _overlay_setting;
}

void Parameters::SetOverlaySetting(OverlaySetting *setting)
{
        _overlay_setting = *setting;

        preference_items_set(OverlayDeviceNumber, &_overlay_setting.enable_device_number);
        preference_items_set(OverlayTimeStamp, &_overlay_setting.enable_timestamp);
        preference_items_set(OverlaySite, &_overlay_setting.enable_site);
        preference_items_set(OverlayDirection, &_overlay_setting.enable_direction);
        preference_items_set(OverlayLane, &_overlay_setting.enable_lane);
        preference_items_set(OverlayPlate, &_overlay_setting.enable_plate);
        preference_items_set(OverlayPassingType, &_overlay_setting.enable_passing_type);
        preference_items_set(OverlayRedlightTime, &_overlay_setting.enable_redlight_time);
        preference_items_set(OverlaySpeed, &_overlay_setting.enable_speed);
        preference_items_set(OverlaySpeedLimit, &_overlay_setting.enable_speed_limit);
        preference_items_set(OverlayOverspeedPercent, &_overlay_setting.enable_overspeed_percent);
        preference_items_set(OverlayExtraInfo1, &_overlay_setting.enable_extra_info1);
        preference_items_set(OverlayExtraInfo2, &_overlay_setting.enable_extra_info2);
        preference_items_set_string(OverlayExtraInfo1Lable, _overlay_setting.extra_overlay_info1);
        preference_items_set_string(OverlayExtraInfo2Lable, _overlay_setting.extra_overlay_info2);
}
