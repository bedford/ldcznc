#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include "gpio.h"
#include "debug.h"

static const char *kFpgaCtlDevice       = "/dev/cfpga";
static const char *kPowerCtlDevice      = "/dev/reset_power";

static const unsigned int kMannualSnap  = 0x4F01;
static const unsigned int kResetFpga    = 0x4F03;
static const unsigned int kResetPower   = 0x4E05;

int GpioCtl::MannualSnap()
{
        int fpga_ctl_handle = open(kFpgaCtlDevice, O_RDWR);

        if (fpga_ctl_handle < 0) {
                Debug(ERROR, "open dm368 io to trig fpga failed: %s, errno: %d", strerror(errno), errno);
                return -1;
        }

        if (ioctl(fpga_ctl_handle, kMannualSnap, NULL) < 0) {
                Debug(ERROR, "set trig fpga failed: %s, errno: %d", strerror(errno), errno);
                close(fpga_ctl_handle);
                return -1;
        }

        close(fpga_ctl_handle);
        return 0;
}

int GpioCtl::ResetFpga()
{
        Debug(INFO, "Reset Fpga");
        int fpga_ctl_handle = open(kFpgaCtlDevice, O_RDWR);

        if (fpga_ctl_handle < 0) {
                Debug(ERROR, "open dm368 io to trig fpga failed: %s, errno: %d", strerror(errno), errno);
                return -1;
        }

        if (ioctl(fpga_ctl_handle, kResetFpga, NULL) < 0) {
                Debug(ERROR, "set trig fpga failed: %s, errno: %d", strerror(errno), errno);
                close(fpga_ctl_handle);
                return -1;
        }

        close(fpga_ctl_handle);
        return 0;
}

int GpioCtl::ResetPower()
{
        Debug(WARN, "Reset Power");
        int power_ctl_handle = open(kPowerCtlDevice, O_RDWR);

        if (power_ctl_handle < 0) {
                Debug(ERROR, "open dm368 io reset power failed: %s, errno: %d", strerror(errno), errno);
                return -1;
        }

        if (ioctl(power_ctl_handle, kResetPower, NULL) < 0) {
                Debug(ERROR, "set trig fpga failed: %s, errno: %d", strerror(errno), errno);
                close(power_ctl_handle);
                return -1;
        }

        close(power_ctl_handle);
        return 0;
}
