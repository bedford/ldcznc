#ifndef _LDCZN_BASE_H_
#define _LDCZN_BASE_H_

#include "ldczn_protocol.h"

enum DeviceMode {
        Sitecap                 = 0,    /* 卡警模式             */
        RedLight                = 1,    /* 闯红灯模式           */
        RedLight_Reverse        = 2,    /* 闯红灯+逆行模式      */
        LimitedPassViolation    = 3,    /* 限行路段违章通行     */
        VideoMonitor            = 4,    /* 视频监控模式         */
};

enum {
        FlashOff,
        FlashOn,
        AutoFlash
};

/**
 * @brief	图像类型
 */
enum {
        IMAGE_TYPE_BAYER_BG,
        IMAGE_TYPE_BAYER_GB,
        IMAGE_TYPE_BAYER_RG,
        IMAGE_TYPE_BAYER_GR,
        IMAGE_TYPE_JPEG,
        IMAGE_TYPE_BMP,
        IMAGE_TYPE_RGB888,
        IMAGE_TYPE_RGB565,
        IMAGE_TYPE_YUV420,
        IMAGE_TYPE_YUYV,
        IMAGE_TYPE_UYVY
};

/**
 * @brief	设备信息
 */
typedef struct device_param {
        DeviceInfo *device_info;        /* 设备运行参数         */
        NetworkParam *network_params;   /* 网络参数             */
        UploadParam *upload_params;     /* 上传参数             */
        TrafficParam *traffic_params;   /* 车流量统计参数       */
} DeviceParam;

/**
 * @brief	车流量信息
 */
typedef struct {
        char start_time[MIN_LENGTH];    /* 统计开始时间                 */
        char end_time[MIN_LENGTH];      /* 统计结束时间                 */
        int period;                     /* 统计周期                     */
        int lane;                       /* 通行车道                     */
        int total_num;                  /* 统计时间内车辆总数           */
        int avg_len;                    /* 平均车长(单位:分米)          */
        int highspeed_num;              /* 超速车辆数                   */
        int lowspeed_num;               /* 低速车辆数                   */
        int bigcar_num;                 /* 大车数量                     */
        int midcar_num;                 /* 中型车数量                   */
        int smallcar_num;               /* 小车数量                     */
        int motorcar_num;               /* 摩托车数量                   */
        int hugecar_num;                /* 超长车数量                   */
        float avg_speed;                /* 平均车速 公里/小时           */
        float time_usage;               /* 时间占用率                   */
        float space_usage;              /* 空间占用率                   */
        float carnum_per_meter;         /* 车头间距   米/辆             */
        float carnum_per_second;        /* 车头时距  秒/辆              */
        float carnum_per_kilometer;     /* 每公里的车辆数,车辆密度      */
} TrafficStatus;

/**
 * @var		typedef struct _vehicle_info vehicle_info
 *
 * @brief	抓拍车辆信息
 *
 * 		抓拍方式: 0 - 不区分车头车尾; 1 - 车头; 2 - 车尾
 *
 */
typedef struct {
        char snap_nbr[32];              /* 抓拍编号             */
        char plate_nbr[32];             /* 车牌号码             */
        int plate_type;                 /* 车牌类型             */
        int plate_color;                /* 车牌颜色             */
        int vehicle_type;               /* 车辆类型             */
        int lane;                       /* 车道                 */
        int drive_mode;                 /* 通行方式             */
        int vehicle_speed;              /* 目标车速             */
        int vehicle_length;             /* 目标车长             */
        int base_speed;                 /* 本车速度             */
        int snap_mode;                  /* 抓拍方式(移动、固定) */
        int snap_verify_ID;             /* 抓拍校验码(图片合成) */
        struct timeval passing_time;    /* 通过时刻             */
        int redlight_time;              /* 红灯开始时间(单位:秒)*/
        int offence_type;               /* 违法行为             */
        int over_speed_percent;         /* 超速百分比           */
        void *reserver;                 /* 保留字段             */
} TriggerInfo;

/**
 * @brief	抓拍图像信息
 */
struct _vehicle_image {
        unsigned int image_type;        /* 图像类型     */
        unsigned int image_width;       /* 有效像素宽度 */
        unsigned int image_height;      /* 有效像素高度 */
        unsigned int image_stride;      /* 图像宽度     */
        unsigned int image_length;      /* 数据长度     */
        unsigned int image_gray;        /* 图像灰度     */
        struct timeval capture_time;    /* 抓拍时间     */
        char *image_buffer;             /* 数据缓存地址 */
};

/**
 * @brief	图像信息
 */
typedef struct {
        struct _vehicle_image vehicle_image[3];
        unsigned int frame;     /* 图像帧数 */
} VehicleImageInfo;

/**
 * @brief	抓拍信息
 */
typedef struct capture_info {
        TriggerInfo *pTrigger_info;             /* 触发信息     */
        VehicleImageInfo *pVehicle_image_info;  /* 图像信息     */
        DeviceInfo *pDevice_info;               /* 设备信息     */
        char bSaved;                            /* 是否已经保存 */
} CaptureInfo;

#define MEGA_SIZE		(1024 * 1024)
#define CAPTUREINFO_SIZE	(sizeof(CaptureInfo))
#define TRIGGERINFO_SIZE	(sizeof(TriggerInfo))
#define VEHICLEIMAGEINFO_SIZE	(sizeof(VehicleImageInfo))
#define DEVICEINFO_SIZE		(sizeof(DeviceInfo))
#define FIRSET_FRAMT_OFFSET 	(CAPTUREINFO_SIZE + \
				 TRIGGERINFO_SIZE + \
				 VEHICLEIMAGEINFO_SIZE + \
				 DEVICEINFO_SIZE)
#define SECOND_FRAMT_OFFSET	(FIRSET_FRAMT_OFFSET + MEGA_SIZE)
#define THIRD_FRAMT_OFFSET	(SECOND_FRAMT_OFFSET + MEGA_SIZE)

/**
 * @brief	图像处理参数
 */
typedef struct picture_params {
        unsigned char Jpeg_quality;     /* jpeg压缩比   */
        unsigned char brightness;       /* 亮度         */
        unsigned char contrast;         /* 对比度       */
        unsigned char saturation;       /* 饱和度       */
        unsigned char hue;              /* 色调         */
} PictureParams;

#endif
