#ifndef _DEVICE_PARAMS_H_
#define _DEVICE_PARAMS_H_

#include "ldczn_base.h"

enum _DeviceStatus {
        NORMAL                  = 0x00000000,
        DETECTOR_FAULT          = 0x00000001,
        SIGNALMODULE_FAULT      = 0x00000002,
        FLASHLIGHT_FAULT        = 0x00000004,
        RADAR_FAULT             = 0x00000008,
        REBOOT_SYSTEM           = 0x00000010,
        SIGNAL_MODULE_TESTING   = 0x00000020,
        NO_FLASH_DISK           = 0x00000040
};

class Parameters {
public:
        static Parameters *GetInstance();

        CameraParam GetCameraParam();
        void SetCameraParam(CameraParam *param);

        NetworkParam GetNetworkParam();
        void SetNetworkParam(NetworkParam *param);

        UploadParam GetUploadParam();
        void SetUploadParam(UploadParam *param);

        DeviceStatus GetDeviceStatus();
        void SetDeviceStatus(DeviceStatus *status);
        void ChangeDeviceStatus(unsigned int status);

        TrafficParam GetTrafficParam();
        void SetTrafficParam(TrafficParam *param);

        FlashParam GetFlashParam();
        void SetFlashParam(FlashParam *param);

        DeviceInfo GetDeviceInfo();
        void SetDeviceInfo(DeviceInfo *info);

        StorageInfo GetStorageInfo();
        void SetStorageInfo(StorageInfo *info);

        ProgramVersion GetProgramVersion();

        RedLightZone GetRedlightZone();
        void SetRedlightZone(RedLightZone *zone);

        OverlaySetting GetOverlaySetting();
        void SetOverlaySetting(OverlaySetting *setting);

private:
         Parameters();
        ~Parameters();

        void InitParameters();
        void DeinitParameters();

        void InitDeviceStatus();
        void InitUploadParam();
        void InitDeviceInfo();
        void InitStorageInfo();
        void InitProgramVersion();
        void InitCameraParam();
        void InitNetworkParam();
        void InitTrafficParam();
        void InitFlashParam();
        void InitRedlightZone();
        void InitOverlaySetting();

        UploadParam _upload_params;             /* 网络上传参数         */
        DeviceStatus _device_status;            /* 设备状态             */
        DeviceInfo _device_info;                /* 设备参数             */
        StorageInfo _storage_info;              /* 设备存储状况         */
        CameraParam _camera_params;             /* 前端参数             */
        NetworkParam _network_params;           /* 网络参数             */
        ProgramVersion _program_version;        /* 程序版本             */
        TrafficParam _traffic_params;           /* 车流量统计参数       */
        FlashParam _flash_params;               /* 闪光灯参数           */
        RedLightZone _redlight_zone;
        OverlaySetting _overlay_setting;

        static Parameters *_param;
};

#endif
