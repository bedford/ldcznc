/**
 * @file sensor.h
 * @brief	
 * @author hrh <huangrh@landuntec.com>
 * @version 1.0.0
 * @date 2011-11-28
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef _SENSOR_H_
#define _SENSOR_H_

/**
 * @brief	前端sensor控制类	
 */
class Sensor {
      public:
        int SetSensorExposure(unsigned int exposure);
        int SetSensorMaxExp(unsigned int exposure);
        int SetSensorMinExp(unsigned int exposure);

        int SetSensorGain(unsigned int gain);
        int SetSensorMaxGain(unsigned int gain);
        int SetSensorMinGain(unsigned int gain);

        int SetSensorFlashOn();
        int SetSensorFlashOff();

        int SetSensorLedOn();
        int SetSensorLedOff();

        int SetSensorConstantLightOn();
        int SetSensorConstantLightOff();

        int SetSensorVideo();
        int SetSensorCapture();

        int SetSensorRgain(unsigned int gain);
        int SetSensorBgain(unsigned int gain);

        int SetSensorAEAuto();
        int SetSensorAEManual();
        int SetSensorAEMethod(unsigned int method);
        int SetSensorAEZone(unsigned int zone);
        int SetSensorAEAdjustNumber(unsigned int number);

        int SetSensorFlashDelay(unsigned int time);
        int SetSensorLEDMultiple(unsigned int time);
        int SetSensorRedLightEfficient(unsigned int time);
        int SetSensorRedLightDelay(unsigned int time);

        int SetSensorVideoTargetGray(unsigned int gray);
        int SetSensorCaptureTargetGray(unsigned int gray);

        int SetSensorContinousCapNum(unsigned int num);
        int SetSensorContinousCapDuration(unsigned int time);

        int SetSensorSyncOn();
        int SetSensorSyncOff();
        int SetSensorRedLightSyncSource(unsigned int mode);

        int SetSensorEnablePreProcess();
        int SetSensorDisablePreProcess();

        int SetSensorSharpenFactor(unsigned int factor);
        int SetSensorADSampleBits(unsigned int bit);

        bool SetSensorHandle(int handle);
        bool GetVersion();
        int GetCurrentAvgLuma();

        static Sensor *GetInstance();

      private:

        Sensor();
        ~Sensor();

        int SetFPGAParam(unsigned char addr, unsigned char val);
        int SetExposure(unsigned int exposure, unsigned char low_addr, unsigned char high_addr);
        int SetGain(unsigned int gain, unsigned char low_addr, unsigned char high_addr);
        int SetSensorUchar(unsigned char addr, unsigned int setting);

        void InitRegisterAttribute();
        bool CheckRegisterWriteable(unsigned char addr);
        bool CheckRegisterReadable(unsigned char addr);

        int _sensorHandle;

        static Sensor *_Sensor;
};

#endif
