#ifndef __DeviceControlCBI_h__
#define __DeviceControlCBI_h__

#include <DeviceControlCB.h>

namespace LDTSSInterface
{

class DeviceControlCBI : virtual public DeviceControlCB
{
public:

    virtual LDTSSInterface::SurveyDeviceAttribute GetAttribute(const Ice::Current&);
    virtual bool SetParam(const std::string&, const std::string&, const Ice::Current&);
    virtual std::string GetParam(const std::string&, const Ice::Current&);
    virtual LDTSSInterface::StringStringDict GetAllParams(const Ice::Current&);
    virtual bool Reset(const Ice::Current&);
    virtual LDTSSInterface::StringStringDict GetCameraAllParams(const std::string&,
                                        const Ice::Current&);
    virtual bool SetCameraAllParams(const std::string&,
                                        const LDTSSInterface::StringStringDict&,
                                        const Ice::Current&);
    virtual LDTSSInterface::DeviceRuntimeParams GetRuntimeParams(const Ice::Current&);
    virtual bool SetRuntimeParams(const LDTSSInterface::DeviceRuntimeParams&,
                                        const Ice::Current&);
    virtual LDTSSInterface::NetworkParams GetNetworkParams(const Ice::Current&);
    virtual bool SetNetworkParams(const LDTSSInterface::NetworkParams&, const Ice::Current&);
    virtual Ice::ByteSeq ReadConfigFile(Ice::Int, const Ice::Current&);
    virtual bool WriteConfigFile(Ice::Int, const Ice::ByteSeq&, const Ice::Current&);
    virtual bool ExecuteCommand(const std::string&, const std::string&, const Ice::Current&);
    virtual bool Reboot(const Ice::Current&);
    virtual bool CalibrateTime(const std::string&, const Ice::Current&);
    virtual LDTSSInterface::SurveyDeviceStatus GetDeviceStatus(const Ice::Current&);
    virtual Ice::ByteSeq GetLog(Ice::Int, const Ice::Current&);
    virtual Ice::Int Diagnostics(const Ice::Current&);
    virtual bool OneShot(const std::string&, LDTSSInterface::VehicleImageDescriptionSeq&,
                                        const Ice::Current&);
    virtual Ice::ByteSeq ReadImage(const std::string&,
                                        const LDTSSInterface::VehicleImageIdentity&,
                                        Ice::Int, Ice::Int, const Ice::Current&);
    virtual Ice::ByteSeq DownloadRecording(const std::string&, Ice::Int,
                                        Ice::Int, const Ice::Current&);
    virtual LDTSSInterface::PassingVehicleSeq QueryVehicleByTime(const std::string&,
                                        const std::string&, Ice::Int, Ice::Int,
                                        const Ice::Current&);
    virtual std::string GetId(const Ice::Current&);
    virtual std::string GetCaCode(const Ice::Current&);
    virtual bool Certificate(const Ice::ByteSeq&, const Ice::Current&);

    virtual bool InspectVehicle(const LDTSSInterface::InspectedVehicle&, bool override,
                                        const Ice::Current&);
    virtual bool CancelVehicle(const std::string&, const std::string&,
                                        const std::string&,
                                        const std::string&,
                                        const Ice::Current&);
    virtual LDTSSInterface::InspectedVehicleSeq QueryVehicle(const std::string&,
                                        const std::string&,
                                        const Ice::Current&);
    virtual bool BatchInspectedVehicle(const LDTSSInterface::InspectedVehicleSeq&, const Ice::Current&);
    virtual LDTSSInterface::InspectedVehicleSeq ListVehicle(const std::string&,
                                        Ice::Int, Ice::Int, const Ice::Current&);
    virtual Ice::Int CountVehicle(const std::string&, const Ice::Current&);
    virtual bool AddVagueData(const Ice::StringSeq&, const Ice::Current&);
    virtual Ice::StringSeq QueryVagueData(const Ice::Current&);
};

}

#endif
