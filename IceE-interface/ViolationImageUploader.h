// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `ViolationImageUploader.ice'

#ifndef ____GeneratedCpp_ViolationImageUploader_h__
#define ____GeneratedCpp_ViolationImageUploader_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#ifdef ICEE_HAS_AMI
#   include <IceE/OutgoingAsync.h>
#endif
#include <IceE/UserExceptionFactory.h>
#include <IceE/FactoryTable.h>
#include <Base.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace LDTSSInterface
{

class ViolationImageUploader;

}

}

namespace LDTSSInterface
{

class ViolationImageUploader;
bool operator==(const ViolationImageUploader&, const ViolationImageUploader&);
bool operator<(const ViolationImageUploader&, const ViolationImageUploader&);

}

namespace IceInternal
{

::Ice::Object* upCast(::LDTSSInterface::ViolationImageUploader*);
::IceProxy::Ice::Object* upCast(::IceProxy::LDTSSInterface::ViolationImageUploader*);

}

namespace LDTSSInterface
{

typedef ::IceInternal::Handle< ::LDTSSInterface::ViolationImageUploader> ViolationImageUploaderPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::LDTSSInterface::ViolationImageUploader> ViolationImageUploaderPrx;

void __read(::IceInternal::BasicStream*, ViolationImageUploaderPrx&);
void __patch__ViolationImageUploaderPtr(void*, ::Ice::ObjectPtr&);

}

namespace LDTSSInterface
{

}

namespace LDTSSInterface
{

class ViolationImageUploader : virtual public ::Ice::Object
{
public:

    typedef ViolationImageUploaderPrx ProxyType;
    typedef ViolationImageUploaderPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual bool UploadImageFragment(const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___UploadImageFragment(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle&, bool, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___UploadViolationVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::FragmentInfoSeq GetFragmentInfo(const ::LDTSSInterface::VehicleImageIdentity&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetFragmentInfo(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace LDTSSInterface
{
#ifdef ICEE_HAS_AMI

class AMI_ViolationImageUploader_UploadImageFragment : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::ViolationImageUploaderPrx&, const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_ViolationImageUploader_UploadImageFragment> AMI_ViolationImageUploader_UploadImageFragmentPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_ViolationImageUploader_UploadViolationVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::ViolationImageUploaderPrx&, const ::LDTSSInterface::ViolationVehicle&, bool, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_ViolationImageUploader_UploadViolationVehicle> AMI_ViolationImageUploader_UploadViolationVehiclePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_ViolationImageUploader_GetFragmentInfo : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::FragmentInfoSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::ViolationImageUploaderPrx&, const ::LDTSSInterface::VehicleImageIdentity&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_ViolationImageUploader_GetFragmentInfo> AMI_ViolationImageUploader_GetFragmentInfoPtr;
#endif

}

namespace IceProxy
{

namespace LDTSSInterface
{

class ViolationImageUploader : virtual public ::IceProxy::Ice::Object
{
public:

    bool UploadImageFragment(const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>& imageData)
    {
        return UploadImageFragment(identity, offset, imageData, 0);
    }
    bool UploadImageFragment(const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>& imageData, const ::Ice::Context& __ctx)
    {
        return UploadImageFragment(identity, offset, imageData, &__ctx);
    }
    
private:

    bool UploadImageFragment(const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool UploadImageFragment_async(const ::LDTSSInterface::AMI_ViolationImageUploader_UploadImageFragmentPtr&, const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>&);
    bool UploadImageFragment_async(const ::LDTSSInterface::AMI_ViolationImageUploader_UploadImageFragmentPtr&, const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>&, const ::Ice::Context&);
    #endif

    bool UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle& violation, bool historyData)
    {
        return UploadViolationVehicle(violation, historyData, 0);
    }
    bool UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle& violation, bool historyData, const ::Ice::Context& __ctx)
    {
        return UploadViolationVehicle(violation, historyData, &__ctx);
    }
    
private:

    bool UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle&, bool, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool UploadViolationVehicle_async(const ::LDTSSInterface::AMI_ViolationImageUploader_UploadViolationVehiclePtr&, const ::LDTSSInterface::ViolationVehicle&, bool);
    bool UploadViolationVehicle_async(const ::LDTSSInterface::AMI_ViolationImageUploader_UploadViolationVehiclePtr&, const ::LDTSSInterface::ViolationVehicle&, bool, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::FragmentInfoSeq GetFragmentInfo(const ::LDTSSInterface::VehicleImageIdentity& identity)
    {
        return GetFragmentInfo(identity, 0);
    }
    ::LDTSSInterface::FragmentInfoSeq GetFragmentInfo(const ::LDTSSInterface::VehicleImageIdentity& identity, const ::Ice::Context& __ctx)
    {
        return GetFragmentInfo(identity, &__ctx);
    }
    
private:

    ::LDTSSInterface::FragmentInfoSeq GetFragmentInfo(const ::LDTSSInterface::VehicleImageIdentity&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetFragmentInfo_async(const ::LDTSSInterface::AMI_ViolationImageUploader_GetFragmentInfoPtr&, const ::LDTSSInterface::VehicleImageIdentity&);
    bool GetFragmentInfo_async(const ::LDTSSInterface::AMI_ViolationImageUploader_GetFragmentInfoPtr&, const ::LDTSSInterface::VehicleImageIdentity&, const ::Ice::Context&);
    #endif
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_secure(bool __secure) const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_twoway() const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_oneway() const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_batchOneway() const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_datagram() const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_batchDatagram() const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<ViolationImageUploader> ice_timeout(int __timeout) const
    {
        return dynamic_cast<ViolationImageUploader*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
