#ifndef _OFFENCE_H_
#define _OFFENCE_H_


typedef struct _offenceCode
{
        unsigned int    offenceType;
        unsigned int    lowerBound;
        unsigned int    upperBound;
        unsigned int    overSpeedPercent;
        char            violationCode[32];
        char            violationDescription[256];

} offenceCode;


bool set_offence_code(offenceCode *code, int length, const char *filename);

bool read_offence_code(offenceCode *code, int *length, const char *filename);


#endif

