// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `DeviceControlCB.ice'

#include <DeviceControlCB.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/LocalException.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

::Ice::Object* IceInternal::upCast(::LDTSSInterface::DeviceControlCB* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::LDTSSInterface::DeviceControlCB* p) { return p; }

void
LDTSSInterface::__read(::IceInternal::BasicStream* __is, ::LDTSSInterface::DeviceControlCBPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::LDTSSInterface::DeviceControlCB;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __LDTSSInterface__DeviceControlCB_ids[4] =
{
    "::Ice::Object",
    "::LDTSSInterface::DeviceControl",
    "::LDTSSInterface::DeviceControlCB",
    "::LDTSSInterface::VehicleInspection"
};

bool
LDTSSInterface::DeviceControlCB::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__LDTSSInterface__DeviceControlCB_ids, __LDTSSInterface__DeviceControlCB_ids + 4, _s);
}

::std::vector< ::std::string>
LDTSSInterface::DeviceControlCB::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__LDTSSInterface__DeviceControlCB_ids[0], &__LDTSSInterface__DeviceControlCB_ids[4]);
}

const ::std::string&
LDTSSInterface::DeviceControlCB::ice_id(const ::Ice::Current&) const
{
    return __LDTSSInterface__DeviceControlCB_ids[2];
}

const ::std::string&
LDTSSInterface::DeviceControlCB::ice_staticId()
{
    return __LDTSSInterface__DeviceControlCB_ids[2];
}

#ifndef ICEE_PURE_CLIENT
static ::std::string __LDTSSInterface__DeviceControlCB_all[] =
{
    "AddVagueData",
    "BatchInspectedVehicle",
    "CalibrateTime",
    "CancelVehicle",
    "Certificate",
    "CountVehicle",
    "Diagnostics",
    "DownloadRecording",
    "ExecuteCommand",
    "GetAllParams",
    "GetAttribute",
    "GetCaCode",
    "GetCameraAllParams",
    "GetDeviceStatus",
    "GetId",
    "GetLog",
    "GetNetworkParams",
    "GetParam",
    "GetRuntimeParams",
    "InspectVehicle",
    "ListVehicle",
    "OneShot",
    "QueryVagueData",
    "QueryVehicle",
    "QueryVehicleByTime",
    "ReadConfigFile",
    "ReadImage",
    "Reboot",
    "Reset",
    "SetCameraAllParams",
    "SetNetworkParams",
    "SetParam",
    "SetRuntimeParams",
    "WriteConfigFile",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

::Ice::DispatchStatus
LDTSSInterface::DeviceControlCB::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__LDTSSInterface__DeviceControlCB_all, __LDTSSInterface__DeviceControlCB_all + 38, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __LDTSSInterface__DeviceControlCB_all)
    {
        case 0:
        {
            return ___AddVagueData(in, current);
        }
        case 1:
        {
            return ___BatchInspectedVehicle(in, current);
        }
        case 2:
        {
            return ___CalibrateTime(in, current);
        }
        case 3:
        {
            return ___CancelVehicle(in, current);
        }
        case 4:
        {
            return ___Certificate(in, current);
        }
        case 5:
        {
            return ___CountVehicle(in, current);
        }
        case 6:
        {
            return ___Diagnostics(in, current);
        }
        case 7:
        {
            return ___DownloadRecording(in, current);
        }
        case 8:
        {
            return ___ExecuteCommand(in, current);
        }
        case 9:
        {
            return ___GetAllParams(in, current);
        }
        case 10:
        {
            return ___GetAttribute(in, current);
        }
        case 11:
        {
            return ___GetCaCode(in, current);
        }
        case 12:
        {
            return ___GetCameraAllParams(in, current);
        }
        case 13:
        {
            return ___GetDeviceStatus(in, current);
        }
        case 14:
        {
            return ___GetId(in, current);
        }
        case 15:
        {
            return ___GetLog(in, current);
        }
        case 16:
        {
            return ___GetNetworkParams(in, current);
        }
        case 17:
        {
            return ___GetParam(in, current);
        }
        case 18:
        {
            return ___GetRuntimeParams(in, current);
        }
        case 19:
        {
            return ___InspectVehicle(in, current);
        }
        case 20:
        {
            return ___ListVehicle(in, current);
        }
        case 21:
        {
            return ___OneShot(in, current);
        }
        case 22:
        {
            return ___QueryVagueData(in, current);
        }
        case 23:
        {
            return ___QueryVehicle(in, current);
        }
        case 24:
        {
            return ___QueryVehicleByTime(in, current);
        }
        case 25:
        {
            return ___ReadConfigFile(in, current);
        }
        case 26:
        {
            return ___ReadImage(in, current);
        }
        case 27:
        {
            return ___Reboot(in, current);
        }
        case 28:
        {
            return ___Reset(in, current);
        }
        case 29:
        {
            return ___SetCameraAllParams(in, current);
        }
        case 30:
        {
            return ___SetNetworkParams(in, current);
        }
        case 31:
        {
            return ___SetParam(in, current);
        }
        case 32:
        {
            return ___SetRuntimeParams(in, current);
        }
        case 33:
        {
            return ___WriteConfigFile(in, current);
        }
        case 34:
        {
            return ___ice_id(in, current);
        }
        case 35:
        {
            return ___ice_ids(in, current);
        }
        case 36:
        {
            return ___ice_isA(in, current);
        }
        case 37:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
LDTSSInterface::DeviceControlCB::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
LDTSSInterface::DeviceControlCB::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
LDTSSInterface::operator==(const ::LDTSSInterface::DeviceControlCB& l, const ::LDTSSInterface::DeviceControlCB& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
LDTSSInterface::operator<(const ::LDTSSInterface::DeviceControlCB& l, const ::LDTSSInterface::DeviceControlCB& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
LDTSSInterface::__patch__DeviceControlCBPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::LDTSSInterface::DeviceControlCBPtr* p = static_cast< ::LDTSSInterface::DeviceControlCBPtr*>(__addr);
    assert(p);
    *p = ::LDTSSInterface::DeviceControlCBPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::LDTSSInterface::DeviceControlCB::ice_staticId(), v->ice_id());
    }
}

const ::std::string&
IceProxy::LDTSSInterface::DeviceControlCB::ice_staticId()
{
    return __LDTSSInterface__DeviceControlCB_ids[2];
}

::IceProxy::Ice::Object*
IceProxy::LDTSSInterface::DeviceControlCB::__newInstance() const
{
    return new DeviceControlCB;
}
