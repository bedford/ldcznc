// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `DeviceMonitor.ice'

#include <DeviceMonitor.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/LocalException.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __LDTSSInterface__DeviceMonitor__ReportDeviceStatus_name = "ReportDeviceStatus";

static const ::std::string __LDTSSInterface__DeviceMonitor__UploadDeviceFault_name = "UploadDeviceFault";

static const ::std::string __LDTSSInterface__DeviceMonitor__RegisterDevice_name = "RegisterDevice";

::Ice::Object* IceInternal::upCast(::LDTSSInterface::DeviceMonitor* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::LDTSSInterface::DeviceMonitor* p) { return p; }

void
LDTSSInterface::__read(::IceInternal::BasicStream* __is, ::LDTSSInterface::DeviceMonitorPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::LDTSSInterface::DeviceMonitor;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __LDTSSInterface__DeviceMonitor_ids[2] =
{
    "::Ice::Object",
    "::LDTSSInterface::DeviceMonitor"
};

bool
LDTSSInterface::DeviceMonitor::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__LDTSSInterface__DeviceMonitor_ids, __LDTSSInterface__DeviceMonitor_ids + 2, _s);
}

::std::vector< ::std::string>
LDTSSInterface::DeviceMonitor::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__LDTSSInterface__DeviceMonitor_ids[0], &__LDTSSInterface__DeviceMonitor_ids[2]);
}

const ::std::string&
LDTSSInterface::DeviceMonitor::ice_id(const ::Ice::Current&) const
{
    return __LDTSSInterface__DeviceMonitor_ids[1];
}

const ::std::string&
LDTSSInterface::DeviceMonitor::ice_staticId()
{
    return __LDTSSInterface__DeviceMonitor_ids[1];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceMonitor::___ReportDeviceStatus(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::SurveyDeviceStatus deviceStatus;
    deviceStatus.__read(__is);
    ::IceInternal::BasicStream* __os = __inS.os();
    try
    {
        ReportDeviceStatus(deviceStatus, __current);
    }
    catch(const ::LDTSSInterface::DeviceNoFoundException& __ex)
    {
        __os->write(__ex);
        return ::Ice::DispatchUserException;
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceMonitor::___UploadDeviceFault(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::SurveyDeviceFault fault;
    fault.__read(__is);
    ::IceInternal::BasicStream* __os = __inS.os();
    try
    {
        UploadDeviceFault(fault, __current);
    }
    catch(const ::LDTSSInterface::DeviceNoFoundException& __ex)
    {
        __os->write(__ex);
        return ::Ice::DispatchUserException;
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceMonitor::___RegisterDevice(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string deviceNo;
    ::LDTSSInterface::SurveyDeviceAttribute deviceAttribute;
    ::LDTSSInterface::DeviceRuntimeParams runtimeParams;
    __is->read(deviceNo);
    deviceAttribute.__read(__is);
    runtimeParams.__read(__is);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = RegisterDevice(deviceNo, deviceAttribute, runtimeParams, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __LDTSSInterface__DeviceMonitor_all[] =
{
    "RegisterDevice",
    "ReportDeviceStatus",
    "UploadDeviceFault",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

::Ice::DispatchStatus
LDTSSInterface::DeviceMonitor::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__LDTSSInterface__DeviceMonitor_all, __LDTSSInterface__DeviceMonitor_all + 7, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __LDTSSInterface__DeviceMonitor_all)
    {
        case 0:
        {
            return ___RegisterDevice(in, current);
        }
        case 1:
        {
            return ___ReportDeviceStatus(in, current);
        }
        case 2:
        {
            return ___UploadDeviceFault(in, current);
        }
        case 3:
        {
            return ___ice_id(in, current);
        }
        case 4:
        {
            return ___ice_ids(in, current);
        }
        case 5:
        {
            return ___ice_isA(in, current);
        }
        case 6:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
LDTSSInterface::DeviceMonitor::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
LDTSSInterface::DeviceMonitor::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
LDTSSInterface::operator==(const ::LDTSSInterface::DeviceMonitor& l, const ::LDTSSInterface::DeviceMonitor& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
LDTSSInterface::operator<(const ::LDTSSInterface::DeviceMonitor& l, const ::LDTSSInterface::DeviceMonitor& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
LDTSSInterface::__patch__DeviceMonitorPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::LDTSSInterface::DeviceMonitorPtr* p = static_cast< ::LDTSSInterface::DeviceMonitorPtr*>(__addr);
    assert(p);
    *p = ::LDTSSInterface::DeviceMonitorPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::LDTSSInterface::DeviceMonitor::ice_staticId(), v->ice_id());
    }
}
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceMonitor_ReportDeviceStatus::__invoke(const ::LDTSSInterface::DeviceMonitorPrx& __prx, const ::LDTSSInterface::SurveyDeviceStatus& deviceStatus, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceMonitor__ReportDeviceStatus_name);
        __prepare(__prx, __LDTSSInterface__DeviceMonitor__ReportDeviceStatus_name, ::Ice::Normal, __ctx);
        deviceStatus.__write(__os);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceMonitor_ReportDeviceStatus::__response(bool __ok)
{
    try
    {
        if(!__ok)
        {
            try
            {
                __is->throwException();
            }
            catch(const ::LDTSSInterface::DeviceNoFoundException& __ex)
            {
                __exception(__ex);
            }
            catch(const ::Ice::UserException& __ex)
            {
                throw ::Ice::UnknownUserException(__FILE__, __LINE__, __ex.ice_name());
            }
            return;
        }
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response();
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceMonitor_UploadDeviceFault::__invoke(const ::LDTSSInterface::DeviceMonitorPrx& __prx, const ::LDTSSInterface::SurveyDeviceFault& fault, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceMonitor__UploadDeviceFault_name);
        __prepare(__prx, __LDTSSInterface__DeviceMonitor__UploadDeviceFault_name, ::Ice::Idempotent, __ctx);
        fault.__write(__os);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceMonitor_UploadDeviceFault::__response(bool __ok)
{
    try
    {
        if(!__ok)
        {
            try
            {
                __is->throwException();
            }
            catch(const ::LDTSSInterface::DeviceNoFoundException& __ex)
            {
                __exception(__ex);
            }
            catch(const ::Ice::UserException& __ex)
            {
                throw ::Ice::UnknownUserException(__FILE__, __LINE__, __ex.ice_name());
            }
            return;
        }
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response();
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceMonitor_RegisterDevice::__invoke(const ::LDTSSInterface::DeviceMonitorPrx& __prx, const ::std::string& deviceNo, const ::LDTSSInterface::SurveyDeviceAttribute& deviceAttribute, const ::LDTSSInterface::DeviceRuntimeParams& runtimeParams, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceMonitor__RegisterDevice_name);
        __prepare(__prx, __LDTSSInterface__DeviceMonitor__RegisterDevice_name, ::Ice::Idempotent, __ctx);
        __os->write(deviceNo);
        deviceAttribute.__write(__os);
        runtimeParams.__write(__os);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceMonitor_RegisterDevice::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif

void
IceProxy::LDTSSInterface::DeviceMonitor::ReportDeviceStatus(const ::LDTSSInterface::SurveyDeviceStatus& deviceStatus, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceMonitor__ReportDeviceStatus_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceMonitor__ReportDeviceStatus_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                deviceStatus.__write(__os);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    try
                    {
                        __outS.is()->throwException();
                    }
                    catch(const ::LDTSSInterface::DeviceNoFoundException&)
                    {
                        throw;
                    }
                    catch(const ::Ice::UserException& __ex)
                    {
                        ::Ice::UnknownUserException __uex(__FILE__, __LINE__);
                        __uex.unknown = __ex.ice_name();
                        throw __uex;
                    }
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceMonitor::ReportDeviceStatus_async(const ::LDTSSInterface::AMI_DeviceMonitor_ReportDeviceStatusPtr& __cb, const ::LDTSSInterface::SurveyDeviceStatus& deviceStatus)
{
    return __cb->__invoke(this, deviceStatus, 0);
}

bool
IceProxy::LDTSSInterface::DeviceMonitor::ReportDeviceStatus_async(const ::LDTSSInterface::AMI_DeviceMonitor_ReportDeviceStatusPtr& __cb, const ::LDTSSInterface::SurveyDeviceStatus& deviceStatus, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, deviceStatus, &__ctx);
}
#endif

void
IceProxy::LDTSSInterface::DeviceMonitor::UploadDeviceFault(const ::LDTSSInterface::SurveyDeviceFault& fault, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceMonitor__UploadDeviceFault_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceMonitor__UploadDeviceFault_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                fault.__write(__os);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    try
                    {
                        __outS.is()->throwException();
                    }
                    catch(const ::LDTSSInterface::DeviceNoFoundException&)
                    {
                        throw;
                    }
                    catch(const ::Ice::UserException& __ex)
                    {
                        ::Ice::UnknownUserException __uex(__FILE__, __LINE__);
                        __uex.unknown = __ex.ice_name();
                        throw __uex;
                    }
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceMonitor::UploadDeviceFault_async(const ::LDTSSInterface::AMI_DeviceMonitor_UploadDeviceFaultPtr& __cb, const ::LDTSSInterface::SurveyDeviceFault& fault)
{
    return __cb->__invoke(this, fault, 0);
}

bool
IceProxy::LDTSSInterface::DeviceMonitor::UploadDeviceFault_async(const ::LDTSSInterface::AMI_DeviceMonitor_UploadDeviceFaultPtr& __cb, const ::LDTSSInterface::SurveyDeviceFault& fault, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, fault, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceMonitor::RegisterDevice(const ::std::string& deviceNo, const ::LDTSSInterface::SurveyDeviceAttribute& deviceAttribute, const ::LDTSSInterface::DeviceRuntimeParams& runtimeParams, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceMonitor__RegisterDevice_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceMonitor__RegisterDevice_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(deviceNo);
                deviceAttribute.__write(__os);
                runtimeParams.__write(__os);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceMonitor::RegisterDevice_async(const ::LDTSSInterface::AMI_DeviceMonitor_RegisterDevicePtr& __cb, const ::std::string& deviceNo, const ::LDTSSInterface::SurveyDeviceAttribute& deviceAttribute, const ::LDTSSInterface::DeviceRuntimeParams& runtimeParams)
{
    return __cb->__invoke(this, deviceNo, deviceAttribute, runtimeParams, 0);
}

bool
IceProxy::LDTSSInterface::DeviceMonitor::RegisterDevice_async(const ::LDTSSInterface::AMI_DeviceMonitor_RegisterDevicePtr& __cb, const ::std::string& deviceNo, const ::LDTSSInterface::SurveyDeviceAttribute& deviceAttribute, const ::LDTSSInterface::DeviceRuntimeParams& runtimeParams, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, deviceNo, deviceAttribute, runtimeParams, &__ctx);
}
#endif

const ::std::string&
IceProxy::LDTSSInterface::DeviceMonitor::ice_staticId()
{
    return __LDTSSInterface__DeviceMonitor_ids[1];
}

::IceProxy::Ice::Object*
IceProxy::LDTSSInterface::DeviceMonitor::__newInstance() const
{
    return new DeviceMonitor;
}
