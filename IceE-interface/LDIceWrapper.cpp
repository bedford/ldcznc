#include <DeviceControlCBI.h>
#include "LDIceWrapper.h"
#include "IceHelper.h"
#include "ProxyUtil.h"

#define MY_CATCH()                                                              \
catch (const Ice::ConnectionLostException &ex)                                  \
{                                                                               \
        _bInit = false;                                                         \
        if(_communicator) {                                                     \
                Ice::Error err(_communicator->getLogger());                     \
                err << __FILE__ << "(" << __LINE__ <<")" << ex.what();          \
        }                                                                       \
}                                                                               \
catch (const Ice::Exception &ex)                                                \
{                                                                               \
        _bInit = false;                                                         \
        if(_communicator) {                                                     \
                Ice::Error err(_communicator->getLogger());                     \
                err << __FILE__ << "(" << __LINE__ <<")" << ex.what();          \
        }                                                                       \
}                                                                               \
catch (...)                                                                     \
{                                                                               \
        _bInit = false;                                                         \
        if(_communicator) {                                                     \
                Ice::Error err(_communicator->getLogger());                     \
                err << __FILE__ << "(" << __LINE__ <<")" << "unknow exception"; \
        }                                                                       \
}

LDIceWrapper* LDIceWrapper::_sLDIceWrapper = 0;
static const int kLoopInterval = 5;
static const int kSessionTimeout = 30;
static bool bEnableHeart = false;

LDIceWrapper::LDIceWrapper()
:_bInit(false)
,_passingImageUploader(0)
,_violationImageUploader(0)
,_deviceMonitor(0)
,_infoUploader(0)
,_deviceCtrlPrxReport(0)
,_deviceControl(0)
,_communicator(0)
,_adapter(0)
,_session(0)
,_router(0)
{
}

LDIceWrapper::~LDIceWrapper()
{

}

//初始化，调用其他接口前，必须先调用该函数
bool LDIceWrapper::Init(std::string deviceNo)
{
        _device_number = deviceNo;
        Start();

        return true;
}

void LDIceWrapper::Run()
{
        InitCommunicator();
        static int cnt = 0;
        while (!IsTerminated()) {
                if (!_bInit) {
                        ReleaseProxy();
                        GetUploadProxy();
                        printf("IceE Register callback proxy to server\n");
                        if (RegisterCallback(_device_number)) {
                                _bInit = true;
                                bEnableHeart = false;
                                cnt = 0;
                        }
                } else {
                        cnt++;
                        if ((cnt * kLoopInterval) >= kSessionTimeout) {
                                bEnableHeart = true;
                                cnt = 0;
                        }
                }
                sleep(kLoopInterval);
        }
        printf("exit LDIceWrapper run\n");

        _bInit = false;
        ReleaseProxy();
        ReleaseServant();
        ReleaseCommunicator();
}

bool LDIceWrapper::InitCommunicator()
{
        try {
                //初始化communicator
                if (!IceHelper::InitIce("CameraIce.Config", _communicator)) {
                        Ice::Error err(_communicator->getLogger());
                        err <<  "_communicator invalid";
                        return false;
                }
        }
        MY_CATCH()

        return false;
}

bool LDIceWrapper::GetUploadProxy()
{
        try {
                //获取信息上传代理
                _infoUploader = ProxyUtil::GetSurveyDataUploaderPrx(_communicator);
                if (!_infoUploader) {
                        Ice::Error err(_communicator->getLogger());
                        err <<  "_infoUploader invalid";
                        return false;
                }

                //通备图像上传代理
                _passingImageUploader = ProxyUtil::GetPassingImageUploaderPrx(_communicator);
                if (!_passingImageUploader) {
                        Ice::Error err(_communicator->getLogger());
                        err <<  "_passingImageUploader invalid";
                        return false;
                }

                //设备状态上传代理
                _deviceMonitor = ProxyUtil::GetDeviceMonitorPrx(_communicator);
                if (!_deviceMonitor) {
                        Ice::Error err(_communicator->getLogger());
                        err <<  "_deviceMonitor invalid";
                        return false;
                }

                //违法图像上传代理
                _violationImageUploader = ProxyUtil::GetViolationImageUploaderPrx(_communicator);
                if (!_violationImageUploader) {
                        Ice::Error err(_communicator->getLogger());
                        err <<  "_violationImageUploader invalid";
                        return false;
                }

                return true;
        }
        MY_CATCH()

        return false;
}

bool LDIceWrapper::ReleaseCommunicator()
{
        try {
                if (_communicator) {
                        _communicator->shutdown();
                        _communicator = 0;
                }

                return true;
        } catch(...){
                printf("error in release IceE\n");
        }

        return false;
}

//释放资源，程序退出前必须调用该函数
bool LDIceWrapper::ReleaseProxy()
{
        try {
                if (_infoUploader){
                        _infoUploader = 0;
                }

                if (_passingImageUploader) {
                        _passingImageUploader = 0;
                }

                if (_deviceMonitor) {
                        _deviceMonitor = 0;
                }

                if (_violationImageUploader) {
                        _violationImageUploader = 0;
                }

                if (_deviceCtrlPrxReport) {
                        _deviceCtrlPrxReport = 0;
                }

                return true;
        }
        MY_CATCH()

        return false;
}

void LDIceWrapper::ReleaseServant()
{
        try {
                if (_adapter) {
                        _adapter->deactivate();
                        _adapter->destroy();
                        _adapter = NULL;
                }

                if (_deviceControl) {
                        _deviceControl = 0;
                }

                if (_router) {
                        if (_session) {
                                _session->destroy();
                                _session = 0;
                        }
                        _router = 0;
                }
        } catch(...) {
                printf("error in release IceE\n");
        }
}

//上传过车信息及图像，如查images.size为零，则只上传过车信息（用于区间测速）
bool LDIceWrapper::UploadPassingVehicle(const LDTSSInterface::PassingVehicle &vehicle,
                        const LDTSSInterface::VehicleImageSeq &images, bool history)
{
        if (_bInit) {
                try {
                        //只上传信息
                        if (images.size() == 0) {
                                return _infoUploader->UploadPassingVehicle(vehicle, history);
                        }

                        //上传图像和信息，必须先上传信息，再上传图片
                        //传信息
                        if (!_passingImageUploader->UploadPassingVehicle(vehicle, history)) {
                                Ice::Error err(_communicator->getLogger());
                                err << "upload passing vehicle information failed";
                                return false;
                        }

                        //传图片
                        for (LDTSSInterface::VehicleImageSeq::const_iterator iter = images.begin();
                                        iter != images.end(); iter++) {
                                if (!_passingImageUploader->UploadImageFragment(
                                        iter->imageDescription.imageIdentity, 0,
                                        std::make_pair(&(iter->imageData[0]),
                                                &(iter->imageData[0]) + iter->imageData.size()))) {
                                        return false;
                                }
                        }
                        return true;
                }
                MY_CATCH()
        }

        return false;
}

//上传违法车辆信息及图像，如查images.size为零，则只上传过车信息（用于区间测速）
bool LDIceWrapper::UploadViolationVehicle(const LDTSSInterface::ViolationVehicle &vehicle,
                const LDTSSInterface::VehicleImageSeq &images, bool history)
{
        if (_bInit) {
                try {
                        //只上传信息
                        if (images.size() == 0) {
                                return _infoUploader->UploadViolationVehicle(vehicle,history);
                        }

                        //上传图像和信息，必须先上传信息，再上传图片
                        //传信息
                        if(!_violationImageUploader->UploadViolationVehicle(vehicle, history)) {
                                Ice::Error err(_communicator->getLogger());
                                err << "upload passing vehicle information failed";
                                return false;
                        }

                        //传图片
                        for (LDTSSInterface::VehicleImageSeq::const_iterator iter = images.begin();
                                        iter != images.end();
                                        iter++) {
                                if (!_violationImageUploader->UploadImageFragment(
                                        iter->imageDescription.imageIdentity, 0,
                                        std::make_pair(&(iter->imageData[0]),
                                                &(iter->imageData[0]) + iter->imageData.size()))) {
                                        return false;
                                }
                        }
                        return true;
                }
                MY_CATCH()
        }

        return false;
}


//上传流量统记信息，
bool LDIceWrapper::UploadTrafficStats(const LDTSSInterface::TrafficStats &stats, bool history)
{
        if (_bInit) {
                try {
                        return _infoUploader->UploadTrafficStats(stats, history);
                }
                MY_CATCH()
        }

        return false;
}

//上传设备态信息，
bool LDIceWrapper::UploadDeviceStatus(const LDTSSInterface::SurveyDeviceStatus &status)
{
        if (!_bInit) {
                return false;
        }

        if (!bEnableHeart) {
                return false;
        }

        bEnableHeart = false;

        try {
                _deviceMonitor->ReportDeviceStatus(status);
                return true;
        } catch(LDTSSInterface::DeviceNoFoundException) {
                throw;
        }
        MY_CATCH()

        return false;
}

bool LDIceWrapper::RegisterDevice(const ::std::string& deviceNo,
                const ::LDTSSInterface::SurveyDeviceAttribute& deviceAttribute,
                const ::LDTSSInterface::DeviceRuntimeParams& runtimeParams)
{
        if (_bInit) {
                try {
                        return _deviceMonitor->RegisterDevice(deviceNo,
                                                        deviceAttribute,
                                                        runtimeParams);
                }
                MY_CATCH()
        }

        return false;
}

LDIceWrapper *LDIceWrapper::GetInstance()
{
        if (_sLDIceWrapper == 0) {
                static LDIceWrapper instance;
                _sLDIceWrapper = &instance;
        }

        return _sLDIceWrapper;
}

bool LDIceWrapper::Release()
{
        Terminate();
        Join();
        if (_sLDIceWrapper != 0) {
                _sLDIceWrapper = 0;
        }

        return true;
}

bool LDIceWrapper::RegisterCallback(std::string deviceNo)
{
        try {
                if (!_deviceCtrlPrxReport) {
                        if (!CreateSession()) {
                                Ice::Error err(_communicator->getLogger());
                                err <<  "start session failed";
                                return false;
                        }

                        if (!StartServant(_deviceControl)) {
                                Ice::Error err(_communicator->getLogger());
                                err <<  "start servant failed";
                                return false;
                        }

                        _deviceCtrlPrxReport =
                                ProxyUtil::GetDeviceCtrlPrxReportPrx(_communicator);
                        if (!_deviceCtrlPrxReport) {
                                Ice::Error err(_communicator->getLogger());
                                err <<  "_deviceCtrlPrxReport invalid";
                                return false;
                        }
                }

                Ice::Context context;
                std::string override;
                context["_fwd"] = "t";
                if (!override.empty()) {
                        context["_ovrd"] = override;
                }

                _deviceCtrlPrxReport->Report(deviceNo, _deviceControl, context);

                return true;
        }
        MY_CATCH()

        return false;
}

bool LDIceWrapper::StartServant(LDTSSInterface::DeviceControlCBPrx &callback)
{
        try {
                if (_adapter) {
                        try {
                                _adapter->deactivate();
                                _adapter->destroy();
                                _adapter = NULL;
                        } catch (...) { }
                }

                _adapter = _communicator->createObjectAdapterWithRouter("DemoGlacier2.Client", _router);

                Glacier2::RouterPrx tmp_router = Glacier2::RouterPrx::checkedCast(_router);
                Ice::Identity callbackReceiverIdent;
                callbackReceiverIdent.name = "callbackReceiver";
                callbackReceiverIdent.category = tmp_router->getCategoryForClient();

                LDTSSInterface::DeviceControlCBI *cb = new LDTSSInterface::DeviceControlCBI;
                callback = LDTSSInterface::DeviceControlCBPrx::uncheckedCast(
                                _adapter->add(cb, callbackReceiverIdent));

                _adapter->activate();

                return true;
        }
        MY_CATCH()

        return false;
}

bool LDIceWrapper::CreateSession()
{
        if (_session) {
                try {
                        _session->destroy();
                        _session = 0;
                }
                MY_CATCH()
        }

        std::string id = "device";
        std::string pw = "device";

        try {
                if (_communicator) {
                        _router = _communicator->getDefaultRouter();
                }

                if (!_router) {
                        Ice::Error err(_communicator->getLogger());
                        err <<  "no default router";
                        return false;
                }

                Glacier2::RouterPrx tmp_router = Glacier2::RouterPrx::checkedCast(_router);
                if (!tmp_router) {
                        Ice::Error err(_communicator->getLogger());
                        err << "configured router is not a Glacier2 router";
                        return false;
                }

                _session = Glacier2::SessionPrx::uncheckedCast(
                                                tmp_router->createSession(id, pw));

                int tmp_timeout = (int)(tmp_router->getSessionTimeout() / 2);
                printf("session timeout is %d\n", tmp_timeout);
                return true;
        }
        catch (const Glacier2::PermissionDeniedException &ex)
        {
                Ice::Error err(_communicator->getLogger());
                err << "permissiong deny exception :" << ex.reason.c_str();
                return false;
        }
        catch (const Glacier2::CannotCreateSessionException &ex)
        {
                Ice::Error err(_communicator->getLogger());
                err << "cant create session exception :" << ex.reason.c_str();
                return false;
        }
        MY_CATCH()

        return false;
}
