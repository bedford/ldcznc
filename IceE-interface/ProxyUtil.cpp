#include <IceE/Communicator.h>
#include "ProxyUtil.h"
#include "IceHelper.h"

LDTSSInterface::SurveyDataUploaderPrx ProxyUtil::GetSurveyDataUploaderPrx(
                const Ice::CommunicatorPtr& communicator)
{
        LDTSSInterface::SurveyDataUploaderPrx proxy = NULL;
        IceHelper::GetProxy(communicator, "SurveyDataUploader.Proxy", proxy);
        return proxy;
}

LDTSSInterface::ViolationImageUploaderPrx ProxyUtil::GetViolationImageUploaderPrx(
                const Ice::CommunicatorPtr& communicator)
{
        LDTSSInterface::ViolationImageUploaderPrx proxy = NULL;
        IceHelper::GetProxy(communicator, "ViolationImageUploader.Proxy", proxy);
        return proxy;
}

LDTSSInterface::PassingImageUploaderPrx ProxyUtil::GetPassingImageUploaderPrx(
                const Ice::CommunicatorPtr& communicator)
{
        LDTSSInterface::PassingImageUploaderPrx proxy = NULL;
        IceHelper::GetProxy(communicator, "PassingImageUploader.Proxy", proxy);
        return proxy;
}

LDTSSInterface::DeviceMonitorPrx ProxyUtil::GetDeviceMonitorPrx(
                const Ice::CommunicatorPtr& communicator)
{
        LDTSSInterface::DeviceMonitorPrx proxy = NULL;
        IceHelper::GetProxy(communicator, "DeviceMonitor.Proxy", proxy);
        return proxy;
}

LDTSSInterface::DeviceCtrlPrxReportPrx ProxyUtil::GetDeviceCtrlPrxReportPrx(
                const Ice::CommunicatorPtr& communicator)
{
        LDTSSInterface::DeviceCtrlPrxReportPrx proxy = NULL;
        IceHelper::GetProxy(communicator, "DevicePrxReport.Proxy", proxy);
        return proxy;
}
