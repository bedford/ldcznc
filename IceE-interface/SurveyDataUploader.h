// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `SurveyDataUploader.ice'

#ifndef ____GeneratedCpp_SurveyDataUploader_h__
#define ____GeneratedCpp_SurveyDataUploader_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#ifdef ICEE_HAS_AMI
#   include <IceE/OutgoingAsync.h>
#endif
#include <IceE/UserExceptionFactory.h>
#include <IceE/FactoryTable.h>
#include <Base.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace LDTSSInterface
{

class SurveyDataUploader;

}

}

namespace LDTSSInterface
{

class SurveyDataUploader;
bool operator==(const SurveyDataUploader&, const SurveyDataUploader&);
bool operator<(const SurveyDataUploader&, const SurveyDataUploader&);

}

namespace IceInternal
{

::Ice::Object* upCast(::LDTSSInterface::SurveyDataUploader*);
::IceProxy::Ice::Object* upCast(::IceProxy::LDTSSInterface::SurveyDataUploader*);

}

namespace LDTSSInterface
{

typedef ::IceInternal::Handle< ::LDTSSInterface::SurveyDataUploader> SurveyDataUploaderPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::LDTSSInterface::SurveyDataUploader> SurveyDataUploaderPrx;

void __read(::IceInternal::BasicStream*, SurveyDataUploaderPrx&);
void __patch__SurveyDataUploaderPtr(void*, ::Ice::ObjectPtr&);

}

namespace LDTSSInterface
{

}

namespace LDTSSInterface
{

class SurveyDataUploader : virtual public ::Ice::Object
{
public:

    typedef SurveyDataUploaderPrx ProxyType;
    typedef SurveyDataUploaderPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual bool UploadPassingVehicle(const ::LDTSSInterface::PassingVehicle&, bool, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___UploadPassingVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool UploadTrafficStats(const ::LDTSSInterface::TrafficStats&, bool, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___UploadTrafficStats(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle&, bool, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___UploadViolationVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace LDTSSInterface
{
#ifdef ICEE_HAS_AMI

class AMI_SurveyDataUploader_UploadPassingVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::SurveyDataUploaderPrx&, const ::LDTSSInterface::PassingVehicle&, bool, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_SurveyDataUploader_UploadPassingVehicle> AMI_SurveyDataUploader_UploadPassingVehiclePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_SurveyDataUploader_UploadTrafficStats : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::SurveyDataUploaderPrx&, const ::LDTSSInterface::TrafficStats&, bool, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_SurveyDataUploader_UploadTrafficStats> AMI_SurveyDataUploader_UploadTrafficStatsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_SurveyDataUploader_UploadViolationVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::SurveyDataUploaderPrx&, const ::LDTSSInterface::ViolationVehicle&, bool, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_SurveyDataUploader_UploadViolationVehicle> AMI_SurveyDataUploader_UploadViolationVehiclePtr;
#endif

}

namespace IceProxy
{

namespace LDTSSInterface
{

class SurveyDataUploader : virtual public ::IceProxy::Ice::Object
{
public:

    bool UploadPassingVehicle(const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData)
    {
        return UploadPassingVehicle(vehicle, historyData, 0);
    }
    bool UploadPassingVehicle(const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData, const ::Ice::Context& __ctx)
    {
        return UploadPassingVehicle(vehicle, historyData, &__ctx);
    }
    
private:

    bool UploadPassingVehicle(const ::LDTSSInterface::PassingVehicle&, bool, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool UploadPassingVehicle_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadPassingVehiclePtr&, const ::LDTSSInterface::PassingVehicle&, bool);
    bool UploadPassingVehicle_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadPassingVehiclePtr&, const ::LDTSSInterface::PassingVehicle&, bool, const ::Ice::Context&);
    #endif

    bool UploadTrafficStats(const ::LDTSSInterface::TrafficStats& stats, bool historyData)
    {
        return UploadTrafficStats(stats, historyData, 0);
    }
    bool UploadTrafficStats(const ::LDTSSInterface::TrafficStats& stats, bool historyData, const ::Ice::Context& __ctx)
    {
        return UploadTrafficStats(stats, historyData, &__ctx);
    }
    
private:

    bool UploadTrafficStats(const ::LDTSSInterface::TrafficStats&, bool, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool UploadTrafficStats_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadTrafficStatsPtr&, const ::LDTSSInterface::TrafficStats&, bool);
    bool UploadTrafficStats_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadTrafficStatsPtr&, const ::LDTSSInterface::TrafficStats&, bool, const ::Ice::Context&);
    #endif

    bool UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle& vehicle, bool historyData)
    {
        return UploadViolationVehicle(vehicle, historyData, 0);
    }
    bool UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle& vehicle, bool historyData, const ::Ice::Context& __ctx)
    {
        return UploadViolationVehicle(vehicle, historyData, &__ctx);
    }
    
private:

    bool UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle&, bool, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool UploadViolationVehicle_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadViolationVehiclePtr&, const ::LDTSSInterface::ViolationVehicle&, bool);
    bool UploadViolationVehicle_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadViolationVehiclePtr&, const ::LDTSSInterface::ViolationVehicle&, bool, const ::Ice::Context&);
    #endif
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_secure(bool __secure) const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_twoway() const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_oneway() const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_batchOneway() const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_datagram() const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_batchDatagram() const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<SurveyDataUploader> ice_timeout(int __timeout) const
    {
        return dynamic_cast<SurveyDataUploader*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
