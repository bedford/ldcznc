// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `DeviceControl.ice'

#ifndef ____GeneratedCpp_DeviceControl_h__
#define ____GeneratedCpp_DeviceControl_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#ifdef ICEE_HAS_AMI
#   include <IceE/OutgoingAsync.h>
#endif
#include <IceE/UserExceptionFactory.h>
#include <IceE/FactoryTable.h>
#include <Base.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace LDTSSInterface
{

class DeviceControl;

}

}

namespace LDTSSInterface
{

class DeviceControl;
bool operator==(const DeviceControl&, const DeviceControl&);
bool operator<(const DeviceControl&, const DeviceControl&);

}

namespace IceInternal
{

::Ice::Object* upCast(::LDTSSInterface::DeviceControl*);
::IceProxy::Ice::Object* upCast(::IceProxy::LDTSSInterface::DeviceControl*);

}

namespace LDTSSInterface
{

typedef ::IceInternal::Handle< ::LDTSSInterface::DeviceControl> DeviceControlPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::LDTSSInterface::DeviceControl> DeviceControlPrx;

void __read(::IceInternal::BasicStream*, DeviceControlPrx&);
void __patch__DeviceControlPtr(void*, ::Ice::ObjectPtr&);

}

namespace LDTSSInterface
{

}

namespace LDTSSInterface
{

class DeviceControl : virtual public ::Ice::Object
{
public:

    typedef DeviceControlPrx ProxyType;
    typedef DeviceControlPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual ::LDTSSInterface::SurveyDeviceAttribute GetAttribute(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetAttribute(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool SetParam(const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___SetParam(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::std::string GetParam(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetParam(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::StringStringDict GetAllParams(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetAllParams(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool Reset(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___Reset(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::StringStringDict GetCameraAllParams(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetCameraAllParams(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool SetCameraAllParams(const ::std::string&, const ::LDTSSInterface::StringStringDict&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___SetCameraAllParams(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::DeviceRuntimeParams GetRuntimeParams(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetRuntimeParams(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool SetRuntimeParams(const ::LDTSSInterface::DeviceRuntimeParams&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___SetRuntimeParams(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::NetworkParams GetNetworkParams(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetNetworkParams(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool SetNetworkParams(const ::LDTSSInterface::NetworkParams&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___SetNetworkParams(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::Ice::ByteSeq ReadConfigFile(::Ice::Int, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___ReadConfigFile(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool WriteConfigFile(::Ice::Int, const ::Ice::ByteSeq&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___WriteConfigFile(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool ExecuteCommand(const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___ExecuteCommand(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool Reboot(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___Reboot(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool CalibrateTime(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___CalibrateTime(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::SurveyDeviceStatus GetDeviceStatus(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetDeviceStatus(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::Ice::ByteSeq GetLog(::Ice::Int, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetLog(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::Ice::Int Diagnostics(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___Diagnostics(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool OneShot(const ::std::string&, ::LDTSSInterface::VehicleImageDescriptionSeq&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___OneShot(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::Ice::ByteSeq ReadImage(const ::std::string&, const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, ::Ice::Int, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___ReadImage(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::Ice::ByteSeq DownloadRecording(const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___DownloadRecording(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::PassingVehicleSeq QueryVehicleByTime(const ::std::string&, const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___QueryVehicleByTime(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::std::string GetId(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetId(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::std::string GetCaCode(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___GetCaCode(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool Certificate(const ::Ice::ByteSeq&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___Certificate(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace LDTSSInterface
{
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetAttribute : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::SurveyDeviceAttribute&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetAttribute> AMI_DeviceControl_GetAttributePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_SetParam : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_SetParam> AMI_DeviceControl_SetParamPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetParam : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::std::string&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetParam> AMI_DeviceControl_GetParamPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetAllParams : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::StringStringDict&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetAllParams> AMI_DeviceControl_GetAllParamsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_Reset : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_Reset> AMI_DeviceControl_ResetPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetCameraAllParams : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::StringStringDict&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetCameraAllParams> AMI_DeviceControl_GetCameraAllParamsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_SetCameraAllParams : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::LDTSSInterface::StringStringDict&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_SetCameraAllParams> AMI_DeviceControl_SetCameraAllParamsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetRuntimeParams : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::DeviceRuntimeParams&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetRuntimeParams> AMI_DeviceControl_GetRuntimeParamsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_SetRuntimeParams : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::LDTSSInterface::DeviceRuntimeParams&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_SetRuntimeParams> AMI_DeviceControl_SetRuntimeParamsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetNetworkParams : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::NetworkParams&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetNetworkParams> AMI_DeviceControl_GetNetworkParamsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_SetNetworkParams : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::LDTSSInterface::NetworkParams&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_SetNetworkParams> AMI_DeviceControl_SetNetworkParamsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_ReadConfigFile : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::Ice::ByteSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, ::Ice::Int, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_ReadConfigFile> AMI_DeviceControl_ReadConfigFilePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_WriteConfigFile : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, ::Ice::Int, const ::Ice::ByteSeq&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_WriteConfigFile> AMI_DeviceControl_WriteConfigFilePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_ExecuteCommand : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_ExecuteCommand> AMI_DeviceControl_ExecuteCommandPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_Reboot : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_Reboot> AMI_DeviceControl_RebootPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_CalibrateTime : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_CalibrateTime> AMI_DeviceControl_CalibrateTimePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetDeviceStatus : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::SurveyDeviceStatus&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetDeviceStatus> AMI_DeviceControl_GetDeviceStatusPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetLog : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::Ice::ByteSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, ::Ice::Int, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetLog> AMI_DeviceControl_GetLogPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_Diagnostics : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(::Ice::Int) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_Diagnostics> AMI_DeviceControl_DiagnosticsPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_OneShot : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool, const ::LDTSSInterface::VehicleImageDescriptionSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_OneShot> AMI_DeviceControl_OneShotPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_ReadImage : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::Ice::ByteSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, ::Ice::Int, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_ReadImage> AMI_DeviceControl_ReadImagePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_DownloadRecording : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::Ice::ByteSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_DownloadRecording> AMI_DeviceControl_DownloadRecordingPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_QueryVehicleByTime : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::PassingVehicleSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::std::string&, const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_QueryVehicleByTime> AMI_DeviceControl_QueryVehicleByTimePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetId : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::std::string&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetId> AMI_DeviceControl_GetIdPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_GetCaCode : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::std::string&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_GetCaCode> AMI_DeviceControl_GetCaCodePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceControl_Certificate : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceControlPrx&, const ::Ice::ByteSeq&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceControl_Certificate> AMI_DeviceControl_CertificatePtr;
#endif

}

namespace IceProxy
{

namespace LDTSSInterface
{

class DeviceControl : virtual public ::IceProxy::Ice::Object
{
public:

    ::LDTSSInterface::SurveyDeviceAttribute GetAttribute()
    {
        return GetAttribute(0);
    }
    ::LDTSSInterface::SurveyDeviceAttribute GetAttribute(const ::Ice::Context& __ctx)
    {
        return GetAttribute(&__ctx);
    }
    
private:

    ::LDTSSInterface::SurveyDeviceAttribute GetAttribute(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetAttribute_async(const ::LDTSSInterface::AMI_DeviceControl_GetAttributePtr&);
    bool GetAttribute_async(const ::LDTSSInterface::AMI_DeviceControl_GetAttributePtr&, const ::Ice::Context&);
    #endif

    bool SetParam(const ::std::string& paramName, const ::std::string& paramValue)
    {
        return SetParam(paramName, paramValue, 0);
    }
    bool SetParam(const ::std::string& paramName, const ::std::string& paramValue, const ::Ice::Context& __ctx)
    {
        return SetParam(paramName, paramValue, &__ctx);
    }
    
private:

    bool SetParam(const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool SetParam_async(const ::LDTSSInterface::AMI_DeviceControl_SetParamPtr&, const ::std::string&, const ::std::string&);
    bool SetParam_async(const ::LDTSSInterface::AMI_DeviceControl_SetParamPtr&, const ::std::string&, const ::std::string&, const ::Ice::Context&);
    #endif

    ::std::string GetParam(const ::std::string& paramName)
    {
        return GetParam(paramName, 0);
    }
    ::std::string GetParam(const ::std::string& paramName, const ::Ice::Context& __ctx)
    {
        return GetParam(paramName, &__ctx);
    }
    
private:

    ::std::string GetParam(const ::std::string&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetParam_async(const ::LDTSSInterface::AMI_DeviceControl_GetParamPtr&, const ::std::string&);
    bool GetParam_async(const ::LDTSSInterface::AMI_DeviceControl_GetParamPtr&, const ::std::string&, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::StringStringDict GetAllParams()
    {
        return GetAllParams(0);
    }
    ::LDTSSInterface::StringStringDict GetAllParams(const ::Ice::Context& __ctx)
    {
        return GetAllParams(&__ctx);
    }
    
private:

    ::LDTSSInterface::StringStringDict GetAllParams(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetAllParamsPtr&);
    bool GetAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetAllParamsPtr&, const ::Ice::Context&);
    #endif

    bool Reset()
    {
        return Reset(0);
    }
    bool Reset(const ::Ice::Context& __ctx)
    {
        return Reset(&__ctx);
    }
    
private:

    bool Reset(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool Reset_async(const ::LDTSSInterface::AMI_DeviceControl_ResetPtr&);
    bool Reset_async(const ::LDTSSInterface::AMI_DeviceControl_ResetPtr&, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::StringStringDict GetCameraAllParams(const ::std::string& cameraId)
    {
        return GetCameraAllParams(cameraId, 0);
    }
    ::LDTSSInterface::StringStringDict GetCameraAllParams(const ::std::string& cameraId, const ::Ice::Context& __ctx)
    {
        return GetCameraAllParams(cameraId, &__ctx);
    }
    
private:

    ::LDTSSInterface::StringStringDict GetCameraAllParams(const ::std::string&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetCameraAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetCameraAllParamsPtr&, const ::std::string&);
    bool GetCameraAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetCameraAllParamsPtr&, const ::std::string&, const ::Ice::Context&);
    #endif

    bool SetCameraAllParams(const ::std::string& cameraId, const ::LDTSSInterface::StringStringDict& allParams)
    {
        return SetCameraAllParams(cameraId, allParams, 0);
    }
    bool SetCameraAllParams(const ::std::string& cameraId, const ::LDTSSInterface::StringStringDict& allParams, const ::Ice::Context& __ctx)
    {
        return SetCameraAllParams(cameraId, allParams, &__ctx);
    }
    
private:

    bool SetCameraAllParams(const ::std::string&, const ::LDTSSInterface::StringStringDict&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool SetCameraAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetCameraAllParamsPtr&, const ::std::string&, const ::LDTSSInterface::StringStringDict&);
    bool SetCameraAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetCameraAllParamsPtr&, const ::std::string&, const ::LDTSSInterface::StringStringDict&, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::DeviceRuntimeParams GetRuntimeParams()
    {
        return GetRuntimeParams(0);
    }
    ::LDTSSInterface::DeviceRuntimeParams GetRuntimeParams(const ::Ice::Context& __ctx)
    {
        return GetRuntimeParams(&__ctx);
    }
    
private:

    ::LDTSSInterface::DeviceRuntimeParams GetRuntimeParams(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetRuntimeParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetRuntimeParamsPtr&);
    bool GetRuntimeParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetRuntimeParamsPtr&, const ::Ice::Context&);
    #endif

    bool SetRuntimeParams(const ::LDTSSInterface::DeviceRuntimeParams& params)
    {
        return SetRuntimeParams(params, 0);
    }
    bool SetRuntimeParams(const ::LDTSSInterface::DeviceRuntimeParams& params, const ::Ice::Context& __ctx)
    {
        return SetRuntimeParams(params, &__ctx);
    }
    
private:

    bool SetRuntimeParams(const ::LDTSSInterface::DeviceRuntimeParams&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool SetRuntimeParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetRuntimeParamsPtr&, const ::LDTSSInterface::DeviceRuntimeParams&);
    bool SetRuntimeParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetRuntimeParamsPtr&, const ::LDTSSInterface::DeviceRuntimeParams&, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::NetworkParams GetNetworkParams()
    {
        return GetNetworkParams(0);
    }
    ::LDTSSInterface::NetworkParams GetNetworkParams(const ::Ice::Context& __ctx)
    {
        return GetNetworkParams(&__ctx);
    }
    
private:

    ::LDTSSInterface::NetworkParams GetNetworkParams(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetNetworkParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetNetworkParamsPtr&);
    bool GetNetworkParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetNetworkParamsPtr&, const ::Ice::Context&);
    #endif

    bool SetNetworkParams(const ::LDTSSInterface::NetworkParams& params)
    {
        return SetNetworkParams(params, 0);
    }
    bool SetNetworkParams(const ::LDTSSInterface::NetworkParams& params, const ::Ice::Context& __ctx)
    {
        return SetNetworkParams(params, &__ctx);
    }
    
private:

    bool SetNetworkParams(const ::LDTSSInterface::NetworkParams&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool SetNetworkParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetNetworkParamsPtr&, const ::LDTSSInterface::NetworkParams&);
    bool SetNetworkParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetNetworkParamsPtr&, const ::LDTSSInterface::NetworkParams&, const ::Ice::Context&);
    #endif

    ::Ice::ByteSeq ReadConfigFile(::Ice::Int configType)
    {
        return ReadConfigFile(configType, 0);
    }
    ::Ice::ByteSeq ReadConfigFile(::Ice::Int configType, const ::Ice::Context& __ctx)
    {
        return ReadConfigFile(configType, &__ctx);
    }
    
private:

    ::Ice::ByteSeq ReadConfigFile(::Ice::Int, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool ReadConfigFile_async(const ::LDTSSInterface::AMI_DeviceControl_ReadConfigFilePtr&, ::Ice::Int);
    bool ReadConfigFile_async(const ::LDTSSInterface::AMI_DeviceControl_ReadConfigFilePtr&, ::Ice::Int, const ::Ice::Context&);
    #endif

    bool WriteConfigFile(::Ice::Int configType, const ::Ice::ByteSeq& configFile)
    {
        return WriteConfigFile(configType, configFile, 0);
    }
    bool WriteConfigFile(::Ice::Int configType, const ::Ice::ByteSeq& configFile, const ::Ice::Context& __ctx)
    {
        return WriteConfigFile(configType, configFile, &__ctx);
    }
    
private:

    bool WriteConfigFile(::Ice::Int, const ::Ice::ByteSeq&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool WriteConfigFile_async(const ::LDTSSInterface::AMI_DeviceControl_WriteConfigFilePtr&, ::Ice::Int, const ::Ice::ByteSeq&);
    bool WriteConfigFile_async(const ::LDTSSInterface::AMI_DeviceControl_WriteConfigFilePtr&, ::Ice::Int, const ::Ice::ByteSeq&, const ::Ice::Context&);
    #endif

    bool ExecuteCommand(const ::std::string& cmdName, const ::std::string& cmdParam)
    {
        return ExecuteCommand(cmdName, cmdParam, 0);
    }
    bool ExecuteCommand(const ::std::string& cmdName, const ::std::string& cmdParam, const ::Ice::Context& __ctx)
    {
        return ExecuteCommand(cmdName, cmdParam, &__ctx);
    }
    
private:

    bool ExecuteCommand(const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool ExecuteCommand_async(const ::LDTSSInterface::AMI_DeviceControl_ExecuteCommandPtr&, const ::std::string&, const ::std::string&);
    bool ExecuteCommand_async(const ::LDTSSInterface::AMI_DeviceControl_ExecuteCommandPtr&, const ::std::string&, const ::std::string&, const ::Ice::Context&);
    #endif

    bool Reboot()
    {
        return Reboot(0);
    }
    bool Reboot(const ::Ice::Context& __ctx)
    {
        return Reboot(&__ctx);
    }
    
private:

    bool Reboot(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool Reboot_async(const ::LDTSSInterface::AMI_DeviceControl_RebootPtr&);
    bool Reboot_async(const ::LDTSSInterface::AMI_DeviceControl_RebootPtr&, const ::Ice::Context&);
    #endif

    bool CalibrateTime(const ::std::string& time)
    {
        return CalibrateTime(time, 0);
    }
    bool CalibrateTime(const ::std::string& time, const ::Ice::Context& __ctx)
    {
        return CalibrateTime(time, &__ctx);
    }
    
private:

    bool CalibrateTime(const ::std::string&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool CalibrateTime_async(const ::LDTSSInterface::AMI_DeviceControl_CalibrateTimePtr&, const ::std::string&);
    bool CalibrateTime_async(const ::LDTSSInterface::AMI_DeviceControl_CalibrateTimePtr&, const ::std::string&, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::SurveyDeviceStatus GetDeviceStatus()
    {
        return GetDeviceStatus(0);
    }
    ::LDTSSInterface::SurveyDeviceStatus GetDeviceStatus(const ::Ice::Context& __ctx)
    {
        return GetDeviceStatus(&__ctx);
    }
    
private:

    ::LDTSSInterface::SurveyDeviceStatus GetDeviceStatus(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetDeviceStatus_async(const ::LDTSSInterface::AMI_DeviceControl_GetDeviceStatusPtr&);
    bool GetDeviceStatus_async(const ::LDTSSInterface::AMI_DeviceControl_GetDeviceStatusPtr&, const ::Ice::Context&);
    #endif

    ::Ice::ByteSeq GetLog(::Ice::Int logType)
    {
        return GetLog(logType, 0);
    }
    ::Ice::ByteSeq GetLog(::Ice::Int logType, const ::Ice::Context& __ctx)
    {
        return GetLog(logType, &__ctx);
    }
    
private:

    ::Ice::ByteSeq GetLog(::Ice::Int, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetLog_async(const ::LDTSSInterface::AMI_DeviceControl_GetLogPtr&, ::Ice::Int);
    bool GetLog_async(const ::LDTSSInterface::AMI_DeviceControl_GetLogPtr&, ::Ice::Int, const ::Ice::Context&);
    #endif

    ::Ice::Int Diagnostics()
    {
        return Diagnostics(0);
    }
    ::Ice::Int Diagnostics(const ::Ice::Context& __ctx)
    {
        return Diagnostics(&__ctx);
    }
    
private:

    ::Ice::Int Diagnostics(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool Diagnostics_async(const ::LDTSSInterface::AMI_DeviceControl_DiagnosticsPtr&);
    bool Diagnostics_async(const ::LDTSSInterface::AMI_DeviceControl_DiagnosticsPtr&, const ::Ice::Context&);
    #endif

    bool OneShot(const ::std::string& cameraId, ::LDTSSInterface::VehicleImageDescriptionSeq& images)
    {
        return OneShot(cameraId, images, 0);
    }
    bool OneShot(const ::std::string& cameraId, ::LDTSSInterface::VehicleImageDescriptionSeq& images, const ::Ice::Context& __ctx)
    {
        return OneShot(cameraId, images, &__ctx);
    }
    
private:

    bool OneShot(const ::std::string&, ::LDTSSInterface::VehicleImageDescriptionSeq&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool OneShot_async(const ::LDTSSInterface::AMI_DeviceControl_OneShotPtr&, const ::std::string&);
    bool OneShot_async(const ::LDTSSInterface::AMI_DeviceControl_OneShotPtr&, const ::std::string&, const ::Ice::Context&);
    #endif

    ::Ice::ByteSeq ReadImage(const ::std::string& cameraId, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, ::Ice::Int length)
    {
        return ReadImage(cameraId, identity, offset, length, 0);
    }
    ::Ice::ByteSeq ReadImage(const ::std::string& cameraId, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, ::Ice::Int length, const ::Ice::Context& __ctx)
    {
        return ReadImage(cameraId, identity, offset, length, &__ctx);
    }
    
private:

    ::Ice::ByteSeq ReadImage(const ::std::string&, const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, ::Ice::Int, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool ReadImage_async(const ::LDTSSInterface::AMI_DeviceControl_ReadImagePtr&, const ::std::string&, const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, ::Ice::Int);
    bool ReadImage_async(const ::LDTSSInterface::AMI_DeviceControl_ReadImagePtr&, const ::std::string&, const ::LDTSSInterface::VehicleImageIdentity&, ::Ice::Int, ::Ice::Int, const ::Ice::Context&);
    #endif

    ::Ice::ByteSeq DownloadRecording(const ::std::string& recordingFile, ::Ice::Int offset, ::Ice::Int length)
    {
        return DownloadRecording(recordingFile, offset, length, 0);
    }
    ::Ice::ByteSeq DownloadRecording(const ::std::string& recordingFile, ::Ice::Int offset, ::Ice::Int length, const ::Ice::Context& __ctx)
    {
        return DownloadRecording(recordingFile, offset, length, &__ctx);
    }
    
private:

    ::Ice::ByteSeq DownloadRecording(const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool DownloadRecording_async(const ::LDTSSInterface::AMI_DeviceControl_DownloadRecordingPtr&, const ::std::string&, ::Ice::Int, ::Ice::Int);
    bool DownloadRecording_async(const ::LDTSSInterface::AMI_DeviceControl_DownloadRecordingPtr&, const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::PassingVehicleSeq QueryVehicleByTime(const ::std::string& startTime, const ::std::string& endTime, ::Ice::Int pageSize, ::Ice::Int pageIndex)
    {
        return QueryVehicleByTime(startTime, endTime, pageSize, pageIndex, 0);
    }
    ::LDTSSInterface::PassingVehicleSeq QueryVehicleByTime(const ::std::string& startTime, const ::std::string& endTime, ::Ice::Int pageSize, ::Ice::Int pageIndex, const ::Ice::Context& __ctx)
    {
        return QueryVehicleByTime(startTime, endTime, pageSize, pageIndex, &__ctx);
    }
    
private:

    ::LDTSSInterface::PassingVehicleSeq QueryVehicleByTime(const ::std::string&, const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool QueryVehicleByTime_async(const ::LDTSSInterface::AMI_DeviceControl_QueryVehicleByTimePtr&, const ::std::string&, const ::std::string&, ::Ice::Int, ::Ice::Int);
    bool QueryVehicleByTime_async(const ::LDTSSInterface::AMI_DeviceControl_QueryVehicleByTimePtr&, const ::std::string&, const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context&);
    #endif

    ::std::string GetId()
    {
        return GetId(0);
    }
    ::std::string GetId(const ::Ice::Context& __ctx)
    {
        return GetId(&__ctx);
    }
    
private:

    ::std::string GetId(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetId_async(const ::LDTSSInterface::AMI_DeviceControl_GetIdPtr&);
    bool GetId_async(const ::LDTSSInterface::AMI_DeviceControl_GetIdPtr&, const ::Ice::Context&);
    #endif

    ::std::string GetCaCode()
    {
        return GetCaCode(0);
    }
    ::std::string GetCaCode(const ::Ice::Context& __ctx)
    {
        return GetCaCode(&__ctx);
    }
    
private:

    ::std::string GetCaCode(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool GetCaCode_async(const ::LDTSSInterface::AMI_DeviceControl_GetCaCodePtr&);
    bool GetCaCode_async(const ::LDTSSInterface::AMI_DeviceControl_GetCaCodePtr&, const ::Ice::Context&);
    #endif

    bool Certificate(const ::Ice::ByteSeq& caFile)
    {
        return Certificate(caFile, 0);
    }
    bool Certificate(const ::Ice::ByteSeq& caFile, const ::Ice::Context& __ctx)
    {
        return Certificate(caFile, &__ctx);
    }
    
private:

    bool Certificate(const ::Ice::ByteSeq&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool Certificate_async(const ::LDTSSInterface::AMI_DeviceControl_CertificatePtr&, const ::Ice::ByteSeq&);
    bool Certificate_async(const ::LDTSSInterface::AMI_DeviceControl_CertificatePtr&, const ::Ice::ByteSeq&, const ::Ice::Context&);
    #endif
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_secure(bool __secure) const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<DeviceControl> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<DeviceControl> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_twoway() const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_oneway() const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_batchOneway() const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_datagram() const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_batchDatagram() const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControl> ice_timeout(int __timeout) const
    {
        return dynamic_cast<DeviceControl*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
