#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "offence_parse.h"


bool set_offence_code(offenceCode *code, int length, const char *filename)
{
        FILE *fp = fopen(filename, "wb");
        if (fp == NULL)
                return false;

        char tmp[512];
        int index = 0;
        for ( ; index < length; index++) {
                sprintf(tmp, "%s,%s,%d,%d,%d,%d,\n",
                                code[index].violationCode,
                                code[index].violationDescription,
                                code[index].offenceType,
                                code[index].overSpeedPercent,
                                code[index].lowerBound,
                                code[index].upperBound);
                fwrite(tmp, strlen(tmp), 1, fp);
        }
        
        fclose(fp);
        return true;
}


bool read_offence_code(offenceCode *code, int *length, const char *filename)
{
        FILE *fp = fopen(filename, "rb");
        if (fp == NULL)
                return false;

        char buf[256];
        char *dest = NULL;
        char *next = NULL;
        *length = 0;
        offenceCode *tmpCode = code;

        while (!feof(fp)) {
                if (fgets(buf, sizeof(buf), fp) != NULL) {
                        dest = strtok_r(buf, ",", &next);
                        memcpy(tmpCode->violationCode, dest, strlen(dest));
                        dest = strtok_r(NULL, ",", &next);
                        memcpy(tmpCode->violationDescription, dest, strlen(dest));
                        dest = strtok_r(NULL, ",", &next);
                        tmpCode->offenceType = atoi(dest);
                        dest = strtok_r(NULL, ",", &next);
                        tmpCode->overSpeedPercent = atoi(dest);
                        dest = strtok_r(NULL, ",", &next);
                        tmpCode->lowerBound = atoi(dest);
                        dest = strtok_r(NULL, ",", &next);
                        tmpCode->upperBound = atoi(dest);

                        memset(buf, 0, sizeof(buf));

                        tmpCode++;
                        (*length)++;
                }
        }

        return true;
}

