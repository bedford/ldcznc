// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `PassingImageUploader.ice'

#include <PassingImageUploader.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/LocalException.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __LDTSSInterface__PassingImageUploader__UploadImageFragment_name = "UploadImageFragment";

static const ::std::string __LDTSSInterface__PassingImageUploader__UploadPassingVehicle_name = "UploadPassingVehicle";

static const ::std::string __LDTSSInterface__PassingImageUploader__GetFragmentInfo_name = "GetFragmentInfo";

::Ice::Object* IceInternal::upCast(::LDTSSInterface::PassingImageUploader* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::LDTSSInterface::PassingImageUploader* p) { return p; }

void
LDTSSInterface::__read(::IceInternal::BasicStream* __is, ::LDTSSInterface::PassingImageUploaderPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::LDTSSInterface::PassingImageUploader;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __LDTSSInterface__PassingImageUploader_ids[2] =
{
    "::Ice::Object",
    "::LDTSSInterface::PassingImageUploader"
};

bool
LDTSSInterface::PassingImageUploader::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__LDTSSInterface__PassingImageUploader_ids, __LDTSSInterface__PassingImageUploader_ids + 2, _s);
}

::std::vector< ::std::string>
LDTSSInterface::PassingImageUploader::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__LDTSSInterface__PassingImageUploader_ids[0], &__LDTSSInterface__PassingImageUploader_ids[2]);
}

const ::std::string&
LDTSSInterface::PassingImageUploader::ice_id(const ::Ice::Current&) const
{
    return __LDTSSInterface__PassingImageUploader_ids[1];
}

const ::std::string&
LDTSSInterface::PassingImageUploader::ice_staticId()
{
    return __LDTSSInterface__PassingImageUploader_ids[1];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::PassingImageUploader::___UploadImageFragment(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::VehicleImageIdentity identity;
    ::Ice::Int offset;
    ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> imageData;
    identity.__read(__is);
    __is->read(offset);
    __is->read(imageData);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = UploadImageFragment(identity, offset, imageData, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::PassingImageUploader::___UploadPassingVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::PassingVehicle vehicle;
    bool historyData;
    vehicle.__read(__is);
    __is->read(historyData);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = UploadPassingVehicle(vehicle, historyData, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::PassingImageUploader::___GetFragmentInfo(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::VehicleImageIdentity identity;
    identity.__read(__is);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::FragmentInfoSeq __ret = GetFragmentInfo(identity, __current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeFragmentInfoSeq(__os, &__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __LDTSSInterface__PassingImageUploader_all[] =
{
    "GetFragmentInfo",
    "UploadImageFragment",
    "UploadPassingVehicle",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

::Ice::DispatchStatus
LDTSSInterface::PassingImageUploader::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__LDTSSInterface__PassingImageUploader_all, __LDTSSInterface__PassingImageUploader_all + 7, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __LDTSSInterface__PassingImageUploader_all)
    {
        case 0:
        {
            return ___GetFragmentInfo(in, current);
        }
        case 1:
        {
            return ___UploadImageFragment(in, current);
        }
        case 2:
        {
            return ___UploadPassingVehicle(in, current);
        }
        case 3:
        {
            return ___ice_id(in, current);
        }
        case 4:
        {
            return ___ice_ids(in, current);
        }
        case 5:
        {
            return ___ice_isA(in, current);
        }
        case 6:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
LDTSSInterface::PassingImageUploader::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
LDTSSInterface::PassingImageUploader::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
LDTSSInterface::operator==(const ::LDTSSInterface::PassingImageUploader& l, const ::LDTSSInterface::PassingImageUploader& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
LDTSSInterface::operator<(const ::LDTSSInterface::PassingImageUploader& l, const ::LDTSSInterface::PassingImageUploader& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
LDTSSInterface::__patch__PassingImageUploaderPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::LDTSSInterface::PassingImageUploaderPtr* p = static_cast< ::LDTSSInterface::PassingImageUploaderPtr*>(__addr);
    assert(p);
    *p = ::LDTSSInterface::PassingImageUploaderPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::LDTSSInterface::PassingImageUploader::ice_staticId(), v->ice_id());
    }
}
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_PassingImageUploader_UploadImageFragment::__invoke(const ::LDTSSInterface::PassingImageUploaderPrx& __prx, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>& imageData, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__PassingImageUploader__UploadImageFragment_name);
        __prepare(__prx, __LDTSSInterface__PassingImageUploader__UploadImageFragment_name, ::Ice::Idempotent, __ctx);
        identity.__write(__os);
        __os->write(offset);
        __os->write(imageData.first, imageData.second);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_PassingImageUploader_UploadImageFragment::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_PassingImageUploader_UploadPassingVehicle::__invoke(const ::LDTSSInterface::PassingImageUploaderPrx& __prx, const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__PassingImageUploader__UploadPassingVehicle_name);
        __prepare(__prx, __LDTSSInterface__PassingImageUploader__UploadPassingVehicle_name, ::Ice::Idempotent, __ctx);
        vehicle.__write(__os);
        __os->write(historyData);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_PassingImageUploader_UploadPassingVehicle::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_PassingImageUploader_GetFragmentInfo::__invoke(const ::LDTSSInterface::PassingImageUploaderPrx& __prx, const ::LDTSSInterface::VehicleImageIdentity& identity, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__PassingImageUploader__GetFragmentInfo_name);
        __prepare(__prx, __LDTSSInterface__PassingImageUploader__GetFragmentInfo_name, ::Ice::Idempotent, __ctx);
        identity.__write(__os);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_PassingImageUploader_GetFragmentInfo::__response(bool __ok)
{
    ::LDTSSInterface::FragmentInfoSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::LDTSSInterface::__readFragmentInfoSeq(__is, __ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif

bool
IceProxy::LDTSSInterface::PassingImageUploader::UploadImageFragment(const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>& imageData, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__PassingImageUploader__UploadImageFragment_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__PassingImageUploader__UploadImageFragment_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                identity.__write(__os);
                __os->write(offset);
                __os->write(imageData.first, imageData.second);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::PassingImageUploader::UploadImageFragment_async(const ::LDTSSInterface::AMI_PassingImageUploader_UploadImageFragmentPtr& __cb, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>& imageData)
{
    return __cb->__invoke(this, identity, offset, imageData, 0);
}

bool
IceProxy::LDTSSInterface::PassingImageUploader::UploadImageFragment_async(const ::LDTSSInterface::AMI_PassingImageUploader_UploadImageFragmentPtr& __cb, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, const ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*>& imageData, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, identity, offset, imageData, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::PassingImageUploader::UploadPassingVehicle(const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__PassingImageUploader__UploadPassingVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__PassingImageUploader__UploadPassingVehicle_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                vehicle.__write(__os);
                __os->write(historyData);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::PassingImageUploader::UploadPassingVehicle_async(const ::LDTSSInterface::AMI_PassingImageUploader_UploadPassingVehiclePtr& __cb, const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData)
{
    return __cb->__invoke(this, vehicle, historyData, 0);
}

bool
IceProxy::LDTSSInterface::PassingImageUploader::UploadPassingVehicle_async(const ::LDTSSInterface::AMI_PassingImageUploader_UploadPassingVehiclePtr& __cb, const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, vehicle, historyData, &__ctx);
}
#endif

::LDTSSInterface::FragmentInfoSeq
IceProxy::LDTSSInterface::PassingImageUploader::GetFragmentInfo(const ::LDTSSInterface::VehicleImageIdentity& identity, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__PassingImageUploader__GetFragmentInfo_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__PassingImageUploader__GetFragmentInfo_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                identity.__write(__os);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::FragmentInfoSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::LDTSSInterface::__readFragmentInfoSeq(__is, __ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::PassingImageUploader::GetFragmentInfo_async(const ::LDTSSInterface::AMI_PassingImageUploader_GetFragmentInfoPtr& __cb, const ::LDTSSInterface::VehicleImageIdentity& identity)
{
    return __cb->__invoke(this, identity, 0);
}

bool
IceProxy::LDTSSInterface::PassingImageUploader::GetFragmentInfo_async(const ::LDTSSInterface::AMI_PassingImageUploader_GetFragmentInfoPtr& __cb, const ::LDTSSInterface::VehicleImageIdentity& identity, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, identity, &__ctx);
}
#endif

const ::std::string&
IceProxy::LDTSSInterface::PassingImageUploader::ice_staticId()
{
    return __LDTSSInterface__PassingImageUploader_ids[1];
}

::IceProxy::Ice::Object*
IceProxy::LDTSSInterface::PassingImageUploader::__newInstance() const
{
    return new PassingImageUploader;
}
