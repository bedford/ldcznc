// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `DeviceMonitor.ice'

#ifndef ____GeneratedCpp_DeviceMonitor_h__
#define ____GeneratedCpp_DeviceMonitor_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#ifdef ICEE_HAS_AMI
#   include <IceE/OutgoingAsync.h>
#endif
#include <IceE/UserExceptionFactory.h>
#include <IceE/FactoryTable.h>
#include <Base.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace LDTSSInterface
{

class DeviceMonitor;

}

}

namespace LDTSSInterface
{

class DeviceMonitor;
bool operator==(const DeviceMonitor&, const DeviceMonitor&);
bool operator<(const DeviceMonitor&, const DeviceMonitor&);

}

namespace IceInternal
{

::Ice::Object* upCast(::LDTSSInterface::DeviceMonitor*);
::IceProxy::Ice::Object* upCast(::IceProxy::LDTSSInterface::DeviceMonitor*);

}

namespace LDTSSInterface
{

typedef ::IceInternal::Handle< ::LDTSSInterface::DeviceMonitor> DeviceMonitorPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::LDTSSInterface::DeviceMonitor> DeviceMonitorPrx;

void __read(::IceInternal::BasicStream*, DeviceMonitorPrx&);
void __patch__DeviceMonitorPtr(void*, ::Ice::ObjectPtr&);

}

namespace LDTSSInterface
{

}

namespace LDTSSInterface
{

class DeviceMonitor : virtual public ::Ice::Object
{
public:

    typedef DeviceMonitorPrx ProxyType;
    typedef DeviceMonitorPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual void ReportDeviceStatus(const ::LDTSSInterface::SurveyDeviceStatus&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___ReportDeviceStatus(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void UploadDeviceFault(const ::LDTSSInterface::SurveyDeviceFault&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___UploadDeviceFault(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool RegisterDevice(const ::std::string&, const ::LDTSSInterface::SurveyDeviceAttribute&, const ::LDTSSInterface::DeviceRuntimeParams&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___RegisterDevice(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace LDTSSInterface
{
#ifdef ICEE_HAS_AMI

class AMI_DeviceMonitor_ReportDeviceStatus : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response() = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceMonitorPrx&, const ::LDTSSInterface::SurveyDeviceStatus&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceMonitor_ReportDeviceStatus> AMI_DeviceMonitor_ReportDeviceStatusPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceMonitor_UploadDeviceFault : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response() = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceMonitorPrx&, const ::LDTSSInterface::SurveyDeviceFault&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceMonitor_UploadDeviceFault> AMI_DeviceMonitor_UploadDeviceFaultPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_DeviceMonitor_RegisterDevice : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::DeviceMonitorPrx&, const ::std::string&, const ::LDTSSInterface::SurveyDeviceAttribute&, const ::LDTSSInterface::DeviceRuntimeParams&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_DeviceMonitor_RegisterDevice> AMI_DeviceMonitor_RegisterDevicePtr;
#endif

}

namespace IceProxy
{

namespace LDTSSInterface
{

class DeviceMonitor : virtual public ::IceProxy::Ice::Object
{
public:

    void ReportDeviceStatus(const ::LDTSSInterface::SurveyDeviceStatus& deviceStatus)
    {
        ReportDeviceStatus(deviceStatus, 0);
    }
    void ReportDeviceStatus(const ::LDTSSInterface::SurveyDeviceStatus& deviceStatus, const ::Ice::Context& __ctx)
    {
        ReportDeviceStatus(deviceStatus, &__ctx);
    }
    
private:

    void ReportDeviceStatus(const ::LDTSSInterface::SurveyDeviceStatus&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool ReportDeviceStatus_async(const ::LDTSSInterface::AMI_DeviceMonitor_ReportDeviceStatusPtr&, const ::LDTSSInterface::SurveyDeviceStatus&);
    bool ReportDeviceStatus_async(const ::LDTSSInterface::AMI_DeviceMonitor_ReportDeviceStatusPtr&, const ::LDTSSInterface::SurveyDeviceStatus&, const ::Ice::Context&);
    #endif

    void UploadDeviceFault(const ::LDTSSInterface::SurveyDeviceFault& fault)
    {
        UploadDeviceFault(fault, 0);
    }
    void UploadDeviceFault(const ::LDTSSInterface::SurveyDeviceFault& fault, const ::Ice::Context& __ctx)
    {
        UploadDeviceFault(fault, &__ctx);
    }
    
private:

    void UploadDeviceFault(const ::LDTSSInterface::SurveyDeviceFault&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool UploadDeviceFault_async(const ::LDTSSInterface::AMI_DeviceMonitor_UploadDeviceFaultPtr&, const ::LDTSSInterface::SurveyDeviceFault&);
    bool UploadDeviceFault_async(const ::LDTSSInterface::AMI_DeviceMonitor_UploadDeviceFaultPtr&, const ::LDTSSInterface::SurveyDeviceFault&, const ::Ice::Context&);
    #endif

    bool RegisterDevice(const ::std::string& deviceNo, const ::LDTSSInterface::SurveyDeviceAttribute& deviceAttribute, const ::LDTSSInterface::DeviceRuntimeParams& runtimeParams)
    {
        return RegisterDevice(deviceNo, deviceAttribute, runtimeParams, 0);
    }
    bool RegisterDevice(const ::std::string& deviceNo, const ::LDTSSInterface::SurveyDeviceAttribute& deviceAttribute, const ::LDTSSInterface::DeviceRuntimeParams& runtimeParams, const ::Ice::Context& __ctx)
    {
        return RegisterDevice(deviceNo, deviceAttribute, runtimeParams, &__ctx);
    }
    
private:

    bool RegisterDevice(const ::std::string&, const ::LDTSSInterface::SurveyDeviceAttribute&, const ::LDTSSInterface::DeviceRuntimeParams&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool RegisterDevice_async(const ::LDTSSInterface::AMI_DeviceMonitor_RegisterDevicePtr&, const ::std::string&, const ::LDTSSInterface::SurveyDeviceAttribute&, const ::LDTSSInterface::DeviceRuntimeParams&);
    bool RegisterDevice_async(const ::LDTSSInterface::AMI_DeviceMonitor_RegisterDevicePtr&, const ::std::string&, const ::LDTSSInterface::SurveyDeviceAttribute&, const ::LDTSSInterface::DeviceRuntimeParams&, const ::Ice::Context&);
    #endif
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_secure(bool __secure) const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_twoway() const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_oneway() const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_batchOneway() const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_datagram() const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_batchDatagram() const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceMonitor> ice_timeout(int __timeout) const
    {
        return dynamic_cast<DeviceMonitor*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
