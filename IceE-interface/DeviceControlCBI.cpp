#include <DeviceControlCBI.h>
#include "Adaptor.h"
#include "util.h"

LDTSSInterface::SurveyDeviceAttribute LDTSSInterface::DeviceControlCBI::GetAttribute(
                                        const Ice::Current&)
{
        return Adaptor::getAttribute();
}

bool LDTSSInterface::DeviceControlCBI::SetParam(const ::std::string&,
                                        const ::std::string&,
                                        const Ice::Current&)
{
        return false;
}

std::string LDTSSInterface::DeviceControlCBI::GetParam(
                                        const ::std::string&,
                                        const Ice::Current&)
{
        return std::string();
}

LDTSSInterface::StringStringDict LDTSSInterface::DeviceControlCBI::GetAllParams(
                                                const Ice::Current&)
{
        return LDTSSInterface::StringStringDict();
}

bool LDTSSInterface::DeviceControlCBI::Reset(const Ice::Current&)
{
        return Adaptor::resetRuntimeParams();
}

LDTSSInterface::StringStringDict LDTSSInterface::DeviceControlCBI::GetCameraAllParams(
                                                const std::string&,
                                                const Ice::Current&)
{
        StringStringDict allParams;
        return allParams;
}

bool LDTSSInterface::DeviceControlCBI::SetCameraAllParams(
                                        const std::string&,
                                        const LDTSSInterface::StringStringDict&,
                                        const Ice::Current&)
{
        return true;
}

LDTSSInterface::DeviceRuntimeParams LDTSSInterface::DeviceControlCBI::GetRuntimeParams(
                                        const Ice::Current&)
{
        return Adaptor::getRuntimeParams();
}

bool LDTSSInterface::DeviceControlCBI::SetRuntimeParams(
                                        const LDTSSInterface::DeviceRuntimeParams& params,
                                        const Ice::Current&)
{
        return Adaptor::setRuntimeParams(params);
}

LDTSSInterface::NetworkParams LDTSSInterface::DeviceControlCBI::GetNetworkParams(
                                        const Ice::Current&)
{
        return ::LDTSSInterface::NetworkParams();
}

bool LDTSSInterface::DeviceControlCBI::SetNetworkParams(
                                        const LDTSSInterface::NetworkParams&,
                                        const Ice::Current&)
{
        return false;
}

Ice::ByteSeq LDTSSInterface::DeviceControlCBI::ReadConfigFile(Ice::Int,
                                        const Ice::Current&)
{
        return Ice::ByteSeq();
}

bool LDTSSInterface::DeviceControlCBI::WriteConfigFile(Ice::Int,
                                        const Ice::ByteSeq&,
                                        const Ice::Current&)
{
        return false;
}

bool LDTSSInterface::DeviceControlCBI::ExecuteCommand(
                                        const std::string&,
                                        const std::string&,
                                        const Ice::Current&)
{
        return false;
}

bool LDTSSInterface::DeviceControlCBI::Reboot(const Ice::Current&)
{
        Adaptor::RebootSystem();
        return true;
}

bool LDTSSInterface::DeviceControlCBI::CalibrateTime(const std::string& time,
                                        const Ice::Current&)
{
        Util::CalibrateTime(time.c_str());
        return true;
}

LDTSSInterface::SurveyDeviceStatus LDTSSInterface::DeviceControlCBI::GetDeviceStatus(
                                        const Ice::Current&)
{
        return Adaptor::GetDeviceStatus();
}

Ice::ByteSeq LDTSSInterface::DeviceControlCBI::GetLog(Ice::Int,
                                        const Ice::Current&)
{
        return Ice::ByteSeq();
}

Ice::Int LDTSSInterface::DeviceControlCBI::Diagnostics(const Ice::Current&)
{
        return 0;
}

bool LDTSSInterface::DeviceControlCBI::OneShot(const std::string&,
                                        LDTSSInterface::VehicleImageDescriptionSeq& images,
                                        const Ice::Current&)
{
        struct VehicleImageDescription imageData;
        if (Adaptor::mannualShot(&imageData)) {
                images.push_back(imageData);
                return true;
        } else {
                return false;
        }
}

Ice::ByteSeq LDTSSInterface::DeviceControlCBI::ReadImage(const std::string&,
                                        const LDTSSInterface::VehicleImageIdentity& identity,
                                        Ice::Int, Ice::Int, const Ice::Current&)
{
        return Adaptor::readImage(identity);
}

Ice::ByteSeq LDTSSInterface::DeviceControlCBI::DownloadRecording(const std::string&,
                                        Ice::Int, Ice::Int, const Ice::Current&)
{
        return Ice::ByteSeq();
}

LDTSSInterface::PassingVehicleSeq LDTSSInterface::DeviceControlCBI::QueryVehicleByTime(
                                        const std::string&, const std::string&,
                                        Ice::Int, Ice::Int, const Ice::Current&)
{
        return LDTSSInterface::PassingVehicleSeq();
}

std::string LDTSSInterface::DeviceControlCBI::GetId(const Ice::Current&)
{
        return std::string();
}

std::string LDTSSInterface::DeviceControlCBI::GetCaCode(const Ice::Current&)
{
        return ::std::string();
}

bool LDTSSInterface::DeviceControlCBI::Certificate(const Ice::ByteSeq&, const Ice::Current&)
{
        return false;
}

bool LDTSSInterface::DeviceControlCBI::InspectVehicle(
                                        const LDTSSInterface::InspectedVehicle&,
                                        bool, const Ice::Current&)
{
        return false;
}

bool LDTSSInterface::DeviceControlCBI::CancelVehicle(const std::string&,
                                        const std::string&, const std::string&,
                                        const std::string&, const Ice::Current&)
{
        return false;
}

LDTSSInterface::InspectedVehicleSeq LDTSSInterface::DeviceControlCBI::QueryVehicle(
                                        const std::string&, const std::string&,
                                        const Ice::Current&)
{
        return LDTSSInterface::InspectedVehicleSeq();
}

bool LDTSSInterface::DeviceControlCBI::BatchInspectedVehicle(
                                        const LDTSSInterface::InspectedVehicleSeq&,
                                        const Ice::Current&)
{
        return false;
}

LDTSSInterface::InspectedVehicleSeq LDTSSInterface::DeviceControlCBI::ListVehicle(
                                        const std::string&, Ice::Int,
                                        Ice::Int, const Ice::Current&)
{
        return LDTSSInterface::InspectedVehicleSeq();
}

::Ice::Int LDTSSInterface::DeviceControlCBI::CountVehicle(
                                        const std::string&, const Ice::Current&)
{
        return 0;
}

bool LDTSSInterface::DeviceControlCBI::AddVagueData(const Ice::StringSeq&,
                                        const Ice::Current&)
{
        return false;
}

Ice::StringSeq LDTSSInterface::DeviceControlCBI::QueryVagueData(const Ice::Current&)
{
        return Ice::StringSeq();
}

