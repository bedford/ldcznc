// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `DevicePrxReport.ice'

#include <DevicePrxReport.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/LocalException.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __LDTSSInterface__DeviceCtrlPrxReport__Report_name = "Report";

::Ice::Object* IceInternal::upCast(::LDTSSInterface::DeviceCtrlPrxReport* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::LDTSSInterface::DeviceCtrlPrxReport* p) { return p; }

void
LDTSSInterface::__read(::IceInternal::BasicStream* __is, ::LDTSSInterface::DeviceCtrlPrxReportPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::LDTSSInterface::DeviceCtrlPrxReport;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __LDTSSInterface__DeviceCtrlPrxReport_ids[2] =
{
    "::Ice::Object",
    "::LDTSSInterface::DeviceCtrlPrxReport"
};

bool
LDTSSInterface::DeviceCtrlPrxReport::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__LDTSSInterface__DeviceCtrlPrxReport_ids, __LDTSSInterface__DeviceCtrlPrxReport_ids + 2, _s);
}

::std::vector< ::std::string>
LDTSSInterface::DeviceCtrlPrxReport::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__LDTSSInterface__DeviceCtrlPrxReport_ids[0], &__LDTSSInterface__DeviceCtrlPrxReport_ids[2]);
}

const ::std::string&
LDTSSInterface::DeviceCtrlPrxReport::ice_id(const ::Ice::Current&) const
{
    return __LDTSSInterface__DeviceCtrlPrxReport_ids[1];
}

const ::std::string&
LDTSSInterface::DeviceCtrlPrxReport::ice_staticId()
{
    return __LDTSSInterface__DeviceCtrlPrxReport_ids[1];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceCtrlPrxReport::___Report(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string deviceNo;
    ::LDTSSInterface::DeviceControlCBPrx ctrlCallback;
    __is->read(deviceNo);
    ::LDTSSInterface::__read(__is, ctrlCallback);
    Report(deviceNo, ctrlCallback, __current);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __LDTSSInterface__DeviceCtrlPrxReport_all[] =
{
    "Report",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

::Ice::DispatchStatus
LDTSSInterface::DeviceCtrlPrxReport::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__LDTSSInterface__DeviceCtrlPrxReport_all, __LDTSSInterface__DeviceCtrlPrxReport_all + 5, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __LDTSSInterface__DeviceCtrlPrxReport_all)
    {
        case 0:
        {
            return ___Report(in, current);
        }
        case 1:
        {
            return ___ice_id(in, current);
        }
        case 2:
        {
            return ___ice_ids(in, current);
        }
        case 3:
        {
            return ___ice_isA(in, current);
        }
        case 4:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
LDTSSInterface::DeviceCtrlPrxReport::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
LDTSSInterface::DeviceCtrlPrxReport::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
LDTSSInterface::operator==(const ::LDTSSInterface::DeviceCtrlPrxReport& l, const ::LDTSSInterface::DeviceCtrlPrxReport& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
LDTSSInterface::operator<(const ::LDTSSInterface::DeviceCtrlPrxReport& l, const ::LDTSSInterface::DeviceCtrlPrxReport& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
LDTSSInterface::__patch__DeviceCtrlPrxReportPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::LDTSSInterface::DeviceCtrlPrxReportPtr* p = static_cast< ::LDTSSInterface::DeviceCtrlPrxReportPtr*>(__addr);
    assert(p);
    *p = ::LDTSSInterface::DeviceCtrlPrxReportPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::LDTSSInterface::DeviceCtrlPrxReport::ice_staticId(), v->ice_id());
    }
}

void
IceProxy::LDTSSInterface::DeviceCtrlPrxReport::Report(const ::std::string& deviceNo, const ::LDTSSInterface::DeviceControlCBPrx& ctrlCallback, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceCtrlPrxReport__Report_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(deviceNo);
                __os->write(::Ice::ObjectPrx(::IceInternal::upCast(ctrlCallback.get())));
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}

const ::std::string&
IceProxy::LDTSSInterface::DeviceCtrlPrxReport::ice_staticId()
{
    return __LDTSSInterface__DeviceCtrlPrxReport_ids[1];
}

::IceProxy::Ice::Object*
IceProxy::LDTSSInterface::DeviceCtrlPrxReport::__newInstance() const
{
    return new DeviceCtrlPrxReport;
}
