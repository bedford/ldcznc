// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `SurveyDataUploader.ice'

#include <SurveyDataUploader.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/LocalException.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __LDTSSInterface__SurveyDataUploader__UploadPassingVehicle_name = "UploadPassingVehicle";

static const ::std::string __LDTSSInterface__SurveyDataUploader__UploadTrafficStats_name = "UploadTrafficStats";

static const ::std::string __LDTSSInterface__SurveyDataUploader__UploadViolationVehicle_name = "UploadViolationVehicle";

::Ice::Object* IceInternal::upCast(::LDTSSInterface::SurveyDataUploader* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::LDTSSInterface::SurveyDataUploader* p) { return p; }

void
LDTSSInterface::__read(::IceInternal::BasicStream* __is, ::LDTSSInterface::SurveyDataUploaderPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::LDTSSInterface::SurveyDataUploader;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __LDTSSInterface__SurveyDataUploader_ids[2] =
{
    "::Ice::Object",
    "::LDTSSInterface::SurveyDataUploader"
};

bool
LDTSSInterface::SurveyDataUploader::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__LDTSSInterface__SurveyDataUploader_ids, __LDTSSInterface__SurveyDataUploader_ids + 2, _s);
}

::std::vector< ::std::string>
LDTSSInterface::SurveyDataUploader::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__LDTSSInterface__SurveyDataUploader_ids[0], &__LDTSSInterface__SurveyDataUploader_ids[2]);
}

const ::std::string&
LDTSSInterface::SurveyDataUploader::ice_id(const ::Ice::Current&) const
{
    return __LDTSSInterface__SurveyDataUploader_ids[1];
}

const ::std::string&
LDTSSInterface::SurveyDataUploader::ice_staticId()
{
    return __LDTSSInterface__SurveyDataUploader_ids[1];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::SurveyDataUploader::___UploadPassingVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::PassingVehicle vehicle;
    bool historyData;
    vehicle.__read(__is);
    __is->read(historyData);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = UploadPassingVehicle(vehicle, historyData, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::SurveyDataUploader::___UploadTrafficStats(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::TrafficStats stats;
    bool historyData;
    stats.__read(__is);
    __is->read(historyData);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = UploadTrafficStats(stats, historyData, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::SurveyDataUploader::___UploadViolationVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::ViolationVehicle vehicle;
    bool historyData;
    vehicle.__read(__is);
    __is->read(historyData);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = UploadViolationVehicle(vehicle, historyData, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __LDTSSInterface__SurveyDataUploader_all[] =
{
    "UploadPassingVehicle",
    "UploadTrafficStats",
    "UploadViolationVehicle",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

::Ice::DispatchStatus
LDTSSInterface::SurveyDataUploader::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__LDTSSInterface__SurveyDataUploader_all, __LDTSSInterface__SurveyDataUploader_all + 7, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __LDTSSInterface__SurveyDataUploader_all)
    {
        case 0:
        {
            return ___UploadPassingVehicle(in, current);
        }
        case 1:
        {
            return ___UploadTrafficStats(in, current);
        }
        case 2:
        {
            return ___UploadViolationVehicle(in, current);
        }
        case 3:
        {
            return ___ice_id(in, current);
        }
        case 4:
        {
            return ___ice_ids(in, current);
        }
        case 5:
        {
            return ___ice_isA(in, current);
        }
        case 6:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
LDTSSInterface::SurveyDataUploader::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
LDTSSInterface::SurveyDataUploader::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
LDTSSInterface::operator==(const ::LDTSSInterface::SurveyDataUploader& l, const ::LDTSSInterface::SurveyDataUploader& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
LDTSSInterface::operator<(const ::LDTSSInterface::SurveyDataUploader& l, const ::LDTSSInterface::SurveyDataUploader& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
LDTSSInterface::__patch__SurveyDataUploaderPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::LDTSSInterface::SurveyDataUploaderPtr* p = static_cast< ::LDTSSInterface::SurveyDataUploaderPtr*>(__addr);
    assert(p);
    *p = ::LDTSSInterface::SurveyDataUploaderPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::LDTSSInterface::SurveyDataUploader::ice_staticId(), v->ice_id());
    }
}
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_SurveyDataUploader_UploadPassingVehicle::__invoke(const ::LDTSSInterface::SurveyDataUploaderPrx& __prx, const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__SurveyDataUploader__UploadPassingVehicle_name);
        __prepare(__prx, __LDTSSInterface__SurveyDataUploader__UploadPassingVehicle_name, ::Ice::Idempotent, __ctx);
        vehicle.__write(__os);
        __os->write(historyData);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_SurveyDataUploader_UploadPassingVehicle::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_SurveyDataUploader_UploadTrafficStats::__invoke(const ::LDTSSInterface::SurveyDataUploaderPrx& __prx, const ::LDTSSInterface::TrafficStats& stats, bool historyData, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__SurveyDataUploader__UploadTrafficStats_name);
        __prepare(__prx, __LDTSSInterface__SurveyDataUploader__UploadTrafficStats_name, ::Ice::Idempotent, __ctx);
        stats.__write(__os);
        __os->write(historyData);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_SurveyDataUploader_UploadTrafficStats::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_SurveyDataUploader_UploadViolationVehicle::__invoke(const ::LDTSSInterface::SurveyDataUploaderPrx& __prx, const ::LDTSSInterface::ViolationVehicle& vehicle, bool historyData, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__SurveyDataUploader__UploadViolationVehicle_name);
        __prepare(__prx, __LDTSSInterface__SurveyDataUploader__UploadViolationVehicle_name, ::Ice::Idempotent, __ctx);
        vehicle.__write(__os);
        __os->write(historyData);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_SurveyDataUploader_UploadViolationVehicle::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadPassingVehicle(const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__SurveyDataUploader__UploadPassingVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__SurveyDataUploader__UploadPassingVehicle_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                vehicle.__write(__os);
                __os->write(historyData);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadPassingVehicle_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadPassingVehiclePtr& __cb, const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData)
{
    return __cb->__invoke(this, vehicle, historyData, 0);
}

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadPassingVehicle_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadPassingVehiclePtr& __cb, const ::LDTSSInterface::PassingVehicle& vehicle, bool historyData, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, vehicle, historyData, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadTrafficStats(const ::LDTSSInterface::TrafficStats& stats, bool historyData, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__SurveyDataUploader__UploadTrafficStats_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__SurveyDataUploader__UploadTrafficStats_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                stats.__write(__os);
                __os->write(historyData);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadTrafficStats_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadTrafficStatsPtr& __cb, const ::LDTSSInterface::TrafficStats& stats, bool historyData)
{
    return __cb->__invoke(this, stats, historyData, 0);
}

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadTrafficStats_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadTrafficStatsPtr& __cb, const ::LDTSSInterface::TrafficStats& stats, bool historyData, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, stats, historyData, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadViolationVehicle(const ::LDTSSInterface::ViolationVehicle& vehicle, bool historyData, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__SurveyDataUploader__UploadViolationVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__SurveyDataUploader__UploadViolationVehicle_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                vehicle.__write(__os);
                __os->write(historyData);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadViolationVehicle_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadViolationVehiclePtr& __cb, const ::LDTSSInterface::ViolationVehicle& vehicle, bool historyData)
{
    return __cb->__invoke(this, vehicle, historyData, 0);
}

bool
IceProxy::LDTSSInterface::SurveyDataUploader::UploadViolationVehicle_async(const ::LDTSSInterface::AMI_SurveyDataUploader_UploadViolationVehiclePtr& __cb, const ::LDTSSInterface::ViolationVehicle& vehicle, bool historyData, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, vehicle, historyData, &__ctx);
}
#endif

const ::std::string&
IceProxy::LDTSSInterface::SurveyDataUploader::ice_staticId()
{
    return __LDTSSInterface__SurveyDataUploader_ids[1];
}

::IceProxy::Ice::Object*
IceProxy::LDTSSInterface::SurveyDataUploader::__newInstance() const
{
    return new SurveyDataUploader;
}
