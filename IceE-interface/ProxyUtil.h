//*********************************************************************************
//文件作者:zhouw
//邮箱地址: zw@landuntec.com
//添加时间: 2011-07-26
//功能描述: 提供获取各种上传理功具函数，所有方法为静态方法， 类不可实例化
//*********************************************************************************

#ifndef LD_PROXY_UTIL_H
#define LD_PROXY_UTIL_H

#include <SurveyDataUploader.h>
#include <ViolationImageUploader.h>
#include <PassingImageUploader.h>
#include <DeviceMonitor.h>
#include <DevicePrxReport.h>

//管理对象代理
class ProxyUtil
{
public:
        static LDTSSInterface::SurveyDataUploaderPrx GetSurveyDataUploaderPrx(
                        const Ice::CommunicatorPtr& communicator);

        static LDTSSInterface::ViolationImageUploaderPrx GetViolationImageUploaderPrx(
                        const Ice::CommunicatorPtr& communicator);

        static LDTSSInterface::PassingImageUploaderPrx GetPassingImageUploaderPrx(
                        const Ice::CommunicatorPtr& communicator);

        static LDTSSInterface::DeviceMonitorPrx GetDeviceMonitorPrx(
                        const Ice::CommunicatorPtr& communicator);

        static LDTSSInterface::DeviceCtrlPrxReportPrx GetDeviceCtrlPrxReportPrx(
                        const Ice::CommunicatorPtr& communicator);

private:
        ProxyUtil(void);
        ~ProxyUtil(void);
};

#endif

