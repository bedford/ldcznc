// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `DevicePrxReport.ice'

#ifndef __test_DevicePrxReport_h__
#define __test_DevicePrxReport_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#ifdef ICEE_HAS_AMI
#   include <IceE/OutgoingAsync.h>
#endif
#include <IceE/UserExceptionFactory.h>
#include <IceE/FactoryTable.h>
#include <DeviceControlCB.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace LDTSSInterface
{

class DeviceCtrlPrxReport;

}

}

namespace LDTSSInterface
{

class DeviceCtrlPrxReport;
bool operator==(const DeviceCtrlPrxReport&, const DeviceCtrlPrxReport&);
bool operator<(const DeviceCtrlPrxReport&, const DeviceCtrlPrxReport&);

}

namespace IceInternal
{

::Ice::Object* upCast(::LDTSSInterface::DeviceCtrlPrxReport*);
::IceProxy::Ice::Object* upCast(::IceProxy::LDTSSInterface::DeviceCtrlPrxReport*);

}

namespace LDTSSInterface
{

typedef ::IceInternal::Handle< ::LDTSSInterface::DeviceCtrlPrxReport> DeviceCtrlPrxReportPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::LDTSSInterface::DeviceCtrlPrxReport> DeviceCtrlPrxReportPrx;

void __read(::IceInternal::BasicStream*, DeviceCtrlPrxReportPrx&);
void __patch__DeviceCtrlPrxReportPtr(void*, ::Ice::ObjectPtr&);

}

namespace LDTSSInterface
{

}

namespace LDTSSInterface
{

class DeviceCtrlPrxReport : virtual public ::Ice::Object
{
public:

    typedef DeviceCtrlPrxReportPrx ProxyType;
    typedef DeviceCtrlPrxReportPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual void Report(const ::std::string&, const ::LDTSSInterface::DeviceControlCBPrx&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___Report(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace IceProxy
{

namespace LDTSSInterface
{

class DeviceCtrlPrxReport : virtual public ::IceProxy::Ice::Object
{
public:

    void Report(const ::std::string& deviceNo, const ::LDTSSInterface::DeviceControlCBPrx& ctrlCallback)
    {
        Report(deviceNo, ctrlCallback, 0);
    }
    void Report(const ::std::string& deviceNo, const ::LDTSSInterface::DeviceControlCBPrx& ctrlCallback, const ::Ice::Context& __ctx)
    {
        Report(deviceNo, ctrlCallback, &__ctx);
    }
    
private:

    void Report(const ::std::string&, const ::LDTSSInterface::DeviceControlCBPrx&, const ::Ice::Context*);
    
public:
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_secure(bool __secure) const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_twoway() const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_oneway() const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_batchOneway() const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_datagram() const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_batchDatagram() const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceCtrlPrxReport> ice_timeout(int __timeout) const
    {
        return dynamic_cast<DeviceCtrlPrxReport*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
