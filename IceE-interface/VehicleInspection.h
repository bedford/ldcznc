// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `VehicleInspection.ice'

#ifndef __test_VehicleInspection_h__
#define __test_VehicleInspection_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#ifdef ICEE_HAS_AMI
#   include <IceE/OutgoingAsync.h>
#endif
#include <IceE/UserExceptionFactory.h>
#include <IceE/FactoryTable.h>
#include <Base.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace LDTSSInterface
{

class VehicleInspection;

}

}

namespace LDTSSInterface
{

class VehicleInspection;
bool operator==(const VehicleInspection&, const VehicleInspection&);
bool operator<(const VehicleInspection&, const VehicleInspection&);

}

namespace IceInternal
{

::Ice::Object* upCast(::LDTSSInterface::VehicleInspection*);
::IceProxy::Ice::Object* upCast(::IceProxy::LDTSSInterface::VehicleInspection*);

}

namespace LDTSSInterface
{

typedef ::IceInternal::Handle< ::LDTSSInterface::VehicleInspection> VehicleInspectionPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::LDTSSInterface::VehicleInspection> VehicleInspectionPrx;

void __read(::IceInternal::BasicStream*, VehicleInspectionPrx&);
void __patch__VehicleInspectionPtr(void*, ::Ice::ObjectPtr&);

}

namespace LDTSSInterface
{

}

namespace LDTSSInterface
{

class VehicleInspection : virtual public ::Ice::Object
{
public:

    typedef VehicleInspectionPrx ProxyType;
    typedef VehicleInspectionPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual bool InspectVehicle(const ::LDTSSInterface::InspectedVehicle&, bool, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___InspectVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool CancelVehicle(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___CancelVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::InspectedVehicleSeq QueryVehicle(const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___QueryVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool BatchInspectedVehicle(const ::LDTSSInterface::InspectedVehicleSeq&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___BatchInspectedVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::LDTSSInterface::InspectedVehicleSeq ListVehicle(const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___ListVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::Ice::Int CountVehicle(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___CountVehicle(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual bool AddVagueData(const ::Ice::StringSeq&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___AddVagueData(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::Ice::StringSeq QueryVagueData(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___QueryVagueData(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace LDTSSInterface
{
#ifdef ICEE_HAS_AMI

class AMI_VehicleInspection_InspectVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::VehicleInspectionPrx&, const ::LDTSSInterface::InspectedVehicle&, bool, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_VehicleInspection_InspectVehicle> AMI_VehicleInspection_InspectVehiclePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_VehicleInspection_CancelVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::VehicleInspectionPrx&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_VehicleInspection_CancelVehicle> AMI_VehicleInspection_CancelVehiclePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_VehicleInspection_QueryVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::InspectedVehicleSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::VehicleInspectionPrx&, const ::std::string&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_VehicleInspection_QueryVehicle> AMI_VehicleInspection_QueryVehiclePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_VehicleInspection_BatchInspectedVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::VehicleInspectionPrx&, const ::LDTSSInterface::InspectedVehicleSeq&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_VehicleInspection_BatchInspectedVehicle> AMI_VehicleInspection_BatchInspectedVehiclePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_VehicleInspection_ListVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::LDTSSInterface::InspectedVehicleSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::VehicleInspectionPrx&, const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_VehicleInspection_ListVehicle> AMI_VehicleInspection_ListVehiclePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_VehicleInspection_CountVehicle : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(::Ice::Int) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::VehicleInspectionPrx&, const ::std::string&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_VehicleInspection_CountVehicle> AMI_VehicleInspection_CountVehiclePtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_VehicleInspection_AddVagueData : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(bool) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::VehicleInspectionPrx&, const ::Ice::StringSeq&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_VehicleInspection_AddVagueData> AMI_VehicleInspection_AddVagueDataPtr;
#endif
#ifdef ICEE_HAS_AMI

class AMI_VehicleInspection_QueryVagueData : public ::IceInternal::OutgoingAsync
{
public:

    virtual void ice_response(const ::Ice::StringSeq&) = 0;
    virtual void ice_exception(const ::Ice::Exception&) = 0;

    bool __invoke(const ::LDTSSInterface::VehicleInspectionPrx&, const ::Ice::Context*);

protected:

    virtual void __response(bool);
};

typedef ::IceUtil::Handle< ::LDTSSInterface::AMI_VehicleInspection_QueryVagueData> AMI_VehicleInspection_QueryVagueDataPtr;
#endif

}

namespace IceProxy
{

namespace LDTSSInterface
{

class VehicleInspection : virtual public ::IceProxy::Ice::Object
{
public:

    bool InspectVehicle(const ::LDTSSInterface::InspectedVehicle& vehicle, bool override)
    {
        return InspectVehicle(vehicle, override, 0);
    }
    bool InspectVehicle(const ::LDTSSInterface::InspectedVehicle& vehicle, bool override, const ::Ice::Context& __ctx)
    {
        return InspectVehicle(vehicle, override, &__ctx);
    }
    
private:

    bool InspectVehicle(const ::LDTSSInterface::InspectedVehicle&, bool, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool InspectVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_InspectVehiclePtr&, const ::LDTSSInterface::InspectedVehicle&, bool);
    bool InspectVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_InspectVehiclePtr&, const ::LDTSSInterface::InspectedVehicle&, bool, const ::Ice::Context&);
    #endif

    bool CancelVehicle(const ::std::string& plateNbr, const ::std::string& plateColor, const ::std::string& inspectActione, const ::std::string& inspectDepartment)
    {
        return CancelVehicle(plateNbr, plateColor, inspectActione, inspectDepartment, 0);
    }
    bool CancelVehicle(const ::std::string& plateNbr, const ::std::string& plateColor, const ::std::string& inspectActione, const ::std::string& inspectDepartment, const ::Ice::Context& __ctx)
    {
        return CancelVehicle(plateNbr, plateColor, inspectActione, inspectDepartment, &__ctx);
    }
    
private:

    bool CancelVehicle(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool CancelVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_CancelVehiclePtr&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&);
    bool CancelVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_CancelVehiclePtr&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::InspectedVehicleSeq QueryVehicle(const ::std::string& plateNbr, const ::std::string& plateColor)
    {
        return QueryVehicle(plateNbr, plateColor, 0);
    }
    ::LDTSSInterface::InspectedVehicleSeq QueryVehicle(const ::std::string& plateNbr, const ::std::string& plateColor, const ::Ice::Context& __ctx)
    {
        return QueryVehicle(plateNbr, plateColor, &__ctx);
    }
    
private:

    ::LDTSSInterface::InspectedVehicleSeq QueryVehicle(const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool QueryVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_QueryVehiclePtr&, const ::std::string&, const ::std::string&);
    bool QueryVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_QueryVehiclePtr&, const ::std::string&, const ::std::string&, const ::Ice::Context&);
    #endif

    bool BatchInspectedVehicle(const ::LDTSSInterface::InspectedVehicleSeq& vehicleList)
    {
        return BatchInspectedVehicle(vehicleList, 0);
    }
    bool BatchInspectedVehicle(const ::LDTSSInterface::InspectedVehicleSeq& vehicleList, const ::Ice::Context& __ctx)
    {
        return BatchInspectedVehicle(vehicleList, &__ctx);
    }
    
private:

    bool BatchInspectedVehicle(const ::LDTSSInterface::InspectedVehicleSeq&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool BatchInspectedVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_BatchInspectedVehiclePtr&, const ::LDTSSInterface::InspectedVehicleSeq&);
    bool BatchInspectedVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_BatchInspectedVehiclePtr&, const ::LDTSSInterface::InspectedVehicleSeq&, const ::Ice::Context&);
    #endif

    ::LDTSSInterface::InspectedVehicleSeq ListVehicle(const ::std::string& inspectDepartment, ::Ice::Int pageSize, ::Ice::Int pageIndex)
    {
        return ListVehicle(inspectDepartment, pageSize, pageIndex, 0);
    }
    ::LDTSSInterface::InspectedVehicleSeq ListVehicle(const ::std::string& inspectDepartment, ::Ice::Int pageSize, ::Ice::Int pageIndex, const ::Ice::Context& __ctx)
    {
        return ListVehicle(inspectDepartment, pageSize, pageIndex, &__ctx);
    }
    
private:

    ::LDTSSInterface::InspectedVehicleSeq ListVehicle(const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool ListVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_ListVehiclePtr&, const ::std::string&, ::Ice::Int, ::Ice::Int);
    bool ListVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_ListVehiclePtr&, const ::std::string&, ::Ice::Int, ::Ice::Int, const ::Ice::Context&);
    #endif

    ::Ice::Int CountVehicle(const ::std::string& inspectDepartment)
    {
        return CountVehicle(inspectDepartment, 0);
    }
    ::Ice::Int CountVehicle(const ::std::string& inspectDepartment, const ::Ice::Context& __ctx)
    {
        return CountVehicle(inspectDepartment, &__ctx);
    }
    
private:

    ::Ice::Int CountVehicle(const ::std::string&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool CountVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_CountVehiclePtr&, const ::std::string&);
    bool CountVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_CountVehiclePtr&, const ::std::string&, const ::Ice::Context&);
    #endif

    bool AddVagueData(const ::Ice::StringSeq& dimData)
    {
        return AddVagueData(dimData, 0);
    }
    bool AddVagueData(const ::Ice::StringSeq& dimData, const ::Ice::Context& __ctx)
    {
        return AddVagueData(dimData, &__ctx);
    }
    
private:

    bool AddVagueData(const ::Ice::StringSeq&, const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool AddVagueData_async(const ::LDTSSInterface::AMI_VehicleInspection_AddVagueDataPtr&, const ::Ice::StringSeq&);
    bool AddVagueData_async(const ::LDTSSInterface::AMI_VehicleInspection_AddVagueDataPtr&, const ::Ice::StringSeq&, const ::Ice::Context&);
    #endif

    ::Ice::StringSeq QueryVagueData()
    {
        return QueryVagueData(0);
    }
    ::Ice::StringSeq QueryVagueData(const ::Ice::Context& __ctx)
    {
        return QueryVagueData(&__ctx);
    }
    
private:

    ::Ice::StringSeq QueryVagueData(const ::Ice::Context*);
    
public:
    #ifdef ICEE_HAS_AMI
    bool QueryVagueData_async(const ::LDTSSInterface::AMI_VehicleInspection_QueryVagueDataPtr&);
    bool QueryVagueData_async(const ::LDTSSInterface::AMI_VehicleInspection_QueryVagueDataPtr&, const ::Ice::Context&);
    #endif
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_secure(bool __secure) const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<VehicleInspection> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<VehicleInspection> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_twoway() const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_oneway() const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_batchOneway() const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_datagram() const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_batchDatagram() const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<VehicleInspection> ice_timeout(int __timeout) const
    {
        return dynamic_cast<VehicleInspection*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
