/**
 * @file Adaptor.cpp
 *
 * @brief       1.转换相机的数据结构至服务器定义的数据结构
 *              2.重新封装相应的LDIce类接口的调用
 *
 * @author      hrh
 * @version     1.0.0
 * @date        2011-07-23
 *
 * @verbatim
 * ==============================================================================
 *   Copyright (c) Shenzhen Landun technology Co.,Ltd. 2010
 *   All rights reserved.
 * 
 *   Use of this software is controlled by the terms and conditions found in the
 *   license agreement under which this software has been supplied or provided.
 * ==============================================================================
 * @endverbatim
 *
 */

#include "LDIceWrapper.h"
#include "DeviceControlCBI.h"
#include "Adaptor.h"
#include "offence_parse.h"
#include "parameters.h"
#include "snap_type.h"

#include <vector>
#include <map>

enum {
        SnapShotModePanarama = 0x01,
        SnapShotModeVehicleHead = 0x10,
        SnapShotModePlate = 0x100,
        SnapShotModeTail = 0x1001
};

/**
 * @brief       全局车流量信息，从启动至下一次重启的总量
 */
struct statsInfo {
        int vehicle_counts;
        int violation_counts;
        int overspeed_counts;
        int alarm_counts;
        int large_vehicle_counts;
        int small_vehicle_counts;
};

static struct statsInfo _statsInfo;
static SurveyDeviceStatus retDeviceStatus;
static string startTime;

struct _Violation_Code {
        offenceCode Code[15];
        int length;
};

static struct _Violation_Code _ViolationCode;

map < int, string > overSpeedOffenceCode;
map < int, string > otherOffenceCode;

bool Adaptor::ReleaseAdaptor()
{
        printf("cleaner clean ice source\n");
        return LDIceWrapper::GetInstance()->Release();
}

static void update_offence_code(void)
{
        int index = 0;
        for (; index < _ViolationCode.length; index++) {
                if (_ViolationCode.Code[index].offenceType == 0) {
                        overSpeedOffenceCode.insert(pair < int,
                                                    string >
                                                    (_ViolationCode.Code[index].overSpeedPercent,
                                                     _ViolationCode.Code[index].violationCode));
                } else {
                        otherOffenceCode.insert(pair < int,
                                                string > (_ViolationCode.Code[index].offenceType,
                                                          _ViolationCode.Code[index].
                                                          violationCode));
                }
        }
#define DEBUG
#ifdef DEBUG
        map < int, string >::iterator iter;
        for (iter = otherOffenceCode.begin(); iter != otherOffenceCode.end(); iter++) {
                printf("index :%d is %s\n", iter->first, iter->second.c_str());
        }

        for (iter = overSpeedOffenceCode.begin(); iter != overSpeedOffenceCode.end(); iter++) {
                printf("index :%d is %s\n", iter->first, iter->second.c_str());
        }
#endif
}

bool Adaptor::InitAdaptor()
{
        char tmp[64];
        struct timeval tv;
        gettimeofday(&tv, NULL);
        struct tm *pTm = localtime(&(tv.tv_sec));
        strftime(tmp, 64, "%Y-%m-%d %H:%M:%S", pTm);
        sprintf(tmp, "%s.%03d", tmp, (int)(tv.tv_usec / 1000));
        startTime = tmp;

        read_offence_code(_ViolationCode.Code, &_ViolationCode.length, "offence.txt");

        update_offence_code();

        DeviceInfo info = Parameters::GetInstance()->GetDeviceInfo();
        return LDIceWrapper::GetInstance()->Init(info.device_number);
}

vector < struct SpeedLimit >Adaptor::FillSpeedLimit(DeviceInfo * device_info)
{
        vector < struct SpeedLimit >ret;
        struct SpeedLimit speedLimit;

        speedLimit.vehicleType = "01";
        speedLimit.lane = 0;
        speedLimit.roadOverSpeedLimit = device_info->vehicle_speed_limit;
        speedLimit.roadUnderSpeedLimit = device_info->min_speed_limit;
        speedLimit.overSpeedMargin = device_info->capture_speed - device_info->vehicle_speed_limit;
        if (speedLimit.overSpeedMargin < 0) {
                speedLimit.overSpeedMargin = 0;
        }
        speedLimit.underSpeedMargin = 0;
        ret.push_back(speedLimit);

        speedLimit.vehicleType = "02";
        speedLimit.roadOverSpeedLimit = device_info->car_speed_limit;
        speedLimit.roadUnderSpeedLimit = device_info->min_speed_limit;
        speedLimit.overSpeedMargin = device_info->capture_speed - device_info->car_speed_limit;
        if (speedLimit.overSpeedMargin < 0) {
                speedLimit.overSpeedMargin = 0;
        }
        ret.push_back(speedLimit);

        speedLimit.vehicleType = "05";
        speedLimit.roadOverSpeedLimit = 0;
        speedLimit.roadUnderSpeedLimit = 0;
        speedLimit.overSpeedMargin = 0;
        ret.push_back(speedLimit);

        speedLimit.vehicleType = "00";
        speedLimit.roadOverSpeedLimit = device_info->car_speed_limit;
        speedLimit.roadUnderSpeedLimit = device_info->min_speed_limit;
        speedLimit.overSpeedMargin = device_info->capture_speed - device_info->car_speed_limit;
        if (speedLimit.overSpeedMargin < 0) {
                speedLimit.overSpeedMargin = 0;
        }
        ret.push_back(speedLimit);

        return ret;
}

void Adaptor::FillRuntimeParams(DeviceInfo * device_info,
                                TrafficParam * traffic_param,
                                UploadParam * upload_param, DeviceRuntimeParams * runtimeParams)
{
        runtimeParams->deviceNo = device_info->device_number;
        runtimeParams->roadCode = device_info->road_code;
        runtimeParams->roadName = device_info->site;
        runtimeParams->surveyUpDirection = device_info->direction_text;
        //runtimeParams.surveyDownDirection     = params->driveDirection;
        runtimeParams->department = device_info->department_code;
        runtimeParams->surveyServer = upload_param->upload_server;
        runtimeParams->trafficStatsPeriod = traffic_param->period;

        runtimeParams->redlightMargin.push_back(60);
        runtimeParams->vehicleLengthLevel.largeVehicleLengthLevel =
            traffic_param->large_vehicle_length;
        runtimeParams->vehicleLengthLevel.smallVehicleLengthLevel =
            traffic_param->small_vehicle_length;
        runtimeParams->vehicleLengthLevel.motorVehicleLengthLevel =
            traffic_param->motor_vehicle_length;
        runtimeParams->vehicleLengthLevel.longVehicleLengthLevel =
            traffic_param->long_vehicle_length;

        struct ViolationCode tmpCode;
        int index = 0;
        for (index = 0; index < _ViolationCode.length; index++) {
                tmpCode.violationType = _ViolationCode.Code[index].offenceType;
                tmpCode.roadSpeedLimit.lowerBound = _ViolationCode.Code[index].lowerBound;
                tmpCode.roadSpeedLimit.upperBound = _ViolationCode.Code[index].upperBound;
                tmpCode.overSpeedPercent = _ViolationCode.Code[index].overSpeedPercent;
                tmpCode.code = _ViolationCode.Code[index].violationCode;
                tmpCode.violationDesc = _ViolationCode.Code[index].violationDescription;

                runtimeParams->violationCodeTable.push_back(tmpCode);
        }
}

void Adaptor::FillDeviceAttribute(DeviceInfo * device_info,
                                  NetworkParam * network_param,
                                  SurveyDeviceAttribute * deviceAttribute)
{
        deviceAttribute->staticIpAddr = network_param->device_ip;
        deviceAttribute->manufactureId = device_info->serial_number;
        deviceAttribute->manufactor = "LDTEC";
        deviceAttribute->remoteControlPort = 35020;
        deviceAttribute->protocolType = "LDFixProcotol";
        if (device_info->device_mode == RedLight
                        || device_info->device_mode == RedLight_Reverse) {
                deviceAttribute->surveyMode = 6;
        } else {
                deviceAttribute->surveyMode = 1;
        }

        CameraInfo tmp_cameraInfo;
        tmp_cameraInfo.cameraId = device_info->serial_number;
        tmp_cameraInfo.ipAddr = network_param->device_ip;
        deviceAttribute->cameras.push_back(tmp_cameraInfo);

        deviceAttribute->extendedProperties["software_version"] = "1.0.0";
}

bool Adaptor::SendHeartBeat(DeviceStatus * device, DeviceParam * params)
{
        try {
                SurveyDeviceStatus deviceStatus;
                deviceStatus.status = device->status;
                deviceStatus.deviceNo = params->device_info->device_number;
                deviceStatus.deviceIpAddr = params->network_params->device_ip;
                deviceStatus.deviceTime = device->current_time;
                deviceStatus.startTime = startTime;

                char tmp[128];
                sprintf(tmp, "%s,%s", params->device_info->latitude,
                        params->device_info->longtitude);
                deviceStatus.gpsLocation = tmp;

                deviceStatus.statsInfo["vehicleTotal"] = _statsInfo.vehicle_counts;
                deviceStatus.statsInfo["underSpeedTotal"] = _statsInfo.violation_counts;
                deviceStatus.statsInfo["overSpeedTotal"] = _statsInfo.overspeed_counts;
                deviceStatus.statsInfo["suspectVehicles"] = _statsInfo.alarm_counts;
                deviceStatus.statsInfo["largeVehicleTotal"] = _statsInfo.large_vehicle_counts;
                deviceStatus.statsInfo["smallVehicleTotal"] = _statsInfo.small_vehicle_counts;

                retDeviceStatus = deviceStatus;
                return LDIceWrapper::GetInstance()->UploadDeviceStatus(deviceStatus);

        }
        catch(LDTSSInterface::DeviceNoFoundException) {
                printf("device not register, register it now\n");

                DeviceInfo *device_info = params->device_info;
                TrafficParam *traffic_param = params->traffic_params;
                UploadParam *upload_param = params->upload_params;
                NetworkParam *network_param = params->network_params;

                string deviceNo = device_info->device_number;
                SurveyDeviceAttribute deviceAttribute;
                DeviceRuntimeParams runtimeParams;

                FillDeviceAttribute(device_info, network_param, &deviceAttribute);
                FillRuntimeParams(device_info, traffic_param, upload_param, &runtimeParams);
                runtimeParams.speedLimits = FillSpeedLimit(device_info);
                return LDIceWrapper::GetInstance()->RegisterDevice(deviceNo, deviceAttribute,
                                                                   runtimeParams);
        }

        return false;
}

bool Adaptor::SendTrafficStatus(DeviceParam * params, TrafficStatus * stat)
{
        TrafficStats current_stat;

        current_stat.roadCode = params->device_info->road_code;
        current_stat.deviceNo = params->device_info->device_number;
        current_stat.driveDirection = params->device_info->direction_text;
        current_stat.statsTime = stat->end_time;
        current_stat.driveLane = stat->lane;
        current_stat.statsPeriod = params->traffic_params->period;
        current_stat.vehicleTotal = stat->total_num;
        current_stat.vehicleSpeed = stat->avg_speed;
        current_stat.vehicleLength = (float)stat->avg_len / 10;
        current_stat.timeOccupyRatio = stat->time_usage;
        current_stat.spaceOccupyRatio = stat->space_usage;
        current_stat.spaceHeadway = stat->carnum_per_meter;
        current_stat.timeHeadway = stat->carnum_per_second;
        current_stat.density = stat->carnum_per_kilometer;
        current_stat.overSpeedTotal = stat->highspeed_num;
        current_stat.underSpeedTotal = stat->lowspeed_num;
        current_stat.largeVehicleTotal = stat->bigcar_num;
        current_stat.middleVehicleTotal = stat->midcar_num;
        current_stat.smallVehicleTotal = stat->smallcar_num;
        current_stat.motorVehicleTotal = stat->motorcar_num;
        current_stat.longVehicleTotal = stat->hugecar_num;

        _statsInfo.vehicle_counts += stat->total_num;
        _statsInfo.violation_counts += stat->highspeed_num;
        _statsInfo.violation_counts += stat->lowspeed_num;
        _statsInfo.overspeed_counts += stat->highspeed_num;
        _statsInfo.alarm_counts = 0;
        _statsInfo.large_vehicle_counts += stat->hugecar_num;
        _statsInfo.large_vehicle_counts += stat->bigcar_num;
        _statsInfo.small_vehicle_counts += stat->smallcar_num;

        LDIceWrapper::GetInstance()->UploadTrafficStats(current_stat, false);
        printf("send traffic statics\n");
        return true;
}

void Adaptor::RebootSystem(void)
{
        DeviceStatus device_status = Parameters::GetInstance()->GetDeviceStatus();
        unsigned int status = device_status.status | REBOOT_SYSTEM;
        Parameters::GetInstance()->ChangeDeviceStatus(status);
}

unsigned int Adaptor::ConverOffenceType(unsigned int code)
{
        unsigned int offence_type = IceSnapType_NormalPass;

        switch (code) {
        case DeviceSnapType_LimitedPass:
                offence_type = IceSnapType_LimitedPass;
                break;
        case DeviceSnapType_RedLightPass:
                offence_type = IceSnapType_RedLightPass;
                break;
        case DeviceSnapType_HighSpeedPass:
                offence_type = IceSnapType_HighSpeedPass;
                break;
        case DeviceSnapType_ReversePass:
                offence_type = IceSnapType_ReversePass;
                break;
        case DeviceSnapType_NormalPass:
        default:
                offence_type = IceSnapType_NormalPass;
                break;
        }

        return offence_type;
}

PassingVehicle Adaptor::PacketPassingVehicleInfo(CaptureInfo * capture_info)
{
        PassingVehicle vehicle;
        TriggerInfo *trigger_info = capture_info->pTrigger_info;
        DeviceInfo *device_info = capture_info->pDevice_info;
        VehicleImageInfo *image_info = capture_info->pVehicle_image_info;

        vehicle.deviceNo = device_info->device_number;
        vehicle.roadCode = device_info->road_code;
        vehicle.driveDirection = device_info->direction_text;
        vehicle.snapNbr = trigger_info->snap_nbr;
        vehicle.lane = trigger_info->lane;
        vehicle.plateNbr = trigger_info->plate_nbr;
        //vehicle.plateType     = trigger_info->plate_type;
        vehicle.plateType = "";

        char tmp[128];
        sprintf(tmp, "%s,%s", device_info->latitude, device_info->longtitude);
        vehicle.gpsLocationInfo = tmp;

        struct tm *pTm = localtime(&(trigger_info->passing_time.tv_sec));
        strftime(tmp, 64, "%Y-%m-%d %H:%M:%S", pTm);
        sprintf(tmp, "%s.%03ld", tmp, trigger_info->passing_time.tv_usec / 1000);
        vehicle.captureTime = tmp;

        vehicle.vehicleSpeed = (float)trigger_info->vehicle_speed;
        vehicle.driveMode = ConverOffenceType(trigger_info->offence_type);
        vehicle.vehicleLength = (float)trigger_info->vehicle_length;
        vehicle.plateColor = trigger_info->plate_color;
        vehicle.vehicleType = trigger_info->vehicle_type;

        vehicle.extendedProperties["department"] = device_info->department_code;
        vehicle.extendedProperties["roadName"] = device_info->site;
        sprintf(tmp, "%d", image_info->frame);
        vehicle.extendedProperties["snap_frame"] = tmp;

        return vehicle;
}

bool Adaptor::UploadPassingVehicle(CaptureInfo * captureInfo, bool bImageInfo)
{
        TriggerInfo *trigger_info = captureInfo->pTrigger_info;
        PassingVehicle vehicle = PacketPassingVehicleInfo(captureInfo);

        struct VehicleImageDescription imageDescriptions = PacketImageInfo(captureInfo, 0);
        vehicle.imageDescriptions.push_back(imageDescriptions);

        vehicle.violationBehaviors = ViolationCodeCheck(trigger_info);

        vector < VehicleImage > images;
        if (bImageInfo) {
                VehicleImage sample;
                sample.imageDescription = imageDescriptions;
                sample.imageData.insert(sample.imageData.end(),
                                        captureInfo->pVehicle_image_info->vehicle_image[0].
                                        image_buffer,
                                        (captureInfo->pVehicle_image_info->vehicle_image[0].
                                         image_buffer +
                                         captureInfo->pVehicle_image_info->vehicle_image[0].
                                         image_length));
                images.push_back(sample);
        }

        return LDIceWrapper::GetInstance()->UploadPassingVehicle(vehicle, images, false);
}

vector < string > Adaptor::ViolationCodeCheck(TriggerInfo * trigger_info)
{
        vector < string > ret;
        map < int, string >::iterator iter;
        map < int, string >::iterator lower;
        unsigned int offence_type = ConverOffenceType(trigger_info->offence_type);

        if (offence_type == IceSnapType_HighSpeedPass) {
                iter = overSpeedOffenceCode.begin();
                iter++;
                lower = overSpeedOffenceCode.begin();
                for (; iter != overSpeedOffenceCode.end(); iter++, lower++) {
                        if (trigger_info->over_speed_percent < iter->first &&
                            trigger_info->over_speed_percent > lower->first) {
                                ret.push_back(lower->second);
                                break;
                        }
                }
                //如果超过所有上限，对应的违法代码为最后一个map对应的代码
                if (iter == overSpeedOffenceCode.end()) {
                        ret.push_back(lower->second);
                }

        } else {

                iter = otherOffenceCode.find(offence_type);
                if (iter != otherOffenceCode.end()) {
                        ret.push_back(iter->second);
                }
        }

        return ret;
}

struct VehicleImageDescription Adaptor::PacketImageInfo(CaptureInfo * capture_info, int index)
{
        TriggerInfo *trigger_info = capture_info->pTrigger_info;
        VehicleImageInfo *image_info = capture_info->pVehicle_image_info;
        DeviceInfo *device_info = capture_info->pDevice_info;

        struct VehicleImageDescription imageDescriptions;
        imageDescriptions.imageIdentity.deviceNo = device_info->device_number;
        imageDescriptions.imageIdentity.snapNbr = trigger_info->snap_nbr;
        imageDescriptions.snapShotMode = SnapShotModePlate;
        imageDescriptions.format = Jpeg;

        char guid[128];
        sprintf(guid, "%s_%s%d", device_info->device_number, trigger_info->snap_nbr, index);
        imageDescriptions.imageIdentity.guid = guid;

        char tmp[128];
        struct tm *pTm = localtime(&(image_info->vehicle_image[index].capture_time.tv_sec));
        strftime(tmp, 64, "%Y-%m-%d %H:%M:%S", pTm);
        sprintf(tmp, "%s.%03ld", tmp, image_info->vehicle_image[index].capture_time.tv_usec / 1000);

        imageDescriptions.frameIndex = index;
        imageDescriptions.timeStamp = tmp;
        imageDescriptions.imageWidth = image_info->vehicle_image[index].image_width;
        imageDescriptions.imageHeight = image_info->vehicle_image[index].image_height;
        imageDescriptions.imageDataLength = image_info->vehicle_image[index].image_length;

        return imageDescriptions;
}

ViolationVehicle Adaptor::PacketViolationVehicleInfo(CaptureInfo * capture_info)
{
        ViolationVehicle vehicle;
        TriggerInfo *trigger_info = capture_info->pTrigger_info;
        DeviceInfo *device_info = capture_info->pDevice_info;
        VehicleImageInfo *image_info = capture_info->pVehicle_image_info;

        char tmp[128];
        sprintf(tmp, "%s,%s", device_info->latitude, device_info->longtitude);
        vehicle.gpsLocationInfo = tmp;

        struct tm *pTm = localtime(&(trigger_info->passing_time.tv_sec));
        strftime(tmp, 64, "%Y-%m-%d %H:%M:%S", pTm);
        sprintf(tmp, "%s.%03ld", tmp, trigger_info->passing_time.tv_usec / 1000);
        vehicle.captureTime = tmp;

        vehicle.deviceNo = device_info->device_number;
        vehicle.roadCode = device_info->road_code;
        vehicle.driveDirection = device_info->direction_text;
        vehicle.snapNbr = trigger_info->snap_nbr;
        vehicle.lane = trigger_info->lane;
        vehicle.plateNbr = trigger_info->plate_nbr;
        //vehicle.plateType     = trigger_info->plate_type;
        vehicle.plateType = "";

        vehicle.vehicleSpeed = (float)trigger_info->vehicle_speed;
        vehicle.plateColor = trigger_info->plate_color;
        vehicle.vehicleType = trigger_info->vehicle_type;

        vehicle.extendedProperties["department"] = device_info->department_code;
        vehicle.extendedProperties["roadName"] = device_info->site;
        sprintf(tmp, "%d", image_info->frame);
        vehicle.extendedProperties["snap_frame"] = tmp;

        return vehicle;
}

bool Adaptor::UploadViolationVehicle(CaptureInfo * captureInfo, bool bImageInfo)
{
        ViolationVehicle vehicle = PacketViolationVehicleInfo(captureInfo);
        TriggerInfo *trigger_info = captureInfo->pTrigger_info;
        VehicleImageInfo *image_info = captureInfo->pVehicle_image_info;
        DeviceInfo *device_info = captureInfo->pDevice_info;

        vehicle.roadSpeedLimits = FillSpeedLimit(device_info);
        vehicle.violationBehaviors = ViolationCodeCheck(trigger_info);

        vector < VehicleImage > images;
        struct VehicleImageDescription imageDescriptions;

        for (unsigned int i = 0; i < image_info->frame; i++) {
                imageDescriptions = PacketImageInfo(captureInfo, i);
                vehicle.imageDescriptions.push_back(imageDescriptions);

                if (bImageInfo) {
                        VehicleImage sample;
                        sample.imageDescription = imageDescriptions;
                        sample.imageData.insert(sample.imageData.end(),
                                                image_info->vehicle_image[i].image_buffer,
                                                image_info->vehicle_image[i].image_buffer +
                                                image_info->vehicle_image[i].image_length);
                        images.push_back(sample);
                }
        }

        LDIceWrapper::GetInstance()->UploadViolationVehicle(vehicle, images, false);
        return true;
}

SurveyDeviceStatus Adaptor::GetDeviceStatus(void)
{
        return retDeviceStatus;
}

SurveyDeviceAttribute Adaptor::getAttribute(void)
{
        DeviceInfo info = Parameters::GetInstance()->GetDeviceInfo();
        NetworkParam network_param = Parameters::GetInstance()->GetNetworkParam();

        SurveyDeviceAttribute deviceAttribute;
        deviceAttribute.staticIpAddr = network_param.device_ip;
        deviceAttribute.manufactureId = info.serial_number;
        deviceAttribute.manufactor = "LDTEC";

        if (info.device_mode == RedLight
                        || info.device_mode == RedLight_Reverse) {
                deviceAttribute.surveyMode = 6;
        } else {
                deviceAttribute.surveyMode = 1;
        }

        struct CameraInfo cameraInfo;
        cameraInfo.cameraId = info.serial_number;
        cameraInfo.ipAddr = network_param.device_ip;
        deviceAttribute.cameras.push_back(cameraInfo);

        return deviceAttribute;
}

DeviceRuntimeParams Adaptor::getRuntimeParams(void)
{
        DeviceInfo info = Parameters::GetInstance()->GetDeviceInfo();
        TrafficParam traffic_param = Parameters::GetInstance()->GetTrafficParam();
        NetworkParam network_param = Parameters::GetInstance()->GetNetworkParam();
        UploadParam upload_param = Parameters::GetInstance()->GetUploadParam();

        DeviceRuntimeParams runtimeParams;

        FillRuntimeParams(&info, &traffic_param, &upload_param, &runtimeParams);
        runtimeParams.speedLimits = FillSpeedLimit(&info);

        struct ViolationCode tmpCode;
        int index = 0;
        for (; index < _ViolationCode.length; index++) {
                tmpCode.violationType = _ViolationCode.Code[index].offenceType;
                tmpCode.roadSpeedLimit.lowerBound = (float)_ViolationCode.Code[index].lowerBound;
                tmpCode.roadSpeedLimit.upperBound = (float)_ViolationCode.Code[index].upperBound;
                tmpCode.overSpeedPercent = _ViolationCode.Code[index].overSpeedPercent;
                tmpCode.code = _ViolationCode.Code[index].violationCode;
                tmpCode.violationDesc = _ViolationCode.Code[index].violationDescription;

                runtimeParams.violationCodeTable.push_back(tmpCode);
        }

        return runtimeParams;
}

bool Adaptor::setRuntimeParams(DeviceRuntimeParams runtimeParams)
{
        DeviceInfo device_info = Parameters::GetInstance()->GetDeviceInfo();
        TrafficParam traffic_param = Parameters::GetInstance()->GetTrafficParam();
        UploadParam upload_param = Parameters::GetInstance()->GetUploadParam();

        memcpy(device_info.device_number, runtimeParams.deviceNo.c_str(),
               sizeof(device_info.device_number));
        memcpy(device_info.site, runtimeParams.roadName.c_str(), sizeof(device_info.site));
        memcpy(device_info.road_code, runtimeParams.roadCode.c_str(),
               sizeof(device_info.road_code));
        memcpy(device_info.direction_text, runtimeParams.surveyUpDirection.c_str(),
               sizeof(device_info.direction_text));
        memcpy(device_info.department_code, runtimeParams.department.c_str(),
               sizeof(device_info.department_code));

        memcpy(upload_param.upload_server, runtimeParams.surveyServer.c_str(),
               sizeof(upload_param.upload_server));

        traffic_param.period = runtimeParams.trafficStatsPeriod;
        traffic_param.large_vehicle_length =
            runtimeParams.vehicleLengthLevel.largeVehicleLengthLevel;
        traffic_param.small_vehicle_length =
            runtimeParams.vehicleLengthLevel.smallVehicleLengthLevel;
        traffic_param.motor_vehicle_length =
            runtimeParams.vehicleLengthLevel.motorVehicleLengthLevel;
        traffic_param.long_vehicle_length = runtimeParams.vehicleLengthLevel.longVehicleLengthLevel;

        _ViolationCode.length = runtimeParams.violationCodeTable.size();
        if (_ViolationCode.length > 15)
                _ViolationCode.length = 15;

        int index = 0;
        for (; index < _ViolationCode.length; index++) {
                _ViolationCode.Code[index].offenceType =
                    runtimeParams.violationCodeTable[index].violationType;
                _ViolationCode.Code[index].lowerBound =
                    runtimeParams.violationCodeTable[index].roadSpeedLimit.lowerBound;
                _ViolationCode.Code[index].upperBound =
                    runtimeParams.violationCodeTable[index].roadSpeedLimit.upperBound;
                _ViolationCode.Code[index].overSpeedPercent =
                    runtimeParams.violationCodeTable[index].overSpeedPercent;
                memcpy(_ViolationCode.Code[index].violationCode,
                       runtimeParams.violationCodeTable[index].code.c_str(),
                       runtimeParams.violationCodeTable[index].code.size());

                memcpy(_ViolationCode.Code[index].violationDescription,
                       runtimeParams.violationCodeTable[index].violationDesc.c_str(),
                       runtimeParams.violationCodeTable[index].violationDesc.size());
        }

        set_offence_code(_ViolationCode.Code, _ViolationCode.length, "offence.txt");
        update_offence_code();

        Parameters::GetInstance()->SetDeviceInfo(&device_info);
        Parameters::GetInstance()->SetTrafficParam(&traffic_param);
        Parameters::GetInstance()->SetUploadParam(&upload_param);

        return true;
}

bool Adaptor::resetRuntimeParams(void)
{
        return true;
}

/**
 * @fn  ICE::ByteSeq readImage(VehicleImageIdentity identity)
 *
 * @brief       按identity读取某图像信息
 *
 * @param       [in] identity   图像唯一号
 *
 * @return      [out] Ice::ByteSeq 图像缓存序列
 *
 */
Ice::ByteSeq Adaptor::readImage(VehicleImageIdentity identity)
{
        Ice::ByteSeq sample;
#if 0
        const char *snapNbr = identity.snapNbr.c_str();
        VehicleCaptureInfo captureInfo = (*m_ReadImage_handle) (snapNbr);

        if (captureInfo.pSampleInfo != NULL && captureInfo.pSampleInfo->m_pData != NULL) {

                sample.insert(sample.end(), captureInfo.pSampleInfo->m_pData,
                              captureInfo.pSampleInfo->m_pData +
                              captureInfo.pSampleInfo->m_nDataLen);

        } else {
                char tmp = 0;
                sample.push_back(tmp);
        }
#endif
        return sample;
}

/**
 * @fn  mannualShot 
 *
 * @brief       手动抓拍，并通过out参数data返回图像相关信息（不含图像本身）
 *
 * @param       [out] data
 *
 * @return
 */
bool Adaptor::mannualShot(VehicleImageDescription * data)
{
#if 0
        MannualShotDescription imageDescription = (*m_MannualShot_handle) ();

        data->imageIdentity.deviceNo = imageDescription.deviceNo;

        //data->snapShotMode    = 12;
        data->frameIndex = 0;

        char tmp[64];
        struct tm *pTm = localtime(&(imageDescription.timeStamp.tv_sec));
        strftime(tmp, 64, "%Y%m%d%H%M%S", pTm);
        sprintf(imageDescription.snapNbr, "%s%03d", tmp,
                (int)(imageDescription.timeStamp.tv_usec / 1000));

        data->imageIdentity.snapNbr = imageDescription.snapNbr;

        char guid[128];
        sprintf(guid, "%s_%s%d", imageDescription.deviceNo, imageDescription.snapNbr, 0);
        data->imageIdentity.guid = guid;

        strftime(tmp, 64, "%Y-%m-%d %H:%M:%S", pTm);
        sprintf(tmp, "%s.%03d", tmp, (int)(imageDescription.timeStamp.tv_usec / 1000));
        data->timeStamp = tmp;

        data->imageWidth = imageDescription.imageWidth;
        data->imageHeight = imageDescription.imageHeight;
        data->format = Jpeg;
        data->imageDataLength = imageDescription.imageDataLength;
        data->imageWidthStep = imageDescription.imageWidthStep;

        //printf("device No is %s\n", imageDescription.deviceNo);
        //printf("snapNbr is %s\n", imageDescription.snapNbr);
        //printf("guid is %s\n", guid);
        //printf("time stamp is %s\n", tmp);
#endif
        return true;
}
