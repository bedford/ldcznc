// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `Base.ice'

#ifndef ____GeneratedCpp_Base_h__
#define ____GeneratedCpp_Base_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/UserExceptionFactory.h>
#include <IceE/FactoryTable.h>
#include <IceE/Identity.h>
#include <IceE/BuiltinSequences.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace LDTSSInterface
{

typedef ::std::map< ::std::string, ::std::string> StringStringDict;
void __writeStringStringDict(::IceInternal::BasicStream*, const StringStringDict&);
void __readStringStringDict(::IceInternal::BasicStream*, StringStringDict&);

typedef ::std::map< ::std::string, ::Ice::Int> StringIntDict;
void __writeStringIntDict(::IceInternal::BasicStream*, const StringIntDict&);
void __readStringIntDict(::IceInternal::BasicStream*, StringIntDict&);

struct ValueRange
{
    ::Ice::Float lowerBound;
    ::Ice::Float upperBound;

    bool operator==(const ValueRange&) const;
    bool operator<(const ValueRange&) const;
    bool operator!=(const ValueRange& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const ValueRange& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const ValueRange& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const ValueRange& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

struct StringValueRange
{
    ::std::string lowerBound;
    ::std::string upperBound;

    bool operator==(const StringValueRange&) const;
    bool operator<(const StringValueRange&) const;
    bool operator!=(const StringValueRange& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const StringValueRange& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const StringValueRange& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const StringValueRange& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

struct SpeedLimit
{
    ::std::string vehicleType;
    ::Ice::Int lane;
    ::Ice::Int roadOverSpeedLimit;
    ::Ice::Int roadUnderSpeedLimit;
    ::Ice::Int overSpeedMargin;
    ::Ice::Int underSpeedMargin;

    bool operator==(const SpeedLimit&) const;
    bool operator<(const SpeedLimit&) const;
    bool operator!=(const SpeedLimit& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const SpeedLimit& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const SpeedLimit& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const SpeedLimit& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::SpeedLimit> SpeedLimitSeq;
void __writeSpeedLimitSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::SpeedLimit*, const ::LDTSSInterface::SpeedLimit*);
void __readSpeedLimitSeq(::IceInternal::BasicStream*, SpeedLimitSeq&);

struct ViolationCode
{
    ::Ice::Int violationType;
    ::LDTSSInterface::ValueRange roadSpeedLimit;
    ::Ice::Int overSpeedPercent;
    ::std::string code;
    ::std::string violationDesc;

    bool operator==(const ViolationCode&) const;
    bool operator<(const ViolationCode&) const;
    bool operator!=(const ViolationCode& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const ViolationCode& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const ViolationCode& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const ViolationCode& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::ViolationCode> ViolationCodeSeq;
void __writeViolationCodeSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::ViolationCode*, const ::LDTSSInterface::ViolationCode*);
void __readViolationCodeSeq(::IceInternal::BasicStream*, ViolationCodeSeq&);

struct VehicleImageIdentity
{
    ::std::string deviceNo;
    ::std::string snapNbr;
    ::std::string guid;

    bool operator==(const VehicleImageIdentity&) const;
    bool operator<(const VehicleImageIdentity&) const;
    bool operator!=(const VehicleImageIdentity& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const VehicleImageIdentity& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const VehicleImageIdentity& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const VehicleImageIdentity& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

struct ImageLocation
{
    ::std::string imageServer;
    ::std::string position;

    bool operator==(const ImageLocation&) const;
    bool operator<(const ImageLocation&) const;
    bool operator!=(const ImageLocation& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const ImageLocation& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const ImageLocation& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const ImageLocation& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

enum ImageFormat
{
    Gray,
    Bayer8GBRG,
    Bayer8GRBG,
    Bayer8BGGR,
    Bayer8RGGB,
    RGB24RGB,
    RGB24BGR,
    Data24YCBCR,
    YUV422UYVY,
    YUV422YUYV,
    Jpeg,
    Jpeg2000,
    Raw8,
    Raw10,
    Raw12,
    Raw16,
    RecordingClip
};

void __write(::IceInternal::BasicStream*, ImageFormat);
void __read(::IceInternal::BasicStream*, ImageFormat&);

struct VehicleImageDescription
{
    ::LDTSSInterface::VehicleImageIdentity imageIdentity;
    ::Ice::Int snapShotMode;
    ::Ice::Int frameIndex;
    ::Ice::Int imageWidth;
    ::Ice::Int imageHeight;
    ::Ice::Int imageWidthStep;
    ::LDTSSInterface::ImageFormat format;
    ::std::string timeStamp;
    ::Ice::Int imageDataLength;
    ::Ice::Int imageDataCRC;
    ::std::string watermark;
    ::LDTSSInterface::ImageLocation location;
    ::LDTSSInterface::StringStringDict properies;

    bool operator==(const VehicleImageDescription&) const;
    bool operator<(const VehicleImageDescription&) const;
    bool operator!=(const VehicleImageDescription& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const VehicleImageDescription& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const VehicleImageDescription& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const VehicleImageDescription& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::VehicleImageDescription> VehicleImageDescriptionSeq;
void __writeVehicleImageDescriptionSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::VehicleImageDescription*, const ::LDTSSInterface::VehicleImageDescription*);
void __readVehicleImageDescriptionSeq(::IceInternal::BasicStream*, VehicleImageDescriptionSeq&);

struct VehicleImage
{
    ::LDTSSInterface::VehicleImageDescription imageDescription;
    ::Ice::ByteSeq imageData;

    bool operator==(const VehicleImage&) const;
    bool operator<(const VehicleImage&) const;
    bool operator!=(const VehicleImage& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const VehicleImage& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const VehicleImage& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const VehicleImage& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::VehicleImage> VehicleImageSeq;
void __writeVehicleImageSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::VehicleImage*, const ::LDTSSInterface::VehicleImage*);
void __readVehicleImageSeq(::IceInternal::BasicStream*, VehicleImageSeq&);

struct InspectedVehicle
{
    ::std::string inspectDepartment;
    ::std::string inspectAction;
    ::std::string inspectTime;
    ::std::string ExpireDate;
    ::std::string plateNbr;
    ::std::string plateColor;
    ::std::string plateType;
    ::std::string vehicleType;
    ::std::string description;
    ::std::string updateTime;
    ::LDTSSInterface::StringStringDict extendedProperties;

    bool operator==(const InspectedVehicle&) const;
    bool operator<(const InspectedVehicle&) const;
    bool operator!=(const InspectedVehicle& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const InspectedVehicle& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const InspectedVehicle& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const InspectedVehicle& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::InspectedVehicle> InspectedVehicleSeq;
void __writeInspectedVehicleSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::InspectedVehicle*, const ::LDTSSInterface::InspectedVehicle*);
void __readInspectedVehicleSeq(::IceInternal::BasicStream*, InspectedVehicleSeq&);

struct PassingVehicle
{
    ::std::string deviceNo;
    ::std::string snapNbr;
    ::std::string roadCode;
    ::std::string driveDirection;
    ::Ice::Int lane;
    ::std::string plateNbr;
    ::std::string plateType;
    ::std::string captureTime;
    ::Ice::Float vehicleSpeed;
    ::Ice::Int driveMode;
    ::Ice::Float vehicleLength;
    ::std::string plateColor;
    ::std::string vehicleType;
    ::Ice::StringSeq violationBehaviors;
    ::LDTSSInterface::InspectedVehicleSeq inspectedVehicles;
    ::std::string gpsLocationInfo;
    ::LDTSSInterface::VehicleImageDescriptionSeq imageDescriptions;
    ::LDTSSInterface::StringStringDict extendedProperties;

    bool operator==(const PassingVehicle&) const;
    bool operator<(const PassingVehicle&) const;
    bool operator!=(const PassingVehicle& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const PassingVehicle& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const PassingVehicle& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const PassingVehicle& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::PassingVehicle> PassingVehicleSeq;
void __writePassingVehicleSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::PassingVehicle*, const ::LDTSSInterface::PassingVehicle*);
void __readPassingVehicleSeq(::IceInternal::BasicStream*, PassingVehicleSeq&);

struct ViolationVehicle
{
    ::std::string deviceNo;
    ::std::string snapNbr;
    ::std::string roadCode;
    ::std::string driveDirection;
    ::Ice::Int lane;
    ::std::string plateNbr;
    ::std::string plateType;
    ::std::string captureTime;
    ::Ice::Float vehicleSpeed;
    ::std::string plateColor;
    ::std::string vehicleType;
    ::Ice::StringSeq violationBehaviors;
    ::LDTSSInterface::SpeedLimitSeq roadSpeedLimits;
    ::std::string redlightTime;
    ::std::string gpsLocationInfo;
    ::LDTSSInterface::StringStringDict extendedProperties;
    ::LDTSSInterface::VehicleImageDescriptionSeq imageDescriptions;

    bool operator==(const ViolationVehicle&) const;
    bool operator<(const ViolationVehicle&) const;
    bool operator!=(const ViolationVehicle& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const ViolationVehicle& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const ViolationVehicle& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const ViolationVehicle& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::ViolationVehicle> ViolationVehicleSeq;
void __writeViolationVehicleSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::ViolationVehicle*, const ::LDTSSInterface::ViolationVehicle*);
void __readViolationVehicleSeq(::IceInternal::BasicStream*, ViolationVehicleSeq&);

struct TrafficStats
{
    ::std::string roadCode;
    ::std::string deviceNo;
    ::std::string driveDirection;
    ::Ice::Int driveLane;
    ::std::string statsTime;
    ::Ice::Int statsPeriod;
    ::Ice::Int vehicleTotal;
    ::Ice::Float vehicleSpeed;
    ::Ice::Float vehicleLength;
    ::Ice::Float timeOccupyRatio;
    ::Ice::Float spaceOccupyRatio;
    ::Ice::Float spaceHeadway;
    ::Ice::Float timeHeadway;
    ::Ice::Float density;
    ::Ice::Int overSpeedTotal;
    ::Ice::Int underSpeedTotal;
    ::Ice::Int largeVehicleTotal;
    ::Ice::Int middleVehicleTotal;
    ::Ice::Int smallVehicleTotal;
    ::Ice::Int motorVehicleTotal;
    ::Ice::Int longVehicleTotal;
    ::LDTSSInterface::StringStringDict otherTrafficParams;

    bool operator==(const TrafficStats&) const;
    bool operator<(const TrafficStats&) const;
    bool operator!=(const TrafficStats& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const TrafficStats& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const TrafficStats& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const TrafficStats& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::TrafficStats> TrafficStatsSeq;
void __writeTrafficStatsSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::TrafficStats*, const ::LDTSSInterface::TrafficStats*);
void __readTrafficStatsSeq(::IceInternal::BasicStream*, TrafficStatsSeq&);

struct SurveyDeviceStatus
{
    ::std::string deviceNo;
    ::std::string deviceIpAddr;
    ::Ice::Int status;
    ::std::string faultDetail;
    ::LDTSSInterface::StringIntDict statsInfo;
    ::std::string gpsLocation;
    ::std::string startTime;
    ::std::string deviceTime;
    ::LDTSSInterface::StringStringDict otherStatusInfo;

    bool operator==(const SurveyDeviceStatus&) const;
    bool operator<(const SurveyDeviceStatus&) const;
    bool operator!=(const SurveyDeviceStatus& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const SurveyDeviceStatus& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const SurveyDeviceStatus& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const SurveyDeviceStatus& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

namespace ModuleCode
{

const ::Ice::Int cameraModule = 0;

const ::Ice::Int radarModule = 1;

const ::Ice::Int coilModule = 2;

const ::Ice::Int upsModule = 3;

const ::Ice::Int gpsModule = 4;

const ::Ice::Int otherModule = 9;

}

namespace StatusFlag
{

const ::Ice::Int DetectorFault = 1;

const ::Ice::Int CameraFault = 2;

const ::Ice::Int FlashFault = 4;

const ::Ice::Int StorageFault = 8;

const ::Ice::Int PowerFault = 16;

const ::Ice::Int ModemFault = 32;

const ::Ice::Int NoVehicle = 64;

const ::Ice::Int OffLine = 128;

const ::Ice::Int Breakage = 256;

const ::Ice::Int Steal = 512;

const ::Ice::Int OtherFault = 1024;

const ::Ice::Int CoilFault = 2048;

const ::Ice::Int RadarFault = 4096;

const ::Ice::Int CaFault = 8192;

const ::Ice::Int GPSFault = 16384;

}

namespace FaultCode
{

const ::Ice::Int FlashError = 1;

const ::Ice::Int CameraDisconnected = 2;

const ::Ice::Int CameraNoImage = 3;

const ::Ice::Int CameraNoSignal = 4;

const ::Ice::Int CameraDataError = 5;

const ::Ice::Int RadarNoSignal = 100;

const ::Ice::Int RadarDisconnected = 101;

const ::Ice::Int RadarNoSpeed = 102;

const ::Ice::Int RadarDataError = 103;

const ::Ice::Int RadarHighFrequency = 104;

const ::Ice::Int RadarAmplifier = 105;

const ::Ice::Int RadarCPU = 106;

const ::Ice::Int PowerUtilityFail = 200;

const ::Ice::Int PowerBatteryLow = 201;

const ::Ice::Int PowerUpsFail = 202;

const ::Ice::Int PowerUpsNoSignale = 203;

const ::Ice::Int CoilDisconnected = 300;

const ::Ice::Int CoilNoSignal = 301;

const ::Ice::Int CoilDataError = 302;

const ::Ice::Int FaultNoCapture = 400;

const ::Ice::Int FaultNoVehilce = 401;

const ::Ice::Int FaultStorageFail = 402;

const ::Ice::Int FaultNoSoftDog = 403;

const ::Ice::Int FaultBreakage = 500;

const ::Ice::Int FaultSteal = 501;

const ::Ice::Int NetCreateFail = 600;

const ::Ice::Int NetConnectFail = 601;

const ::Ice::Int NetServerFail = 602;

const ::Ice::Int GPSPortFault = 700;

const ::Ice::Int GPSNoDataFault = 701;

const ::Ice::Int GPSParseFault = 702;

const ::Ice::Int GPSSetTimeFault = 703;

}

struct CameraInfo
{
    ::std::string cameraId;
    ::Ice::Int cameraType;
    ::std::string ipAddr;
    ::LDTSSInterface::StringStringDict otherProperties;

    bool operator==(const CameraInfo&) const;
    bool operator<(const CameraInfo&) const;
    bool operator!=(const CameraInfo& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const CameraInfo& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const CameraInfo& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const CameraInfo& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::CameraInfo> CameraInfoSeq;
void __writeCameraInfoSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::CameraInfo*, const ::LDTSSInterface::CameraInfo*);
void __readCameraInfoSeq(::IceInternal::BasicStream*, CameraInfoSeq&);

struct SurveyDeviceAttribute
{
    ::std::string staticIpAddr;
    ::Ice::Int remoteControlPort;
    ::LDTSSInterface::CameraInfoSeq cameras;
    ::LDTSSInterface::StringStringDict components;
    ::Ice::Int surveyMode;
    ::std::string model;
    ::std::string manufactor;
    ::std::string manufactureId;
    ::std::string protocolType;
    ::std::string protocolVersion;
    ::Ice::Int inspectionMode;
    ::LDTSSInterface::StringStringDict extendedProperties;

    bool operator==(const SurveyDeviceAttribute&) const;
    bool operator<(const SurveyDeviceAttribute&) const;
    bool operator!=(const SurveyDeviceAttribute& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const SurveyDeviceAttribute& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const SurveyDeviceAttribute& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const SurveyDeviceAttribute& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

struct VehicleTypeLenthLevel
{
    ::Ice::Float largeVehicleLengthLevel;
    ::Ice::Float smallVehicleLengthLevel;
    ::Ice::Float motorVehicleLengthLevel;
    ::Ice::Float longVehicleLengthLevel;

    bool operator==(const VehicleTypeLenthLevel&) const;
    bool operator<(const VehicleTypeLenthLevel&) const;
    bool operator!=(const VehicleTypeLenthLevel& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const VehicleTypeLenthLevel& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const VehicleTypeLenthLevel& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const VehicleTypeLenthLevel& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

struct NetworkParams
{
    ::std::string mac;
    ::std::string ip;
    ::std::string ipMask;

    bool operator==(const NetworkParams&) const;
    bool operator<(const NetworkParams&) const;
    bool operator!=(const NetworkParams& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const NetworkParams& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const NetworkParams& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const NetworkParams& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

struct DeviceRuntimeParams
{
    ::std::string deviceNo;
    ::std::string roadCode;
    ::std::string roadName;
    ::std::string gpsLocation;
    ::std::string surveyUpDirection;
    ::std::string surveyDownDirection;
    ::std::string department;
    ::std::string surveyServer;
    ::std::string matchServer;
    ::LDTSSInterface::VehicleTypeLenthLevel vehicleLengthLevel;
    ::LDTSSInterface::ViolationCodeSeq violationCodeTable;
    ::Ice::Int trafficStatsPeriod;
    ::LDTSSInterface::SpeedLimitSeq speedLimits;
    ::Ice::IntSeq redlightMargin;
    ::LDTSSInterface::StringStringDict otherParams;

    bool operator==(const DeviceRuntimeParams&) const;
    bool operator<(const DeviceRuntimeParams&) const;
    bool operator!=(const DeviceRuntimeParams& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const DeviceRuntimeParams& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const DeviceRuntimeParams& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const DeviceRuntimeParams& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

class DatabaseException : public ::Ice::UserException
{
public:

    DatabaseException() {}
    explicit DatabaseException(const ::std::string&);
    virtual ~DatabaseException() throw();

    virtual ::std::string ice_name() const;
    virtual ::Ice::Exception* ice_clone() const;
    virtual void ice_throw() const;

    static const ::IceInternal::UserExceptionFactoryPtr& ice_factory();

    ::std::string message;

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

static DatabaseException __DatabaseException_init;

class GeneralUserException : public ::Ice::UserException
{
public:

    GeneralUserException() {}
    explicit GeneralUserException(const ::std::string&);
    virtual ~GeneralUserException() throw();

    virtual ::std::string ice_name() const;
    virtual ::Ice::Exception* ice_clone() const;
    virtual void ice_throw() const;

    static const ::IceInternal::UserExceptionFactoryPtr& ice_factory();

    ::std::string message;

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

class DevCommException : public ::Ice::UserException
{
public:

    DevCommException() {}
    explicit DevCommException(const ::std::string&);
    virtual ~DevCommException() throw();

    virtual ::std::string ice_name() const;
    virtual ::Ice::Exception* ice_clone() const;
    virtual void ice_throw() const;

    static const ::IceInternal::UserExceptionFactoryPtr& ice_factory();

    ::std::string message;

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

class DeviceNoFoundException : public ::Ice::UserException
{
public:

    DeviceNoFoundException() {}
    explicit DeviceNoFoundException(const ::std::string&);
    virtual ~DeviceNoFoundException() throw();

    virtual ::std::string ice_name() const;
    virtual ::Ice::Exception* ice_clone() const;
    virtual void ice_throw() const;

    static const ::IceInternal::UserExceptionFactoryPtr& ice_factory();

    ::std::string message;

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

struct FragmentInfo
{
    ::Ice::Int offset;
    ::Ice::Int length;

    bool operator==(const FragmentInfo&) const;
    bool operator<(const FragmentInfo&) const;
    bool operator!=(const FragmentInfo& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const FragmentInfo& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const FragmentInfo& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const FragmentInfo& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::FragmentInfo> FragmentInfoSeq;
void __writeFragmentInfoSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::FragmentInfo*, const ::LDTSSInterface::FragmentInfo*);
void __readFragmentInfoSeq(::IceInternal::BasicStream*, FragmentInfoSeq&);

struct SurveyDeviceFault
{
    ::std::string deviceNo;
    ::std::string faultTime;
    ::Ice::Int faultCode;
    ::std::string faultDetail;

    bool operator==(const SurveyDeviceFault&) const;
    bool operator<(const SurveyDeviceFault&) const;
    bool operator!=(const SurveyDeviceFault& __rhs) const
    {
        return !operator==(__rhs);
    }
    bool operator<=(const SurveyDeviceFault& __rhs) const
    {
        return operator<(__rhs) || operator==(__rhs);
    }
    bool operator>(const SurveyDeviceFault& __rhs) const
    {
        return !operator<(__rhs) && !operator==(__rhs);
    }
    bool operator>=(const SurveyDeviceFault& __rhs) const
    {
        return !operator<(__rhs);
    }

    void __write(::IceInternal::BasicStream*) const;
    void __read(::IceInternal::BasicStream*);
};

typedef ::std::vector< ::LDTSSInterface::SurveyDeviceFault> SurveyDeviceFaultSeq;
void __writeSurveyDeviceFaultSeq(::IceInternal::BasicStream*, const ::LDTSSInterface::SurveyDeviceFault*, const ::LDTSSInterface::SurveyDeviceFault*);
void __readSurveyDeviceFaultSeq(::IceInternal::BasicStream*, SurveyDeviceFaultSeq&);

}

#endif
