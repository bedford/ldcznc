#include "IceHelper.h"

IceHelper::IceHelper(void)
{
}


IceHelper::~IceHelper(void)
{
}

bool IceHelper::StartServantAadapter(Ice::ObjectPtr servant, std::string objectIdendity,
                Ice::CommunicatorPtr communicator, Ice::ObjectAdapterPtr &adapter)
{
        bool pass = false;
        try {
                if(!adapter) {
                        adapter = communicator->createObjectAdapter(objectIdendity); 
                }
                adapter->add(servant, communicator->stringToIdentity(objectIdendity));

                pass = true;
        } catch(const IceUtil::Exception& ex) {
                Ice::Error err(communicator->getLogger());
                err << "create servant exception " << objectIdendity <<  ex.what();
        }

        return pass;
}


bool IceHelper::InitIce(string configFile, Ice::CommunicatorPtr &communicator)
{
        try {
                int argc = 0;
                Ice::InitializationData initData;
                initData.properties = Ice::createProperties();
                initData.properties->load(configFile);
                communicator = Ice::initialize(argc, 0, initData);
        } catch(const IceUtil::Exception& ex) {
                Ice::Error err(communicator->getLogger());
                err <<"Iniatialize Ice Exception " << ex.what();
                return false;
        }
        return true;
}

