#ifndef LDICEADAPTOR_H_
#define LDICEADAPTOR_H_

#include <Base.h>
#include "ldczn_base.h"

using namespace std;
using namespace LDTSSInterface;

class Adaptor
{
public:

        static bool SendHeartBeat(DeviceStatus *device, DeviceParam *params);
        static bool SendTrafficStatus(DeviceParam *params, TrafficStatus *stat);

        static bool UploadPassingVehicle(CaptureInfo *captureInfo, bool bImageInfo);
        static bool UploadViolationVehicle(CaptureInfo *captureInfo, bool bImageInfo);
        
        static LDTSSInterface::SurveyDeviceAttribute getAttribute(void);
        static LDTSSInterface::DeviceRuntimeParams getRuntimeParams(void);
        static bool setRuntimeParams(LDTSSInterface::DeviceRuntimeParams runtimeParams);
        static LDTSSInterface::SurveyDeviceStatus GetDeviceStatus(void);

        static Ice::ByteSeq readImage(VehicleImageIdentity identity);
        static bool mannualShot(VehicleImageDescription *data);
        static bool resetRuntimeParams(void);
        
        static bool ReleaseAdaptor();
        static bool InitAdaptor();
        static void RebootSystem(void);

private:

        Adaptor() { };
        ~Adaptor() { };

        static void FillRuntimeParams(DeviceInfo *device_info, TrafficParam *traffic_param,
                                UploadParam *upload_param, DeviceRuntimeParams *runtimeParams);

        static void FillDeviceAttribute(DeviceInfo *device_info, NetworkParam *network_param,
                                SurveyDeviceAttribute *deviceAttribute);
        static std::vector<struct SpeedLimit> FillSpeedLimit(DeviceInfo *device_info);
        static std::vector<string> ViolationCodeCheck(TriggerInfo *trigger_info);

        static struct VehicleImageDescription PacketImageInfo(CaptureInfo *capture_info, int index);
        static ViolationVehicle PacketViolationVehicleInfo(CaptureInfo *capture_info);
        static PassingVehicle PacketPassingVehicleInfo(CaptureInfo *capture_info);

        static unsigned int ConverOffenceType(unsigned int code);
};

#endif

