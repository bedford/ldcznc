// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `DeviceControl.ice'

#include <DeviceControl.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/LocalException.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __LDTSSInterface__DeviceControl__GetAttribute_name = "GetAttribute";

static const ::std::string __LDTSSInterface__DeviceControl__SetParam_name = "SetParam";

static const ::std::string __LDTSSInterface__DeviceControl__GetParam_name = "GetParam";

static const ::std::string __LDTSSInterface__DeviceControl__GetAllParams_name = "GetAllParams";

static const ::std::string __LDTSSInterface__DeviceControl__Reset_name = "Reset";

static const ::std::string __LDTSSInterface__DeviceControl__GetCameraAllParams_name = "GetCameraAllParams";

static const ::std::string __LDTSSInterface__DeviceControl__SetCameraAllParams_name = "SetCameraAllParams";

static const ::std::string __LDTSSInterface__DeviceControl__GetRuntimeParams_name = "GetRuntimeParams";

static const ::std::string __LDTSSInterface__DeviceControl__SetRuntimeParams_name = "SetRuntimeParams";

static const ::std::string __LDTSSInterface__DeviceControl__GetNetworkParams_name = "GetNetworkParams";

static const ::std::string __LDTSSInterface__DeviceControl__SetNetworkParams_name = "SetNetworkParams";

static const ::std::string __LDTSSInterface__DeviceControl__ReadConfigFile_name = "ReadConfigFile";

static const ::std::string __LDTSSInterface__DeviceControl__WriteConfigFile_name = "WriteConfigFile";

static const ::std::string __LDTSSInterface__DeviceControl__ExecuteCommand_name = "ExecuteCommand";

static const ::std::string __LDTSSInterface__DeviceControl__Reboot_name = "Reboot";

static const ::std::string __LDTSSInterface__DeviceControl__CalibrateTime_name = "CalibrateTime";

static const ::std::string __LDTSSInterface__DeviceControl__GetDeviceStatus_name = "GetDeviceStatus";

static const ::std::string __LDTSSInterface__DeviceControl__GetLog_name = "GetLog";

static const ::std::string __LDTSSInterface__DeviceControl__Diagnostics_name = "Diagnostics";

static const ::std::string __LDTSSInterface__DeviceControl__OneShot_name = "OneShot";

static const ::std::string __LDTSSInterface__DeviceControl__ReadImage_name = "ReadImage";

static const ::std::string __LDTSSInterface__DeviceControl__DownloadRecording_name = "DownloadRecording";

static const ::std::string __LDTSSInterface__DeviceControl__QueryVehicleByTime_name = "QueryVehicleByTime";

static const ::std::string __LDTSSInterface__DeviceControl__GetId_name = "GetId";

static const ::std::string __LDTSSInterface__DeviceControl__GetCaCode_name = "GetCaCode";

static const ::std::string __LDTSSInterface__DeviceControl__Certificate_name = "Certificate";

::Ice::Object* IceInternal::upCast(::LDTSSInterface::DeviceControl* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::LDTSSInterface::DeviceControl* p) { return p; }

void
LDTSSInterface::__read(::IceInternal::BasicStream* __is, ::LDTSSInterface::DeviceControlPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::LDTSSInterface::DeviceControl;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __LDTSSInterface__DeviceControl_ids[2] =
{
    "::Ice::Object",
    "::LDTSSInterface::DeviceControl"
};

bool
LDTSSInterface::DeviceControl::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__LDTSSInterface__DeviceControl_ids, __LDTSSInterface__DeviceControl_ids + 2, _s);
}

::std::vector< ::std::string>
LDTSSInterface::DeviceControl::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__LDTSSInterface__DeviceControl_ids[0], &__LDTSSInterface__DeviceControl_ids[2]);
}

const ::std::string&
LDTSSInterface::DeviceControl::ice_id(const ::Ice::Current&) const
{
    return __LDTSSInterface__DeviceControl_ids[1];
}

const ::std::string&
LDTSSInterface::DeviceControl::ice_staticId()
{
    return __LDTSSInterface__DeviceControl_ids[1];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetAttribute(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::SurveyDeviceAttribute __ret = GetAttribute(__current);
    __ret.__write(__os);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___SetParam(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string paramName;
    ::std::string paramValue;
    __is->read(paramName);
    __is->read(paramValue);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = SetParam(paramName, paramValue, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetParam(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string paramName;
    __is->read(paramName);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = GetParam(paramName, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetAllParams(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::StringStringDict __ret = GetAllParams(__current);
    ::LDTSSInterface::__writeStringStringDict(__os, __ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___Reset(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = Reset(__current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetCameraAllParams(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string cameraId;
    __is->read(cameraId);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::StringStringDict __ret = GetCameraAllParams(cameraId, __current);
    ::LDTSSInterface::__writeStringStringDict(__os, __ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___SetCameraAllParams(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string cameraId;
    ::LDTSSInterface::StringStringDict allParams;
    __is->read(cameraId);
    ::LDTSSInterface::__readStringStringDict(__is, allParams);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = SetCameraAllParams(cameraId, allParams, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetRuntimeParams(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::DeviceRuntimeParams __ret = GetRuntimeParams(__current);
    __ret.__write(__os);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___SetRuntimeParams(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::DeviceRuntimeParams params;
    params.__read(__is);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = SetRuntimeParams(params, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetNetworkParams(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::NetworkParams __ret = GetNetworkParams(__current);
    __ret.__write(__os);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___SetNetworkParams(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::NetworkParams params;
    params.__read(__is);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = SetNetworkParams(params, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___ReadConfigFile(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::Ice::Int configType;
    __is->read(configType);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::ByteSeq __ret = ReadConfigFile(configType, __current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___WriteConfigFile(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::Ice::Int configType;
    ::Ice::ByteSeq configFile;
    __is->read(configType);
    ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> ___configFile;
    __is->read(___configFile);
    ::std::vector< ::Ice::Byte>(___configFile.first, ___configFile.second).swap(configFile);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = WriteConfigFile(configType, configFile, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___ExecuteCommand(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string cmdName;
    ::std::string cmdParam;
    __is->read(cmdName);
    __is->read(cmdParam);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = ExecuteCommand(cmdName, cmdParam, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___Reboot(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = Reboot(__current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___CalibrateTime(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string time;
    __is->read(time);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = CalibrateTime(time, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetDeviceStatus(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::SurveyDeviceStatus __ret = GetDeviceStatus(__current);
    __ret.__write(__os);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetLog(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::Ice::Int logType;
    __is->read(logType);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::ByteSeq __ret = GetLog(logType, __current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___Diagnostics(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::Int __ret = Diagnostics(__current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___OneShot(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string cameraId;
    __is->read(cameraId);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::VehicleImageDescriptionSeq images;
    bool __ret = OneShot(cameraId, images, __current);
    if(images.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeVehicleImageDescriptionSeq(__os, &images[0], &images[0] + images.size());
    }
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___ReadImage(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string cameraId;
    ::LDTSSInterface::VehicleImageIdentity identity;
    ::Ice::Int offset;
    ::Ice::Int length;
    __is->read(cameraId);
    identity.__read(__is);
    __is->read(offset);
    __is->read(length);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::ByteSeq __ret = ReadImage(cameraId, identity, offset, length, __current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___DownloadRecording(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string recordingFile;
    ::Ice::Int offset;
    ::Ice::Int length;
    __is->read(recordingFile);
    __is->read(offset);
    __is->read(length);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::ByteSeq __ret = DownloadRecording(recordingFile, offset, length, __current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___QueryVehicleByTime(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string startTime;
    ::std::string endTime;
    ::Ice::Int pageSize;
    ::Ice::Int pageIndex;
    __is->read(startTime);
    __is->read(endTime);
    __is->read(pageSize);
    __is->read(pageIndex);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::PassingVehicleSeq __ret = QueryVehicleByTime(startTime, endTime, pageSize, pageIndex, __current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writePassingVehicleSeq(__os, &__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetId(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = GetId(__current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___GetCaCode(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = GetCaCode(__current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::DeviceControl::___Certificate(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::Ice::ByteSeq caFile;
    ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> ___caFile;
    __is->read(___caFile);
    ::std::vector< ::Ice::Byte>(___caFile.first, ___caFile.second).swap(caFile);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = Certificate(caFile, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __LDTSSInterface__DeviceControl_all[] =
{
    "CalibrateTime",
    "Certificate",
    "Diagnostics",
    "DownloadRecording",
    "ExecuteCommand",
    "GetAllParams",
    "GetAttribute",
    "GetCaCode",
    "GetCameraAllParams",
    "GetDeviceStatus",
    "GetId",
    "GetLog",
    "GetNetworkParams",
    "GetParam",
    "GetRuntimeParams",
    "OneShot",
    "QueryVehicleByTime",
    "ReadConfigFile",
    "ReadImage",
    "Reboot",
    "Reset",
    "SetCameraAllParams",
    "SetNetworkParams",
    "SetParam",
    "SetRuntimeParams",
    "WriteConfigFile",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

::Ice::DispatchStatus
LDTSSInterface::DeviceControl::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__LDTSSInterface__DeviceControl_all, __LDTSSInterface__DeviceControl_all + 30, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __LDTSSInterface__DeviceControl_all)
    {
        case 0:
        {
            return ___CalibrateTime(in, current);
        }
        case 1:
        {
            return ___Certificate(in, current);
        }
        case 2:
        {
            return ___Diagnostics(in, current);
        }
        case 3:
        {
            return ___DownloadRecording(in, current);
        }
        case 4:
        {
            return ___ExecuteCommand(in, current);
        }
        case 5:
        {
            return ___GetAllParams(in, current);
        }
        case 6:
        {
            return ___GetAttribute(in, current);
        }
        case 7:
        {
            return ___GetCaCode(in, current);
        }
        case 8:
        {
            return ___GetCameraAllParams(in, current);
        }
        case 9:
        {
            return ___GetDeviceStatus(in, current);
        }
        case 10:
        {
            return ___GetId(in, current);
        }
        case 11:
        {
            return ___GetLog(in, current);
        }
        case 12:
        {
            return ___GetNetworkParams(in, current);
        }
        case 13:
        {
            return ___GetParam(in, current);
        }
        case 14:
        {
            return ___GetRuntimeParams(in, current);
        }
        case 15:
        {
            return ___OneShot(in, current);
        }
        case 16:
        {
            return ___QueryVehicleByTime(in, current);
        }
        case 17:
        {
            return ___ReadConfigFile(in, current);
        }
        case 18:
        {
            return ___ReadImage(in, current);
        }
        case 19:
        {
            return ___Reboot(in, current);
        }
        case 20:
        {
            return ___Reset(in, current);
        }
        case 21:
        {
            return ___SetCameraAllParams(in, current);
        }
        case 22:
        {
            return ___SetNetworkParams(in, current);
        }
        case 23:
        {
            return ___SetParam(in, current);
        }
        case 24:
        {
            return ___SetRuntimeParams(in, current);
        }
        case 25:
        {
            return ___WriteConfigFile(in, current);
        }
        case 26:
        {
            return ___ice_id(in, current);
        }
        case 27:
        {
            return ___ice_ids(in, current);
        }
        case 28:
        {
            return ___ice_isA(in, current);
        }
        case 29:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
LDTSSInterface::DeviceControl::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
LDTSSInterface::DeviceControl::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
LDTSSInterface::operator==(const ::LDTSSInterface::DeviceControl& l, const ::LDTSSInterface::DeviceControl& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
LDTSSInterface::operator<(const ::LDTSSInterface::DeviceControl& l, const ::LDTSSInterface::DeviceControl& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
LDTSSInterface::__patch__DeviceControlPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::LDTSSInterface::DeviceControlPtr* p = static_cast< ::LDTSSInterface::DeviceControlPtr*>(__addr);
    assert(p);
    *p = ::LDTSSInterface::DeviceControlPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::LDTSSInterface::DeviceControl::ice_staticId(), v->ice_id());
    }
}
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetAttribute::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetAttribute_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetAttribute_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetAttribute::__response(bool __ok)
{
    ::LDTSSInterface::SurveyDeviceAttribute __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __ret.__read(__is);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_SetParam::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& paramName, const ::std::string& paramValue, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__SetParam_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__SetParam_name, ::Ice::Idempotent, __ctx);
        __os->write(paramName);
        __os->write(paramValue);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_SetParam::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetParam::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& paramName, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetParam_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetParam_name, ::Ice::Idempotent, __ctx);
        __os->write(paramName);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetParam::__response(bool __ok)
{
    ::std::string __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetAllParams::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetAllParams_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetAllParams_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetAllParams::__response(bool __ok)
{
    ::LDTSSInterface::StringStringDict __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::LDTSSInterface::__readStringStringDict(__is, __ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_Reset::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__Reset_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__Reset_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_Reset::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetCameraAllParams::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& cameraId, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetCameraAllParams_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetCameraAllParams_name, ::Ice::Idempotent, __ctx);
        __os->write(cameraId);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetCameraAllParams::__response(bool __ok)
{
    ::LDTSSInterface::StringStringDict __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::LDTSSInterface::__readStringStringDict(__is, __ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_SetCameraAllParams::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& cameraId, const ::LDTSSInterface::StringStringDict& allParams, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__SetCameraAllParams_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__SetCameraAllParams_name, ::Ice::Idempotent, __ctx);
        __os->write(cameraId);
        ::LDTSSInterface::__writeStringStringDict(__os, allParams);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_SetCameraAllParams::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetRuntimeParams::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetRuntimeParams_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetRuntimeParams_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetRuntimeParams::__response(bool __ok)
{
    ::LDTSSInterface::DeviceRuntimeParams __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __ret.__read(__is);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_SetRuntimeParams::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::LDTSSInterface::DeviceRuntimeParams& params, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__SetRuntimeParams_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__SetRuntimeParams_name, ::Ice::Idempotent, __ctx);
        params.__write(__os);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_SetRuntimeParams::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetNetworkParams::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetNetworkParams_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetNetworkParams_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetNetworkParams::__response(bool __ok)
{
    ::LDTSSInterface::NetworkParams __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __ret.__read(__is);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_SetNetworkParams::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::LDTSSInterface::NetworkParams& params, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__SetNetworkParams_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__SetNetworkParams_name, ::Ice::Idempotent, __ctx);
        params.__write(__os);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_SetNetworkParams::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_ReadConfigFile::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, ::Ice::Int configType, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__ReadConfigFile_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__ReadConfigFile_name, ::Ice::Idempotent, __ctx);
        __os->write(configType);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_ReadConfigFile::__response(bool __ok)
{
    ::Ice::ByteSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> _____ret;
        __is->read(_____ret);
        ::std::vector< ::Ice::Byte>(_____ret.first, _____ret.second).swap(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_WriteConfigFile::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, ::Ice::Int configType, const ::Ice::ByteSeq& configFile, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__WriteConfigFile_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__WriteConfigFile_name, ::Ice::Idempotent, __ctx);
        __os->write(configType);
        if(configFile.size() == 0)
        {
            __os->writeSize(0);
        }
        else
        {
            __os->write(&configFile[0], &configFile[0] + configFile.size());
        }
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_WriteConfigFile::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_ExecuteCommand::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& cmdName, const ::std::string& cmdParam, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__ExecuteCommand_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__ExecuteCommand_name, ::Ice::Normal, __ctx);
        __os->write(cmdName);
        __os->write(cmdParam);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_ExecuteCommand::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_Reboot::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__Reboot_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__Reboot_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_Reboot::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_CalibrateTime::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& time, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__CalibrateTime_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__CalibrateTime_name, ::Ice::Normal, __ctx);
        __os->write(time);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_CalibrateTime::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetDeviceStatus::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetDeviceStatus_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetDeviceStatus_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetDeviceStatus::__response(bool __ok)
{
    ::LDTSSInterface::SurveyDeviceStatus __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __ret.__read(__is);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetLog::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, ::Ice::Int logType, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetLog_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetLog_name, ::Ice::Idempotent, __ctx);
        __os->write(logType);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetLog::__response(bool __ok)
{
    ::Ice::ByteSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> _____ret;
        __is->read(_____ret);
        ::std::vector< ::Ice::Byte>(_____ret.first, _____ret.second).swap(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_Diagnostics::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__Diagnostics_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__Diagnostics_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_Diagnostics::__response(bool __ok)
{
    ::Ice::Int __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_OneShot::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& cameraId, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__OneShot_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__OneShot_name, ::Ice::Normal, __ctx);
        __os->write(cameraId);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_OneShot::__response(bool __ok)
{
    ::LDTSSInterface::VehicleImageDescriptionSeq images;
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::LDTSSInterface::__readVehicleImageDescriptionSeq(__is, images);
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret, images);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_ReadImage::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& cameraId, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, ::Ice::Int length, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__ReadImage_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__ReadImage_name, ::Ice::Idempotent, __ctx);
        __os->write(cameraId);
        identity.__write(__os);
        __os->write(offset);
        __os->write(length);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_ReadImage::__response(bool __ok)
{
    ::Ice::ByteSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> _____ret;
        __is->read(_____ret);
        ::std::vector< ::Ice::Byte>(_____ret.first, _____ret.second).swap(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_DownloadRecording::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& recordingFile, ::Ice::Int offset, ::Ice::Int length, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__DownloadRecording_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__DownloadRecording_name, ::Ice::Idempotent, __ctx);
        __os->write(recordingFile);
        __os->write(offset);
        __os->write(length);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_DownloadRecording::__response(bool __ok)
{
    ::Ice::ByteSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> _____ret;
        __is->read(_____ret);
        ::std::vector< ::Ice::Byte>(_____ret.first, _____ret.second).swap(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_QueryVehicleByTime::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::std::string& startTime, const ::std::string& endTime, ::Ice::Int pageSize, ::Ice::Int pageIndex, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__QueryVehicleByTime_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__QueryVehicleByTime_name, ::Ice::Idempotent, __ctx);
        __os->write(startTime);
        __os->write(endTime);
        __os->write(pageSize);
        __os->write(pageIndex);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_QueryVehicleByTime::__response(bool __ok)
{
    ::LDTSSInterface::PassingVehicleSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::LDTSSInterface::__readPassingVehicleSeq(__is, __ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetId::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetId_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetId_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetId::__response(bool __ok)
{
    ::std::string __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_GetCaCode::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__GetCaCode_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__GetCaCode_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_GetCaCode::__response(bool __ok)
{
    ::std::string __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_DeviceControl_Certificate::__invoke(const ::LDTSSInterface::DeviceControlPrx& __prx, const ::Ice::ByteSeq& caFile, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__DeviceControl__Certificate_name);
        __prepare(__prx, __LDTSSInterface__DeviceControl__Certificate_name, ::Ice::Idempotent, __ctx);
        if(caFile.size() == 0)
        {
            __os->writeSize(0);
        }
        else
        {
            __os->write(&caFile[0], &caFile[0] + caFile.size());
        }
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_DeviceControl_Certificate::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif

::LDTSSInterface::SurveyDeviceAttribute
IceProxy::LDTSSInterface::DeviceControl::GetAttribute(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetAttribute_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetAttribute_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::SurveyDeviceAttribute __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __ret.__read(__is);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetAttribute_async(const ::LDTSSInterface::AMI_DeviceControl_GetAttributePtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetAttribute_async(const ::LDTSSInterface::AMI_DeviceControl_GetAttributePtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::SetParam(const ::std::string& paramName, const ::std::string& paramValue, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__SetParam_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__SetParam_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(paramName);
                __os->write(paramValue);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::SetParam_async(const ::LDTSSInterface::AMI_DeviceControl_SetParamPtr& __cb, const ::std::string& paramName, const ::std::string& paramValue)
{
    return __cb->__invoke(this, paramName, paramValue, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::SetParam_async(const ::LDTSSInterface::AMI_DeviceControl_SetParamPtr& __cb, const ::std::string& paramName, const ::std::string& paramValue, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, paramName, paramValue, &__ctx);
}
#endif

::std::string
IceProxy::LDTSSInterface::DeviceControl::GetParam(const ::std::string& paramName, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetParam_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetParam_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(paramName);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::std::string __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetParam_async(const ::LDTSSInterface::AMI_DeviceControl_GetParamPtr& __cb, const ::std::string& paramName)
{
    return __cb->__invoke(this, paramName, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetParam_async(const ::LDTSSInterface::AMI_DeviceControl_GetParamPtr& __cb, const ::std::string& paramName, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, paramName, &__ctx);
}
#endif

::LDTSSInterface::StringStringDict
IceProxy::LDTSSInterface::DeviceControl::GetAllParams(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetAllParams_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetAllParams_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::StringStringDict __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::LDTSSInterface::__readStringStringDict(__is, __ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetAllParamsPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetAllParamsPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::Reset(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__Reset_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__Reset_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::Reset_async(const ::LDTSSInterface::AMI_DeviceControl_ResetPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::Reset_async(const ::LDTSSInterface::AMI_DeviceControl_ResetPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

::LDTSSInterface::StringStringDict
IceProxy::LDTSSInterface::DeviceControl::GetCameraAllParams(const ::std::string& cameraId, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetCameraAllParams_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetCameraAllParams_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(cameraId);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::StringStringDict __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::LDTSSInterface::__readStringStringDict(__is, __ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetCameraAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetCameraAllParamsPtr& __cb, const ::std::string& cameraId)
{
    return __cb->__invoke(this, cameraId, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetCameraAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetCameraAllParamsPtr& __cb, const ::std::string& cameraId, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, cameraId, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::SetCameraAllParams(const ::std::string& cameraId, const ::LDTSSInterface::StringStringDict& allParams, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__SetCameraAllParams_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__SetCameraAllParams_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(cameraId);
                ::LDTSSInterface::__writeStringStringDict(__os, allParams);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::SetCameraAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetCameraAllParamsPtr& __cb, const ::std::string& cameraId, const ::LDTSSInterface::StringStringDict& allParams)
{
    return __cb->__invoke(this, cameraId, allParams, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::SetCameraAllParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetCameraAllParamsPtr& __cb, const ::std::string& cameraId, const ::LDTSSInterface::StringStringDict& allParams, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, cameraId, allParams, &__ctx);
}
#endif

::LDTSSInterface::DeviceRuntimeParams
IceProxy::LDTSSInterface::DeviceControl::GetRuntimeParams(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetRuntimeParams_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetRuntimeParams_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::DeviceRuntimeParams __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __ret.__read(__is);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetRuntimeParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetRuntimeParamsPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetRuntimeParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetRuntimeParamsPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::SetRuntimeParams(const ::LDTSSInterface::DeviceRuntimeParams& params, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__SetRuntimeParams_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__SetRuntimeParams_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                params.__write(__os);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::SetRuntimeParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetRuntimeParamsPtr& __cb, const ::LDTSSInterface::DeviceRuntimeParams& params)
{
    return __cb->__invoke(this, params, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::SetRuntimeParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetRuntimeParamsPtr& __cb, const ::LDTSSInterface::DeviceRuntimeParams& params, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, params, &__ctx);
}
#endif

::LDTSSInterface::NetworkParams
IceProxy::LDTSSInterface::DeviceControl::GetNetworkParams(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetNetworkParams_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetNetworkParams_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::NetworkParams __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __ret.__read(__is);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetNetworkParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetNetworkParamsPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetNetworkParams_async(const ::LDTSSInterface::AMI_DeviceControl_GetNetworkParamsPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::SetNetworkParams(const ::LDTSSInterface::NetworkParams& params, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__SetNetworkParams_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__SetNetworkParams_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                params.__write(__os);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::SetNetworkParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetNetworkParamsPtr& __cb, const ::LDTSSInterface::NetworkParams& params)
{
    return __cb->__invoke(this, params, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::SetNetworkParams_async(const ::LDTSSInterface::AMI_DeviceControl_SetNetworkParamsPtr& __cb, const ::LDTSSInterface::NetworkParams& params, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, params, &__ctx);
}
#endif

::Ice::ByteSeq
IceProxy::LDTSSInterface::DeviceControl::ReadConfigFile(::Ice::Int configType, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__ReadConfigFile_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__ReadConfigFile_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(configType);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::Ice::ByteSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> _____ret;
                __is->read(_____ret);
                ::std::vector< ::Ice::Byte>(_____ret.first, _____ret.second).swap(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::ReadConfigFile_async(const ::LDTSSInterface::AMI_DeviceControl_ReadConfigFilePtr& __cb, ::Ice::Int configType)
{
    return __cb->__invoke(this, configType, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::ReadConfigFile_async(const ::LDTSSInterface::AMI_DeviceControl_ReadConfigFilePtr& __cb, ::Ice::Int configType, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, configType, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::WriteConfigFile(::Ice::Int configType, const ::Ice::ByteSeq& configFile, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__WriteConfigFile_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__WriteConfigFile_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(configType);
                if(configFile.size() == 0)
                {
                    __os->writeSize(0);
                }
                else
                {
                    __os->write(&configFile[0], &configFile[0] + configFile.size());
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::WriteConfigFile_async(const ::LDTSSInterface::AMI_DeviceControl_WriteConfigFilePtr& __cb, ::Ice::Int configType, const ::Ice::ByteSeq& configFile)
{
    return __cb->__invoke(this, configType, configFile, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::WriteConfigFile_async(const ::LDTSSInterface::AMI_DeviceControl_WriteConfigFilePtr& __cb, ::Ice::Int configType, const ::Ice::ByteSeq& configFile, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, configType, configFile, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::ExecuteCommand(const ::std::string& cmdName, const ::std::string& cmdParam, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__ExecuteCommand_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__ExecuteCommand_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(cmdName);
                __os->write(cmdParam);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::ExecuteCommand_async(const ::LDTSSInterface::AMI_DeviceControl_ExecuteCommandPtr& __cb, const ::std::string& cmdName, const ::std::string& cmdParam)
{
    return __cb->__invoke(this, cmdName, cmdParam, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::ExecuteCommand_async(const ::LDTSSInterface::AMI_DeviceControl_ExecuteCommandPtr& __cb, const ::std::string& cmdName, const ::std::string& cmdParam, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, cmdName, cmdParam, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::Reboot(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__Reboot_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__Reboot_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::Reboot_async(const ::LDTSSInterface::AMI_DeviceControl_RebootPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::Reboot_async(const ::LDTSSInterface::AMI_DeviceControl_RebootPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::CalibrateTime(const ::std::string& time, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__CalibrateTime_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__CalibrateTime_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(time);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::CalibrateTime_async(const ::LDTSSInterface::AMI_DeviceControl_CalibrateTimePtr& __cb, const ::std::string& time)
{
    return __cb->__invoke(this, time, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::CalibrateTime_async(const ::LDTSSInterface::AMI_DeviceControl_CalibrateTimePtr& __cb, const ::std::string& time, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, time, &__ctx);
}
#endif

::LDTSSInterface::SurveyDeviceStatus
IceProxy::LDTSSInterface::DeviceControl::GetDeviceStatus(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetDeviceStatus_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetDeviceStatus_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::SurveyDeviceStatus __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __ret.__read(__is);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetDeviceStatus_async(const ::LDTSSInterface::AMI_DeviceControl_GetDeviceStatusPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetDeviceStatus_async(const ::LDTSSInterface::AMI_DeviceControl_GetDeviceStatusPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

::Ice::ByteSeq
IceProxy::LDTSSInterface::DeviceControl::GetLog(::Ice::Int logType, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetLog_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetLog_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(logType);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::Ice::ByteSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> _____ret;
                __is->read(_____ret);
                ::std::vector< ::Ice::Byte>(_____ret.first, _____ret.second).swap(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetLog_async(const ::LDTSSInterface::AMI_DeviceControl_GetLogPtr& __cb, ::Ice::Int logType)
{
    return __cb->__invoke(this, logType, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetLog_async(const ::LDTSSInterface::AMI_DeviceControl_GetLogPtr& __cb, ::Ice::Int logType, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, logType, &__ctx);
}
#endif

::Ice::Int
IceProxy::LDTSSInterface::DeviceControl::Diagnostics(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__Diagnostics_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__Diagnostics_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::Ice::Int __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::Diagnostics_async(const ::LDTSSInterface::AMI_DeviceControl_DiagnosticsPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::Diagnostics_async(const ::LDTSSInterface::AMI_DeviceControl_DiagnosticsPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::OneShot(const ::std::string& cameraId, ::LDTSSInterface::VehicleImageDescriptionSeq& images, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__OneShot_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__OneShot_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(cameraId);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::LDTSSInterface::__readVehicleImageDescriptionSeq(__is, images);
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::OneShot_async(const ::LDTSSInterface::AMI_DeviceControl_OneShotPtr& __cb, const ::std::string& cameraId)
{
    return __cb->__invoke(this, cameraId, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::OneShot_async(const ::LDTSSInterface::AMI_DeviceControl_OneShotPtr& __cb, const ::std::string& cameraId, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, cameraId, &__ctx);
}
#endif

::Ice::ByteSeq
IceProxy::LDTSSInterface::DeviceControl::ReadImage(const ::std::string& cameraId, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, ::Ice::Int length, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__ReadImage_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__ReadImage_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(cameraId);
                identity.__write(__os);
                __os->write(offset);
                __os->write(length);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::Ice::ByteSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> _____ret;
                __is->read(_____ret);
                ::std::vector< ::Ice::Byte>(_____ret.first, _____ret.second).swap(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::ReadImage_async(const ::LDTSSInterface::AMI_DeviceControl_ReadImagePtr& __cb, const ::std::string& cameraId, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, ::Ice::Int length)
{
    return __cb->__invoke(this, cameraId, identity, offset, length, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::ReadImage_async(const ::LDTSSInterface::AMI_DeviceControl_ReadImagePtr& __cb, const ::std::string& cameraId, const ::LDTSSInterface::VehicleImageIdentity& identity, ::Ice::Int offset, ::Ice::Int length, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, cameraId, identity, offset, length, &__ctx);
}
#endif

::Ice::ByteSeq
IceProxy::LDTSSInterface::DeviceControl::DownloadRecording(const ::std::string& recordingFile, ::Ice::Int offset, ::Ice::Int length, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__DownloadRecording_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__DownloadRecording_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(recordingFile);
                __os->write(offset);
                __os->write(length);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::Ice::ByteSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> _____ret;
                __is->read(_____ret);
                ::std::vector< ::Ice::Byte>(_____ret.first, _____ret.second).swap(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::DownloadRecording_async(const ::LDTSSInterface::AMI_DeviceControl_DownloadRecordingPtr& __cb, const ::std::string& recordingFile, ::Ice::Int offset, ::Ice::Int length)
{
    return __cb->__invoke(this, recordingFile, offset, length, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::DownloadRecording_async(const ::LDTSSInterface::AMI_DeviceControl_DownloadRecordingPtr& __cb, const ::std::string& recordingFile, ::Ice::Int offset, ::Ice::Int length, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, recordingFile, offset, length, &__ctx);
}
#endif

::LDTSSInterface::PassingVehicleSeq
IceProxy::LDTSSInterface::DeviceControl::QueryVehicleByTime(const ::std::string& startTime, const ::std::string& endTime, ::Ice::Int pageSize, ::Ice::Int pageIndex, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__QueryVehicleByTime_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__QueryVehicleByTime_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(startTime);
                __os->write(endTime);
                __os->write(pageSize);
                __os->write(pageIndex);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::PassingVehicleSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::LDTSSInterface::__readPassingVehicleSeq(__is, __ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::QueryVehicleByTime_async(const ::LDTSSInterface::AMI_DeviceControl_QueryVehicleByTimePtr& __cb, const ::std::string& startTime, const ::std::string& endTime, ::Ice::Int pageSize, ::Ice::Int pageIndex)
{
    return __cb->__invoke(this, startTime, endTime, pageSize, pageIndex, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::QueryVehicleByTime_async(const ::LDTSSInterface::AMI_DeviceControl_QueryVehicleByTimePtr& __cb, const ::std::string& startTime, const ::std::string& endTime, ::Ice::Int pageSize, ::Ice::Int pageIndex, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, startTime, endTime, pageSize, pageIndex, &__ctx);
}
#endif

::std::string
IceProxy::LDTSSInterface::DeviceControl::GetId(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetId_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetId_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::std::string __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetId_async(const ::LDTSSInterface::AMI_DeviceControl_GetIdPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetId_async(const ::LDTSSInterface::AMI_DeviceControl_GetIdPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

::std::string
IceProxy::LDTSSInterface::DeviceControl::GetCaCode(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__GetCaCode_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__GetCaCode_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::std::string __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::GetCaCode_async(const ::LDTSSInterface::AMI_DeviceControl_GetCaCodePtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::GetCaCode_async(const ::LDTSSInterface::AMI_DeviceControl_GetCaCodePtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::DeviceControl::Certificate(const ::Ice::ByteSeq& caFile, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__DeviceControl__Certificate_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__DeviceControl__Certificate_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                if(caFile.size() == 0)
                {
                    __os->writeSize(0);
                }
                else
                {
                    __os->write(&caFile[0], &caFile[0] + caFile.size());
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::DeviceControl::Certificate_async(const ::LDTSSInterface::AMI_DeviceControl_CertificatePtr& __cb, const ::Ice::ByteSeq& caFile)
{
    return __cb->__invoke(this, caFile, 0);
}

bool
IceProxy::LDTSSInterface::DeviceControl::Certificate_async(const ::LDTSSInterface::AMI_DeviceControl_CertificatePtr& __cb, const ::Ice::ByteSeq& caFile, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, caFile, &__ctx);
}
#endif

const ::std::string&
IceProxy::LDTSSInterface::DeviceControl::ice_staticId()
{
    return __LDTSSInterface__DeviceControl_ids[1];
}

::IceProxy::Ice::Object*
IceProxy::LDTSSInterface::DeviceControl::__newInstance() const
{
    return new DeviceControl;
}
