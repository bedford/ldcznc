// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `Base.ice'

#include <Base.h>
#include <IceE/BasicStream.h>
#include <IceE/Object.h>
#include <IceE/LocalException.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

void
LDTSSInterface::__writeStringStringDict(::IceInternal::BasicStream* __os, const ::LDTSSInterface::StringStringDict& v)
{
    __os->writeSize(::Ice::Int(v.size()));
    ::LDTSSInterface::StringStringDict::const_iterator p;
    for(p = v.begin(); p != v.end(); ++p)
    {
        __os->write(p->first);
        __os->write(p->second);
    }
}

void
LDTSSInterface::__readStringStringDict(::IceInternal::BasicStream* __is, ::LDTSSInterface::StringStringDict& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    while(sz--)
    {
        ::std::pair<const  ::std::string, ::std::string> pair;
        __is->read(const_cast< ::std::string&>(pair.first));
        ::LDTSSInterface::StringStringDict::iterator __i = v.insert(v.end(), pair);
        __is->read(__i->second);
    }
}

void
LDTSSInterface::__writeStringIntDict(::IceInternal::BasicStream* __os, const ::LDTSSInterface::StringIntDict& v)
{
    __os->writeSize(::Ice::Int(v.size()));
    ::LDTSSInterface::StringIntDict::const_iterator p;
    for(p = v.begin(); p != v.end(); ++p)
    {
        __os->write(p->first);
        __os->write(p->second);
    }
}

void
LDTSSInterface::__readStringIntDict(::IceInternal::BasicStream* __is, ::LDTSSInterface::StringIntDict& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    while(sz--)
    {
        ::std::pair<const  ::std::string, ::Ice::Int> pair;
        __is->read(const_cast< ::std::string&>(pair.first));
        ::LDTSSInterface::StringIntDict::iterator __i = v.insert(v.end(), pair);
        __is->read(__i->second);
    }
}

bool
LDTSSInterface::ValueRange::operator==(const ValueRange& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(lowerBound != __rhs.lowerBound)
    {
        return false;
    }
    if(upperBound != __rhs.upperBound)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::ValueRange::operator<(const ValueRange& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(lowerBound < __rhs.lowerBound)
    {
        return true;
    }
    else if(__rhs.lowerBound < lowerBound)
    {
        return false;
    }
    if(upperBound < __rhs.upperBound)
    {
        return true;
    }
    else if(__rhs.upperBound < upperBound)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::ValueRange::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(lowerBound);
    __os->write(upperBound);
}

void
LDTSSInterface::ValueRange::__read(::IceInternal::BasicStream* __is)
{
    __is->read(lowerBound);
    __is->read(upperBound);
}

bool
LDTSSInterface::StringValueRange::operator==(const StringValueRange& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(lowerBound != __rhs.lowerBound)
    {
        return false;
    }
    if(upperBound != __rhs.upperBound)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::StringValueRange::operator<(const StringValueRange& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(lowerBound < __rhs.lowerBound)
    {
        return true;
    }
    else if(__rhs.lowerBound < lowerBound)
    {
        return false;
    }
    if(upperBound < __rhs.upperBound)
    {
        return true;
    }
    else if(__rhs.upperBound < upperBound)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::StringValueRange::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(lowerBound);
    __os->write(upperBound);
}

void
LDTSSInterface::StringValueRange::__read(::IceInternal::BasicStream* __is)
{
    __is->read(lowerBound);
    __is->read(upperBound);
}

bool
LDTSSInterface::SpeedLimit::operator==(const SpeedLimit& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(vehicleType != __rhs.vehicleType)
    {
        return false;
    }
    if(lane != __rhs.lane)
    {
        return false;
    }
    if(roadOverSpeedLimit != __rhs.roadOverSpeedLimit)
    {
        return false;
    }
    if(roadUnderSpeedLimit != __rhs.roadUnderSpeedLimit)
    {
        return false;
    }
    if(overSpeedMargin != __rhs.overSpeedMargin)
    {
        return false;
    }
    if(underSpeedMargin != __rhs.underSpeedMargin)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::SpeedLimit::operator<(const SpeedLimit& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(vehicleType < __rhs.vehicleType)
    {
        return true;
    }
    else if(__rhs.vehicleType < vehicleType)
    {
        return false;
    }
    if(lane < __rhs.lane)
    {
        return true;
    }
    else if(__rhs.lane < lane)
    {
        return false;
    }
    if(roadOverSpeedLimit < __rhs.roadOverSpeedLimit)
    {
        return true;
    }
    else if(__rhs.roadOverSpeedLimit < roadOverSpeedLimit)
    {
        return false;
    }
    if(roadUnderSpeedLimit < __rhs.roadUnderSpeedLimit)
    {
        return true;
    }
    else if(__rhs.roadUnderSpeedLimit < roadUnderSpeedLimit)
    {
        return false;
    }
    if(overSpeedMargin < __rhs.overSpeedMargin)
    {
        return true;
    }
    else if(__rhs.overSpeedMargin < overSpeedMargin)
    {
        return false;
    }
    if(underSpeedMargin < __rhs.underSpeedMargin)
    {
        return true;
    }
    else if(__rhs.underSpeedMargin < underSpeedMargin)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::SpeedLimit::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(vehicleType);
    __os->write(lane);
    __os->write(roadOverSpeedLimit);
    __os->write(roadUnderSpeedLimit);
    __os->write(overSpeedMargin);
    __os->write(underSpeedMargin);
}

void
LDTSSInterface::SpeedLimit::__read(::IceInternal::BasicStream* __is)
{
    __is->read(vehicleType);
    __is->read(lane);
    __is->read(roadOverSpeedLimit);
    __is->read(roadUnderSpeedLimit);
    __is->read(overSpeedMargin);
    __is->read(underSpeedMargin);
}

void
LDTSSInterface::__writeSpeedLimitSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::SpeedLimit* begin, const ::LDTSSInterface::SpeedLimit* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readSpeedLimitSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::SpeedLimitSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 21);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::ViolationCode::operator==(const ViolationCode& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(violationType != __rhs.violationType)
    {
        return false;
    }
    if(roadSpeedLimit != __rhs.roadSpeedLimit)
    {
        return false;
    }
    if(overSpeedPercent != __rhs.overSpeedPercent)
    {
        return false;
    }
    if(code != __rhs.code)
    {
        return false;
    }
    if(violationDesc != __rhs.violationDesc)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::ViolationCode::operator<(const ViolationCode& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(violationType < __rhs.violationType)
    {
        return true;
    }
    else if(__rhs.violationType < violationType)
    {
        return false;
    }
    if(roadSpeedLimit < __rhs.roadSpeedLimit)
    {
        return true;
    }
    else if(__rhs.roadSpeedLimit < roadSpeedLimit)
    {
        return false;
    }
    if(overSpeedPercent < __rhs.overSpeedPercent)
    {
        return true;
    }
    else if(__rhs.overSpeedPercent < overSpeedPercent)
    {
        return false;
    }
    if(code < __rhs.code)
    {
        return true;
    }
    else if(__rhs.code < code)
    {
        return false;
    }
    if(violationDesc < __rhs.violationDesc)
    {
        return true;
    }
    else if(__rhs.violationDesc < violationDesc)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::ViolationCode::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(violationType);
    roadSpeedLimit.__write(__os);
    __os->write(overSpeedPercent);
    __os->write(code);
    __os->write(violationDesc);
}

void
LDTSSInterface::ViolationCode::__read(::IceInternal::BasicStream* __is)
{
    __is->read(violationType);
    roadSpeedLimit.__read(__is);
    __is->read(overSpeedPercent);
    __is->read(code);
    __is->read(violationDesc);
}

void
LDTSSInterface::__writeViolationCodeSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::ViolationCode* begin, const ::LDTSSInterface::ViolationCode* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readViolationCodeSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::ViolationCodeSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 18);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::VehicleImageIdentity::operator==(const VehicleImageIdentity& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(deviceNo != __rhs.deviceNo)
    {
        return false;
    }
    if(snapNbr != __rhs.snapNbr)
    {
        return false;
    }
    if(guid != __rhs.guid)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::VehicleImageIdentity::operator<(const VehicleImageIdentity& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(deviceNo < __rhs.deviceNo)
    {
        return true;
    }
    else if(__rhs.deviceNo < deviceNo)
    {
        return false;
    }
    if(snapNbr < __rhs.snapNbr)
    {
        return true;
    }
    else if(__rhs.snapNbr < snapNbr)
    {
        return false;
    }
    if(guid < __rhs.guid)
    {
        return true;
    }
    else if(__rhs.guid < guid)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::VehicleImageIdentity::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(deviceNo);
    __os->write(snapNbr);
    __os->write(guid);
}

void
LDTSSInterface::VehicleImageIdentity::__read(::IceInternal::BasicStream* __is)
{
    __is->read(deviceNo);
    __is->read(snapNbr);
    __is->read(guid);
}

bool
LDTSSInterface::ImageLocation::operator==(const ImageLocation& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(imageServer != __rhs.imageServer)
    {
        return false;
    }
    if(position != __rhs.position)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::ImageLocation::operator<(const ImageLocation& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(imageServer < __rhs.imageServer)
    {
        return true;
    }
    else if(__rhs.imageServer < imageServer)
    {
        return false;
    }
    if(position < __rhs.position)
    {
        return true;
    }
    else if(__rhs.position < position)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::ImageLocation::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(imageServer);
    __os->write(position);
}

void
LDTSSInterface::ImageLocation::__read(::IceInternal::BasicStream* __is)
{
    __is->read(imageServer);
    __is->read(position);
}

void
LDTSSInterface::__write(::IceInternal::BasicStream* __os, ::LDTSSInterface::ImageFormat v)
{
    __os->write(static_cast< ::Ice::Byte>(v), 17);
}

void
LDTSSInterface::__read(::IceInternal::BasicStream* __is, ::LDTSSInterface::ImageFormat& v)
{
    ::Ice::Byte val;
    __is->read(val, 17);
    v = static_cast< ::LDTSSInterface::ImageFormat>(val);
}

bool
LDTSSInterface::VehicleImageDescription::operator==(const VehicleImageDescription& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(imageIdentity != __rhs.imageIdentity)
    {
        return false;
    }
    if(snapShotMode != __rhs.snapShotMode)
    {
        return false;
    }
    if(frameIndex != __rhs.frameIndex)
    {
        return false;
    }
    if(imageWidth != __rhs.imageWidth)
    {
        return false;
    }
    if(imageHeight != __rhs.imageHeight)
    {
        return false;
    }
    if(imageWidthStep != __rhs.imageWidthStep)
    {
        return false;
    }
    if(format != __rhs.format)
    {
        return false;
    }
    if(timeStamp != __rhs.timeStamp)
    {
        return false;
    }
    if(imageDataLength != __rhs.imageDataLength)
    {
        return false;
    }
    if(imageDataCRC != __rhs.imageDataCRC)
    {
        return false;
    }
    if(watermark != __rhs.watermark)
    {
        return false;
    }
    if(location != __rhs.location)
    {
        return false;
    }
    if(properies != __rhs.properies)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::VehicleImageDescription::operator<(const VehicleImageDescription& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(imageIdentity < __rhs.imageIdentity)
    {
        return true;
    }
    else if(__rhs.imageIdentity < imageIdentity)
    {
        return false;
    }
    if(snapShotMode < __rhs.snapShotMode)
    {
        return true;
    }
    else if(__rhs.snapShotMode < snapShotMode)
    {
        return false;
    }
    if(frameIndex < __rhs.frameIndex)
    {
        return true;
    }
    else if(__rhs.frameIndex < frameIndex)
    {
        return false;
    }
    if(imageWidth < __rhs.imageWidth)
    {
        return true;
    }
    else if(__rhs.imageWidth < imageWidth)
    {
        return false;
    }
    if(imageHeight < __rhs.imageHeight)
    {
        return true;
    }
    else if(__rhs.imageHeight < imageHeight)
    {
        return false;
    }
    if(imageWidthStep < __rhs.imageWidthStep)
    {
        return true;
    }
    else if(__rhs.imageWidthStep < imageWidthStep)
    {
        return false;
    }
    if(format < __rhs.format)
    {
        return true;
    }
    else if(__rhs.format < format)
    {
        return false;
    }
    if(timeStamp < __rhs.timeStamp)
    {
        return true;
    }
    else if(__rhs.timeStamp < timeStamp)
    {
        return false;
    }
    if(imageDataLength < __rhs.imageDataLength)
    {
        return true;
    }
    else if(__rhs.imageDataLength < imageDataLength)
    {
        return false;
    }
    if(imageDataCRC < __rhs.imageDataCRC)
    {
        return true;
    }
    else if(__rhs.imageDataCRC < imageDataCRC)
    {
        return false;
    }
    if(watermark < __rhs.watermark)
    {
        return true;
    }
    else if(__rhs.watermark < watermark)
    {
        return false;
    }
    if(location < __rhs.location)
    {
        return true;
    }
    else if(__rhs.location < location)
    {
        return false;
    }
    if(properies < __rhs.properies)
    {
        return true;
    }
    else if(__rhs.properies < properies)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::VehicleImageDescription::__write(::IceInternal::BasicStream* __os) const
{
    imageIdentity.__write(__os);
    __os->write(snapShotMode);
    __os->write(frameIndex);
    __os->write(imageWidth);
    __os->write(imageHeight);
    __os->write(imageWidthStep);
    ::LDTSSInterface::__write(__os, format);
    __os->write(timeStamp);
    __os->write(imageDataLength);
    __os->write(imageDataCRC);
    __os->write(watermark);
    location.__write(__os);
    ::LDTSSInterface::__writeStringStringDict(__os, properies);
}

void
LDTSSInterface::VehicleImageDescription::__read(::IceInternal::BasicStream* __is)
{
    imageIdentity.__read(__is);
    __is->read(snapShotMode);
    __is->read(frameIndex);
    __is->read(imageWidth);
    __is->read(imageHeight);
    __is->read(imageWidthStep);
    ::LDTSSInterface::__read(__is, format);
    __is->read(timeStamp);
    __is->read(imageDataLength);
    __is->read(imageDataCRC);
    __is->read(watermark);
    location.__read(__is);
    ::LDTSSInterface::__readStringStringDict(__is, properies);
}

void
LDTSSInterface::__writeVehicleImageDescriptionSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::VehicleImageDescription* begin, const ::LDTSSInterface::VehicleImageDescription* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readVehicleImageDescriptionSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::VehicleImageDescriptionSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 37);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::VehicleImage::operator==(const VehicleImage& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(imageDescription != __rhs.imageDescription)
    {
        return false;
    }
    if(imageData != __rhs.imageData)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::VehicleImage::operator<(const VehicleImage& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(imageDescription < __rhs.imageDescription)
    {
        return true;
    }
    else if(__rhs.imageDescription < imageDescription)
    {
        return false;
    }
    if(imageData < __rhs.imageData)
    {
        return true;
    }
    else if(__rhs.imageData < imageData)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::VehicleImage::__write(::IceInternal::BasicStream* __os) const
{
    imageDescription.__write(__os);
    if(imageData.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&imageData[0], &imageData[0] + imageData.size());
    }
}

void
LDTSSInterface::VehicleImage::__read(::IceInternal::BasicStream* __is)
{
    imageDescription.__read(__is);
    ::std::pair<const ::Ice::Byte*, const ::Ice::Byte*> ___imageData;
    __is->read(___imageData);
    ::std::vector< ::Ice::Byte>(___imageData.first, ___imageData.second).swap(imageData);
}

void
LDTSSInterface::__writeVehicleImageSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::VehicleImage* begin, const ::LDTSSInterface::VehicleImage* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readVehicleImageSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::VehicleImageSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 38);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::InspectedVehicle::operator==(const InspectedVehicle& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(inspectDepartment != __rhs.inspectDepartment)
    {
        return false;
    }
    if(inspectAction != __rhs.inspectAction)
    {
        return false;
    }
    if(inspectTime != __rhs.inspectTime)
    {
        return false;
    }
    if(ExpireDate != __rhs.ExpireDate)
    {
        return false;
    }
    if(plateNbr != __rhs.plateNbr)
    {
        return false;
    }
    if(plateColor != __rhs.plateColor)
    {
        return false;
    }
    if(plateType != __rhs.plateType)
    {
        return false;
    }
    if(vehicleType != __rhs.vehicleType)
    {
        return false;
    }
    if(description != __rhs.description)
    {
        return false;
    }
    if(updateTime != __rhs.updateTime)
    {
        return false;
    }
    if(extendedProperties != __rhs.extendedProperties)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::InspectedVehicle::operator<(const InspectedVehicle& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(inspectDepartment < __rhs.inspectDepartment)
    {
        return true;
    }
    else if(__rhs.inspectDepartment < inspectDepartment)
    {
        return false;
    }
    if(inspectAction < __rhs.inspectAction)
    {
        return true;
    }
    else if(__rhs.inspectAction < inspectAction)
    {
        return false;
    }
    if(inspectTime < __rhs.inspectTime)
    {
        return true;
    }
    else if(__rhs.inspectTime < inspectTime)
    {
        return false;
    }
    if(ExpireDate < __rhs.ExpireDate)
    {
        return true;
    }
    else if(__rhs.ExpireDate < ExpireDate)
    {
        return false;
    }
    if(plateNbr < __rhs.plateNbr)
    {
        return true;
    }
    else if(__rhs.plateNbr < plateNbr)
    {
        return false;
    }
    if(plateColor < __rhs.plateColor)
    {
        return true;
    }
    else if(__rhs.plateColor < plateColor)
    {
        return false;
    }
    if(plateType < __rhs.plateType)
    {
        return true;
    }
    else if(__rhs.plateType < plateType)
    {
        return false;
    }
    if(vehicleType < __rhs.vehicleType)
    {
        return true;
    }
    else if(__rhs.vehicleType < vehicleType)
    {
        return false;
    }
    if(description < __rhs.description)
    {
        return true;
    }
    else if(__rhs.description < description)
    {
        return false;
    }
    if(updateTime < __rhs.updateTime)
    {
        return true;
    }
    else if(__rhs.updateTime < updateTime)
    {
        return false;
    }
    if(extendedProperties < __rhs.extendedProperties)
    {
        return true;
    }
    else if(__rhs.extendedProperties < extendedProperties)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::InspectedVehicle::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(inspectDepartment);
    __os->write(inspectAction);
    __os->write(inspectTime);
    __os->write(ExpireDate);
    __os->write(plateNbr);
    __os->write(plateColor);
    __os->write(plateType);
    __os->write(vehicleType);
    __os->write(description);
    __os->write(updateTime);
    ::LDTSSInterface::__writeStringStringDict(__os, extendedProperties);
}

void
LDTSSInterface::InspectedVehicle::__read(::IceInternal::BasicStream* __is)
{
    __is->read(inspectDepartment);
    __is->read(inspectAction);
    __is->read(inspectTime);
    __is->read(ExpireDate);
    __is->read(plateNbr);
    __is->read(plateColor);
    __is->read(plateType);
    __is->read(vehicleType);
    __is->read(description);
    __is->read(updateTime);
    ::LDTSSInterface::__readStringStringDict(__is, extendedProperties);
}

void
LDTSSInterface::__writeInspectedVehicleSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::InspectedVehicle* begin, const ::LDTSSInterface::InspectedVehicle* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readInspectedVehicleSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::InspectedVehicleSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 11);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::PassingVehicle::operator==(const PassingVehicle& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(deviceNo != __rhs.deviceNo)
    {
        return false;
    }
    if(snapNbr != __rhs.snapNbr)
    {
        return false;
    }
    if(roadCode != __rhs.roadCode)
    {
        return false;
    }
    if(driveDirection != __rhs.driveDirection)
    {
        return false;
    }
    if(lane != __rhs.lane)
    {
        return false;
    }
    if(plateNbr != __rhs.plateNbr)
    {
        return false;
    }
    if(plateType != __rhs.plateType)
    {
        return false;
    }
    if(captureTime != __rhs.captureTime)
    {
        return false;
    }
    if(vehicleSpeed != __rhs.vehicleSpeed)
    {
        return false;
    }
    if(driveMode != __rhs.driveMode)
    {
        return false;
    }
    if(vehicleLength != __rhs.vehicleLength)
    {
        return false;
    }
    if(plateColor != __rhs.plateColor)
    {
        return false;
    }
    if(vehicleType != __rhs.vehicleType)
    {
        return false;
    }
    if(violationBehaviors != __rhs.violationBehaviors)
    {
        return false;
    }
    if(inspectedVehicles != __rhs.inspectedVehicles)
    {
        return false;
    }
    if(gpsLocationInfo != __rhs.gpsLocationInfo)
    {
        return false;
    }
    if(imageDescriptions != __rhs.imageDescriptions)
    {
        return false;
    }
    if(extendedProperties != __rhs.extendedProperties)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::PassingVehicle::operator<(const PassingVehicle& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(deviceNo < __rhs.deviceNo)
    {
        return true;
    }
    else if(__rhs.deviceNo < deviceNo)
    {
        return false;
    }
    if(snapNbr < __rhs.snapNbr)
    {
        return true;
    }
    else if(__rhs.snapNbr < snapNbr)
    {
        return false;
    }
    if(roadCode < __rhs.roadCode)
    {
        return true;
    }
    else if(__rhs.roadCode < roadCode)
    {
        return false;
    }
    if(driveDirection < __rhs.driveDirection)
    {
        return true;
    }
    else if(__rhs.driveDirection < driveDirection)
    {
        return false;
    }
    if(lane < __rhs.lane)
    {
        return true;
    }
    else if(__rhs.lane < lane)
    {
        return false;
    }
    if(plateNbr < __rhs.plateNbr)
    {
        return true;
    }
    else if(__rhs.plateNbr < plateNbr)
    {
        return false;
    }
    if(plateType < __rhs.plateType)
    {
        return true;
    }
    else if(__rhs.plateType < plateType)
    {
        return false;
    }
    if(captureTime < __rhs.captureTime)
    {
        return true;
    }
    else if(__rhs.captureTime < captureTime)
    {
        return false;
    }
    if(vehicleSpeed < __rhs.vehicleSpeed)
    {
        return true;
    }
    else if(__rhs.vehicleSpeed < vehicleSpeed)
    {
        return false;
    }
    if(driveMode < __rhs.driveMode)
    {
        return true;
    }
    else if(__rhs.driveMode < driveMode)
    {
        return false;
    }
    if(vehicleLength < __rhs.vehicleLength)
    {
        return true;
    }
    else if(__rhs.vehicleLength < vehicleLength)
    {
        return false;
    }
    if(plateColor < __rhs.plateColor)
    {
        return true;
    }
    else if(__rhs.plateColor < plateColor)
    {
        return false;
    }
    if(vehicleType < __rhs.vehicleType)
    {
        return true;
    }
    else if(__rhs.vehicleType < vehicleType)
    {
        return false;
    }
    if(violationBehaviors < __rhs.violationBehaviors)
    {
        return true;
    }
    else if(__rhs.violationBehaviors < violationBehaviors)
    {
        return false;
    }
    if(inspectedVehicles < __rhs.inspectedVehicles)
    {
        return true;
    }
    else if(__rhs.inspectedVehicles < inspectedVehicles)
    {
        return false;
    }
    if(gpsLocationInfo < __rhs.gpsLocationInfo)
    {
        return true;
    }
    else if(__rhs.gpsLocationInfo < gpsLocationInfo)
    {
        return false;
    }
    if(imageDescriptions < __rhs.imageDescriptions)
    {
        return true;
    }
    else if(__rhs.imageDescriptions < imageDescriptions)
    {
        return false;
    }
    if(extendedProperties < __rhs.extendedProperties)
    {
        return true;
    }
    else if(__rhs.extendedProperties < extendedProperties)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::PassingVehicle::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(deviceNo);
    __os->write(snapNbr);
    __os->write(roadCode);
    __os->write(driveDirection);
    __os->write(lane);
    __os->write(plateNbr);
    __os->write(plateType);
    __os->write(captureTime);
    __os->write(vehicleSpeed);
    __os->write(driveMode);
    __os->write(vehicleLength);
    __os->write(plateColor);
    __os->write(vehicleType);
    if(violationBehaviors.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&violationBehaviors[0], &violationBehaviors[0] + violationBehaviors.size());
    }
    if(inspectedVehicles.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeInspectedVehicleSeq(__os, &inspectedVehicles[0], &inspectedVehicles[0] + inspectedVehicles.size());
    }
    __os->write(gpsLocationInfo);
    if(imageDescriptions.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeVehicleImageDescriptionSeq(__os, &imageDescriptions[0], &imageDescriptions[0] + imageDescriptions.size());
    }
    ::LDTSSInterface::__writeStringStringDict(__os, extendedProperties);
}

void
LDTSSInterface::PassingVehicle::__read(::IceInternal::BasicStream* __is)
{
    __is->read(deviceNo);
    __is->read(snapNbr);
    __is->read(roadCode);
    __is->read(driveDirection);
    __is->read(lane);
    __is->read(plateNbr);
    __is->read(plateType);
    __is->read(captureTime);
    __is->read(vehicleSpeed);
    __is->read(driveMode);
    __is->read(vehicleLength);
    __is->read(plateColor);
    __is->read(vehicleType);
    __is->read(violationBehaviors);
    ::LDTSSInterface::__readInspectedVehicleSeq(__is, inspectedVehicles);
    __is->read(gpsLocationInfo);
    ::LDTSSInterface::__readVehicleImageDescriptionSeq(__is, imageDescriptions);
    ::LDTSSInterface::__readStringStringDict(__is, extendedProperties);
}

void
LDTSSInterface::__writePassingVehicleSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::PassingVehicle* begin, const ::LDTSSInterface::PassingVehicle* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readPassingVehicleSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::PassingVehicleSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 30);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::ViolationVehicle::operator==(const ViolationVehicle& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(deviceNo != __rhs.deviceNo)
    {
        return false;
    }
    if(snapNbr != __rhs.snapNbr)
    {
        return false;
    }
    if(roadCode != __rhs.roadCode)
    {
        return false;
    }
    if(driveDirection != __rhs.driveDirection)
    {
        return false;
    }
    if(lane != __rhs.lane)
    {
        return false;
    }
    if(plateNbr != __rhs.plateNbr)
    {
        return false;
    }
    if(plateType != __rhs.plateType)
    {
        return false;
    }
    if(captureTime != __rhs.captureTime)
    {
        return false;
    }
    if(vehicleSpeed != __rhs.vehicleSpeed)
    {
        return false;
    }
    if(plateColor != __rhs.plateColor)
    {
        return false;
    }
    if(vehicleType != __rhs.vehicleType)
    {
        return false;
    }
    if(violationBehaviors != __rhs.violationBehaviors)
    {
        return false;
    }
    if(roadSpeedLimits != __rhs.roadSpeedLimits)
    {
        return false;
    }
    if(redlightTime != __rhs.redlightTime)
    {
        return false;
    }
    if(gpsLocationInfo != __rhs.gpsLocationInfo)
    {
        return false;
    }
    if(extendedProperties != __rhs.extendedProperties)
    {
        return false;
    }
    if(imageDescriptions != __rhs.imageDescriptions)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::ViolationVehicle::operator<(const ViolationVehicle& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(deviceNo < __rhs.deviceNo)
    {
        return true;
    }
    else if(__rhs.deviceNo < deviceNo)
    {
        return false;
    }
    if(snapNbr < __rhs.snapNbr)
    {
        return true;
    }
    else if(__rhs.snapNbr < snapNbr)
    {
        return false;
    }
    if(roadCode < __rhs.roadCode)
    {
        return true;
    }
    else if(__rhs.roadCode < roadCode)
    {
        return false;
    }
    if(driveDirection < __rhs.driveDirection)
    {
        return true;
    }
    else if(__rhs.driveDirection < driveDirection)
    {
        return false;
    }
    if(lane < __rhs.lane)
    {
        return true;
    }
    else if(__rhs.lane < lane)
    {
        return false;
    }
    if(plateNbr < __rhs.plateNbr)
    {
        return true;
    }
    else if(__rhs.plateNbr < plateNbr)
    {
        return false;
    }
    if(plateType < __rhs.plateType)
    {
        return true;
    }
    else if(__rhs.plateType < plateType)
    {
        return false;
    }
    if(captureTime < __rhs.captureTime)
    {
        return true;
    }
    else if(__rhs.captureTime < captureTime)
    {
        return false;
    }
    if(vehicleSpeed < __rhs.vehicleSpeed)
    {
        return true;
    }
    else if(__rhs.vehicleSpeed < vehicleSpeed)
    {
        return false;
    }
    if(plateColor < __rhs.plateColor)
    {
        return true;
    }
    else if(__rhs.plateColor < plateColor)
    {
        return false;
    }
    if(vehicleType < __rhs.vehicleType)
    {
        return true;
    }
    else if(__rhs.vehicleType < vehicleType)
    {
        return false;
    }
    if(violationBehaviors < __rhs.violationBehaviors)
    {
        return true;
    }
    else if(__rhs.violationBehaviors < violationBehaviors)
    {
        return false;
    }
    if(roadSpeedLimits < __rhs.roadSpeedLimits)
    {
        return true;
    }
    else if(__rhs.roadSpeedLimits < roadSpeedLimits)
    {
        return false;
    }
    if(redlightTime < __rhs.redlightTime)
    {
        return true;
    }
    else if(__rhs.redlightTime < redlightTime)
    {
        return false;
    }
    if(gpsLocationInfo < __rhs.gpsLocationInfo)
    {
        return true;
    }
    else if(__rhs.gpsLocationInfo < gpsLocationInfo)
    {
        return false;
    }
    if(extendedProperties < __rhs.extendedProperties)
    {
        return true;
    }
    else if(__rhs.extendedProperties < extendedProperties)
    {
        return false;
    }
    if(imageDescriptions < __rhs.imageDescriptions)
    {
        return true;
    }
    else if(__rhs.imageDescriptions < imageDescriptions)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::ViolationVehicle::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(deviceNo);
    __os->write(snapNbr);
    __os->write(roadCode);
    __os->write(driveDirection);
    __os->write(lane);
    __os->write(plateNbr);
    __os->write(plateType);
    __os->write(captureTime);
    __os->write(vehicleSpeed);
    __os->write(plateColor);
    __os->write(vehicleType);
    if(violationBehaviors.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&violationBehaviors[0], &violationBehaviors[0] + violationBehaviors.size());
    }
    if(roadSpeedLimits.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeSpeedLimitSeq(__os, &roadSpeedLimits[0], &roadSpeedLimits[0] + roadSpeedLimits.size());
    }
    __os->write(redlightTime);
    __os->write(gpsLocationInfo);
    ::LDTSSInterface::__writeStringStringDict(__os, extendedProperties);
    if(imageDescriptions.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeVehicleImageDescriptionSeq(__os, &imageDescriptions[0], &imageDescriptions[0] + imageDescriptions.size());
    }
}

void
LDTSSInterface::ViolationVehicle::__read(::IceInternal::BasicStream* __is)
{
    __is->read(deviceNo);
    __is->read(snapNbr);
    __is->read(roadCode);
    __is->read(driveDirection);
    __is->read(lane);
    __is->read(plateNbr);
    __is->read(plateType);
    __is->read(captureTime);
    __is->read(vehicleSpeed);
    __is->read(plateColor);
    __is->read(vehicleType);
    __is->read(violationBehaviors);
    ::LDTSSInterface::__readSpeedLimitSeq(__is, roadSpeedLimits);
    __is->read(redlightTime);
    __is->read(gpsLocationInfo);
    ::LDTSSInterface::__readStringStringDict(__is, extendedProperties);
    ::LDTSSInterface::__readVehicleImageDescriptionSeq(__is, imageDescriptions);
}

void
LDTSSInterface::__writeViolationVehicleSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::ViolationVehicle* begin, const ::LDTSSInterface::ViolationVehicle* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readViolationVehicleSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::ViolationVehicleSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 23);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::TrafficStats::operator==(const TrafficStats& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(roadCode != __rhs.roadCode)
    {
        return false;
    }
    if(deviceNo != __rhs.deviceNo)
    {
        return false;
    }
    if(driveDirection != __rhs.driveDirection)
    {
        return false;
    }
    if(driveLane != __rhs.driveLane)
    {
        return false;
    }
    if(statsTime != __rhs.statsTime)
    {
        return false;
    }
    if(statsPeriod != __rhs.statsPeriod)
    {
        return false;
    }
    if(vehicleTotal != __rhs.vehicleTotal)
    {
        return false;
    }
    if(vehicleSpeed != __rhs.vehicleSpeed)
    {
        return false;
    }
    if(vehicleLength != __rhs.vehicleLength)
    {
        return false;
    }
    if(timeOccupyRatio != __rhs.timeOccupyRatio)
    {
        return false;
    }
    if(spaceOccupyRatio != __rhs.spaceOccupyRatio)
    {
        return false;
    }
    if(spaceHeadway != __rhs.spaceHeadway)
    {
        return false;
    }
    if(timeHeadway != __rhs.timeHeadway)
    {
        return false;
    }
    if(density != __rhs.density)
    {
        return false;
    }
    if(overSpeedTotal != __rhs.overSpeedTotal)
    {
        return false;
    }
    if(underSpeedTotal != __rhs.underSpeedTotal)
    {
        return false;
    }
    if(largeVehicleTotal != __rhs.largeVehicleTotal)
    {
        return false;
    }
    if(middleVehicleTotal != __rhs.middleVehicleTotal)
    {
        return false;
    }
    if(smallVehicleTotal != __rhs.smallVehicleTotal)
    {
        return false;
    }
    if(motorVehicleTotal != __rhs.motorVehicleTotal)
    {
        return false;
    }
    if(longVehicleTotal != __rhs.longVehicleTotal)
    {
        return false;
    }
    if(otherTrafficParams != __rhs.otherTrafficParams)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::TrafficStats::operator<(const TrafficStats& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(roadCode < __rhs.roadCode)
    {
        return true;
    }
    else if(__rhs.roadCode < roadCode)
    {
        return false;
    }
    if(deviceNo < __rhs.deviceNo)
    {
        return true;
    }
    else if(__rhs.deviceNo < deviceNo)
    {
        return false;
    }
    if(driveDirection < __rhs.driveDirection)
    {
        return true;
    }
    else if(__rhs.driveDirection < driveDirection)
    {
        return false;
    }
    if(driveLane < __rhs.driveLane)
    {
        return true;
    }
    else if(__rhs.driveLane < driveLane)
    {
        return false;
    }
    if(statsTime < __rhs.statsTime)
    {
        return true;
    }
    else if(__rhs.statsTime < statsTime)
    {
        return false;
    }
    if(statsPeriod < __rhs.statsPeriod)
    {
        return true;
    }
    else if(__rhs.statsPeriod < statsPeriod)
    {
        return false;
    }
    if(vehicleTotal < __rhs.vehicleTotal)
    {
        return true;
    }
    else if(__rhs.vehicleTotal < vehicleTotal)
    {
        return false;
    }
    if(vehicleSpeed < __rhs.vehicleSpeed)
    {
        return true;
    }
    else if(__rhs.vehicleSpeed < vehicleSpeed)
    {
        return false;
    }
    if(vehicleLength < __rhs.vehicleLength)
    {
        return true;
    }
    else if(__rhs.vehicleLength < vehicleLength)
    {
        return false;
    }
    if(timeOccupyRatio < __rhs.timeOccupyRatio)
    {
        return true;
    }
    else if(__rhs.timeOccupyRatio < timeOccupyRatio)
    {
        return false;
    }
    if(spaceOccupyRatio < __rhs.spaceOccupyRatio)
    {
        return true;
    }
    else if(__rhs.spaceOccupyRatio < spaceOccupyRatio)
    {
        return false;
    }
    if(spaceHeadway < __rhs.spaceHeadway)
    {
        return true;
    }
    else if(__rhs.spaceHeadway < spaceHeadway)
    {
        return false;
    }
    if(timeHeadway < __rhs.timeHeadway)
    {
        return true;
    }
    else if(__rhs.timeHeadway < timeHeadway)
    {
        return false;
    }
    if(density < __rhs.density)
    {
        return true;
    }
    else if(__rhs.density < density)
    {
        return false;
    }
    if(overSpeedTotal < __rhs.overSpeedTotal)
    {
        return true;
    }
    else if(__rhs.overSpeedTotal < overSpeedTotal)
    {
        return false;
    }
    if(underSpeedTotal < __rhs.underSpeedTotal)
    {
        return true;
    }
    else if(__rhs.underSpeedTotal < underSpeedTotal)
    {
        return false;
    }
    if(largeVehicleTotal < __rhs.largeVehicleTotal)
    {
        return true;
    }
    else if(__rhs.largeVehicleTotal < largeVehicleTotal)
    {
        return false;
    }
    if(middleVehicleTotal < __rhs.middleVehicleTotal)
    {
        return true;
    }
    else if(__rhs.middleVehicleTotal < middleVehicleTotal)
    {
        return false;
    }
    if(smallVehicleTotal < __rhs.smallVehicleTotal)
    {
        return true;
    }
    else if(__rhs.smallVehicleTotal < smallVehicleTotal)
    {
        return false;
    }
    if(motorVehicleTotal < __rhs.motorVehicleTotal)
    {
        return true;
    }
    else if(__rhs.motorVehicleTotal < motorVehicleTotal)
    {
        return false;
    }
    if(longVehicleTotal < __rhs.longVehicleTotal)
    {
        return true;
    }
    else if(__rhs.longVehicleTotal < longVehicleTotal)
    {
        return false;
    }
    if(otherTrafficParams < __rhs.otherTrafficParams)
    {
        return true;
    }
    else if(__rhs.otherTrafficParams < otherTrafficParams)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::TrafficStats::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(roadCode);
    __os->write(deviceNo);
    __os->write(driveDirection);
    __os->write(driveLane);
    __os->write(statsTime);
    __os->write(statsPeriod);
    __os->write(vehicleTotal);
    __os->write(vehicleSpeed);
    __os->write(vehicleLength);
    __os->write(timeOccupyRatio);
    __os->write(spaceOccupyRatio);
    __os->write(spaceHeadway);
    __os->write(timeHeadway);
    __os->write(density);
    __os->write(overSpeedTotal);
    __os->write(underSpeedTotal);
    __os->write(largeVehicleTotal);
    __os->write(middleVehicleTotal);
    __os->write(smallVehicleTotal);
    __os->write(motorVehicleTotal);
    __os->write(longVehicleTotal);
    ::LDTSSInterface::__writeStringStringDict(__os, otherTrafficParams);
}

void
LDTSSInterface::TrafficStats::__read(::IceInternal::BasicStream* __is)
{
    __is->read(roadCode);
    __is->read(deviceNo);
    __is->read(driveDirection);
    __is->read(driveLane);
    __is->read(statsTime);
    __is->read(statsPeriod);
    __is->read(vehicleTotal);
    __is->read(vehicleSpeed);
    __is->read(vehicleLength);
    __is->read(timeOccupyRatio);
    __is->read(spaceOccupyRatio);
    __is->read(spaceHeadway);
    __is->read(timeHeadway);
    __is->read(density);
    __is->read(overSpeedTotal);
    __is->read(underSpeedTotal);
    __is->read(largeVehicleTotal);
    __is->read(middleVehicleTotal);
    __is->read(smallVehicleTotal);
    __is->read(motorVehicleTotal);
    __is->read(longVehicleTotal);
    ::LDTSSInterface::__readStringStringDict(__is, otherTrafficParams);
}

void
LDTSSInterface::__writeTrafficStatsSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::TrafficStats* begin, const ::LDTSSInterface::TrafficStats* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readTrafficStatsSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::TrafficStatsSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 73);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::SurveyDeviceStatus::operator==(const SurveyDeviceStatus& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(deviceNo != __rhs.deviceNo)
    {
        return false;
    }
    if(deviceIpAddr != __rhs.deviceIpAddr)
    {
        return false;
    }
    if(status != __rhs.status)
    {
        return false;
    }
    if(faultDetail != __rhs.faultDetail)
    {
        return false;
    }
    if(statsInfo != __rhs.statsInfo)
    {
        return false;
    }
    if(gpsLocation != __rhs.gpsLocation)
    {
        return false;
    }
    if(startTime != __rhs.startTime)
    {
        return false;
    }
    if(deviceTime != __rhs.deviceTime)
    {
        return false;
    }
    if(otherStatusInfo != __rhs.otherStatusInfo)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::SurveyDeviceStatus::operator<(const SurveyDeviceStatus& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(deviceNo < __rhs.deviceNo)
    {
        return true;
    }
    else if(__rhs.deviceNo < deviceNo)
    {
        return false;
    }
    if(deviceIpAddr < __rhs.deviceIpAddr)
    {
        return true;
    }
    else if(__rhs.deviceIpAddr < deviceIpAddr)
    {
        return false;
    }
    if(status < __rhs.status)
    {
        return true;
    }
    else if(__rhs.status < status)
    {
        return false;
    }
    if(faultDetail < __rhs.faultDetail)
    {
        return true;
    }
    else if(__rhs.faultDetail < faultDetail)
    {
        return false;
    }
    if(statsInfo < __rhs.statsInfo)
    {
        return true;
    }
    else if(__rhs.statsInfo < statsInfo)
    {
        return false;
    }
    if(gpsLocation < __rhs.gpsLocation)
    {
        return true;
    }
    else if(__rhs.gpsLocation < gpsLocation)
    {
        return false;
    }
    if(startTime < __rhs.startTime)
    {
        return true;
    }
    else if(__rhs.startTime < startTime)
    {
        return false;
    }
    if(deviceTime < __rhs.deviceTime)
    {
        return true;
    }
    else if(__rhs.deviceTime < deviceTime)
    {
        return false;
    }
    if(otherStatusInfo < __rhs.otherStatusInfo)
    {
        return true;
    }
    else if(__rhs.otherStatusInfo < otherStatusInfo)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::SurveyDeviceStatus::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(deviceNo);
    __os->write(deviceIpAddr);
    __os->write(status);
    __os->write(faultDetail);
    ::LDTSSInterface::__writeStringIntDict(__os, statsInfo);
    __os->write(gpsLocation);
    __os->write(startTime);
    __os->write(deviceTime);
    ::LDTSSInterface::__writeStringStringDict(__os, otherStatusInfo);
}

void
LDTSSInterface::SurveyDeviceStatus::__read(::IceInternal::BasicStream* __is)
{
    __is->read(deviceNo);
    __is->read(deviceIpAddr);
    __is->read(status);
    __is->read(faultDetail);
    ::LDTSSInterface::__readStringIntDict(__is, statsInfo);
    __is->read(gpsLocation);
    __is->read(startTime);
    __is->read(deviceTime);
    ::LDTSSInterface::__readStringStringDict(__is, otherStatusInfo);
}

bool
LDTSSInterface::CameraInfo::operator==(const CameraInfo& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(cameraId != __rhs.cameraId)
    {
        return false;
    }
    if(cameraType != __rhs.cameraType)
    {
        return false;
    }
    if(ipAddr != __rhs.ipAddr)
    {
        return false;
    }
    if(otherProperties != __rhs.otherProperties)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::CameraInfo::operator<(const CameraInfo& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(cameraId < __rhs.cameraId)
    {
        return true;
    }
    else if(__rhs.cameraId < cameraId)
    {
        return false;
    }
    if(cameraType < __rhs.cameraType)
    {
        return true;
    }
    else if(__rhs.cameraType < cameraType)
    {
        return false;
    }
    if(ipAddr < __rhs.ipAddr)
    {
        return true;
    }
    else if(__rhs.ipAddr < ipAddr)
    {
        return false;
    }
    if(otherProperties < __rhs.otherProperties)
    {
        return true;
    }
    else if(__rhs.otherProperties < otherProperties)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::CameraInfo::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(cameraId);
    __os->write(cameraType);
    __os->write(ipAddr);
    ::LDTSSInterface::__writeStringStringDict(__os, otherProperties);
}

void
LDTSSInterface::CameraInfo::__read(::IceInternal::BasicStream* __is)
{
    __is->read(cameraId);
    __is->read(cameraType);
    __is->read(ipAddr);
    ::LDTSSInterface::__readStringStringDict(__is, otherProperties);
}

void
LDTSSInterface::__writeCameraInfoSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::CameraInfo* begin, const ::LDTSSInterface::CameraInfo* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readCameraInfoSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::CameraInfoSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 7);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}

bool
LDTSSInterface::SurveyDeviceAttribute::operator==(const SurveyDeviceAttribute& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(staticIpAddr != __rhs.staticIpAddr)
    {
        return false;
    }
    if(remoteControlPort != __rhs.remoteControlPort)
    {
        return false;
    }
    if(cameras != __rhs.cameras)
    {
        return false;
    }
    if(components != __rhs.components)
    {
        return false;
    }
    if(surveyMode != __rhs.surveyMode)
    {
        return false;
    }
    if(model != __rhs.model)
    {
        return false;
    }
    if(manufactor != __rhs.manufactor)
    {
        return false;
    }
    if(manufactureId != __rhs.manufactureId)
    {
        return false;
    }
    if(protocolType != __rhs.protocolType)
    {
        return false;
    }
    if(protocolVersion != __rhs.protocolVersion)
    {
        return false;
    }
    if(inspectionMode != __rhs.inspectionMode)
    {
        return false;
    }
    if(extendedProperties != __rhs.extendedProperties)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::SurveyDeviceAttribute::operator<(const SurveyDeviceAttribute& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(staticIpAddr < __rhs.staticIpAddr)
    {
        return true;
    }
    else if(__rhs.staticIpAddr < staticIpAddr)
    {
        return false;
    }
    if(remoteControlPort < __rhs.remoteControlPort)
    {
        return true;
    }
    else if(__rhs.remoteControlPort < remoteControlPort)
    {
        return false;
    }
    if(cameras < __rhs.cameras)
    {
        return true;
    }
    else if(__rhs.cameras < cameras)
    {
        return false;
    }
    if(components < __rhs.components)
    {
        return true;
    }
    else if(__rhs.components < components)
    {
        return false;
    }
    if(surveyMode < __rhs.surveyMode)
    {
        return true;
    }
    else if(__rhs.surveyMode < surveyMode)
    {
        return false;
    }
    if(model < __rhs.model)
    {
        return true;
    }
    else if(__rhs.model < model)
    {
        return false;
    }
    if(manufactor < __rhs.manufactor)
    {
        return true;
    }
    else if(__rhs.manufactor < manufactor)
    {
        return false;
    }
    if(manufactureId < __rhs.manufactureId)
    {
        return true;
    }
    else if(__rhs.manufactureId < manufactureId)
    {
        return false;
    }
    if(protocolType < __rhs.protocolType)
    {
        return true;
    }
    else if(__rhs.protocolType < protocolType)
    {
        return false;
    }
    if(protocolVersion < __rhs.protocolVersion)
    {
        return true;
    }
    else if(__rhs.protocolVersion < protocolVersion)
    {
        return false;
    }
    if(inspectionMode < __rhs.inspectionMode)
    {
        return true;
    }
    else if(__rhs.inspectionMode < inspectionMode)
    {
        return false;
    }
    if(extendedProperties < __rhs.extendedProperties)
    {
        return true;
    }
    else if(__rhs.extendedProperties < extendedProperties)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::SurveyDeviceAttribute::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(staticIpAddr);
    __os->write(remoteControlPort);
    if(cameras.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeCameraInfoSeq(__os, &cameras[0], &cameras[0] + cameras.size());
    }
    ::LDTSSInterface::__writeStringStringDict(__os, components);
    __os->write(surveyMode);
    __os->write(model);
    __os->write(manufactor);
    __os->write(manufactureId);
    __os->write(protocolType);
    __os->write(protocolVersion);
    __os->write(inspectionMode);
    ::LDTSSInterface::__writeStringStringDict(__os, extendedProperties);
}

void
LDTSSInterface::SurveyDeviceAttribute::__read(::IceInternal::BasicStream* __is)
{
    __is->read(staticIpAddr);
    __is->read(remoteControlPort);
    ::LDTSSInterface::__readCameraInfoSeq(__is, cameras);
    ::LDTSSInterface::__readStringStringDict(__is, components);
    __is->read(surveyMode);
    __is->read(model);
    __is->read(manufactor);
    __is->read(manufactureId);
    __is->read(protocolType);
    __is->read(protocolVersion);
    __is->read(inspectionMode);
    ::LDTSSInterface::__readStringStringDict(__is, extendedProperties);
}

bool
LDTSSInterface::VehicleTypeLenthLevel::operator==(const VehicleTypeLenthLevel& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(largeVehicleLengthLevel != __rhs.largeVehicleLengthLevel)
    {
        return false;
    }
    if(smallVehicleLengthLevel != __rhs.smallVehicleLengthLevel)
    {
        return false;
    }
    if(motorVehicleLengthLevel != __rhs.motorVehicleLengthLevel)
    {
        return false;
    }
    if(longVehicleLengthLevel != __rhs.longVehicleLengthLevel)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::VehicleTypeLenthLevel::operator<(const VehicleTypeLenthLevel& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(largeVehicleLengthLevel < __rhs.largeVehicleLengthLevel)
    {
        return true;
    }
    else if(__rhs.largeVehicleLengthLevel < largeVehicleLengthLevel)
    {
        return false;
    }
    if(smallVehicleLengthLevel < __rhs.smallVehicleLengthLevel)
    {
        return true;
    }
    else if(__rhs.smallVehicleLengthLevel < smallVehicleLengthLevel)
    {
        return false;
    }
    if(motorVehicleLengthLevel < __rhs.motorVehicleLengthLevel)
    {
        return true;
    }
    else if(__rhs.motorVehicleLengthLevel < motorVehicleLengthLevel)
    {
        return false;
    }
    if(longVehicleLengthLevel < __rhs.longVehicleLengthLevel)
    {
        return true;
    }
    else if(__rhs.longVehicleLengthLevel < longVehicleLengthLevel)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::VehicleTypeLenthLevel::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(largeVehicleLengthLevel);
    __os->write(smallVehicleLengthLevel);
    __os->write(motorVehicleLengthLevel);
    __os->write(longVehicleLengthLevel);
}

void
LDTSSInterface::VehicleTypeLenthLevel::__read(::IceInternal::BasicStream* __is)
{
    __is->read(largeVehicleLengthLevel);
    __is->read(smallVehicleLengthLevel);
    __is->read(motorVehicleLengthLevel);
    __is->read(longVehicleLengthLevel);
}

bool
LDTSSInterface::NetworkParams::operator==(const NetworkParams& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(mac != __rhs.mac)
    {
        return false;
    }
    if(ip != __rhs.ip)
    {
        return false;
    }
    if(ipMask != __rhs.ipMask)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::NetworkParams::operator<(const NetworkParams& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(mac < __rhs.mac)
    {
        return true;
    }
    else if(__rhs.mac < mac)
    {
        return false;
    }
    if(ip < __rhs.ip)
    {
        return true;
    }
    else if(__rhs.ip < ip)
    {
        return false;
    }
    if(ipMask < __rhs.ipMask)
    {
        return true;
    }
    else if(__rhs.ipMask < ipMask)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::NetworkParams::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(mac);
    __os->write(ip);
    __os->write(ipMask);
}

void
LDTSSInterface::NetworkParams::__read(::IceInternal::BasicStream* __is)
{
    __is->read(mac);
    __is->read(ip);
    __is->read(ipMask);
}

bool
LDTSSInterface::DeviceRuntimeParams::operator==(const DeviceRuntimeParams& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(deviceNo != __rhs.deviceNo)
    {
        return false;
    }
    if(roadCode != __rhs.roadCode)
    {
        return false;
    }
    if(roadName != __rhs.roadName)
    {
        return false;
    }
    if(gpsLocation != __rhs.gpsLocation)
    {
        return false;
    }
    if(surveyUpDirection != __rhs.surveyUpDirection)
    {
        return false;
    }
    if(surveyDownDirection != __rhs.surveyDownDirection)
    {
        return false;
    }
    if(department != __rhs.department)
    {
        return false;
    }
    if(surveyServer != __rhs.surveyServer)
    {
        return false;
    }
    if(matchServer != __rhs.matchServer)
    {
        return false;
    }
    if(vehicleLengthLevel != __rhs.vehicleLengthLevel)
    {
        return false;
    }
    if(violationCodeTable != __rhs.violationCodeTable)
    {
        return false;
    }
    if(trafficStatsPeriod != __rhs.trafficStatsPeriod)
    {
        return false;
    }
    if(speedLimits != __rhs.speedLimits)
    {
        return false;
    }
    if(redlightMargin != __rhs.redlightMargin)
    {
        return false;
    }
    if(otherParams != __rhs.otherParams)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::DeviceRuntimeParams::operator<(const DeviceRuntimeParams& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(deviceNo < __rhs.deviceNo)
    {
        return true;
    }
    else if(__rhs.deviceNo < deviceNo)
    {
        return false;
    }
    if(roadCode < __rhs.roadCode)
    {
        return true;
    }
    else if(__rhs.roadCode < roadCode)
    {
        return false;
    }
    if(roadName < __rhs.roadName)
    {
        return true;
    }
    else if(__rhs.roadName < roadName)
    {
        return false;
    }
    if(gpsLocation < __rhs.gpsLocation)
    {
        return true;
    }
    else if(__rhs.gpsLocation < gpsLocation)
    {
        return false;
    }
    if(surveyUpDirection < __rhs.surveyUpDirection)
    {
        return true;
    }
    else if(__rhs.surveyUpDirection < surveyUpDirection)
    {
        return false;
    }
    if(surveyDownDirection < __rhs.surveyDownDirection)
    {
        return true;
    }
    else if(__rhs.surveyDownDirection < surveyDownDirection)
    {
        return false;
    }
    if(department < __rhs.department)
    {
        return true;
    }
    else if(__rhs.department < department)
    {
        return false;
    }
    if(surveyServer < __rhs.surveyServer)
    {
        return true;
    }
    else if(__rhs.surveyServer < surveyServer)
    {
        return false;
    }
    if(matchServer < __rhs.matchServer)
    {
        return true;
    }
    else if(__rhs.matchServer < matchServer)
    {
        return false;
    }
    if(vehicleLengthLevel < __rhs.vehicleLengthLevel)
    {
        return true;
    }
    else if(__rhs.vehicleLengthLevel < vehicleLengthLevel)
    {
        return false;
    }
    if(violationCodeTable < __rhs.violationCodeTable)
    {
        return true;
    }
    else if(__rhs.violationCodeTable < violationCodeTable)
    {
        return false;
    }
    if(trafficStatsPeriod < __rhs.trafficStatsPeriod)
    {
        return true;
    }
    else if(__rhs.trafficStatsPeriod < trafficStatsPeriod)
    {
        return false;
    }
    if(speedLimits < __rhs.speedLimits)
    {
        return true;
    }
    else if(__rhs.speedLimits < speedLimits)
    {
        return false;
    }
    if(redlightMargin < __rhs.redlightMargin)
    {
        return true;
    }
    else if(__rhs.redlightMargin < redlightMargin)
    {
        return false;
    }
    if(otherParams < __rhs.otherParams)
    {
        return true;
    }
    else if(__rhs.otherParams < otherParams)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::DeviceRuntimeParams::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(deviceNo);
    __os->write(roadCode);
    __os->write(roadName);
    __os->write(gpsLocation);
    __os->write(surveyUpDirection);
    __os->write(surveyDownDirection);
    __os->write(department);
    __os->write(surveyServer);
    __os->write(matchServer);
    vehicleLengthLevel.__write(__os);
    if(violationCodeTable.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeViolationCodeSeq(__os, &violationCodeTable[0], &violationCodeTable[0] + violationCodeTable.size());
    }
    __os->write(trafficStatsPeriod);
    if(speedLimits.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeSpeedLimitSeq(__os, &speedLimits[0], &speedLimits[0] + speedLimits.size());
    }
    if(redlightMargin.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&redlightMargin[0], &redlightMargin[0] + redlightMargin.size());
    }
    ::LDTSSInterface::__writeStringStringDict(__os, otherParams);
}

void
LDTSSInterface::DeviceRuntimeParams::__read(::IceInternal::BasicStream* __is)
{
    __is->read(deviceNo);
    __is->read(roadCode);
    __is->read(roadName);
    __is->read(gpsLocation);
    __is->read(surveyUpDirection);
    __is->read(surveyDownDirection);
    __is->read(department);
    __is->read(surveyServer);
    __is->read(matchServer);
    vehicleLengthLevel.__read(__is);
    ::LDTSSInterface::__readViolationCodeSeq(__is, violationCodeTable);
    __is->read(trafficStatsPeriod);
    ::LDTSSInterface::__readSpeedLimitSeq(__is, speedLimits);
    __is->read(redlightMargin);
    ::LDTSSInterface::__readStringStringDict(__is, otherParams);
}

LDTSSInterface::DatabaseException::DatabaseException(const ::std::string& __ice_message) :
    ::Ice::UserException(),
    message(__ice_message)
{
}

LDTSSInterface::DatabaseException::~DatabaseException() throw()
{
}

static const char* __LDTSSInterface__DatabaseException_name = "LDTSSInterface::DatabaseException";

::std::string
LDTSSInterface::DatabaseException::ice_name() const
{
    return __LDTSSInterface__DatabaseException_name;
}

::Ice::Exception*
LDTSSInterface::DatabaseException::ice_clone() const
{
    return new DatabaseException(*this);
}

void
LDTSSInterface::DatabaseException::ice_throw() const
{
    throw *this;
}

void
LDTSSInterface::DatabaseException::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(::std::string("::LDTSSInterface::DatabaseException"), false);
    __os->startWriteSlice();
    __os->write(message);
    __os->endWriteSlice();
}

void
LDTSSInterface::DatabaseException::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->read(myId, false);
    }
    __is->startReadSlice();
    __is->read(message);
    __is->endReadSlice();
}

struct __F__LDTSSInterface__DatabaseException : public ::IceInternal::UserExceptionFactory
{
    virtual void
    createAndThrow()
    {
        throw ::LDTSSInterface::DatabaseException();
    }
};

static ::IceInternal::UserExceptionFactoryPtr __F__LDTSSInterface__DatabaseException__Ptr = new __F__LDTSSInterface__DatabaseException;

const ::IceInternal::UserExceptionFactoryPtr&
LDTSSInterface::DatabaseException::ice_factory()
{
    return __F__LDTSSInterface__DatabaseException__Ptr;
}

class __F__LDTSSInterface__DatabaseException__Init
{
public:

    __F__LDTSSInterface__DatabaseException__Init()
    {
        ::IceInternal::factoryTable->addExceptionFactory("::LDTSSInterface::DatabaseException", ::LDTSSInterface::DatabaseException::ice_factory());
    }

    ~__F__LDTSSInterface__DatabaseException__Init()
    {
        ::IceInternal::factoryTable->removeExceptionFactory("::LDTSSInterface::DatabaseException");
    }
};

static __F__LDTSSInterface__DatabaseException__Init __F__LDTSSInterface__DatabaseException__i;

#ifdef __APPLE__
extern "C" { void __F__LDTSSInterface__DatabaseException__initializer() {} }
#endif

LDTSSInterface::GeneralUserException::GeneralUserException(const ::std::string& __ice_message) :
    ::Ice::UserException(),
    message(__ice_message)
{
}

LDTSSInterface::GeneralUserException::~GeneralUserException() throw()
{
}

static const char* __LDTSSInterface__GeneralUserException_name = "LDTSSInterface::GeneralUserException";

::std::string
LDTSSInterface::GeneralUserException::ice_name() const
{
    return __LDTSSInterface__GeneralUserException_name;
}

::Ice::Exception*
LDTSSInterface::GeneralUserException::ice_clone() const
{
    return new GeneralUserException(*this);
}

void
LDTSSInterface::GeneralUserException::ice_throw() const
{
    throw *this;
}

void
LDTSSInterface::GeneralUserException::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(::std::string("::LDTSSInterface::GeneralUserException"), false);
    __os->startWriteSlice();
    __os->write(message);
    __os->endWriteSlice();
}

void
LDTSSInterface::GeneralUserException::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->read(myId, false);
    }
    __is->startReadSlice();
    __is->read(message);
    __is->endReadSlice();
}

struct __F__LDTSSInterface__GeneralUserException : public ::IceInternal::UserExceptionFactory
{
    virtual void
    createAndThrow()
    {
        throw ::LDTSSInterface::GeneralUserException();
    }
};

static ::IceInternal::UserExceptionFactoryPtr __F__LDTSSInterface__GeneralUserException__Ptr = new __F__LDTSSInterface__GeneralUserException;

const ::IceInternal::UserExceptionFactoryPtr&
LDTSSInterface::GeneralUserException::ice_factory()
{
    return __F__LDTSSInterface__GeneralUserException__Ptr;
}

class __F__LDTSSInterface__GeneralUserException__Init
{
public:

    __F__LDTSSInterface__GeneralUserException__Init()
    {
        ::IceInternal::factoryTable->addExceptionFactory("::LDTSSInterface::GeneralUserException", ::LDTSSInterface::GeneralUserException::ice_factory());
    }

    ~__F__LDTSSInterface__GeneralUserException__Init()
    {
        ::IceInternal::factoryTable->removeExceptionFactory("::LDTSSInterface::GeneralUserException");
    }
};

static __F__LDTSSInterface__GeneralUserException__Init __F__LDTSSInterface__GeneralUserException__i;

#ifdef __APPLE__
extern "C" { void __F__LDTSSInterface__GeneralUserException__initializer() {} }
#endif

LDTSSInterface::DevCommException::DevCommException(const ::std::string& __ice_message) :
    ::Ice::UserException(),
    message(__ice_message)
{
}

LDTSSInterface::DevCommException::~DevCommException() throw()
{
}

static const char* __LDTSSInterface__DevCommException_name = "LDTSSInterface::DevCommException";

::std::string
LDTSSInterface::DevCommException::ice_name() const
{
    return __LDTSSInterface__DevCommException_name;
}

::Ice::Exception*
LDTSSInterface::DevCommException::ice_clone() const
{
    return new DevCommException(*this);
}

void
LDTSSInterface::DevCommException::ice_throw() const
{
    throw *this;
}

void
LDTSSInterface::DevCommException::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(::std::string("::LDTSSInterface::DevCommException"), false);
    __os->startWriteSlice();
    __os->write(message);
    __os->endWriteSlice();
}

void
LDTSSInterface::DevCommException::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->read(myId, false);
    }
    __is->startReadSlice();
    __is->read(message);
    __is->endReadSlice();
}

struct __F__LDTSSInterface__DevCommException : public ::IceInternal::UserExceptionFactory
{
    virtual void
    createAndThrow()
    {
        throw ::LDTSSInterface::DevCommException();
    }
};

static ::IceInternal::UserExceptionFactoryPtr __F__LDTSSInterface__DevCommException__Ptr = new __F__LDTSSInterface__DevCommException;

const ::IceInternal::UserExceptionFactoryPtr&
LDTSSInterface::DevCommException::ice_factory()
{
    return __F__LDTSSInterface__DevCommException__Ptr;
}

class __F__LDTSSInterface__DevCommException__Init
{
public:

    __F__LDTSSInterface__DevCommException__Init()
    {
        ::IceInternal::factoryTable->addExceptionFactory("::LDTSSInterface::DevCommException", ::LDTSSInterface::DevCommException::ice_factory());
    }

    ~__F__LDTSSInterface__DevCommException__Init()
    {
        ::IceInternal::factoryTable->removeExceptionFactory("::LDTSSInterface::DevCommException");
    }
};

static __F__LDTSSInterface__DevCommException__Init __F__LDTSSInterface__DevCommException__i;

#ifdef __APPLE__
extern "C" { void __F__LDTSSInterface__DevCommException__initializer() {} }
#endif

LDTSSInterface::DeviceNoFoundException::DeviceNoFoundException(const ::std::string& __ice_message) :
    ::Ice::UserException(),
    message(__ice_message)
{
}

LDTSSInterface::DeviceNoFoundException::~DeviceNoFoundException() throw()
{
}

static const char* __LDTSSInterface__DeviceNoFoundException_name = "LDTSSInterface::DeviceNoFoundException";

::std::string
LDTSSInterface::DeviceNoFoundException::ice_name() const
{
    return __LDTSSInterface__DeviceNoFoundException_name;
}

::Ice::Exception*
LDTSSInterface::DeviceNoFoundException::ice_clone() const
{
    return new DeviceNoFoundException(*this);
}

void
LDTSSInterface::DeviceNoFoundException::ice_throw() const
{
    throw *this;
}

void
LDTSSInterface::DeviceNoFoundException::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(::std::string("::LDTSSInterface::DeviceNoFoundException"), false);
    __os->startWriteSlice();
    __os->write(message);
    __os->endWriteSlice();
}

void
LDTSSInterface::DeviceNoFoundException::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->read(myId, false);
    }
    __is->startReadSlice();
    __is->read(message);
    __is->endReadSlice();
}

struct __F__LDTSSInterface__DeviceNoFoundException : public ::IceInternal::UserExceptionFactory
{
    virtual void
    createAndThrow()
    {
        throw ::LDTSSInterface::DeviceNoFoundException();
    }
};

static ::IceInternal::UserExceptionFactoryPtr __F__LDTSSInterface__DeviceNoFoundException__Ptr = new __F__LDTSSInterface__DeviceNoFoundException;

const ::IceInternal::UserExceptionFactoryPtr&
LDTSSInterface::DeviceNoFoundException::ice_factory()
{
    return __F__LDTSSInterface__DeviceNoFoundException__Ptr;
}

class __F__LDTSSInterface__DeviceNoFoundException__Init
{
public:

    __F__LDTSSInterface__DeviceNoFoundException__Init()
    {
        ::IceInternal::factoryTable->addExceptionFactory("::LDTSSInterface::DeviceNoFoundException", ::LDTSSInterface::DeviceNoFoundException::ice_factory());
    }

    ~__F__LDTSSInterface__DeviceNoFoundException__Init()
    {
        ::IceInternal::factoryTable->removeExceptionFactory("::LDTSSInterface::DeviceNoFoundException");
    }
};

static __F__LDTSSInterface__DeviceNoFoundException__Init __F__LDTSSInterface__DeviceNoFoundException__i;

#ifdef __APPLE__
extern "C" { void __F__LDTSSInterface__DeviceNoFoundException__initializer() {} }
#endif

bool
LDTSSInterface::FragmentInfo::operator==(const FragmentInfo& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(offset != __rhs.offset)
    {
        return false;
    }
    if(length != __rhs.length)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::FragmentInfo::operator<(const FragmentInfo& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(offset < __rhs.offset)
    {
        return true;
    }
    else if(__rhs.offset < offset)
    {
        return false;
    }
    if(length < __rhs.length)
    {
        return true;
    }
    else if(__rhs.length < length)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::FragmentInfo::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(offset);
    __os->write(length);
}

void
LDTSSInterface::FragmentInfo::__read(::IceInternal::BasicStream* __is)
{
    __is->read(offset);
    __is->read(length);
}

void
LDTSSInterface::__writeFragmentInfoSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::FragmentInfo* begin, const ::LDTSSInterface::FragmentInfo* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readFragmentInfoSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::FragmentInfoSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->checkFixedSeq(sz, 8);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
    }
}

bool
LDTSSInterface::SurveyDeviceFault::operator==(const SurveyDeviceFault& __rhs) const
{
    if(this == &__rhs)
    {
        return true;
    }
    if(deviceNo != __rhs.deviceNo)
    {
        return false;
    }
    if(faultTime != __rhs.faultTime)
    {
        return false;
    }
    if(faultCode != __rhs.faultCode)
    {
        return false;
    }
    if(faultDetail != __rhs.faultDetail)
    {
        return false;
    }
    return true;
}

bool
LDTSSInterface::SurveyDeviceFault::operator<(const SurveyDeviceFault& __rhs) const
{
    if(this == &__rhs)
    {
        return false;
    }
    if(deviceNo < __rhs.deviceNo)
    {
        return true;
    }
    else if(__rhs.deviceNo < deviceNo)
    {
        return false;
    }
    if(faultTime < __rhs.faultTime)
    {
        return true;
    }
    else if(__rhs.faultTime < faultTime)
    {
        return false;
    }
    if(faultCode < __rhs.faultCode)
    {
        return true;
    }
    else if(__rhs.faultCode < faultCode)
    {
        return false;
    }
    if(faultDetail < __rhs.faultDetail)
    {
        return true;
    }
    else if(__rhs.faultDetail < faultDetail)
    {
        return false;
    }
    return false;
}

void
LDTSSInterface::SurveyDeviceFault::__write(::IceInternal::BasicStream* __os) const
{
    __os->write(deviceNo);
    __os->write(faultTime);
    __os->write(faultCode);
    __os->write(faultDetail);
}

void
LDTSSInterface::SurveyDeviceFault::__read(::IceInternal::BasicStream* __is)
{
    __is->read(deviceNo);
    __is->read(faultTime);
    __is->read(faultCode);
    __is->read(faultDetail);
}

void
LDTSSInterface::__writeSurveyDeviceFaultSeq(::IceInternal::BasicStream* __os, const ::LDTSSInterface::SurveyDeviceFault* begin, const ::LDTSSInterface::SurveyDeviceFault* end)
{
    ::Ice::Int size = static_cast< ::Ice::Int>(end - begin);
    __os->writeSize(size);
    for(int i = 0; i < size; ++i)
    {
        begin[i].__write(__os);
    }
}

void
LDTSSInterface::__readSurveyDeviceFaultSeq(::IceInternal::BasicStream* __is, ::LDTSSInterface::SurveyDeviceFaultSeq& v)
{
    ::Ice::Int sz;
    __is->readSize(sz);
    __is->startSeq(sz, 7);
    v.resize(sz);
    for(int i = 0; i < sz; ++i)
    {
        v[i].__read(__is);
        __is->checkSeq();
        __is->endElement();
    }
    __is->endSeq(sz);
}
