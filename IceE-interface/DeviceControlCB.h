// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `DeviceControlCB.ice'

#ifndef __test_DeviceControlCB_h__
#define __test_DeviceControlCB_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#ifdef ICEE_HAS_AMI
#   include <IceE/OutgoingAsync.h>
#endif
#include <IceE/UserExceptionFactory.h>
#include <IceE/FactoryTable.h>
#include <Base.h>
#include <VehicleInspection.h>
#include <DeviceControl.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace LDTSSInterface
{

class DeviceControlCB;

}

}

namespace LDTSSInterface
{

class DeviceControlCB;
bool operator==(const DeviceControlCB&, const DeviceControlCB&);
bool operator<(const DeviceControlCB&, const DeviceControlCB&);

}

namespace IceInternal
{

::Ice::Object* upCast(::LDTSSInterface::DeviceControlCB*);
::IceProxy::Ice::Object* upCast(::IceProxy::LDTSSInterface::DeviceControlCB*);

}

namespace LDTSSInterface
{

typedef ::IceInternal::Handle< ::LDTSSInterface::DeviceControlCB> DeviceControlCBPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::LDTSSInterface::DeviceControlCB> DeviceControlCBPrx;

void __read(::IceInternal::BasicStream*, DeviceControlCBPrx&);
void __patch__DeviceControlCBPtr(void*, ::Ice::ObjectPtr&);

}

namespace LDTSSInterface
{

}

namespace LDTSSInterface
{

class DeviceControlCB : virtual public ::LDTSSInterface::VehicleInspection,
                        virtual public ::LDTSSInterface::DeviceControl
{
public:

    typedef DeviceControlCBPrx ProxyType;
    typedef DeviceControlCBPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace IceProxy
{

namespace LDTSSInterface
{

class DeviceControlCB : virtual public ::IceProxy::LDTSSInterface::VehicleInspection,
                        virtual public ::IceProxy::LDTSSInterface::DeviceControl
{
public:
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_secure(bool __secure) const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_twoway() const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_oneway() const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_batchOneway() const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_datagram() const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_batchDatagram() const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<DeviceControlCB> ice_timeout(int __timeout) const
    {
        return dynamic_cast<DeviceControlCB*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
