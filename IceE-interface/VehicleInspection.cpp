// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `VehicleInspection.ice'

#include <VehicleInspection.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/LocalException.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __LDTSSInterface__VehicleInspection__InspectVehicle_name = "InspectVehicle";

static const ::std::string __LDTSSInterface__VehicleInspection__CancelVehicle_name = "CancelVehicle";

static const ::std::string __LDTSSInterface__VehicleInspection__QueryVehicle_name = "QueryVehicle";

static const ::std::string __LDTSSInterface__VehicleInspection__BatchInspectedVehicle_name = "BatchInspectedVehicle";

static const ::std::string __LDTSSInterface__VehicleInspection__ListVehicle_name = "ListVehicle";

static const ::std::string __LDTSSInterface__VehicleInspection__CountVehicle_name = "CountVehicle";

static const ::std::string __LDTSSInterface__VehicleInspection__AddVagueData_name = "AddVagueData";

static const ::std::string __LDTSSInterface__VehicleInspection__QueryVagueData_name = "QueryVagueData";

::Ice::Object* IceInternal::upCast(::LDTSSInterface::VehicleInspection* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::LDTSSInterface::VehicleInspection* p) { return p; }

void
LDTSSInterface::__read(::IceInternal::BasicStream* __is, ::LDTSSInterface::VehicleInspectionPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::LDTSSInterface::VehicleInspection;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __LDTSSInterface__VehicleInspection_ids[2] =
{
    "::Ice::Object",
    "::LDTSSInterface::VehicleInspection"
};

bool
LDTSSInterface::VehicleInspection::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__LDTSSInterface__VehicleInspection_ids, __LDTSSInterface__VehicleInspection_ids + 2, _s);
}

::std::vector< ::std::string>
LDTSSInterface::VehicleInspection::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__LDTSSInterface__VehicleInspection_ids[0], &__LDTSSInterface__VehicleInspection_ids[2]);
}

const ::std::string&
LDTSSInterface::VehicleInspection::ice_id(const ::Ice::Current&) const
{
    return __LDTSSInterface__VehicleInspection_ids[1];
}

const ::std::string&
LDTSSInterface::VehicleInspection::ice_staticId()
{
    return __LDTSSInterface__VehicleInspection_ids[1];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::___InspectVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::InspectedVehicle vehicle;
    bool override;
    vehicle.__read(__is);
    __is->read(override);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = InspectVehicle(vehicle, override, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::___CancelVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string plateNbr;
    ::std::string plateColor;
    ::std::string inspectActione;
    ::std::string inspectDepartment;
    __is->read(plateNbr);
    __is->read(plateColor);
    __is->read(inspectActione);
    __is->read(inspectDepartment);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = CancelVehicle(plateNbr, plateColor, inspectActione, inspectDepartment, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::___QueryVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string plateNbr;
    ::std::string plateColor;
    __is->read(plateNbr);
    __is->read(plateColor);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::InspectedVehicleSeq __ret = QueryVehicle(plateNbr, plateColor, __current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeInspectedVehicleSeq(__os, &__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::___BatchInspectedVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::LDTSSInterface::InspectedVehicleSeq vehicleList;
    ::LDTSSInterface::__readInspectedVehicleSeq(__is, vehicleList);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = BatchInspectedVehicle(vehicleList, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::___ListVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string inspectDepartment;
    ::Ice::Int pageSize;
    ::Ice::Int pageIndex;
    __is->read(inspectDepartment);
    __is->read(pageSize);
    __is->read(pageIndex);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::LDTSSInterface::InspectedVehicleSeq __ret = ListVehicle(inspectDepartment, pageSize, pageIndex, __current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        ::LDTSSInterface::__writeInspectedVehicleSeq(__os, &__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::___CountVehicle(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string inspectDepartment;
    __is->read(inspectDepartment);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::Int __ret = CountVehicle(inspectDepartment, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::___AddVagueData(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::Ice::StringSeq dimData;
    __is->read(dimData);
    ::IceInternal::BasicStream* __os = __inS.os();
    bool __ret = AddVagueData(dimData, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::___QueryVagueData(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::StringSeq __ret = QueryVagueData(__current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __LDTSSInterface__VehicleInspection_all[] =
{
    "AddVagueData",
    "BatchInspectedVehicle",
    "CancelVehicle",
    "CountVehicle",
    "InspectVehicle",
    "ListVehicle",
    "QueryVagueData",
    "QueryVehicle",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

::Ice::DispatchStatus
LDTSSInterface::VehicleInspection::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__LDTSSInterface__VehicleInspection_all, __LDTSSInterface__VehicleInspection_all + 12, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __LDTSSInterface__VehicleInspection_all)
    {
        case 0:
        {
            return ___AddVagueData(in, current);
        }
        case 1:
        {
            return ___BatchInspectedVehicle(in, current);
        }
        case 2:
        {
            return ___CancelVehicle(in, current);
        }
        case 3:
        {
            return ___CountVehicle(in, current);
        }
        case 4:
        {
            return ___InspectVehicle(in, current);
        }
        case 5:
        {
            return ___ListVehicle(in, current);
        }
        case 6:
        {
            return ___QueryVagueData(in, current);
        }
        case 7:
        {
            return ___QueryVehicle(in, current);
        }
        case 8:
        {
            return ___ice_id(in, current);
        }
        case 9:
        {
            return ___ice_ids(in, current);
        }
        case 10:
        {
            return ___ice_isA(in, current);
        }
        case 11:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
LDTSSInterface::VehicleInspection::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
LDTSSInterface::VehicleInspection::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
LDTSSInterface::operator==(const ::LDTSSInterface::VehicleInspection& l, const ::LDTSSInterface::VehicleInspection& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
LDTSSInterface::operator<(const ::LDTSSInterface::VehicleInspection& l, const ::LDTSSInterface::VehicleInspection& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
LDTSSInterface::__patch__VehicleInspectionPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::LDTSSInterface::VehicleInspectionPtr* p = static_cast< ::LDTSSInterface::VehicleInspectionPtr*>(__addr);
    assert(p);
    *p = ::LDTSSInterface::VehicleInspectionPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::LDTSSInterface::VehicleInspection::ice_staticId(), v->ice_id());
    }
}
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_VehicleInspection_InspectVehicle::__invoke(const ::LDTSSInterface::VehicleInspectionPrx& __prx, const ::LDTSSInterface::InspectedVehicle& vehicle, bool override, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__VehicleInspection__InspectVehicle_name);
        __prepare(__prx, __LDTSSInterface__VehicleInspection__InspectVehicle_name, ::Ice::Normal, __ctx);
        vehicle.__write(__os);
        __os->write(override);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_VehicleInspection_InspectVehicle::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_VehicleInspection_CancelVehicle::__invoke(const ::LDTSSInterface::VehicleInspectionPrx& __prx, const ::std::string& plateNbr, const ::std::string& plateColor, const ::std::string& inspectActione, const ::std::string& inspectDepartment, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__VehicleInspection__CancelVehicle_name);
        __prepare(__prx, __LDTSSInterface__VehicleInspection__CancelVehicle_name, ::Ice::Idempotent, __ctx);
        __os->write(plateNbr);
        __os->write(plateColor);
        __os->write(inspectActione);
        __os->write(inspectDepartment);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_VehicleInspection_CancelVehicle::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_VehicleInspection_QueryVehicle::__invoke(const ::LDTSSInterface::VehicleInspectionPrx& __prx, const ::std::string& plateNbr, const ::std::string& plateColor, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__VehicleInspection__QueryVehicle_name);
        __prepare(__prx, __LDTSSInterface__VehicleInspection__QueryVehicle_name, ::Ice::Idempotent, __ctx);
        __os->write(plateNbr);
        __os->write(plateColor);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_VehicleInspection_QueryVehicle::__response(bool __ok)
{
    ::LDTSSInterface::InspectedVehicleSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::LDTSSInterface::__readInspectedVehicleSeq(__is, __ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_VehicleInspection_BatchInspectedVehicle::__invoke(const ::LDTSSInterface::VehicleInspectionPrx& __prx, const ::LDTSSInterface::InspectedVehicleSeq& vehicleList, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__VehicleInspection__BatchInspectedVehicle_name);
        __prepare(__prx, __LDTSSInterface__VehicleInspection__BatchInspectedVehicle_name, ::Ice::Idempotent, __ctx);
        if(vehicleList.size() == 0)
        {
            __os->writeSize(0);
        }
        else
        {
            ::LDTSSInterface::__writeInspectedVehicleSeq(__os, &vehicleList[0], &vehicleList[0] + vehicleList.size());
        }
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_VehicleInspection_BatchInspectedVehicle::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_VehicleInspection_ListVehicle::__invoke(const ::LDTSSInterface::VehicleInspectionPrx& __prx, const ::std::string& inspectDepartment, ::Ice::Int pageSize, ::Ice::Int pageIndex, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__VehicleInspection__ListVehicle_name);
        __prepare(__prx, __LDTSSInterface__VehicleInspection__ListVehicle_name, ::Ice::Idempotent, __ctx);
        __os->write(inspectDepartment);
        __os->write(pageSize);
        __os->write(pageIndex);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_VehicleInspection_ListVehicle::__response(bool __ok)
{
    ::LDTSSInterface::InspectedVehicleSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        ::LDTSSInterface::__readInspectedVehicleSeq(__is, __ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_VehicleInspection_CountVehicle::__invoke(const ::LDTSSInterface::VehicleInspectionPrx& __prx, const ::std::string& inspectDepartment, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__VehicleInspection__CountVehicle_name);
        __prepare(__prx, __LDTSSInterface__VehicleInspection__CountVehicle_name, ::Ice::Idempotent, __ctx);
        __os->write(inspectDepartment);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_VehicleInspection_CountVehicle::__response(bool __ok)
{
    ::Ice::Int __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_VehicleInspection_AddVagueData::__invoke(const ::LDTSSInterface::VehicleInspectionPrx& __prx, const ::Ice::StringSeq& dimData, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__VehicleInspection__AddVagueData_name);
        __prepare(__prx, __LDTSSInterface__VehicleInspection__AddVagueData_name, ::Ice::Idempotent, __ctx);
        if(dimData.size() == 0)
        {
            __os->writeSize(0);
        }
        else
        {
            __os->write(&dimData[0], &dimData[0] + dimData.size());
        }
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_VehicleInspection_AddVagueData::__response(bool __ok)
{
    bool __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif
#ifdef ICEE_HAS_AMI

bool
LDTSSInterface::AMI_VehicleInspection_QueryVagueData::__invoke(const ::LDTSSInterface::VehicleInspectionPrx& __prx, const ::Ice::Context* __ctx)
{
    __acquireCallback(__prx);
    try
    {
        __prx->__checkTwowayOnly(__LDTSSInterface__VehicleInspection__QueryVagueData_name);
        __prepare(__prx, __LDTSSInterface__VehicleInspection__QueryVagueData_name, ::Ice::Idempotent, __ctx);
        __os->endWriteEncaps();
        return __send();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __releaseCallback(__ex);
        return false;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
}

void
LDTSSInterface::AMI_VehicleInspection_QueryVagueData::__response(bool __ok)
{
    ::Ice::StringSeq __ret;
    try
    {
        if(!__ok)
        {
            __is->throwUnknownUserException();
        }
        __is->read(__ret);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __finished(__ex);
        return;
    }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
    catch(...)
    {
        throw;
    }
#endif
    ice_response(__ret);
    __releaseCallback();
}
#endif

bool
IceProxy::LDTSSInterface::VehicleInspection::InspectVehicle(const ::LDTSSInterface::InspectedVehicle& vehicle, bool override, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__VehicleInspection__InspectVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__VehicleInspection__InspectVehicle_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                vehicle.__write(__os);
                __os->write(override);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::VehicleInspection::InspectVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_InspectVehiclePtr& __cb, const ::LDTSSInterface::InspectedVehicle& vehicle, bool override)
{
    return __cb->__invoke(this, vehicle, override, 0);
}

bool
IceProxy::LDTSSInterface::VehicleInspection::InspectVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_InspectVehiclePtr& __cb, const ::LDTSSInterface::InspectedVehicle& vehicle, bool override, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, vehicle, override, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::VehicleInspection::CancelVehicle(const ::std::string& plateNbr, const ::std::string& plateColor, const ::std::string& inspectActione, const ::std::string& inspectDepartment, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__VehicleInspection__CancelVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__VehicleInspection__CancelVehicle_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(plateNbr);
                __os->write(plateColor);
                __os->write(inspectActione);
                __os->write(inspectDepartment);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::VehicleInspection::CancelVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_CancelVehiclePtr& __cb, const ::std::string& plateNbr, const ::std::string& plateColor, const ::std::string& inspectActione, const ::std::string& inspectDepartment)
{
    return __cb->__invoke(this, plateNbr, plateColor, inspectActione, inspectDepartment, 0);
}

bool
IceProxy::LDTSSInterface::VehicleInspection::CancelVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_CancelVehiclePtr& __cb, const ::std::string& plateNbr, const ::std::string& plateColor, const ::std::string& inspectActione, const ::std::string& inspectDepartment, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, plateNbr, plateColor, inspectActione, inspectDepartment, &__ctx);
}
#endif

::LDTSSInterface::InspectedVehicleSeq
IceProxy::LDTSSInterface::VehicleInspection::QueryVehicle(const ::std::string& plateNbr, const ::std::string& plateColor, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__VehicleInspection__QueryVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__VehicleInspection__QueryVehicle_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(plateNbr);
                __os->write(plateColor);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::InspectedVehicleSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::LDTSSInterface::__readInspectedVehicleSeq(__is, __ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::VehicleInspection::QueryVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_QueryVehiclePtr& __cb, const ::std::string& plateNbr, const ::std::string& plateColor)
{
    return __cb->__invoke(this, plateNbr, plateColor, 0);
}

bool
IceProxy::LDTSSInterface::VehicleInspection::QueryVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_QueryVehiclePtr& __cb, const ::std::string& plateNbr, const ::std::string& plateColor, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, plateNbr, plateColor, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::VehicleInspection::BatchInspectedVehicle(const ::LDTSSInterface::InspectedVehicleSeq& vehicleList, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__VehicleInspection__BatchInspectedVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__VehicleInspection__BatchInspectedVehicle_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                if(vehicleList.size() == 0)
                {
                    __os->writeSize(0);
                }
                else
                {
                    ::LDTSSInterface::__writeInspectedVehicleSeq(__os, &vehicleList[0], &vehicleList[0] + vehicleList.size());
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::VehicleInspection::BatchInspectedVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_BatchInspectedVehiclePtr& __cb, const ::LDTSSInterface::InspectedVehicleSeq& vehicleList)
{
    return __cb->__invoke(this, vehicleList, 0);
}

bool
IceProxy::LDTSSInterface::VehicleInspection::BatchInspectedVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_BatchInspectedVehiclePtr& __cb, const ::LDTSSInterface::InspectedVehicleSeq& vehicleList, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, vehicleList, &__ctx);
}
#endif

::LDTSSInterface::InspectedVehicleSeq
IceProxy::LDTSSInterface::VehicleInspection::ListVehicle(const ::std::string& inspectDepartment, ::Ice::Int pageSize, ::Ice::Int pageIndex, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__VehicleInspection__ListVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__VehicleInspection__ListVehicle_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(inspectDepartment);
                __os->write(pageSize);
                __os->write(pageIndex);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::LDTSSInterface::InspectedVehicleSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                ::LDTSSInterface::__readInspectedVehicleSeq(__is, __ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::VehicleInspection::ListVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_ListVehiclePtr& __cb, const ::std::string& inspectDepartment, ::Ice::Int pageSize, ::Ice::Int pageIndex)
{
    return __cb->__invoke(this, inspectDepartment, pageSize, pageIndex, 0);
}

bool
IceProxy::LDTSSInterface::VehicleInspection::ListVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_ListVehiclePtr& __cb, const ::std::string& inspectDepartment, ::Ice::Int pageSize, ::Ice::Int pageIndex, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, inspectDepartment, pageSize, pageIndex, &__ctx);
}
#endif

::Ice::Int
IceProxy::LDTSSInterface::VehicleInspection::CountVehicle(const ::std::string& inspectDepartment, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__VehicleInspection__CountVehicle_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__VehicleInspection__CountVehicle_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(inspectDepartment);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::Ice::Int __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::VehicleInspection::CountVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_CountVehiclePtr& __cb, const ::std::string& inspectDepartment)
{
    return __cb->__invoke(this, inspectDepartment, 0);
}

bool
IceProxy::LDTSSInterface::VehicleInspection::CountVehicle_async(const ::LDTSSInterface::AMI_VehicleInspection_CountVehiclePtr& __cb, const ::std::string& inspectDepartment, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, inspectDepartment, &__ctx);
}
#endif

bool
IceProxy::LDTSSInterface::VehicleInspection::AddVagueData(const ::Ice::StringSeq& dimData, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__VehicleInspection__AddVagueData_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__VehicleInspection__AddVagueData_name, ::Ice::Idempotent, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                if(dimData.size() == 0)
                {
                    __os->writeSize(0);
                }
                else
                {
                    __os->write(&dimData[0], &dimData[0] + dimData.size());
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                bool __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::VehicleInspection::AddVagueData_async(const ::LDTSSInterface::AMI_VehicleInspection_AddVagueDataPtr& __cb, const ::Ice::StringSeq& dimData)
{
    return __cb->__invoke(this, dimData, 0);
}

bool
IceProxy::LDTSSInterface::VehicleInspection::AddVagueData_async(const ::LDTSSInterface::AMI_VehicleInspection_AddVagueDataPtr& __cb, const ::Ice::StringSeq& dimData, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, dimData, &__ctx);
}
#endif

::Ice::StringSeq
IceProxy::LDTSSInterface::VehicleInspection::QueryVagueData(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__LDTSSInterface__VehicleInspection__QueryVagueData_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __LDTSSInterface__VehicleInspection__QueryVagueData_name, ::Ice::Idempotent, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::Ice::StringSeq __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__handler, __ex, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}
#ifdef ICEE_HAS_AMI

bool
IceProxy::LDTSSInterface::VehicleInspection::QueryVagueData_async(const ::LDTSSInterface::AMI_VehicleInspection_QueryVagueDataPtr& __cb)
{
    return __cb->__invoke(this, 0);
}

bool
IceProxy::LDTSSInterface::VehicleInspection::QueryVagueData_async(const ::LDTSSInterface::AMI_VehicleInspection_QueryVagueDataPtr& __cb, const ::Ice::Context& __ctx)
{
    return __cb->__invoke(this, &__ctx);
}
#endif

const ::std::string&
IceProxy::LDTSSInterface::VehicleInspection::ice_staticId()
{
    return __LDTSSInterface__VehicleInspection_ids[1];
}

::IceProxy::Ice::Object*
IceProxy::LDTSSInterface::VehicleInspection::__newInstance() const
{
    return new VehicleInspection;
}
