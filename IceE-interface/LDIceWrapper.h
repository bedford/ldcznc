//*********************************************************************************
//文件作者:zhouw
//邮箱地址: zw@landuntec.com
//添加时间: 2011-07-26
//功能描述: 与监控服务器通信类，主要封装了上传各种数据接口，并启动设备控制服务，回
//调类需使用者从LDTSSInterface::DeviceControl继承，实现相关接口后，传入。该类以单体
//模式实现
//*********************************************************************************

#ifndef LD_ICE_WRAPPER_H
#define LD_ICE_WRAPPER_H

#include <IceE/Communicator.h>
#include <Router.h>
#include <DeviceControlCB.h>
#include <SurveyDataUploader.h>
#include <ViolationImageUploader.h>
#include <PassingImageUploader.h>
#include <DeviceMonitor.h>
#include <DevicePrxReport.h>
#include "thread.h"

class LDIceWrapper : public Thread
{
public:
        //初始化，调用其他接口前，必须先调用该函数
        bool Init(std::string deviceNo);

        //释放资源，程序退出前必须调用该函数
        bool Release();

        //上传过车信息及图像，如查images.size为零，则只上传过车信息（用于区间测速）
        bool UploadPassingVehicle(const LDTSSInterface::PassingVehicle &vehicle,
                        const LDTSSInterface::VehicleImageSeq& images,bool history);

        //上传违法车辆信息及图像，如查images.size为零，则只上传过车信息（用于区间测速）
        bool UploadViolationVehicle(const LDTSSInterface::ViolationVehicle &vehicle,
                        const LDTSSInterface::VehicleImageSeq& images,bool history);

        //上传流量统记信息，
        bool UploadTrafficStats(const LDTSSInterface::TrafficStats &stats, bool history);

        //上传设备态信息，
        bool UploadDeviceStatus(const LDTSSInterface::SurveyDeviceStatus& status);

        bool RegisterDevice(const ::std::string& deviceNo,
                        const ::LDTSSInterface::SurveyDeviceAttribute& deviceAttribute,
                        const ::LDTSSInterface::DeviceRuntimeParams& runtimeParams);

        static LDIceWrapper* GetInstance();

private:
        LDIceWrapper();
        ~LDIceWrapper();
        bool _bInit;
        std::string _device_number;

        bool GetUploadProxy();
        bool StartServant(LDTSSInterface::DeviceControlCBPrx &callback);
        bool RegisterCallback(std::string deviceNo);
        bool CreateSession();

        virtual void Run();
        bool ReleaseProxy();
        void ReleaseServant();
        bool InitCommunicator();
        bool ReleaseCommunicator();

        //过车图像上传代理
        LDTSSInterface::PassingImageUploaderPrx _passingImageUploader;
        //违法图像上传代理
        LDTSSInterface::ViolationImageUploaderPrx _violationImageUploader;
        //设备状态上传代理
        LDTSSInterface::DeviceMonitorPrx  _deviceMonitor;
        //信息上传代理,可上传流量统计信息,过车信息,违法车辆信息(不含图片)
        LDTSSInterface::SurveyDataUploaderPrx _infoUploader;
        //设备反控回调上报代理
        LDTSSInterface::DeviceCtrlPrxReportPrx _deviceCtrlPrxReport;

        //设备控制对象,当服务器对设备控制或者参数设置时,将回调相关接口
        LDTSSInterface::DeviceControlCBPrx _deviceControl;

        Ice::CommunicatorPtr _communicator;
        Ice::ObjectAdapterPtr _adapter;

        Glacier2::SessionPrx _session;
        Ice::RouterPrx _router;

        static LDIceWrapper* _sLDIceWrapper;
};

#endif
