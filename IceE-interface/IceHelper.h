//*********************************************************************************
//文件作者:zhouw
//邮箱地址: zw@landuntec.com
//添加时间: 2011-07-26
//功能描述: 对Ice的一些通用操作进行封装，如初始化ICE，获取代理等,所有方法为静态方法， 类不可实例化
//*********************************************************************************

#ifndef ICE_HELPER_H
#define ICE_HELPER_H

#include <IceE/IceE.h>
#include <string>
using namespace std;

class IceHelper
{
public:
        //获取远程对象代理
        template<typename T>
        static bool GetProxy(const Ice::CommunicatorPtr& communicator,
                        string propertyString, T& proxy)
        {
                try {
                        Ice::ObjectPrx obj = communicator->propertyToProxy(propertyString);
                        //proxy = T::checkedCast(obj->ice_twoway()->ice_timeout(-1));
                        proxy = T::uncheckedCast(obj);
                        if (0 == proxy) {
                                Ice::Error err(communicator->getLogger());
                                err << propertyString << " invalid";
                                return false;
                        }
                        return true;
                } catch (Ice::Exception ex) {
                        Ice::Error err(communicator->getLogger());
                        err << ex.what();
                        return false;
                }
        }

        static bool StartServantAadapter(Ice::ObjectPtr servant, std::string propertyName,
                        Ice::CommunicatorPtr communicator, Ice::ObjectAdapterPtr &adapter);

        static bool InitIce(string configFile,Ice::CommunicatorPtr &communicator);

private:
        IceHelper(void);
        ~IceHelper(void);
};

#endif
