/**
 * @file	TcpClient.cpp
 * @brief	TCP客户端
 * @author 	hrh <huangrh@landuntec.com>
 * @version 	1.0.0
 * @date 	2011-12-07
 *
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <unistd.h>
#include <string.h>
#include <signal.h>
#include "socket.h"
#include "tcp_client.h"
#include "image_buffer.h"
#include "debug.h"

static inline void fill_std_header(struct header_std *head,
                                   char encoding,
                                   char *auth_code, char msg_type, unsigned int msg_size)
{
        const char magic[] = PROTOCOL_MAGIC;
        memcpy(head->magic, magic, sizeof(magic));

        head->protocol_major    = PROTOCOL_MAJOR;
        head->protocol_minor    = PROTOCOL_MINOR;
        head->encoding          = encoding;
        memcpy(head->auth_code, auth_code, sizeof(head->auth_code));

        head->msg_type = msg_type;
        head->msg_size = msg_size;
}

static void processSignal(int signo)
{
        Debug(DEBUG, "signal is %d", signo);
        signal(signo, processSignal);
}

int TcpClient::SendHeartBeat()
{
        if (local_sock == -1) {
                return -1;
        }
        int bytes = -1;

        PacketRequest packet;
        char auth_code[sizeof(packet.head.auth_code)];
        fill_std_header(&packet.head,
                        ENCODING_TYPE_RAW, auth_code, MESSAGE_TYPE_REQ, sizeof(packet));

        packet.req.id   = 0;
        packet.req.type = REQ_TYPE_HEARTBEAT;

        bytes = Socket::Writen(local_sock, &packet, sizeof(packet), 500);
        if (bytes < 0) {
                return -1;
        } else if (bytes == 0) {
                Socket::Close(local_sock);
                local_sock              = -1;
                _connected_status       = false;
                Debug(INFO, "peer closed");
                return -1;
        } else if ((unsigned int)bytes < sizeof(packet)) {
                return -1;
        }

        return bytes;
}

int TcpClient::SendData(struct cmemBuffer *image, int type, SensorCurrentParams * sensor_param)
{
        if (local_sock == -1) {
                Debug(INFO, "no local sock");
                return -1;
        }
        int bytes = -1;

        struct packet_img_trans_req packet;
        char auth_code[sizeof(packet.request.head.auth_code)];
        fill_std_header(&packet.request.head,
                        ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_REQ, sizeof(packet) + image->size);

        packet.request.req.id = 0;
        if (type == CAPTURE_FRAME) {
                packet.request.req.type = REQ_TYPE_IMAGE_UPLOAD;
        } else {
                packet.request.req.type = REQ_TYPE_VIDEO_UPLOAD;
        }

        memcpy(&packet.params, sensor_param, sizeof(SensorCurrentParams));

        packet.trans.info_len = 0;
        packet.trans.data_len = image->size;

        bytes = Socket::Writen(local_sock, &packet, sizeof(packet), 500);
        Debug(DEBUG, "send image : header %d bytes, jpeg %d bytes", sizeof(packet), image->size);

        if (bytes < 0) {
                return -1;
        } else if (bytes == 0) {
                Debug(INFO, "peer closed with header");
                return -1;
        } else if ((unsigned int)bytes < sizeof(packet)) {
                Debug(INFO, "send less");
                return -1;
        }

        bytes = Socket::Writen(local_sock, (void *)image->vir_addr, image->size, 2000);
        if (bytes < 0) {
                return -1;
        } else if (bytes == 0) {
                Debug(INFO, "peer closed with image data");
                return -1;
        }

        return bytes;
}

TcpClient::TcpClient()
{
        _connect_flag           = false;
        _connected_status       = false;
        local_sock              = -1;
        memset(client_addr, 0, sizeof(client_addr));
        signal(SIGPIPE, processSignal);
}

TcpClient::~TcpClient()
{
        Terminate();
        if (local_sock >= 0) {
                Socket::Close(local_sock);
                Debug(DEBUG, "Close socket");
        }

        local_sock              = -1;
        _connected_status       = false;
        _connect_flag           = false;
        Debug(DEBUG, "deconstrution tcp_client");
}

bool TcpClient::ConnectClient()
{
        static int times        = 0;
        int client_port         = 39003;

        if (local_sock == -1) {
                if ((local_sock = Socket::CreateTcp()) < 0) {
                        Debug(INFO, "create Tcp socket failed");
                        return false;
                }
        }

        Socket::SetTcpNonblock(local_sock);
        if (Socket::Connect(local_sock, client_addr, client_port, 500)) {
                times++;
                if (times == 500) {
                        Debug(INFO, "connect failed, addr : %s, port: %d", client_addr, client_port);
                        times = 0;
                }
                return false;
        }

        Debug(DEBUG, "connect PC demo server OK");
        _connected_status = true;
        return true;
}

void TcpClient::UploadClient()
{
        struct image_buffer_description *image = NULL;
        if (transport_buffer_get(&image) == 0) {

                if (!_connected_status) {
                        transport_buffer_free(image);
                        image = NULL;
                        return;
                }

                if (SendData(&image->info, image->type, &image->param) <= 0) {
                        if (local_sock >= 0) {
                                Socket::Close(local_sock);
                                local_sock = -1;
                                _connected_status = false;
                        }
                }
                transport_buffer_free(image);
                image = NULL;
        }
}

void TcpClient::ClearBuffer()
{
        struct image_buffer_description *image = NULL;
        while (transport_buffer_get(&image) == 0) {

                transport_buffer_free(image);
                image = NULL;
                usleep(5000);
        }
        Debug(INFO, "after free buffer");
}

void TcpClient::Run()
{
        int cnt = 0;
        while (!IsTerminated()) {

                if (!_connected_status) {
                        ConnectClient();
                }

                UploadClient();
                usleep(20000);

                if (_connected_status) {
                        cnt++;
                        if (cnt > 200) {
                                cnt = 0;
                                SendHeartBeat();
                        }
                }
        }

        Debug(DEBUG, "exit tcpclient");
        ClearBuffer();

        local_sock              = -1;
        _connected_status       = false;
        _connect_flag           = false;
}

void TcpClient::SetClient(struct _ClientInfo *info)
{
        memcpy(client_addr, info->addr, sizeof(info->addr));

        if (local_sock >= 0) {
                Socket::Close(local_sock);
                local_sock = -1;
                _connected_status = false;
        }
}

void TcpClient::SetConnectFlag(bool flag)
{
        _connect_flag = flag;
}
