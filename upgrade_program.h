#ifndef _UPGRADE_PROGRAM_
#define _UPGRADE_PROGRAM_

class UpgradeProgram {
public:
        static int UpdateProgram(unsigned int type, char* buf); 

private:
        static int ReceiveFile(char *file_name, int total_length);
        static int ExcuteUpdate(char *file_name, unsigned int type);
};

#endif
