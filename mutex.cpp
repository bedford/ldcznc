/**
 * @file        mutex.cpp
 * @brief       互斥锁公共基础类
 * @author      hrh <huangrh@landuntec.com>
 * @version     1.0.0
 * @date        2012-05-16
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include "mutex.h"

Mutex::Mutex()
{
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE_NP);
        pthread_mutex_init(&_mutex, &attr);
        pthread_mutexattr_destroy(&attr);
}

Mutex::~Mutex()
{
        pthread_mutex_destroy(&_mutex);
}

bool Mutex::Lock()
{
        if (pthread_mutex_lock(&_mutex)) {
                return false;
        }
        return true;
}

bool Mutex::tryLock()
{
        int rc = pthread_mutex_trylock(&_mutex);
        return (rc == 0);
}

bool Mutex::Unlock()
{
        int rc = pthread_mutex_unlock(&_mutex);
        return (rc == 0);
}
