#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include <signal.h>
#include "image_buffer.h"

#include "mutex.h"
#include "tcp_server.h"
#include "tcp_client.h"
#include "udp_server.h"
#include "arbiter.h"
#include "fix_capture.h"
#include "debug.h"
#include "survey.h"
#include "uart.h"
#include "gpio.h"
#include "util.h"
#include "watchdog.h"

#include "parameters.h"

static bool bRun = true;

static void signal_handler(int signum)
{
        Debug(INFO, "signum is %d", signum);
        switch (signum) {
        case SIGCHLD:
        case SIGABRT:
        case SIGKILL:
                break;
        case SIGINT:
        case SIGTERM:
                bRun = false;
                break;
        default:
                break;
        }
}

/**
 * @fn		static void set_signal_handler(int signum, void func(int))
 *
 * @brief	对特定的信号设定处理函数
 *
 * @param	[in] signum		信号号
 * @param	[in] void func(int)	信号处理函数
 *
 */
static void SetSignalHandler(void)
{
        struct sigaction sigAction;
        sigAction.sa_flags = 0;
        sigemptyset(&sigAction.sa_mask);
        sigaddset(&sigAction.sa_mask, SIGINT);
        sigAction.sa_handler = signal_handler;

        sigaction(SIGCHLD, &sigAction, NULL);
        sigaction(SIGABRT, &sigAction, NULL);
        sigaction(SIGTERM, &sigAction, NULL);
        sigaction(SIGKILL, &sigAction, NULL);
        sigaction(SIGINT, &sigAction, NULL);
}

static int get_ccd_type(char *version)
{
        char *dest = NULL;
        char *next = NULL;
        dest = strtok_r(version, "-", &next);
        int type = atoi(dest);

        return type;
}

static int CreateImageBufferList(void)
{
        struct env_image_buffer env_image_buffer;
        env_image_buffer.buf_len = 1024 * 1024;
        env_image_buffer.buf_num = 8;
        env_image_buffer.resd_buf_num_max = 1;
        env_image_buffer.capt_buf_num_max = 4;
        env_image_buffer.tran_buf_num_min = 0;
        env_image_buffer.tran_buf_num_max = 3;
        image_buffer_init(&env_image_buffer);

        return 0;
}

Uart *signal_module = NULL;
TcpClient *tcp_client = NULL;
TcpServer *tcp_server = NULL;
UdpServer *udp_server = NULL;
FixCapture *fix_capture = NULL;
Arbiter *arbiter = NULL;

static int CreateThreadObjects()
{
        ProgramVersion version = Parameters::GetInstance()->GetProgramVersion();
        int ccd_type = get_ccd_type(version.fpga_version);

        DeviceInfo info = Parameters::GetInstance()->GetDeviceInfo();
        Debug(INFO, "device_mode is %d", info.device_mode);
        if (info.device_mode == RedLight
                || info.device_mode == RedLight_Reverse
                || info.device_mode == Sitecap
                || info.device_mode == LimitedPassViolation) {
                Survey::Init();
                signal_module = new Uart();
        }

        tcp_client      = new TcpClient();
        tcp_server      = new TcpServer(tcp_client);
        udp_server      = new UdpServer();
        fix_capture     = new FixCapture();
        arbiter         = new Arbiter();
        arbiter->Init(fix_capture, ccd_type);

        return 0;
}

static int StartThreads()
{
        tcp_client->Start();
        tcp_server->Start();
        udp_server->Start();
        fix_capture->Start();
        arbiter->Start();

        if (signal_module) {
                signal_module->Start();
        }

        return 0;
}

static bool MainThreadLoop()
{
        bool enable_reboot = false;
        DeviceStatus device_status;

        while (bRun) {
                WatchDog::FeedWatchDog();
                sleep(5);
                Survey::DeviceStatusUpload();

                device_status = Parameters::GetInstance()->GetDeviceStatus();
                if ((device_status.status & REBOOT_SYSTEM) == REBOOT_SYSTEM) {
                        enable_reboot = true;
                        break;
                } else if ((device_status.status & SIGNAL_MODULE_TESTING) == SIGNAL_MODULE_TESTING) {
                        if (signal_module) {
                                signal_module->SetSignalModuleTesting();
                        }
                        unsigned int status = device_status.status & (~SIGNAL_MODULE_TESTING);
                        Parameters::GetInstance()->ChangeDeviceStatus(status);
                } else if ((device_status.status & NO_FLASH_DISK) == NO_FLASH_DISK) {
                        Debug(DEBUG, "warning: No flash disk");
                }
        }

        return enable_reboot;
}

static int TerminateThreads()
{
        if (signal_module) {
                signal_module->Terminate();
        }
        arbiter->Terminate();
        fix_capture->Terminate();
        udp_server->Terminate();
        tcp_server->Terminate();
        tcp_client->Terminate();

        if (signal_module) {
                signal_module->Join();
        }
        arbiter->Join();
        fix_capture->Join();
        udp_server->Join();
        tcp_server->Join();
        tcp_client->Join();

        return 0;
}

static int ReleaseResource()
{
        Survey::DeInit();
        image_buffer_deinit();

        if (signal_module) {
                delete signal_module;
        }

        if (arbiter) {
                delete arbiter;
        }

        if (fix_capture) {
                delete fix_capture;
        }

        if (udp_server) {
                delete udp_server;
        }

        if (tcp_server) {
                delete tcp_server;
        }

        if (tcp_client) {
                delete tcp_client;
        }

        return 0;
}

int main(int argc, char *argv[])
{
        GpioCtl::ResetFpga();
        WatchDog::InitWatchDog();

        CreateThreadObjects();
        CreateImageBufferList();
        StartThreads();
        SetSignalHandler();
        bool enable_reboot = MainThreadLoop();

        TerminateThreads();
        ReleaseResource();
        sync();

        if (enable_reboot) {
                GpioCtl::ResetPower();
        }

        return 0;
}
