/**
 * @file	peripherral_manage.h
 * @brief	外设管理模块	
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2011-12-20
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef _PERIPHERRAL_MANAGE_H_
#define _PERIPHERRAL_MANAGE_H_

#include "ldczn_base.h"

class PeripherralManage {

public:
        static bool GetTriggerInfo(TriggerInfo ** info);
        static bool PutTriggerInfo(const TriggerInfo * info);
        static bool ReleaseTriggerInfo(TriggerInfo * info);
        static int  GetTriggerListAvailableSize();
        static void ClearList();

        static void EnableRecv();
        static void DisableRecv();

        static void Init();
        static void Deinit();

private:
        static void ClearListFront();
        static bool PushListBack(const TriggerInfo * info);
        static int  GetTriggerListSize();

         PeripherralManage();
        ~PeripherralManage();
};

#endif
