#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include "mutex.h"
#include "uploader.h"
#include "mem_pool.h"
#include "storage.h"
#include "debug.h"
#include "snap_type.h"
#include "Adaptor.h"

using namespace std;

static const int kMaxUploaderListSize = 5;

Uploader::Uploader()
{
        m_bNoPictureOnDisk = false;
}

Uploader::~Uploader()
{
        if (m_pVehicleInfoStorage) {
                delete m_pVehicleInfoStorage;
        }
        m_pVehicleInfoStorage = NULL;
}

void Uploader::Open(MemPool * pool)
{
        mem_pool = pool;
        m_pVehicleInfoStorage = new Storage();
}

//关闭处理器
void Uploader::Close()
{
        //将内存队列中的数据写入磁盘
        while (true) {
                CaptureInfo *captureInfo = NULL;
                GetSampleFromListFront(&captureInfo);
                if (captureInfo == NULL) {
                        break;
                }
                savePicData(captureInfo, true);
                FreeSample(captureInfo);
        }
}

void Uploader::Process(CaptureInfo * captureInfo)
{
        if (captureInfo == 0) {
                return;
        }

        bool ret = false;

        if (captureInfo->pTrigger_info->offence_type == DeviceSnapType_NormalPass) {
                ret = Adaptor::UploadPassingVehicle(captureInfo, true);
        } else {
                ret = Adaptor::UploadViolationVehicle(captureInfo, true);
                if (ret) {
                        ret = Adaptor::UploadPassingVehicle(captureInfo, true);
                }
        }

        if (!ret) {
                Debug(DEBUG, "send capture info fail");
                this->AddSampleToListFront(captureInfo);
                sleep(1);
        } else {
                //传输成功，删除已写入磁盘的信息
                if (captureInfo->bSaved) {
                        m_pVehicleInfoStorage->RemoveVehicleInfo(
                                        captureInfo->pTrigger_info->snap_nbr);
                        Debug(DEBUG, "remove info from disk done");
                }
                this->m_bLastSendFinish = true;

                FreeSample(captureInfo);
                Debug(DEBUG, "free memory of uploading info");
        }
}

void Uploader::FreeSample(CaptureInfo * captureInfo)
{
        mem_pool->DeAlloc((void *)captureInfo);
}

bool Uploader::savePicData(CaptureInfo * captureInfo, bool bNeedUpload)
{
        if (m_pVehicleInfoStorage != NULL) {
                if (false == m_pVehicleInfoStorage->SaveVehicleInfo(captureInfo, bNeedUpload)) {
                        printf("Save capture infomation error\n");
                        return false;
                }

        }

        return true;
}

void Uploader::AddSampleToListFront(CaptureInfo * captureInfo)
{
        CaptureInfo *saveCaptureInfo = NULL;
        if (m_lstMutex.Lock()) {
                //如果等待传输队列太长，则将最早的数据保存
                if (m_lstCaptureInfo.size() > (list < CaptureInfo * >::size_type) kMaxUploaderListSize) {
                        saveCaptureInfo = m_lstCaptureInfo.back();
                        m_lstCaptureInfo.pop_back();
                }
                //将最新的数据加到队列头
                m_lstCaptureInfo.push_front(captureInfo);
                m_lstMutex.Unlock();
        } else {
                printf("AddSampleToListFront:Lock capture list error\n");
        }

        if (saveCaptureInfo) {
                printf("Save capture information to disk\n");
                savePicData(saveCaptureInfo, true);
                FreeSample(saveCaptureInfo);
                m_bNoPictureOnDisk = false;
        }
}

//先传最新的数据
void Uploader::GetSampleFromListFront(CaptureInfo ** captureInfo)
{
        if (m_lstMutex.Lock()) {
                if (!(m_lstCaptureInfo.empty())) {
                        (*captureInfo) = m_lstCaptureInfo.front();
                        m_lstCaptureInfo.pop_front();
                }
                m_lstMutex.Unlock();
        } else {
                printf("GetSampleFromListFront:Lock capture list error\n");
        }
}

//从磁盘中读取历史数据
void Uploader::AddCaptureInfoFromDisk()
{
        if (m_bNoPictureOnDisk) {
                return;
        }

        CaptureInfo *capture_info[kMaxUploaderListSize];
        CaptureInfo *tmp_info = NULL;
        for (int i = 0; i < kMaxUploaderListSize; i++) {
                capture_info[i] = (CaptureInfo *) mem_pool->Alloc();
                tmp_info = capture_info[i];
                tmp_info->pTrigger_info = (TriggerInfo *) ((char *)tmp_info + CAPTUREINFO_SIZE);
                tmp_info->pVehicle_image_info = (VehicleImageInfo *) ((char *)tmp_info +
                                                                      CAPTUREINFO_SIZE +
                                                                      TRIGGERINFO_SIZE);
                tmp_info->pDevice_info =
                    (DeviceInfo *) ((char *)tmp_info + CAPTUREINFO_SIZE + TRIGGERINFO_SIZE +
                                    VEHICLEIMAGEINFO_SIZE);
                tmp_info->pVehicle_image_info->frame = 0;
                tmp_info->pVehicle_image_info->vehicle_image[0].image_buffer =
                    (char *)tmp_info + FIRSET_FRAMT_OFFSET;
                tmp_info->pVehicle_image_info->vehicle_image[1].image_buffer =
                    (char *)tmp_info + SECOND_FRAMT_OFFSET;
                tmp_info->pVehicle_image_info->vehicle_image[2].image_buffer =
                    (char *)tmp_info + THIRD_FRAMT_OFFSET;
        }

        m_pVehicleInfoStorage->GetVehicleInfo(capture_info, kMaxUploaderListSize);

        int count = 0;
        for (int i = 0; i < kMaxUploaderListSize; i++) {
                tmp_info = capture_info[i];
                if (tmp_info->pVehicle_image_info->frame) {
                        AddSampleToListFront(tmp_info);
                        m_bNoPictureOnDisk = false;

                } else {
                        count++;
                        FreeSample(tmp_info);
                }
        }

        if (count == kMaxUploaderListSize) {
                m_bNoPictureOnDisk = true;
        }
}

void Uploader::Run()
{
        sleep(1);
        while (!IsTerminated()) {

                CaptureInfo *captureInfo = 0;
                GetSampleFromListFront(&captureInfo);

                if (captureInfo == 0) {
                        AddCaptureInfoFromDisk();
                } else {
                        Debug(DEBUG, "snap nbr is %s", captureInfo->pTrigger_info->snap_nbr);
                        Process(captureInfo);
                }
                usleep(500000);
        }

        Close();

        return;
}
