#include <unistd.h>
#include <stdio.h>
#include <sys/reboot.h>
#include <signal.h>
#include <linux/rtc.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <time.h>

#include "util.h"
#include "pref.h"

void Util::Reboot()
{
        kill(1, SIGTERM);
        kill(1, SIGKILL);
        sync();
        sync();
        sync();
        sleep(5);
        reboot(RB_AUTOBOOT);
}

bool Util::From_sys_clock(void)
{
#define TWEAK_USEC 200
        struct tm tm_time;
        struct timeval tv;
        unsigned adj = TWEAK_USEC;
        int rtc = open("/dev/rtc0", O_WRONLY);

        if (rtc < 0)
                return false;

        while (1) {
                unsigned rem_usec;
                time_t t;
                gettimeofday(&tv, NULL);

                t = tv.tv_sec;
                rem_usec = 1000000 - tv.tv_usec;
                if (rem_usec < 1024) {
                      small_rem:
                        t++;
                }

                localtime_r(&t, &tm_time);
                tm_time.tm_isdst = 0;

                gettimeofday(&tv, NULL);
                if (tv.tv_sec < t || (tv.tv_sec == t && tv.tv_usec < 1024)) {
                        break;
                }

                adj += 32;
                if (adj >= 1024) {
                        break;
                }

                rem_usec = 1000000 - tv.tv_usec;
                if (rem_usec < 1024) {
                        goto small_rem;
                }
                usleep(rem_usec - adj);
        }

        ioctl(rtc, RTC_SET_TIME, &tm_time);
        close(rtc);
        return true;
}

bool Util::CalibrateTime(const char *time)
{
        tm current_tm;
        sscanf(time, "%04d-%02d-%02d %02d:%02d:%02d",
               &current_tm.tm_year, &current_tm.tm_mon,
               &current_tm.tm_mday, &current_tm.tm_hour, &current_tm.tm_min, &current_tm.tm_sec);

        current_tm.tm_year -= 1900;
        current_tm.tm_mon -= 1;

        current_tm.tm_isdst = -1;       //不区分是否夏令时

        time_t current_time;
        current_time = mktime(&current_tm);

        if (stime(&current_time) != 0) {
                return false;
        }

        if (From_sys_clock()) {
                return true;
        }

        return false;
}

int Util::GetVersionParams(const char *filename, const char *sec,
                        const char *key, const char *def, char *val)
{
        XMLNode root_node = XMLNode::parseFile(filename);
        XMLNode pref = root_node.getChildNodeWithAttribute("pref", "name", "version");

        if (load_pref(pref, sec, key, def, val) < 0) {
                memcpy(val, def, strlen(def) + 1);
        }

        root_node.writeToFile(filename);

        return 0;
}

int Util::SetBaseParams(const char *filename, const char *sec, const char *key, const char *val)
{
        XMLNode root_node = XMLNode::parseFile(filename);
        XMLNode pref = root_node.getChildNodeWithAttribute("pref", "name", "param_base");
        save_pref(pref, sec, key, val);
        root_node.writeToFile(filename, "utf-8");

        return 0;
}

int Util::GetBaseParams(const char *filename, const char *sec,
                        const char *key, const char *def, char *val)
{
        XMLNode root_node = XMLNode::parseFile(filename);
        XMLNode pref = root_node.getChildNodeWithAttribute("pref", "name", "param_base");

        if (load_pref(pref, sec, key, def, val) < 0) {
                memcpy(val, def, strlen(def) + 1);
        }

        root_node.writeToFile(filename);

        return 0;
}
