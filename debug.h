#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <stdio.h>
#include <syslog.h>

enum {
        DEBUG   = LOG_DEBUG,
        INFO    = LOG_INFO,
        WARN    = LOG_WARNING,
        ERROR   = LOG_ERR,
        FATAL   = LOG_CRIT
};

#define Debug(log_level, format, ...) \
	printf("%s in %s Line[%d]:" format"\n", \
		__FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__); \
        syslog(log_level, format, ##__VA_ARGS__)


#define STATICS_START(name)                     \
        {                                       \
        struct timeval start;                   \
        struct timeval end;                     \
        const char *codec_name = name;          \
        gettimeofday(&start, NULL);             \
        {
#define STATICS_STOP()                                          \
        }                                                       \
                gettimeofday(&end, NULL);                       \
                printf("%s : %ldms\n", codec_name,                 \
                       (end.tv_sec  - start.tv_sec)  * 1000 +   \
                       (end.tv_usec - start.tv_usec) / 1000);   \
                }

#endif

