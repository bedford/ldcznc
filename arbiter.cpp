/**
 * @file	arbiter.cpp
 * @brief	合并抓拍图像和触发数据
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2011-12-20
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "sensor.h"
#include "image_buffer.h"
#include "debug.h"
#include "mutex.h"
#include "peripherral_manage.h"
#include "camera.h"
#include "fix_capture.h"
#include "arbiter.h"
#include "parameters.h"
#include "flash_light.h"
#include "snap_type.h"

//抓拍方式标记位(手动抓拍、视频、外触发抓拍)
enum SensorParamsBytes {
        WORK_MODE,
        TRIGGER_MODE,
        GAIN_UPPER,
        GAIN_LOWER,
        EXPOSURE_UPPER,
        EXPOSURE_LOWER,
        RED_GAIN_UPPER,
        RED_GAIN_LOWER,
        BLUE_GAIN_UPPER,
        BLUE_GAIN_LOWER,
        AVARAGE_LUMA
};

/**
 * @brief       闪光灯调整时间间隔定义(时间间隔 25ms * kFlashLightCtlCnt)
 */
static const int kFlashLightCtlCnt = 40 * 60 * 5;

static unsigned int kSensorParamOffset[16];

/**
 * @fn		void InitSensorParamsOffset()
 *
 * @brief	初始化前端FPGA工作模式,当前成像参数的偏移量
 *
 */
void Arbiter::InitSensorParamsOffset(unsigned int offset)
{
        kSensorParamOffset[WORK_MODE]         = offset + 32;
        kSensorParamOffset[TRIGGER_MODE]      = offset + 48;
        kSensorParamOffset[GAIN_UPPER]        = offset + 21;
        kSensorParamOffset[GAIN_LOWER]        = offset + 20;
        kSensorParamOffset[EXPOSURE_UPPER]    = offset + 19;
        kSensorParamOffset[EXPOSURE_LOWER]    = offset + 18;
        kSensorParamOffset[RED_GAIN_UPPER]    = offset + 23;
        kSensorParamOffset[RED_GAIN_LOWER]    = offset + 22;
        kSensorParamOffset[BLUE_GAIN_UPPER]   = offset + 25;
        kSensorParamOffset[BLUE_GAIN_LOWER]   = offset + 24;
        kSensorParamOffset[AVARAGE_LUMA]      = offset + 16;
}

Arbiter::Arbiter()
{

}

Arbiter::~Arbiter()
{
        _fix_capture = NULL;
        delete _pCamera;
        _pCamera = NULL;
        delete _image_processor;
        _image_processor = NULL;
        Debug(DEBUG, "arbiter deconstruct");
}

void Arbiter::InitFlashParam()
{
        FlashParam flash_param = Parameters::GetInstance()->GetFlashParam();

        Sensor::GetInstance()->SetSensorRedLightDelay(flash_param.redlight_delay);
        Sensor::GetInstance()->SetSensorRedLightEfficient(flash_param.redlight_efficient);
        Sensor::GetInstance()->SetSensorLEDMultiple(flash_param.led_mutiple);

        if (flash_param.flash_mode == FlashOn) {
                Sensor::GetInstance()->SetSensorFlashOn();
        } else if (flash_param.flash_mode == FlashOff) {
                Sensor::GetInstance()->SetSensorFlashOff();
        }

        if (flash_param.led_mode == FlashOn) {
                Sensor::GetInstance()->SetSensorLedOn();
        } else if (flash_param.led_mode == FlashOff) {
                Sensor::GetInstance()->SetSensorLedOff();
        }

        if (flash_param.continuous_light == FlashOn) {
                Sensor::GetInstance()->SetSensorConstantLightOn();
        } else if (flash_param.continuous_light == FlashOff) {
                Sensor::GetInstance()->SetSensorConstantLightOff();
        }

        if (flash_param.redlight_sync_mode) {
                Sensor::GetInstance()->SetSensorSyncOn();
        } else {
                Sensor::GetInstance()->SetSensorSyncOff();
        }

        FlashLightCtl::SetThreshold(flash_param.flash_on_threshold);
        FlashLightCtl::SetFlashMode(flash_param.flash_mode);

        Sensor::GetInstance()->SetSensorFlashDelay(flash_param.flash_delay);
}

void Arbiter::InitSensor()
{
        DeviceInfo device_info = Parameters::GetInstance()->GetDeviceInfo();
        CameraParam camera_param = Parameters::GetInstance()->GetCameraParam();

        if (device_info.device_mode == VideoMonitor) {
                Sensor::GetInstance()->SetSensorVideo();        //初始化相机为视频模式
        } else {
                Sensor::GetInstance()->SetSensorCapture();      //初始化相机为触发模式
        }

        _image_processor->SetJpegQvalue(camera_param.jpeg_quality_value);

        switch (camera_param.aew_mode) {
        case AEWMODE_DISABLE:
                Sensor::GetInstance()->SetSensorAEManual();
                break;
        case AEWMODE_GAIN:
                Sensor::GetInstance()->SetSensorAEAuto();
                Sensor::GetInstance()->SetSensorAEMethod(0x01);
                break;
        case AEWMODE_EXP:
                Sensor::GetInstance()->SetSensorAEAuto();
                Sensor::GetInstance()->SetSensorAEMethod(0x02);
                break;
        case AEWMODE_AUTO:
                Sensor::GetInstance()->SetSensorAEAuto();
                Sensor::GetInstance()->SetSensorAEMethod(0x00);
                break;
        default:
                break;
        }

        Sensor::GetInstance()->SetSensorMaxExp(camera_param.max_exposure);
        Sensor::GetInstance()->SetSensorMinExp(camera_param.min_exposure);
        Sensor::GetInstance()->SetSensorMinGain(camera_param.min_gain);
        Sensor::GetInstance()->SetSensorMaxGain(camera_param.max_gain);
        Sensor::GetInstance()->SetSensorVideoTargetGray(camera_param.video_target_gray);
        Sensor::GetInstance()->SetSensorRgain(camera_param.red_gain);
        Sensor::GetInstance()->SetSensorBgain(camera_param.blue_gain);
        Sensor::GetInstance()->SetSensorAEZone(camera_param.ae_zone);
        Sensor::GetInstance()->SetSensorADSampleBits(camera_param.ad_sample_bits);
        Sensor::GetInstance()->SetSensorSharpenFactor(camera_param.sharpen_factor);
}

void Arbiter::Init(FixCapture * fix_capture, int ccd_type)
{
        _fix_capture = fix_capture;
        _image_processor = new ImageProcess(ccd_type);

        _pCamera = new Camera(ccd_type);
        _pCamera->OpenCamera(); //初始化相机
        _pCamera->InitCamera();

        unsigned int offset = _pCamera->GetSensorStatusOffset();
        InitSensorParamsOffset(offset);

        int handle = _pCamera->GetCameraHandle();       //获取相机设备文件句柄
        Sensor::GetInstance()->SetSensorHandle(handle); //向sensor模块设置相机设备文件句柄

        InitSensor();
        InitFlashParam();
}

void Arbiter::SettingFlashLight()
{
        FlashCtl current_flash_ctl;
        current_flash_ctl.current_gray = Sensor::GetInstance()->GetCurrentAvgLuma();
        Debug(INFO, "current gray is %d", current_flash_ctl.current_gray);

        if (FlashLightCtl::GetLedFlashStatus(&current_flash_ctl)) {
                Debug(INFO, "set flash on");
                Sensor::GetInstance()->SetSensorFlashOn();
        } else {
                Debug(INFO, "set flash off");
                Sensor::GetInstance()->SetSensorFlashOff();
        }
}

void Arbiter::FillSensorParams(SensorCurrentParams * param, unsigned char *vir_addr)
{
        int upper = (((*(vir_addr + kSensorParamOffset[EXPOSURE_UPPER])) & 0x03) << 8);
        int lower = *(vir_addr + kSensorParamOffset[EXPOSURE_LOWER]);
        param->exposure = upper + lower;

        upper = (((*(vir_addr + kSensorParamOffset[GAIN_UPPER])) & 0x03) << 8);
        lower = *(vir_addr + kSensorParamOffset[GAIN_LOWER]);
        param->gain = upper + lower;

        upper = (((*(vir_addr + kSensorParamOffset[RED_GAIN_UPPER])) & 0x03) << 8);
        lower = *(vir_addr + kSensorParamOffset[RED_GAIN_LOWER]);
        param->red_gain = upper + lower;

        upper = (((*(vir_addr + kSensorParamOffset[BLUE_GAIN_UPPER])) & 0x03) << 8);
        lower = *(vir_addr + kSensorParamOffset[BLUE_GAIN_LOWER]);
        param->blue_gain = upper + lower;

        param->avg_luma = *(vir_addr + kSensorParamOffset[AVARAGE_LUMA]);
}

void Arbiter::LogCaptureParameter(SensorCurrentParams * param)
{
        Debug(INFO, "exposure is %d, gain is %d, red_gain is %d, blue_gain is %d, avg_luma is %d",
                        param->exposure, param->gain, param->red_gain, param->blue_gain,
                        param->avg_luma);
}

void Arbiter::Run()
{
        struct cmemBuffer *pBuffer = NULL;
        struct image_buffer_description *image = NULL;
        char snap_mode = -1;
        char trigger_mode = -1;
        int ret = -1;
        _pCamera->StartCapture();       //开始采集数据
        struct timeval tv;
        int flash_cnt = 0;

        while (!IsTerminated()) {

                if (!_pCamera->GetCaptureBuffer(&pBuffer, &tv)) {
                        usleep(25000);
                        flash_cnt++;
                        if (flash_cnt == kFlashLightCtlCnt) {
                                flash_cnt = 0;
                                SettingFlashLight();
                        }
                        continue;
                }
                Debug(DEBUG, "get Buffer from driver");

                if (capture_buffer_alloc(&image)) {
                        Debug(DEBUG, "get image buffer from list failed");
                        _pCamera->PutCaptureBuffer(pBuffer);
                        usleep(15000);
                        continue;
                }
                Debug(DEBUG, "get image buffer from list OK");

                unsigned char *bytes = (unsigned char *)pBuffer->vir_addr;
                snap_mode = *(bytes + kSensorParamOffset[WORK_MODE]);
                trigger_mode = *(bytes + kSensorParamOffset[TRIGGER_MODE]);
                Debug(DEBUG, "snap mode is %d, trigger_mode is %d", snap_mode, trigger_mode);
                FillSensorParams(&image->param, bytes);

                if (snap_mode) {        //触发模式
                        if (trigger_mode) {     //外触发
                                image->type = CAPTURE_FRAME;
                                image->tv = tv;
                                ret = Process(pBuffer, image);
                        } else {        //手动抓拍
                                Debug(INFO, "manual snap");
                                image->type = CAPTURE_FRAME;
                                TriggerInfo info;
                                info.lane = 0;
                                info.offence_type = DeviceSnapType_ManualSnap; 
                                image->tv = tv;
                                info.passing_time = tv;
                                ret = _image_processor->CaptureProcess(pBuffer, image, &info);
                        }
                        LogCaptureParameter(&image->param);
                } else {        //视频模式
                        image->type = VIDEO_FRAME;
                        ret = _image_processor->VideoProcess(pBuffer, image);
                }

                if (ret < 0) {
                        capture_buffer_free(image);
                } else {
                        transport_buffer_put(image);
                }
                image = NULL;

                _pCamera->PutCaptureBuffer(pBuffer);
                Debug(DEBUG, "QBUF to driver");
        }

        _pCamera->CleanupCamera();      //释放相机资源
        return;
}

int Arbiter::Process(struct cmemBuffer *pBuffer, struct image_buffer_description *image)
{
        TriggerInfo *info = NULL;
        int trigger_list_size = PeripherralManage::GetTriggerListAvailableSize();
        Debug(INFO, "trigger list size is %d", trigger_list_size);

        if (trigger_list_size == 0) {
                return -1;
        }

        int index = 0;
        int ret = 0;
        for (; index < trigger_list_size; index++) {
                if (PeripherralManage::GetTriggerInfo(&info)) {
                        if (!IsMatch(image, info)) {
                                PeripherralManage::ReleaseTriggerInfo(info);
                                ret = -1;
                                continue;
                        }

                        if (_image_processor->CaptureProcess(pBuffer, image, info) == -1) {
                                PeripherralManage::ReleaseTriggerInfo(info);
                                ret = -1;
                                continue;
                        }

                        _fix_capture->PutSingleInfo(image, info);
                        if ((index + 1) < trigger_list_size) {
                                ProcessSamePicture(image);
                        }

                } else {
                        Debug(ERROR, "No trigger");
                        ret = -1;
                }
        }
        info = NULL;
        return ret;
}

int Arbiter::ProcessSamePicture(struct image_buffer_description *image)
{
        struct image_buffer_description *pImageBuffer = NULL;
        if (capture_buffer_alloc(&pImageBuffer)) {
                Debug(ERROR, "get image buffer from list failed");
                return -1;
        }

        unsigned int tmp_phy_addr = pImageBuffer->info.phy_addr;
        unsigned int tmp_vir_addr = pImageBuffer->info.vir_addr;
        memcpy(pImageBuffer, image, sizeof(struct image_buffer_description));

        pImageBuffer->info.phy_addr = tmp_phy_addr;
        pImageBuffer->info.vir_addr = tmp_vir_addr;
        memcpy((unsigned char *)pImageBuffer->info.vir_addr,
               (unsigned char *)image->info.vir_addr, image->info.size);

        transport_buffer_put(pImageBuffer);
        pImageBuffer = NULL;

        return 0;
}

bool Arbiter::IsMatch(struct image_buffer_description * image, TriggerInfo * info)
{
        long delta = (image->tv.tv_sec - info->passing_time.tv_sec) * 1000 +
            (image->tv.tv_usec - info->passing_time.tv_usec) / 1000;
        Debug(INFO, "image and trigger info time delta is %ld", delta);
        if (delta < 0 || delta > 250) {
                return false;
        }

        return true;
}
