/**
 * @file	uart.cpp
 * @brief	RS485串口,接收信号处理模块上传的实时数据
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2012-04-25
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011-2012
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <unistd.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

#include "uart.h"
#include "mutex.h"
#include "peripherral_manage.h"
#include "survey.h"
#include "debug.h"
#include "snap_type.h"

static const char *kUartDevice          = "/dev/ttyS1";
static const char *kUartSelector        = "/dev/uart1_select";

static const unsigned int kRxdSelected  = 0x4E01;
static const unsigned int kTxdSelected  = 0x4E02;

static const char kPacketHead   = 0x7E;
static const char kPacketTail   = 0x7F;
static const char kPacketEsc    = 0x7D;

static const int kCmdTypeOffset         = 1;
static const int kLaneOffset            = 2;
static const int kEventOffset           = 3;
static const int kSnapVerifyOffset      = 4;
static const int kVehicleSpeedOffset    = 6;
static const int kVehicleLengthOffset   = 7;
static const int kRedLightTimeOffset    = 8;

static int test_flag = 0;

static const int kDriverMode[5] = {
        DeviceSnapType_LimitedPass,
        DeviceSnapType_RedLightPass,
        DeviceSnapType_NormalPass,
        DeviceSnapType_HighSpeedPass,
        DeviceSnapType_ReversePass
};

Uart::Uart()
{
        uart_fd = -1;
}

Uart::~Uart()
{
        Deinit();
        uart_fd         = -1;
        uart_selector   = -1;
}

void Uart::Deinit()
{
        if (uart_selector >= 0)
                close(uart_selector);

        if (uart_fd >= 0)
                close(uart_fd);

        PeripherralManage::Deinit();
}

void Uart::EnableSend(void)
{
        if (ioctl(uart_selector, kTxdSelected, NULL) < 0) {
                Debug(ERROR, "set failed: %s, errno: %d", strerror(errno), errno);
                close(uart_selector);
                return;
        }
}

void Uart::EnableRecv(void)
{
        if (ioctl(uart_selector, kRxdSelected, NULL) < 0) {
                Debug(ERROR, "set failed: %s, errno: %d", strerror(errno), errno);
                close(uart_selector);
                return;
        }
}

int Uart::SendMessage(char *buffer, int length)
{
        EnableSend();
        int ret = write(uart_fd, buffer, length);
        if (ret < length) {
                ret = -1;
        }

        ret = 0;
        EnableRecv();

        return ret;
}

void Uart::SetSignalModuleTesting()
{
        test_flag = 1;
        char buf[4];
        buf[0] = 0x7E;
        buf[1] = 0xA1;
        buf[2] = 0x01;
        buf[3] = 0x7F;
        SendMessage(buf, sizeof(buf));
        test_flag = 0;
}

void Uart::Init()
{
        struct termios newtio, oldtio;
        const int BAUDRATE = B57600;

        uart_fd = open(kUartDevice, O_RDWR | O_NOCTTY);
        if (uart_fd < 0) {
                Debug(ERROR, "open port errno!");
                return;
        }

        /* 配置串口参数 */
        tcgetattr(uart_fd, &oldtio);
        bzero(&newtio, sizeof(newtio));
        newtio.c_cflag          = BAUDRATE | CS8 | CLOCAL | CREAD;
        newtio.c_iflag          = IGNPAR;
        newtio.c_oflag          &= 0;
        newtio.c_lflag          &= 0;
        newtio.c_cc[VTIME]      = 1;
        newtio.c_cc[VMIN]       = 0;
        tcflush(uart_fd, TCIFLUSH);
        if (tcsetattr(uart_fd, TCSANOW, &newtio) != 0) {
                Debug(ERROR, "com set error");
                return;
        }

        uart_selector = open(kUartSelector, O_RDWR);
        if (uart_selector < 0) {
                Debug(ERROR, "open uart selector failed: %s, errno: %d", strerror(errno), errno);
                return;
        }

        EnableRecv();
        Debug(DEBUG, "Init ttyS1 finish");

        PeripherralManage::Init();
}

void Uart::UploadEventStatus(TriggerInfo * info)
{
        PassingStatus status;
        status.vehicle_length   = info->vehicle_length;
        status.vehicle_speed    = info->vehicle_speed;
        status.event_type       = info->drive_mode;
        status.lane             = info->lane;
        Survey::PassingStaticsUpdate(&status);
}

static void CreateSnapID(TriggerInfo * info)
{
        char tmp[32];
        struct tm *pTm = localtime(&info->passing_time.tv_sec);

        strftime(tmp, 32, "%Y%m%d%H%M%S", pTm);
        sprintf(info->snap_nbr, "%s%03d", tmp, (int)(info->passing_time.tv_usec / 1000));
}

void Uart::ProcessEvent(char *buf, int len)
{
        len = len;

        TriggerInfo info;
        info.lane               = buf[kLaneOffset];
        info.vehicle_speed      = buf[kVehicleSpeedOffset];
        info.vehicle_length     = buf[kVehicleLengthOffset];
        info.snap_verify_ID     = buf[kSnapVerifyOffset];
        info.redlight_time      = buf[kRedLightTimeOffset];
        gettimeofday(&info.passing_time, NULL);
        Debug(INFO, "trigger time is %ld.%ld, lane is %d, redlighttime is %d",
              info.passing_time.tv_sec, info.passing_time.tv_usec, info.lane, info.redlight_time);

        int event_type          = buf[kEventOffset];
        info.drive_mode         = kDriverMode[event_type];
        info.offence_type       = kDriverMode[event_type];

        CreateSnapID(&info);
        PeripherralManage::PutTriggerInfo(&info);
        UploadEventStatus(&info);
}

void Uart::ProcessPacket(char *buf, int len)
{
        char ch = buf[kCmdTypeOffset];
        switch (ch) {
        case 0x60:
                ProcessEvent(buf, len);
                break;
        case 0x62:
                Debug(INFO, "write params to module ack");
                break;
        case 0x64:
                Debug(INFO, "read params to module ack");
                break;
        case 0x66:
                Debug(INFO, "heart beat from module");
                break;
        default:
                Debug(INFO, "unknown message type, %02x", ch);
                break;
        }
}

void Uart::Run()
{
        char buf[MIN_LENGTH];
        char ch;
        int recv_bytes          = -1;
        bool have_esc           = false;
        bool have_packet_head   = false;
        Init();
        while (!IsTerminated()) {

                while (test_flag) {
                        usleep(1000);
                }

                if (read(uart_fd, &ch, 1) < 1) {
                        continue;
                }

                if (recv_bytes == MIN_LENGTH) {
                        recv_bytes = -1;
                        have_esc   = false;
                        have_packet_head = false;
                        continue;
                }

                switch (ch) {
                case kPacketHead:
                        recv_bytes              = 0;
                        have_packet_head        = true;
                        buf[recv_bytes]         = ch;
                        recv_bytes++;
                        break;
                case kPacketTail:
                        if (have_packet_head) {
                                buf[recv_bytes] = ch;
                                recv_bytes++;
                                ProcessPacket(buf, recv_bytes);

                                memset(buf, 0, sizeof(buf));
                                have_packet_head        = false;
                                recv_bytes              = -1;
                        }
                        break;
                case kPacketEsc:
                        have_esc = true;
                        break;
                default:
                        if (have_packet_head) {
                                if (have_esc) {
                                        buf[recv_bytes] = ch ^ 0x20;
                                        have_esc        = false;
                                        recv_bytes++;
                                } else {
                                        buf[recv_bytes] = ch;
                                        recv_bytes++;
                                }
                        }
                        break;
                }
        }
}
