#ifndef _WATCH_DOG_H_
#define _WATCH_DOG_H_

class WatchDog {
public:
        static int InitWatchDog();
        static int FeedWatchDog();

private:
        static int SetWatchDogTimeout();
        static void StartWatchDog();
        static int OpenWatchDog();
};

#endif
