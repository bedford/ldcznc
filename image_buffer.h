/**
 * @file        image_buffer.h
 * @brief       图像缓存队列内存操作接口
 * @author      hrh <huangrh@landuntec.com>
 * @version     1.0.0
 * @date        2012-05-17
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef IMG_BUF_H_
#define IMG_BUF_H_

#include <sys/time.h>
#include "ce_buffer.h"
#include "ldczn_protocol.h"

#define VIDEO_FRAME		1
#define CAPTURE_FRAME		2

struct image_buffer_description {

        struct cmemBuffer info;
        struct timeval tv;
        int type;               /* 视频 = 1, 图像 = 2 */
        SensorCurrentParams param;
};

struct env_image_buffer {

        int buf_len;            /* buffer length */
        int buf_num;            /* total buffer numbers */
        int resd_buf_num_max;   /* maximal reserved buffer number */
        int capt_buf_num_max;   /* maximal capture buffer number */
        int tran_buf_num_min;   /* minimal capture buffer number */
        int tran_buf_num_max;   /* maximal sender buffer number */
};

int image_buffer_init(struct env_image_buffer *env);
int image_buffer_deinit(void);

int capture_buffer_alloc(struct image_buffer_description **desc);
int capture_buffer_free(struct image_buffer_description *desc);
int capture_buffer_get(struct image_buffer_description **desc);
int capture_buffer_put(struct image_buffer_description *desc);

int transport_buffer_alloc(struct image_buffer_description **desc);
int transport_buffer_free(struct image_buffer_description *desc);
int transport_buffer_get(struct image_buffer_description **desc);
int transport_buffer_put(struct image_buffer_description *desc);
int transport_buffer_get_len(void);
int transport_buffer_get_level(void);

#endif
