#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "upgrade_program.h"
#include "ldczn_protocol.h"
#include "parameters.h"
#include "socket.h"
#include "debug.h"

static const int kUpgradePort = 39004;
static const int kRecvBufferLength = 64 * 1024;

int UpgradeProgram::ReceiveFile(char *file_name, int total_length)
{
        FILE *fp = NULL;
        if ((fp = fopen(file_name, "wb")) == NULL) {
                Debug(DEBUG, "Open upgrade temporary file %s failed", file_name);
                return -1;
        }

        int local_sock = -1;
        if ((local_sock = Socket::CreateTcp()) < 0) {
                Debug(DEBUG, "can't create tcp socket");
                return -1;
        }

        char client_addr[MIN_LENGTH];
        Socket::SetTcpNonblock(local_sock);
        UploadParam param = Parameters::GetInstance()->GetUploadParam();
        memcpy(client_addr, param.client_addr, sizeof(client_addr));
        if (Socket::Connect(local_sock, client_addr, kUpgradePort, 3000)) {
                return -1;
        }

        char *recv_buffer = new char[kRecvBufferLength];
        int recv_bytes = 0;
        while (recv_bytes < total_length) {
                int ret = Socket::Read(local_sock, recv_buffer,
                                kRecvBufferLength, 3000);
                if (ret < 0) {
                        Debug(DEBUG, "receive errror");
                        break;
                } else if (ret == 0) {
                        Debug(DEBUG, "peer closed by server");
                        break;
                }

                recv_bytes += ret;
                if (fwrite(recv_buffer, ret, 1, fp) < (unsigned int)ret) {
                        Debug(DEBUG, "write upgrade file failed");
                        break;
                }
        }
        Debug(DEBUG, "total %d bytes", recv_bytes);

        fclose(fp);
        fp = NULL;
        delete[] recv_buffer;
        recv_buffer = NULL;

        if (recv_bytes < total_length) {
                return -1;
        }

        return 0;
}

int UpgradeProgram::ExcuteUpdate(char *file_name, unsigned int type)
{
        const char *target_location = "";
        switch (type) {
        case REQ_MAN_UPG_APP:
                target_location = "/opt/app";
                break;
        case REQ_MAN_UPG_FPGA:
        case REQ_MAN_UPG_ALG:
                target_location = "/opt/bin";
                break;
        default:
                Debug(DEBUG, "not support type");
                return -1;
                break;
        }

        char cmd[MIN_LENGTH];
        sprintf(cmd, "cp %s %s", file_name, target_location);
        int sys_ret = system(cmd);
        if (sys_ret < 0) {
                Debug(DEBUG, "call system failed");
                return -1;
        } else if (sys_ret > 0) {
                Debug(DEBUG, "can't upgrade");
                return -1;
        }

        return 0;
}

int UpgradeProgram::UpdateProgram(unsigned int type, char* buf)
{
        payload_man_upgrade* upgrade = (payload_man_upgrade*)buf;
        Debug(DEBUG, "type is %d, length is %d, name is %s",
                        type, upgrade->total_length, upgrade->file_name);

        char tmp_file_name[MAX_LENGTH] = {0};
        sprintf(tmp_file_name, "/tmp/%s", upgrade->file_name);
        if (ReceiveFile(tmp_file_name, upgrade->total_length)) {
                return -1;
        }

        if (ExcuteUpdate(tmp_file_name, type)) {
                return -1;
        }

        return 0;
}
