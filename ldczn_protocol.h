#ifndef _LDCZN_PROTOCOL_H_
#define _LDCZN_PROTOCOL_H_

#define PROTOCOL_MAGIC {'S', 'Z', 'L', 'D'}

#define PROTOCOL_MAJOR 1
#define PROTOCOL_MINOR 0

#define MIN_LENGTH      32
#define MAX_LENGTH      128

#define REQ_TYPE_MASK           0xFF000000
#define REQ_TYPE_SUB_MASK       0x00FF0000
#define REQ_TYPE_CMD_MASK       0x0000FFFF

/**
 * @brief 数据信息打包方式
 */
enum ENCODEING_TYPE {
        ENCODING_TYPE_RAW       = 0,    /* 原始结构体方式               */
        ENCODING_TYPE_PB        = 1,    /* Google Protocol Buffer       */
        ENCODING_TYPE_XML       = 2     /* xml方式                      */
};

/**
 * @brief 数据包类型
 */
enum MESSAGE_TYPE {
        MESSAGE_TYPE_REQ = 0,   /* 请求 */
        MESSAGE_TYPE_ACK = 1    /* 应答 */
};

enum ACK_STATUS {
        ACK_SUCCESS     = 0x00000000,   /* 成功                 */
        ACK_NOT_FOUND   = 0x10000000,   /* 不存在的配置项       */
        ACK_FAILED      = 0x80000000    /* 失败                 */
};

enum AEW_MODE {
        AEWMODE_DISABLE = 0,
        AEWMODE_GAIN    = 1,
        AEWMODE_EXP     = 2,
        AEWMODE_AUTO    = 3
};

enum REQ_TYPE {
        REQ_TYPE_HEARTBEAT      = 0x01000000,   /* 心跳包       */
        REQ_TYPE_MANUFACTURE    = 0x02000000,   /* 生产类       */
        REQ_TYPE_CONTROL        = 0x03000000,   /* 控制类       */
        REQ_TYPE_DEVICE_SEARCH  = 0x04000000,   /* 设备查找     */
        REQ_TYPE_SET_PARAMETER  = 0x05000000,   /* 设置参数     */
        REQ_TYPE_GET_PARAMETER  = 0x06000000,   /* 获取参数     */
        REQ_TYPE_IMAGE_UPLOAD   = 0x07000000,   /* 图像上传     */
        REQ_TYPE_VIDEO_UPLOAD   = 0x08000000    /* 视频上传     */
};

enum CTL_TYPE {
        CTL_TYPE_REBOOT                 = 0x00010000,   /* 重启                 */
        CTL_TYPE_MANNUAL_SNAP           = 0x00020000,   /* 手动抓拍             */
        CTL_TYPE_VIDEO                  = 0x00030000,   /* 连续图像             */
        CTL_TYPE_CAPTURE                = 0x00040000,   /* 触发模式             */
        CTL_TYPE_SIGNAL_MODULE_TESTING  = 0x00050000    /* 信号处理模块测试模式 */
};

enum REQ_MAN_ {
        REQ_MAN_FMT     = 0x00010000,   /* 格式化硬盘   */
        REQ_MAN_UPG     = 0x00020000,   /* 更新         */
        REQ_MAN_RESET   = 0x00030000,   /* 恢复出厂设置 */
        REQ_MAN_CLR     = 0x00040000,   /* 清除分区     */
};

enum REQ_MAN_UPG_ {
        REQ_MAN_UPG_BACK        = 0x00000001,   /* 备份分区             */
        REQ_MAN_UPG_APP         = 0x00000002,   /* 应用程序             */
        REQ_MAN_UPG_PREF        = 0x00000003,   /* 配置文件             */
        REQ_MAN_UPG_DRV         = 0x00000004,   /* 设备驱动             */
        REQ_MAN_UPG_ALG_MOD     = 0x00000005,   /* DSP算法支持模块      */
        REQ_MAN_UPG_LIB         = 0x00000006,   /* 共享库               */
        REQ_MAN_UPG_ALG         = 0x00000007,   /* DSP算法              */
        REQ_MAN_UPG_MON         = 0x00000008,   /* 监控程序             */
        REQ_MAN_UPG_MCU         = 0x00000009,   /* MCU固件程序          */
        REQ_MAN_UPG_FPGA        = 0x0000000A,   /* 清除分区             */
};

enum REQ_MAN_CLR_ {
        REQ_MAN_CLR_BACK = 0x00000001,  /* 清除备份分区 */
        REQ_MAN_CLR_DATA = 0x00000002,  /* 清除数据分区 */
};

enum PARAM_TYPE {
        PARAM_TYPE_CAMERA               = 0x00010000,   /* 相机成像参数 */
        PARAM_TYPE_NETWORK              = 0x00020000,   /* 网络参数     */
        PARAM_TYPE_UPLOAD               = 0x00030000,   /* 数据上传参数 */
        PARAM_TYPE_TIME                 = 0x00040000,   /* 校时         */
        PARAM_TYPE_FLASH                = 0x00050000,   /* 补光灯参数   */
        PARAM_TYPE_DEVICE_INFO          = 0x00060000,   /* 设备安装属性 */
        PARAM_TYPE_PLATE                = 0x00070000,   /* 车牌识别参数 */
        PARAM_TYPE_VIDEO_DETECT         = 0x00080000,   /* 视频检测参数 */
        PARAM_TYPE_MAN_PROPERTY         = 0x00090000,   /* 设备出厂信息 */
        PARAM_TYPE_TRAFFIC              = 0x000A0000,   /* 车流量参数   */
        PARAM_TYPE_SIGNAL_MODULE        = 0x000B0000,   /* 信号处理模块 */
        PARAM_TYPE_REDLIGHT_ZONE        = 0x000C0000,   /* 信号灯区域   */
        PARAM_TYPE_OVERLAY_SETTING      = 0x000D0000,   /* 叠加参数     */    
};

enum PARAM_MAN_PROPERTY {
        PARAM_MAN_PROPERTY_PROGRAM_VERSION = 0x00000001,        /* 程序版本     */
};

enum PARAM_CAMERA {
        PARAM_CAMERA_DEFAULT_EXPOSURE   = 0x00000000,
        PARAM_CAMERA_MIN_EXPOSURE       = 0x00000001,
        PARAM_CAMERA_MAX_EXPOSURE       = 0x00000002,
        PARAM_CAMERA_DEFAULT_GAIN       = 0x00000003,
        PARAM_CAMERA_MIN_GAIN           = 0x00000004,
        PARAM_CAMERA_MAX_GAIN           = 0x00000005,
        PARAM_CAMERA_RED_GAIN           = 0x00000006,
        PARAM_CAMERA_BLUE_GAIN          = 0x00000007,
        PARAM_CAMERA_VIDEO_TARGET       = 0x00000008,
        PARAM_CAMERA_CAPTURE_TARGET     = 0x00000009,
        PARAM_CAMERA_AEW_MODE           = 0x0000000A,
        PARAM_CAMERA_AD_SAMPLE_BITS     = 0x0000000B,
        PARAM_CAMERA_SHARPEN_FACTOR     = 0x0000000C
};

#pragma pack(push)
#pragma pack(4)

/**
 * @brief 标准协议头
 */
struct header_std {
        char magic[4];          /* "SZLD"       */
        char protocol_major;    /* 1            */
        char protocol_minor;    /* 0            */
        char encoding;          /* 信息打包方式 */
        char auth_code[16];     /* 认证信息     */
        char msg_type;          /* 消息类型     */
        unsigned int msg_size;  /* 消息长度     */
};

struct payload_req {
        unsigned int id;        /* 请求ID       */
        unsigned int type;      /* 请求类型     */
};

struct payload_ack {
        unsigned int id;        /* 请求ID       */
        unsigned int type;      /* 请求类型     */
        unsigned int status;    /* 请求状态     */
};

/**
 * @brief 标准请求数据包
 */
typedef struct {
        struct header_std head;
        struct payload_req req;
} PacketRequest;

/**
 * @brief 标准回应数据包
 */
typedef struct {
        struct header_std head;
        struct payload_ack ack;
} PacketAck;

struct payload_search {
        char serial_number[MIN_LENGTH]; /* 相机序列号                                   */
        char site[MAX_LENGTH];          /* 安装地点(便于后台多台相机时，区分设备)       */
        char device_ip[MIN_LENGTH];     /* 设备IP地址                                   */
};

/**
 * @brief 设备查找回应数据包
 */
struct packet_search_ack {
        PacketAck ack;
        struct payload_search search;
};

/**
 * @brief 相机图像参数值
 */
typedef struct {
        unsigned int max_exposure;              /* 最大曝光时间                         */
        unsigned int min_exposure;              /* 最小曝光时间                         */
        unsigned int max_gain;                  /* 最大增益                             */
        unsigned int min_gain;                  /* 最小增益                             */
        unsigned int default_exposure;          /* 默认及夜间曝光时间                   */
        unsigned int default_gain;              /* 默认及夜间增益                       */
        unsigned int blue_gain;                 /* 蓝色增益初始值                       */
        unsigned int red_gain;                  /* 红色增益初始值                       */
        unsigned int video_target_gray;         /* 视频目标灰度值                       */
        unsigned int trigger_target_gray;       /* 抓拍目标灰度值                       */
        unsigned int ae_zone;                   /* 2A数据统计区域                       */
        unsigned int aew_mode;                  /* AEW 调整模式                         */
        unsigned int jpeg_quality_value;        /* JPEG图像压缩比(取值范围:1~100)       */
        unsigned int ad_sample_bits;            /* AD芯片采样位置                       */
        unsigned int sharpen_factor;            /* 锐化因子                             */
} CameraParam;

/**
 * @brief 相机当前状态信息
 */
typedef struct {
        unsigned int exposure;          /* 当前快门值           */
        unsigned int gain;              /* 当前增益值           */
        unsigned int flash_status;      /* 闪光灯状态           */
        unsigned int blue_gain;         /* 当前蓝色增益值       */
        unsigned int red_gain;          /* 当前红色增益值       */
        unsigned int avg_luma;          /* 当前图像平均亮度     */
} SensorCurrentParams;

/**
 * @brief 上传图像
 */
struct payload_img_trans {
        unsigned int info_len;
        unsigned int data_len;
};

struct packet_img_trans_req {
        PacketRequest request;
        SensorCurrentParams params;
        struct payload_img_trans trans;
};

/**
 * @brief 外设参数
 */
typedef struct {
        unsigned int flash_mode;                /* 闪光灯模式           */
        unsigned int flash_delay;               /* 闪光灯延时           */
        unsigned int led_mode;                  /* LED补光灯模式        */
        unsigned int led_mutiple;               /* LED补光灯倍频        */
        unsigned int continuous_light;          /* 常亮灯模式           */
        unsigned int redlight_delay;            /* 红灯延时             */
        unsigned int redlight_sync_mode;        /* 红灯同步使能         */
        unsigned int redlight_efficient;        /* 红灯抓拍有效时间     */
        unsigned int flash_on_threshold;        /* 开灯阈值             */
} FlashParam;

/**
 * @brief 相机参数设置数据包
 */
struct packet_img_sparm_req {
        PacketRequest request;
        CameraParam parameter;
};

/**
 * @brief 获取相机参数回应数据包
 */
struct packet_img_gparm_ack {
        PacketAck ack;
        CameraParam parameter;
};

/**
 * @brief 闪光灯参数设置
 */
struct packet_flash_sparm_req {
        PacketRequest request;
        FlashParam parameter;
};

/**
 * @brief 获取外设参数回应数据包
 */
struct packet_flash_gparam_ack {
        PacketAck ack;
        FlashParam parameter;
};

/**
 * @brief 设备信息
 */
typedef struct {
        char serial_number[MIN_LENGTH];         /* 相机序列号   */
        char device_number[MIN_LENGTH];         /* 设备编号     */
        char site[MAX_LENGTH];                  /* 安装地点     */
        char direction_text[MAX_LENGTH];        /* 行驶方向     */
        char road_code[MIN_LENGTH];             /* 路段代码     */
        char department_code[MIN_LENGTH];       /* 单位代码     */
        char police_code[MIN_LENGTH];           /* 警员代码     */
        char latitude[MIN_LENGTH];              /* 经度         */
        char longtitude[MIN_LENGTH];            /* 纬度         */
        unsigned int device_mode;               /* 设备工作模式 */
        unsigned int lane;                      /* 安装车道     */
        unsigned int direction_code;            /* 行驶方向代码 */
        unsigned int car_speed_limit;           /* 小车限速     */
        unsigned int vehicle_speed_limit;       /* 大车限速     */
        unsigned int min_speed_limit;           /* 最小限速     */
        unsigned int capture_speed;             /* 起拍速度     */
        unsigned int snap_mode;                 /* 抓拍模式(0,0度;1,90度;2,180度;3,270度) */
} DeviceInfo;

/**
 * @brief 车流量统计参数
 */
typedef struct {
        int period;                     /* 车流量统计周期       */
        int motor_vehicle_length;       /* 摩托车车长上限       */
        int small_vehicle_length;       /* 小车车长上限         */
        int large_vehicle_length;       /* 中型车车长上限       */
        int long_vehicle_length;        /* 大型车车长上限       */
} TrafficParam;

/**
 * @brief 网络参数
 */
typedef struct {
        char mac_addr[MIN_LENGTH];              /* MAC地址      */
        char device_ip[MIN_LENGTH];             /* 设备IP地址   */
        char netmask[MIN_LENGTH];               /* 子网掩码     */
        char gateway[MIN_LENGTH];               /* 网关         */
        char dns_server_main[MIN_LENGTH];       /* DNS服务器1   */
        char dns_server_bak[MIN_LENGTH];        /* DNS服务器2   */

} NetworkParam;

/**
 * @brief 抓拍信息上传参数
 */
typedef struct {
        char upload_server[MAX_LENGTH]; /* 服务器ip/域名        */
        char ntp_server[MAX_LENGTH];    /* NTP校时服务器        */
        char client_addr[MAX_LENGTH];   /* 客户端ip             */
        unsigned int upload_port;       /* 信息上传端口         */
} UploadParam;

/**
 * @brief 设置设备运行参数数据包
 */
struct packet_deviceinfo_sparam_req {
        PacketRequest request;
        DeviceInfo info;
};

/**
 * @brief 获取设备运行参数数据包
 */
struct packet_deviceinfo_gparam_ack {
        PacketAck ack;
        DeviceInfo info;
};

/**
 * @brief 设置上传参数数据包
 */
struct packet_uploadinfo_sparam_req {
        PacketRequest request;
        UploadParam parameter;
};

/**
 * @brief 获取上传参数设置包
 */
struct packet_uploadinfo_gparam_ack {
        PacketAck ack;
        UploadParam parameter;
};

struct packet_networparam_sparam_req {
        PacketRequest request;
        NetworkParam parameter;
};

struct packet_networparam_gparam_ack {
        PacketAck ack;
        NetworkParam parameter;
};

struct rectangle {
        unsigned int top;
        unsigned int left;
        unsigned int width;
        unsigned int height;
};

typedef struct {
        struct rectangle rect[6];
        int rect_num;
} RedLightZone;

struct packet_redlight_gparam_ack {
        PacketAck ack;
        RedLightZone parameter;
};

struct packet_redlight_sparam_req {
        PacketRequest request;
        RedLightZone parameter;
};

typedef struct {
        unsigned short listen_port;
        unsigned int total_length;
        //unsigned short         file_type;
        char file_name[33];
} payload_man_upgrade;

struct packet_man_upgrade_req {
        PacketRequest request;
        payload_man_upgrade update_camera;
};

struct packet_man_upgrade_ack {
        PacketAck ack;
        payload_man_upgrade update_camera;
};

typedef struct {
        char fpga_version[MIN_LENGTH];          /* FPGA程序版本 */
        char dsp_version[MIN_LENGTH];           /* DSP算法版本  */
        char kernel_version[MIN_LENGTH];        /* 内核版本     */
        char filesystem_version[MIN_LENGTH];    /* 文件系统版本 */
        char app_version[MIN_LENGTH];           /* 应用程序版本 */
        char build_kernel_version[MAX_LENGTH];  /* 内核编译版本号               */
        char build_appfs_version[MAX_LENGTH];   /* 应用文件系统编译版本号       */
} ProgramVersion;

struct packet_programversion_gparam_ack {
        PacketAck       ack;
        ProgramVersion  program_version;
};

typedef struct {
        unsigned int enable_device_number;      /* 设备编号     */
        unsigned int enable_timestamp;          /* 通过时间     */
        unsigned int enable_site;               /* 安装地点     */
        unsigned int enable_direction;          /* 行驶方向     */
        unsigned int enable_lane;               /* 车道         */
        unsigned int enable_plate;              /* 车牌号码     */
        unsigned int enable_passing_type;       /* 通行方式     */
        unsigned int enable_redlight_time;      /* 红灯开始时间 */
        unsigned int enable_speed;              /* 车速         */
        unsigned int enable_speed_limit;        /* 限速         */
        unsigned int enable_overspeed_percent;  /* 超速比例     */
        unsigned int enable_extra_info1;        /* 自定义信息1  */
        unsigned int enable_extra_info2;        /* 自定义信息2  */
        char extra_overlay_info1[MAX_LENGTH];   /* 自定义信息1内容 */
        char extra_overlay_info2[MAX_LENGTH];   /* 自定义信息2内容 */
} OverlaySetting;

struct packet_overlaysetting_gparam_ack {
        PacketAck       ack;
        OverlaySetting  overlay_setting;
};

struct packet_overlaysetting_sparam_req {
        PacketRequest   request;
        OverlaySetting  overlay_setting;
};

/**
 * @brief 存储相关信息
 */
typedef struct {
        unsigned int total_capability;          /* 存储器总容量(单位:Mb)        */
        unsigned int remain_capability;         /* 未使用容量                   */
        unsigned int unuploaded_info_num;       /* 未上传数据数量               */
} StorageInfo;

/**
 * @brief 设备状态
 */
typedef struct {
        char current_time[MIN_LENGTH];  /* 当前时间     */
        unsigned int status;            /* 相机状态     */
        unsigned int flash_status;      /* 闪光灯状态   */
        unsigned int total_passing;     /* 通行车辆总数 */
        unsigned int violation_passing; /* 违法车辆总数 */
} DeviceStatus;

typedef struct {
        unsigned short year;    /* 0 ~ 65535    */
        unsigned char mon;      /* 1 ~ 12       */
        unsigned char day;      /* 1 ~ 31       */
        unsigned char hour;     /* 0 ~ 23       */
        unsigned char min;      /* 0 ~ 59       */
        unsigned char sec;      /* 0 ~ 59       */
        unsigned char msec;     /* 0 ~ 999      */
        unsigned char wday;     /* 0 ~ 6        */
} CompareTime;

struct packet_man_comp_time_req {
        PacketRequest request;
        CompareTime time;
};

#pragma pack(pop)

#endif
