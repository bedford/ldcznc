/**
 * @file	resize.h
 * @brief	缩放模块接口
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2011-12-10
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef _RESIZE_H_
#define	_RESIZE_H_

#include "ce_buffer.h"

struct Resize_Object {
        int fd;
};

typedef struct Resize_Object *Resize_Handle;

class Resize {

      public:
        static Resize_Handle ResizeCreate();
        static int ResizeDelete(Resize_Handle handle);
        static int ResizeConfig(Resize_Handle handle,
                                struct cmemBuffer *source_buf,
                                struct cmemBuffer *destination_buf,
                                struct cmemBuffer *destination_buf2);
        static int ResizeExecute(Resize_Handle handle,
                                 struct cmemBuffer *source_buf,
                                 struct cmemBuffer *destination_buf,
                                 struct cmemBuffer *destination_buf2);

};

#endif
