/**
 * @file	arbiter.h
 * @brief	合并抓拍图像和触发数据	
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2011-12-20
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef _ARBITER_H_
#define _ARBITER_H_

#include "thread.h"
#include "image_process.h"

class Camera;
class FixCapture;

class Arbiter:public Thread {

public:
        Arbiter();
        ~Arbiter();
        void Init(FixCapture * fix_capture, int ccd_type);
        void SetFullResolution(bool mode);

protected:
        void Run();

private:
        void InitSensorParamsOffset(unsigned int offset);
        int Process(struct cmemBuffer *pBuffer, struct image_buffer_description *image);
        int ProcessSamePicture(struct image_buffer_description *image);
        void InitSensor();
        void InitFlashParam();

        bool IsMatch(struct image_buffer_description *image, TriggerInfo * info);

        void SettingFlashLight();
        void FillSensorParams(SensorCurrentParams * param, unsigned char *vir_addr);
        void LogCaptureParameter(SensorCurrentParams * param);

        Camera *_pCamera;

        ImageProcess *_image_processor;
        FixCapture *_fix_capture;
};

#endif
