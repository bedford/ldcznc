#include <cassert>
#include "mutex.h"
#include "mem_pool.h"
#include "debug.h"
#include <iostream>

using namespace std;

MemPool::MemPool(unsigned int block_size, int block_reserved)
{
        _pData = NULL;
        Init(block_size, block_reserved);
}

bool MemPool::Init(unsigned int block_size, int blocks)
{
        assert(block_size > 0);
        assert(blocks > 0 && blocks < 15);

        _pData = (char *)malloc((block_size + sizeof(unsigned int)) * blocks);
        if (_pData == NULL) {
                return false;
        }

        _first_available_block  = 0;
        _blocks_amount          = blocks;
        _blocks_available       = blocks;
        _block_size             = block_size;

        //填充内存块的链
        int i   = 0;
        char *p = _pData;
        int *pNext = NULL;

        for (; i != blocks; p += (block_size + sizeof(unsigned int))) {
                pNext   = (int *)(p + block_size);
                *pNext  = ++i;
        }

        return true;
}

int MemPool::Release()
{
        //请确保调用者释放指针方能执行操作
        if (_blocks_available < _blocks_amount) {       //出错说明有内存申请了但还没有被释放
                int Result = _blocks_amount - _blocks_available;
                Debug(WARN, "still %d blocks not release", Result);
                return Result;
        }

        if (_pData != NULL) {
                free((char *)_pData);
                _pData = NULL;
        }

        return 0;
}

void *MemPool::Alloc()
{
        char *pResult = NULL;
        if (_blocks_available > 0) {
                //Debug(DEBUG, "申请到第 %d 块", _first_available_block + 1);
                pResult = _pData + (_first_available_block * (_block_size + sizeof(unsigned int)));
                _first_available_block = *(unsigned int *)(pResult + _block_size);
                memset(pResult, 0, _block_size);        //清0
                _blocks_available--;
        } else {
                return pResult;
        }
        return pResult;
}

void MemPool::DeAlloc(void *p)
{
        assert(Is_ptr(p));      //检查是不是非法指针
        char *toRelease = static_cast < char *>(p);

        //把释放掉的块加入到表头里．新建一个表头,表头下一个块指向原来的第一个可用块
        *((unsigned int *)(toRelease + _block_size)) = _first_available_block;
        //第一个可用块指向表头
        _first_available_block = (toRelease - _pData) / (_block_size + sizeof(unsigned int));
        //块对齐检查
        assert(_first_available_block ==
               (int)((toRelease - _pData) / (_block_size + sizeof(unsigned int))));

        _blocks_available++;
        //Debug(DEBUG, "释放掉第 %d 块", _first_available_block + 1);
}

bool MemPool::Is_ptr(void *p)
{
        //内存不在这个里面。也不是他分配的。
        if (p < _pData
            || p > (_pData + (_block_size + sizeof(unsigned int)) * (_blocks_amount - 1)))
                return false;

        //指针没在blockSize边界上对齐．肯定不是由这个MemPool分配的
        long result = ((char *)p - _pData) % (_block_size + sizeof(unsigned int));
        if (result != 0)
                return false;

        return true;
}

//----------------------------------------------------
//查看还有多少空间可以用
//----------------------------------------------------
int MemPool::Capacity()
{
        return _blocks_available;
}
