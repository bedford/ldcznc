#include "property.h"

struct CameraProperty Property::GetDeviceProperty(int ccd_type)
{
        struct CameraProperty camera_property;
        switch (ccd_type) {
        case LD_500W:
                camera_property.width = 2432;
                camera_property.height = 2049;
                camera_property.height_useful = 2048;
                camera_property.stride = camera_property.width;
                camera_property.line = camera_property.width * 2;
                break;
        case LD_200W:
        default:
                camera_property.width = 1600;
                camera_property.height = 1201;
                camera_property.height_useful = 1200;
                camera_property.stride = camera_property.width;
                camera_property.line = camera_property.width * 2;
                break;
        }

        return camera_property;
}
