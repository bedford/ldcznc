#ifndef _MUTEX_H_
#define _MUTEX_H_

#include <pthread.h>

class Mutex {

      public:
        Mutex();
        ~Mutex();

        bool Lock();
        bool tryLock();
        bool Unlock();

      private:
         pthread_mutex_t _mutex;
};

#endif
