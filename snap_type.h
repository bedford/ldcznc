#ifndef _SNAP_TYPE_H_
#define _SNAP_TYPE_H_

enum IceSnapType {
        IceSnapType_HighSpeedPass           = 0,    /* 超高速       */
        IceSnapType_LowSpeedPass            = 1,    /* 超低速       */
        IceSnapType_RedLightPass            = 2,    /* 闯红灯       */
        IceSnapType_ReversePass             = 3,    /* 逆行         */
        IceSnapType_OccupyLanePass          = 4,    /* 占道行驶     */
        IceSnapType_LeftTurnPass            = 5,    /* 左转         */
        IceSnapType_RightTurnPass           = 6,    /* 右转         */
        IceSnapType_TurnOverPass            = 7,    /* 调头         */
        IceSnapType_Parking                 = 8,    /* 泊车         */
        IceSnapType_NormalPass              = 9,    /* 正常通行     */
        IceSnapType_LimitedPass             = 10,   /* 限行         */
        IceSnapType_FakePlateVehicle        = 100,  /* 套牌车辆     */
};

enum DeviceSnapType {
        DeviceSnapType_LimitedPass      = 0, /* 限行违章        */
        DeviceSnapType_RedLightPass     = 1, /* 闯红灯          */
        DeviceSnapType_NormalPass       = 2, /* 正常通行        */
        DeviceSnapType_HighSpeedPass    = 3, /* 超高速          */
        DeviceSnapType_ReversePass      = 4, /* 逆行            */
        DeviceSnapType_ManualSnap       = 5, /* 手动抓拍        */
};

#endif
