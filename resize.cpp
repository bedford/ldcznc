#include "resize.h"
#include "debug.h"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

#include <media/davinci/videohd.h>
#include <media/davinci/vpfe_capture.h>
#include <media/davinci/dm365_ccdc.h>
#include <media/davinci/dm365_ipipe.h>
#include <media/davinci/imp_previewer.h>
#include <media/davinci/imp_resizer.h>

#define RESIZER_DEVICE   "/dev/davinci_resizer"

/**
 * @fn		Resize_Handle ResizeCreate()
 * @brief	创建缩放模块(含相关资源配置)
 *
 * @return	Resize_Handle
 */
Resize_Handle Resize::ResizeCreate()
{
        Resize_Handle hResize;
        unsigned long oper_mode;

        hResize = (Resize_Handle) calloc(1, sizeof(Resize_Object));
        if (hResize == NULL) {
                Debug(ERROR, "Failed to allocate space for Framecopy Object");
                return NULL;
        }

        /* Open resizer device */
        hResize->fd = open(RESIZER_DEVICE, O_RDWR);

        if (hResize->fd == -1) {
                Debug(ERROR, "Failed to open %s", RESIZER_DEVICE);
                return NULL;
        }

        oper_mode = IMP_MODE_SINGLE_SHOT;
        if (ioctl(hResize->fd, RSZ_S_OPER_MODE, &oper_mode) < 0) {
                Debug(ERROR, "Resizer can't set operation mode");
                goto fac_error;
        }

        if (ioctl(hResize->fd, RSZ_G_OPER_MODE, &oper_mode) < 0) {
                Debug(ERROR, "Resizer can't get operation mode");
                goto fac_error;
        }

        if (oper_mode != IMP_MODE_SINGLE_SHOT) {
                Debug(ERROR, "Resizer can't set operation mode single shot, the mode is %lu", oper_mode);
                goto fac_error;
        }

        return hResize;

fac_error:
        close(hResize->fd);
        hResize->fd = 0;
        return NULL;
}

/**
 * @fn		int ResizeDelete(Resize_Handle handle)
 * @brief	释放缩放模块
 *
 * @param	[in] handle
 *
 * @return	
 */
int Resize::ResizeDelete(Resize_Handle handle)
{
        if (handle) {
                close(handle->fd);
                free(handle);
        }

        return 0;
}

/**
 * @fn		int ResizeConfig(Resize_Handle handle,
 * 				 struct cmemBuffer *source_buf,
 * 				 struct cmemBuffer *destination_buf,
 * 				 struct cmemBuffer *destination_buf2)
 * @brief	配置缩放模块
 *
 * @param	[in] handle
 * @param	[in] source_buf
 * @param	[out] destination_buf
 * @param	[out] destination_buf2
 *
 * @return	
 */
int Resize::ResizeConfig(Resize_Handle handle,
                         struct cmemBuffer *source_buf,
                         struct cmemBuffer *destination_buf, struct cmemBuffer *destination_buf2)
{
        unsigned int hDif;
        unsigned int vDif;
        struct rsz_channel_config rsz_chan_config;
        struct rsz_single_shot_config rsz_ss_config;

        /* Make sure our input parameters are valid */
        if (!handle) {
                Debug(ERROR, "Resize_Handle parameter must not be NULL");
                return -1;
        }

        if (!source_buf) {
                Debug(ERROR, "Source buffer parameter must not be NULL");
                return -1;
        }

        if (!destination_buf) {
                Debug(ERROR, "Destination buffer parameter must not be NULL");
                return -1;
        }

        if (destination_buf->width <= 0) {
                Debug(ERROR, "Destination buffer width must be greater than zero");
                return -1;
        }

        if (destination_buf->height <= 0) {
                Debug(ERROR, "Destination buffer height must be greater than zero");
                return -1;
        }

        if ((source_buf->stride & 0x1F) != 0) {
                Debug(ERROR, "Source buffer pitch must be a multiple of 32 bytes");
                return -1;
        }

        if ((destination_buf->stride & 0x1F) != 0) {
                Debug(ERROR, "Destination buffer pitch must be a multiple of 32 bytes");
                return -1;
        }

        /* Check for valid buffer scaling */
        hDif = source_buf->width * 256 / destination_buf->width;
        vDif = source_buf->height * 256 / destination_buf->height;

        if (hDif < 32) {
                Debug(ERROR, "Horizontal up-scaling must not exceed 8x");
                return -1;
        }

        if (hDif > 4096) {
                Debug(ERROR, "Horizontal down-scaling must not exceed 1/16x");
                return -1;
        }

        if (vDif < 32) {
                Debug(ERROR, "Vertical up-scaling must not exceed 8x");
                return -1;
        }

        if (vDif > 4096) {
                Debug(ERROR, "Vertical down-scaling must not exceed 1/16x");
                return -1;
        }

        /* Setting default configuration in Resizer */
        memset(&rsz_ss_config, 0, sizeof(rsz_ss_config));
        rsz_chan_config.oper_mode       = IMP_MODE_SINGLE_SHOT;
        rsz_chan_config.chain           = 0;
        rsz_chan_config.len             = 0;
        rsz_chan_config.config          = NULL;  /* to set defaults in driver */
        if (ioctl(handle->fd, RSZ_S_CONFIG, &rsz_chan_config) < 0) {
                Debug(ERROR, "Error in setting default configuration for single shot mode");
                goto faco_error;
        }

        /* default configuration setting in Resizer successfull */
        memset(&rsz_ss_config, 0, sizeof(rsz_ss_config));
        rsz_chan_config.oper_mode       = IMP_MODE_SINGLE_SHOT;
        rsz_chan_config.chain           = 0;
        rsz_chan_config.len             = sizeof(rsz_ss_config);
        rsz_chan_config.config          = &rsz_ss_config;
        if (ioctl(handle->fd, RSZ_G_CONFIG, &rsz_chan_config) < 0) {
                Debug(ERROR, "Error in getting resizer channel configuration from driver");
                goto faco_error;
        }

        /* input params are set at the resizer */
        rsz_ss_config.input.pix_fmt             = IPIPE_UYVY;
        rsz_ss_config.input.image_width         = source_buf->width;
        rsz_ss_config.input.image_height        = source_buf->height; 
        rsz_ss_config.input.ppln                = rsz_ss_config.input.image_width + 8;
        rsz_ss_config.input.lpfr                = rsz_ss_config.input.image_height + 10;
        rsz_ss_config.input.line_length         = source_buf->line;

        rsz_ss_config.output1.enable    = 1;
        rsz_ss_config.output1.pix_fmt   = IPIPE_UYVY;
        rsz_ss_config.output1.width     = destination_buf->width;
        rsz_ss_config.output1.height    = destination_buf->height;

        if (destination_buf2 == NULL) {
                rsz_ss_config.output2.enable = 0;
        } else {
                rsz_ss_config.output2.enable = 1;
                rsz_ss_config.output2.pix_fmt   = IPIPE_YUV420SP;
                rsz_ss_config.output2.width     = destination_buf2->width;
                rsz_ss_config.output2.height    = destination_buf2->height;
        }

        rsz_chan_config.oper_mode       = IMP_MODE_SINGLE_SHOT;
        rsz_chan_config.chain           = 0;
        rsz_chan_config.len             = sizeof(rsz_ss_config);
        if (ioctl(handle->fd, RSZ_S_CONFIG, &rsz_chan_config) < 0) {
                Debug(ERROR, "Error in setting default configuration for single shot mode");
                goto faco_error;
        }

        /* read again and verify */
        rsz_chan_config.oper_mode       = IMP_MODE_SINGLE_SHOT;
        rsz_chan_config.chain           = 0;
        rsz_chan_config.len             = sizeof(rsz_ss_config);
        if (ioctl(handle->fd, RSZ_G_CONFIG, &rsz_chan_config) < 0) {
                Debug(ERROR, "Error in getting configuration from driver");
                goto faco_error;
        }

        return 0;

      faco_error:
        close(handle->fd);
        handle->fd = 0;
        return -1;
}

/**
 * @fn		int ResizeExecute(Resize_Handle handle,
 * 				  struct cmemBuffer *source_buf,
 * 				  struct cmemBuffer *destination_buf,
 * 				  struct cmemBuffer *destination_buf2)
 *
 * @brief	执行缩放功能
 *
 * @param	[in] handle
 * @param	[in] source_buf
 * @param	[out] destination_buf
 * @param	[out] destination_buf2
 *
 * @return	
 */
int Resize::ResizeExecute(Resize_Handle handle,
                          struct cmemBuffer *source_buf,
                          struct cmemBuffer *destination_buf, struct cmemBuffer *destination_buf2)
{
        struct imp_convert rsz;

        memset(&rsz, 0, sizeof(rsz));
        rsz.in_buff.index       = -1;
        rsz.in_buff.buf_type    = IMP_BUF_IN;
        rsz.in_buff.offset      = source_buf->vir_addr + source_buf->offset; //add offset
        rsz.in_buff.size        = source_buf->height * source_buf->line;

        rsz.out_buff1.index     = -1;
        rsz.out_buff1.buf_type  = IMP_BUF_OUT1;
        rsz.out_buff1.offset    = destination_buf->vir_addr + destination_buf->offset; //add offset
        rsz.out_buff1.size      = destination_buf->height * destination_buf->line;

        if (destination_buf2 != NULL) {
                rsz.out_buff2.index     = -1;
                rsz.out_buff2.buf_type  = IMP_BUF_OUT2;
                rsz.out_buff2.offset    = destination_buf2->vir_addr;
                rsz.out_buff2.size      = destination_buf2->height * destination_buf2->stride;
        }

        /*
         * The IPIPE requires that the memory offsets of the input and output
         * buffers start on 32-byte boundaries.
         */
        if ((rsz.in_buff.offset & 0x1F) != 0) {
                Debug(ERROR, "in offset");
                return -1;
        }

        if ((rsz.out_buff1.offset & 0x1F) != 0) {
                Debug(ERROR, "out offset");
                return -1;
        }

        if ((rsz.out_buff2.offset & 0x1F) != 0) {
                Debug(ERROR, "out offset");
                return -1;
        }

        /* Start IPIPE operation */
        if (ioctl(handle->fd, RSZ_RESIZE, &rsz) == -1) {
                Debug(ERROR, "Failed RSZ_RESIZE");
                return -1;
        }

        return 0;
}
