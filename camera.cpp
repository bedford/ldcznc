/**
 * @file	camera.cpp
 * @brief	
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2011-11-30
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <iomanip>

/* 标准Linux头文件 */
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include <string.h>

/* DaVinci 特有的内核头文件 */
#include <media/davinci/videohd.h>
#include "camera.h"
#include "debug.h"
#include "property.h"

static const char *deviceName = "/dev/video0";
static const unsigned int NUM_IOCTL_RETIES = 100;
static const int kMaxCaptureNumber = 5;

Camera::Camera(int ccd_type)
{
        _ccd_type = ccd_type;

        CameraProperty property = Property::GetDeviceProperty(_ccd_type);
        _capture_cmem_params.width = property.width;
        _capture_cmem_params.height = property.height;
        _capture_cmem_params.stride = property.stride;
        _capture_cmem_params.line = property.line;
        _capture_cmem_params.max_buffer_number = kMaxCaptureNumber;

        _capture_size.width = property.width;
        _capture_size.height = property.height;
}

Camera::~Camera()
{
        Debug(DEBUG, "camera 500w deconstruct");
}

unsigned int Camera::GetSensorStatusOffset()
{
        unsigned int offset = _capture_cmem_params.line * (_capture_cmem_params.height - 1);

        return offset;
}

bool Camera::OpenCamera()
{
        if (_capture_cmem_params.max_buffer_number == 0) {
                Debug(WARN, "max_buffer_number is zero, it should be no less than 1");
                return false;
        }

        if (!OpenDevice(deviceName)) {
                return false;
        }

        if (!InitDevice()) {
                return false;
        }

        return true;
}

bool Camera::InitCamera()
{
        if (!SetVideoFormat()) {
                return false;
        }

        if (!RequestDriverBuffer()) {
                return false;
        }

        if (!UserPtrInit()) {
                return false;
        }

        return true;
}

bool Camera::InitDevice()
{
        if (!QueryAndCheckCap()) {
                return false;
        }

        if (!EnumInputDevice()) {
                return false;
        }

        if (!QueryAndSetStandard()) {
                return false;
        }

        return true;
}

bool Camera::QueryAndCheckCap()
{
        struct v4l2_capability cap;
        if (ioctl(_CameraFd, VIDIOC_QUERYCAP, &cap) == -1) {
                return false;
        }

        if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
                return false;
        }

        if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
                return false;
        }

        return true;
}

bool Camera::EnumInputDevice()
{
        struct v4l2_input input;
        unsigned int tmp_input;
        input.type = V4L2_INPUT_TYPE_CAMERA;
        input.index = 0;

        while (1) {
                if (ioctl(_CameraFd, VIDIOC_ENUMINPUT, &input) < 0) {
                        return false;
                }

                if (!strcasecmp((char *)(input.name), "ldccdmod")) {
                        break;
                }

                input.index++;
        }

        if (ioctl(_CameraFd, VIDIOC_S_INPUT, &input.index) == -1) {
                return false;
        }

        if (ioctl(_CameraFd, VIDIOC_G_INPUT, &tmp_input) == -1) {
                return false;
        }

        if (tmp_input != input.index) {
                return false;
        }

        return true;
}

bool Camera::QueryAndSetStandard()
{
        unsigned int failCount = 0;
        v4l2_std_id standard;
        int ret = -1;

        if (_ccd_type == LD_500W) {
                standard = V4L2_STD_500W_15;
        } else {
                standard = V4L2_STD_200W_15;
        }

        if (ioctl(_CameraFd, VIDIOC_S_STD, &standard) == -1) {
                return false;
        }

        do {
                ret = ioctl(_CameraFd, VIDIOC_QUERYSTD, &standard);
                if (ret == -1 && errno == EAGAIN) {
                        failCount++;
                }

        } while (ret == -1 && errno == EAGAIN && failCount < NUM_IOCTL_RETIES);

        switch (standard) {
        case V4L2_STD_NTSC:
                Debug(DEBUG, "Input video standard is NTSC.");
                break;
        case V4L2_STD_PAL:
                Debug(DEBUG, "Input video standard is PAL.");
                break;
        case V4L2_STD_200W_15:
                Debug(DEBUG, "Input video standard is 200W-15.");
                break;
        case V4L2_STD_500W_15:
                Debug(DEBUG, "Input video standard is 500W-15.");
                break;
        default:
                Debug(DEBUG, "standard %d is not supported", (int)standard);
                return false;
        }
        
        return true;
}

bool Camera::RequestDriverBuffer()
{
        struct v4l2_requestbuffers req;
        memset(&req, 0, sizeof(req));
        req.count       = _capture_cmem_params.max_buffer_number;
        req.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory      = V4L2_MEMORY_USERPTR;

        /* 获取采集设备驱动的缓存(设备内存) */
        if (ioctl(_CameraFd, VIDIOC_REQBUFS, &req) == -1) {
                Debug(WARN, "VIDIOC_REQBUFS failed: %s, errno is %d", strerror(errno), errno);
                return false;
        }

        if (req.count < _capture_cmem_params.max_buffer_number) {
                Debug(ERROR, "Insufficient buffer memory");
                return false;
        }

        return true;
}

bool Camera::OpenDevice(const char *deviceName)
{
        _CameraFd = open(deviceName, O_RDWR | O_NONBLOCK, 0);
        if (_CameraFd == -1) {
                return false;
        }
        return true;
}

int Camera::GetCameraHandle()
{
        return _CameraFd;
}

bool Camera::StartCapture()
{
        enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (ioctl(_CameraFd, VIDIOC_STREAMON, &type) == -1) {
                Debug(ERROR, "VIDIOC_STREAMON failed");
                return false;
        }

        Debug(INFO, "Stream on success");
        return true;
}

bool Camera::StopCapture(void)
{
        enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (ioctl(_CameraFd, VIDIOC_STREAMOFF, &type) == -1) {
                Debug(ERROR, "VIDIOC_STREAMOFF failed");
                return false;
        }
        Debug(INFO, "Stream off success");
        for (unsigned int bufIndex = 0;
             bufIndex < _capture_cmem_params.max_buffer_number; bufIndex++) {

                buffer_delete(_pCmemBuffer + bufIndex);
        }

        delete[]_pCmemBuffer;
        return true;
}

bool Camera::SetCaptureBufferSize(CaptureCmemParams * params)
{
        _capture_cmem_params = *params;
        return true;
}

bool Camera::SetCaptureSize(CaptureSize * size)
{
        _capture_size = *size;
        return true;
}

bool Camera::UserPtrInit()
{
        struct cmemBuffer *pCapBufs = new struct cmemBuffer[_capture_cmem_params.max_buffer_number];
        struct v4l2_buffer buf;

        for (unsigned int bufIndex = 0;
             bufIndex < _capture_cmem_params.max_buffer_number; bufIndex++) {

                pCapBufs[bufIndex].index        = bufIndex;
                pCapBufs[bufIndex].width        = _capture_cmem_params.width;
                pCapBufs[bufIndex].height       = _capture_cmem_params.height;
                pCapBufs[bufIndex].stride       = _capture_cmem_params.stride;
                pCapBufs[bufIndex].line         = _capture_cmem_params.line;
                pCapBufs[bufIndex].extra_size   = 256;

                if (buffer_create(&pCapBufs[bufIndex]) != 0) {
                        Debug(ERROR, "buffer create failed");
                        return false;
                }

                memset(&buf, 0, sizeof(struct v4l2_buffer));
                buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory      = V4L2_MEMORY_USERPTR;
                buf.index       = pCapBufs[bufIndex].index;
                buf.m.userptr   = pCapBufs[bufIndex].vir_addr;
                buf.length      = pCapBufs[bufIndex].size;

                if (ioctl(_CameraFd, VIDIOC_QBUF, &buf) == -1) {
                        Debug(ERROR, "VIODIOC_QBUF failed: %s, errno: %d", strerror(errno), errno);
                        return false;
                }
        }

        _pCmemBuffer = pCapBufs;
        pCapBufs = NULL;
        return true;
}

bool Camera::SetVideoFormat()
{
        struct v4l2_format fmt;
        memset(&fmt, 0, sizeof(struct v4l2_format));
        fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt.fmt.pix.width       = _capture_size.width;
        fmt.fmt.pix.height      = _capture_size.height;
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_UYVY;
        fmt.fmt.pix.field       = V4L2_FIELD_NONE;

        if (ioctl(_CameraFd, VIDIOC_S_FMT, &fmt) == -1) {
                return false;
        }

        Debug(INFO, "set format width: %d, height: %d", _capture_size.width, _capture_size.height);
        return true;
}

bool Camera::GetCaptureBuffer(struct cmemBuffer ** pCaptureBufs, struct timeval * tv)
{
        struct v4l2_buffer v4l2buf;
        memset(&v4l2buf, 0, sizeof(v4l2buf));

        v4l2buf.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        v4l2buf.memory  = V4L2_MEMORY_USERPTR;

        if (ioctl(_CameraFd, VIDIOC_DQBUF, &v4l2buf) < 0) {

                if (errno == EAGAIN || errno == EINTR) {
                        usleep(2000);
                        return false;
                }
                Debug(ERROR, "VIDIOC_DQBUF error: %s, errno is %d", strerror(errno), errno);
                return false;
        }

        *pCaptureBufs   = &_pCmemBuffer[v4l2buf.index];
        *tv             = v4l2buf.timestamp;
        return true;
}

bool Camera::PutCaptureBuffer(struct cmemBuffer * pCaptureBufs)
{
        struct v4l2_buffer v4l2buf;
        memset(&v4l2buf, 0, sizeof(v4l2buf));

        v4l2buf.type            = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        v4l2buf.memory          = V4L2_MEMORY_USERPTR;
        v4l2buf.index           = pCaptureBufs->index;
        v4l2buf.m.userptr       = pCaptureBufs->vir_addr;
        v4l2buf.length          = pCaptureBufs->size;

        if (ioctl(_CameraFd, VIDIOC_QBUF, &v4l2buf) < 0) {
                Debug(ERROR, "VIDIOC_QBUF error: %s, errno is %d", strerror(errno), errno);
                return false;
        }
        return true;
}

bool Camera::CleanupCamera()
{
        StopCapture();
        close(_CameraFd);
        return true;
}
