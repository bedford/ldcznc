#include "flash_light.h"

enum {
        FlashOff,
        FlashOn,
        AutoFlash
};

static int led_mode = FlashOff;
static const unsigned int kStep = 1;
static unsigned char kThreshold = 30;
static bool bLastStatus = false;
static unsigned int AvgLuma = 0;
static unsigned int index = 0;

bool FlashLightCtl::GetLedFlashStatus(FlashCtl * params)
{
        bool ret = false;
        switch (led_mode) {
        case FlashOn:
                ret = true;
                bLastStatus = true;
                break;
        case FlashOff:
                ret = false;
                break;
        case AutoFlash:
                AvgLuma += params->current_gray;
                ++index;
                if (index == kStep) {
                        unsigned int avg = AvgLuma / kStep;
                        if ((avg < kThreshold) && (!bLastStatus)) {
                                bLastStatus = true;
                        } else if (bLastStatus && ((unsigned char)avg > (kThreshold + 20))) {
                                bLastStatus = false;
                        }

                        ret = bLastStatus;

                        index = 0;
                        AvgLuma = 0;
                }
                break;
        default:
                break;
        }

        return ret;
}

bool FlashLightCtl::SetThreshold(unsigned int value)
{
        kThreshold = value;

        return true;
}

bool FlashLightCtl::SetFlashMode(int mode)
{
        led_mode = mode;

        return true;
}
