#ifndef _SENSOR_REGISTER_H_
#define _SENSOR_REGISTER_H_

enum AeMode {
        AUTO    = 0x00,
        MANNUAL = 0x01
};

enum WorkMode {
        VIDEO   = 0x00,
        CAPTURE = 0x01
};

enum SyncMode {
        SYNC_OFF        = 0x00,
        SYNC_ON         = 0x01
};

enum FlashEnable {
        FLASH_DISABLE   = 0x00,
        FLASH_ENABLE    = 0x01
};

enum PreProcessMode {
        DISABLE_MODE    = 0x00,
        ENABLE_MODE     = 0x01
};

enum RegisterAddress {

        EXPOSURE_HIGH                   = 0x00,
        EXPOSURE_LOW                    = 0x01,
        GAIN_HIGH                       = 0x02,
        GAIN_LOW                        = 0x03,
        RED_GAIN_HIGH                   = 0x04,
        RED_GAIN_LOW                    = 0x05,
        BLUE_GAIN_HIGH                  = 0x06,
        BLUE_GAIN_LOW                   = 0x07,
        MIN_GAIN_HIGH                   = 0x08,
        MIN_GAIN_LOW                    = 0x09,
        MAX_GAIN_HIGH                   = 0x0A,
        MAX_GAIN_LOW                    = 0x0B,
        MIN_EXPOSURE_HIGH               = 0x0C,
        MIN_EXPOSURE_LOW                = 0x0D,
        MAX_EXPOSURE_HIGH               = 0x0E,
        MAX_EXPOSURE_LOW                = 0x0F,

        WORK_MODE                       = 0x10,
        AE_METHOD                       = 0x11,
        AE_MODE                         = 0x12,
        FLASH_DELAY                     = 0x13,
        VIDEO_TARGET                    = 0x14,

        LED_MULTIPLE                    = 0x15,
        RED_LIGHT_SYNC_MODE             = 0x16,
        RED_LIGHT_EFFICIENT             = 0x17,
        CONSTANT_LIGHT_MODE             = 0x18,
        RED_LIGHT_DELAY                 = 0x19,
        FLASH_MODE                      = 0x1A,
        LED_MODE                        = 0x1B,

        CAPTURE_NUMBER                  = 0x20,
        CONTINUE_CAPTURE_DELAY_HIGH     = 0x21,
        CONTINUE_CAPTURE_DELAY_LOW      = 0x22,

        PREPROCESS_ENABLE               = 0x23,
        CAPTURE_TARGET                  = 0x24,
        AE_ZONE_INDEX1                  = 0x25,
        AE_ZONE_INDEX2                  = 0x26,
        AE_ZONE_INDEX3                  = 0x27,
        AE_ZONE_INDEX4                  = 0x28,

        RED_LIGHT_SYNC_SOURCE           = 0x29,
        AE_ADJUST_PICTURE_NUMBER        = 0x2A,         /* AE调整图片张数 */
        AD_SAMPLE_BITS                  = 0x2B,         /* AD采样位       */
        SHARPEN_FACTOR                  = 0x2C,         /* 锐化系数       */

        CCD_TYPE                        = 0x40,
        PROJECT_ID                      = 0x41,
        FPGA_VERSION                    = 0x42,
        AVERAGE_LUMA                    = 0x43,
};

#endif
