/**
 * @file        fix_capture.h
 * @brief       业务逻辑处理模块
 * @author      hrh <huangrh@landuntec.com>
 * @version     1.0.0
 * @date        2012-07-06
 * 
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011-2012
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef _FIX_CAPTURE_H_
#define _FIX_CAPTURE_H_

#include "thread.h"
#include "image_buffer.h"
#include "ldczn_base.h"

class Mutex;
class MemPool;
class Uploader;

class FixCapture:public Thread {
public:
        FixCapture();
        ~FixCapture();

        bool PutSingleInfo(struct image_buffer_description *image, TriggerInfo * info);

protected:
        void Run();

private:
        void ClearPool();
        void Init();
        void Deinit();

        CaptureInfo *CreateCaptureInfo(struct image_buffer_description *image,
                                        TriggerInfo * info);
        void ViolationPassing(struct image_buffer_description *image,
                                        TriggerInfo * info);
        void Enqueue(struct image_buffer_description *image, TriggerInfo *info);

        bool IsTimeout(struct timeval *trigger_time);
        bool IsMatch(struct image_buffer_description *image, TriggerInfo *info);
        void AddSampleToUploader(CaptureInfo * &info);

        Mutex _mutex;
        bool _queue_empty;
        int _queue_size;

        MemPool *_mem_pool;
        Uploader *_uploader;
};

#endif
