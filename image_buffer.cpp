/**
 * @file        image_buffer.cpp
 * @brief       cmem图像缓存队列,缓存处理过后的图像
 * @author      hrh <huangrh@landuntec.com>
 * @version     1.0.0
 * @date        2012-05-16
 *
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <string.h>
#include <unistd.h>
#include <list>
#include "mutex.h"
#include "image_buffer.h"
#include "debug.h"

static struct env_image_buffer env_image_buffer;

static std::list < void *>image_buffer_list;
static std::list < void *>capture_buffer_list;
static std::list < void *>transport_buffer_list;

static int buffer_length = 0;
static Mutex list_mutex;

int image_buffer_init(struct env_image_buffer *env)
{
        int index       = 0;
        void *tmp       = NULL;
        buffer_length   = env->buf_len;

        for (; index < env->buf_num; index++) {
                if (buffer_createEx(&tmp, buffer_length) < 0) {
                        Debug(ERROR, "createEx failed");
                        break;
                }
                image_buffer_list.push_back(tmp);
        }

        memcpy(&env_image_buffer, env, sizeof(*env));
        return 0;
}

int image_buffer_deinit(void)
{
        if (list_mutex.Lock()) {
                void *tmp = NULL;
                while (!image_buffer_list.empty()) {
                        tmp = image_buffer_list.front();
                        image_buffer_list.pop_front();
                        buffer_deleteEx(tmp, buffer_length);
                }
                list_mutex.Unlock();
        }
        return 0;
}

int capture_buffer_alloc(struct image_buffer_description **desc)
{
        void *buffer = NULL;
        struct image_buffer_description *d = NULL;

        list_mutex.Lock();
        if ((image_buffer_list.size() >
             (std::list < void *>::size_type)env_image_buffer.resd_buf_num_max)
            && (capture_buffer_list.size() <
                (std::list < void *>::size_type)env_image_buffer.capt_buf_num_max)) {

                buffer = image_buffer_list.front();
                image_buffer_list.pop_front();
        }
        list_mutex.Unlock();

        if (buffer == NULL) {
                return -1;
        }

        d = (struct image_buffer_description *)buffer;
        d->info.vir_addr = (unsigned int)buffer + sizeof(*d);
        *desc = d;

        return 0;
}

int capture_buffer_free(struct image_buffer_description *desc)
{
        list_mutex.Lock();
        image_buffer_list.push_front((void *)desc);
        list_mutex.Unlock();
        return 0;
}

int capture_buffer_get(struct image_buffer_description **desc)
{
        void *buffer = NULL;
        struct image_buffer_description *d = NULL;

        list_mutex.Lock();
        if (capture_buffer_list.empty()) {
                buffer = NULL;
        } else {
                buffer = capture_buffer_list.front();
                capture_buffer_list.pop_front();
        }
        list_mutex.Unlock();

        if (buffer == NULL) {
                return -1;
        }

        d = (struct image_buffer_description *)buffer;
        d->info.vir_addr = (unsigned int)buffer + sizeof(*d);
        *desc = d;

        return 0;
}

int capture_buffer_put(struct image_buffer_description *desc)
{
        list_mutex.Lock();
        capture_buffer_list.push_back((void *)desc);
        list_mutex.Unlock();
        return 0;
}

/* transport buffer operation */
int transport_buffer_alloc(struct image_buffer_description **desc)
{
        void *buffer = NULL;
        struct image_buffer_description *d = NULL;

        list_mutex.Lock();
        if ((image_buffer_list.size() >
             (std::list < void *>::size_type)env_image_buffer.resd_buf_num_max)
            && (transport_buffer_list.size() <
                (std::list < void *>::size_type)env_image_buffer.tran_buf_num_max)) {
                buffer = image_buffer_list.front();
                image_buffer_list.pop_front();
        }
        list_mutex.Unlock();

        if (buffer == NULL) {
                return -1;
        }

        d = (struct image_buffer_description *)buffer;
        d->info.vir_addr = (unsigned int)buffer + sizeof(*d);
        *desc = d;

        return 0;
}

int transport_buffer_free(struct image_buffer_description *desc)
{
        list_mutex.Lock();
        image_buffer_list.push_front((void *)desc);
        list_mutex.Unlock();
        return 0;
}

int transport_buffer_get(struct image_buffer_description **desc)
{
        void *buffer = NULL;
        struct image_buffer_description *d = NULL;

        list_mutex.Lock();
        if (transport_buffer_list.empty()) {
                buffer = NULL;
        } else {
                buffer = transport_buffer_list.front();
                transport_buffer_list.pop_front();
        }
        list_mutex.Unlock();

        if (buffer == NULL) {
                return -1;
        }

        d = (struct image_buffer_description *)buffer;
        d->info.vir_addr = (unsigned int)buffer + sizeof(*d);
        *desc = d;

        return 0;
}

int transport_buffer_put(struct image_buffer_description *desc)
{
        list_mutex.Lock();
        transport_buffer_list.push_back((void *)desc);
        list_mutex.Unlock();
        return 0;
}

int transport_buffer_get_level(void)
{
        int len = 0;
        int level = 0;
        list_mutex.Lock();
        len = transport_buffer_list.size();
        if (len < env_image_buffer.tran_buf_num_min) {
                level = -1;
        } else if (len > env_image_buffer.tran_buf_num_max) {
                level = 1;
        } else {
                level = 0;
        }
        list_mutex.Unlock();

        return level;
}
