/**
 * @file        image_process.h
 * @brief       图像处理接口
 * @author      hrh <huangrh@landuntec.com>
 * @version     1.0.0
 * @date        2012-07-06
 *
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011-2012
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef _IMAGE_PROCESS_H_
#define _IMAGE_PROCESS_H_

#include "cecodec.h"
#include "ldczn_base.h"

enum CaptureStatus {
        VIDEO,
        CAPTURE
};

typedef struct {
        int top_position;
        int left_position;
        int width;
        int height;
} Rectangle;

class ImageProcess {

public:
        ImageProcess(int type);
        ~ImageProcess();

        void SetJpegQvalue(int value);
        int CaptureProcess(struct cmemBuffer *buffer,
                           struct image_buffer_description *image, TriggerInfo * info);
        int VideoProcess(struct cmemBuffer *buffer, struct image_buffer_description *image);
        void SetRedlightZoneProcess(RedLightZone *redlight_zone);

private:
        void InitProcessor(int type);

        bool OverlayText(struct cmemBuffer &imageInfo, const struct overlay_text *text, int len);
        bool JpegEncodeProcess(struct cmemBuffer &imageInfo, struct cmemBuffer *outBuffer);

        void GetWatermark(char *pWmcode);
        int GetAuthentication(char *serialNumber, char *pAucode);
        int EmbedAntiCounterfeitInfo(const unsigned char *au_code, unsigned int au_code_len,
                                     const unsigned char *wm_code, unsigned int wm_code_len,
                                     unsigned char *jpeg_buf, unsigned int *jpeg_buf_len);
        int CreateJPGFileHash(unsigned char *jpeg_buf, unsigned int *jpeg_buf_len);
        int TextPrepare(TriggerInfo *info);

        bool ReviseRedlight(struct cmemBuffer &imageInfo, Rectangle zone);
        int _zone_number;

        struct jpeg_params _param;
        CaptureStatus _status;
};

#endif
