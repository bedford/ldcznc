/**
 * @file 	udp_server.cpp
 * @brief	UDP服务类的实现	
 * @author	hrh <huangrh@landuntec.com>
 * @version 	1.0.0
 * @date 	2011-12-13
 *
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <string.h>
#include "udp_server.h"
#include "socket.h"
#include "ldczn_protocol.h"
#include "debug.h"

#include "parameters.h"

const int kRecvBufLength = 1024;

static inline void fill_std_header(struct header_std *head,
                                   char encoding,
                                   char *auth_code, char msg_type, unsigned int msg_size)
{
        const char magic[] = PROTOCOL_MAGIC;
        memcpy(head->magic, magic, sizeof(magic));

        head->protocol_major    = PROTOCOL_MAJOR;
        head->protocol_minor    = PROTOCOL_MINOR;
        head->encoding          = encoding;
        memcpy(head->auth_code, auth_code, sizeof(head->auth_code));

        head->msg_type = msg_type;
        head->msg_size = msg_size;
}

UdpServer::UdpServer()
{

}

UdpServer::~UdpServer()
{

}

void UdpServer::BroadcastToClient()
{
        int broadcast_sock;     //广播回复socket
        broadcast_sock = Socket::CreateUdp();
        if (broadcast_sock < 0) {
                Debug(WARN, "Create Udp failed");
                return;
        }

        NetworkParam _network_param     = Parameters::GetInstance()->GetNetworkParam();
        DeviceInfo _device_info         = Parameters::GetInstance()->GetDeviceInfo();

        struct packet_search_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        memcpy(packet.search.serial_number, _device_info.serial_number,
               sizeof(packet.search.serial_number));
        memcpy(packet.search.site, _device_info.site, sizeof(packet.search.site));
        memcpy(packet.search.device_ip, _network_param.device_ip, sizeof(packet.search.device_ip));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = REQ_TYPE_DEVICE_SEARCH;
        packet.ack.ack.status   = ACK_SUCCESS;

        Socket::SendBroadcast(broadcast_sock, 39001, (char *)&packet, sizeof(packet));
        Socket::Close(broadcast_sock);
}

void UdpServer::Init()
{
        server_sock = Socket::CreateUdp();
        if (server_sock < 0) {
                Debug(INFO, "Create udp failed");
                return;
        }

        if (Socket::Bind(server_sock, 39000) < 0) {
                Debug(INFO, "Bind port 39000 failed");
                return;
        }
}

void UdpServer::Process(char *buf, int len)
{
        struct header_std *head = (struct header_std *)buf;
        if ((size_t)len < sizeof(struct header_std)) {
                Debug(INFO, "len of recv buf is too short");
                return;
        }

        struct payload_req *req = (struct payload_req *)(buf + sizeof(*head));
        if ((req->type & 0xFF000000) == REQ_TYPE_DEVICE_SEARCH) {
                BroadcastToClient();
        }
        Debug(DEBUG, "request type is %d", req->type);
}

void UdpServer::Run()
{
        Init();
        Debug(DEBUG, "start udp server");

        int ret         = -1;
        int port        = 0;
        int bytes       = -1;

        char addr[MIN_LENGTH];
        char buf[kRecvBufLength];

        while (!IsTerminated()) {

                ret = Socket::CheckRead(server_sock, 2000);
                if (ret < 0) {
                        Debug(INFO, "Check socket error");
                        break;
                }

                if (ret > 0) {
                        if ((bytes = Socket::RecvFrom(server_sock, buf, addr, &port)) < 0) {
                                Debug(INFO, "udpserver recvfrom error");
                                break;
                        }
                        Debug(DEBUG, "recv from %s:%d", addr, port);
                        Process(buf, bytes);
                }
        }

        Socket::Close(server_sock);
        Debug(DEBUG, "exit udp server");
}
