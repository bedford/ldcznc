#ifndef _FLASH_LIGHT_CTL_H_
#define _FLASH_LIGHT_CTL_H_

typedef struct {
        unsigned char current_gray;
} FlashCtl;

class FlashLightCtl {

      public:
        static bool GetLedFlashStatus(FlashCtl * params);
        static bool SetThreshold(unsigned int value);
        static bool SetFlashMode(int mode);
};

#endif
