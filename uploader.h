#ifndef _UPLOADER_H_
#define _UPLOADER_H_

#include <list>
#include "thread.h"
#include "ldczn_base.h"

class Mutex;
class MemPool;
class Storage;

class Uploader:public Thread {
      public:
        Uploader();
        ~Uploader();
        void AddSampleToListFront(CaptureInfo * captureInfo);
        void GetSampleFromListFront(CaptureInfo ** captureInfo);
        void SaveOneShot(CaptureInfo * captureInfo);

        void Open(MemPool * pool);
        void Close();

      protected:

        void Run();             //从线程类中继承

        Mutex m_lstMutex;
        std::list < CaptureInfo * >m_lstCaptureInfo;
        Storage *m_pVehicleInfoStorage;

      private:
        void FreeSample(CaptureInfo * captureInfo);
        void Process(CaptureInfo * captureInfo);
        bool savePicData(CaptureInfo * captureInfo, bool bNeedUpload);
        void AddCaptureInfoFromDisk();

        bool m_bLastSendFinish;         //上次是不传送成功
        bool m_bNoPictureOnDisk;        //没有可传的图片在磁盘上

        struct timeval LastTime;
        MemPool *mem_pool;
};

#endif
