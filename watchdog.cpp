#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/watchdog.h>
#include <errno.h>
#include <string.h>
#include "watchdog.h"
#include "debug.h"

static int wtd_fd = -1;
static const char *kWatchDog = "/dev/watchdog";

int WatchDog::FeedWatchDog()
{
        char buf[8];
        if (write(wtd_fd, buf, sizeof(buf)) < 0) {
                Debug(WARN, "feed watchdog failed");
                return -1;
        }

        return 0;
}

int WatchDog::OpenWatchDog()
{
        if ((wtd_fd = open(kWatchDog, O_RDWR)) < 0) {
                Debug(WARN, "Can't open watchdog device");
                return -1;
        }

        Debug(DEBUG, "Open watchdog success");
        return 0;
}

void WatchDog::StartWatchDog()
{
        ioctl(wtd_fd, WDIOC_STARTWATCHDOG, NULL);
}

int WatchDog::SetWatchDogTimeout()
{
        int timeout = 180;
        if (ioctl(wtd_fd, WDIOC_SETRESTARTTIME, (unsigned long)&timeout) < 0) {
                Debug(WARN, "set watch dog timeout time failed");
                return -1;
        }

        return 0;
}

int WatchDog::InitWatchDog()
{
        int ret = OpenWatchDog();
        if (ret < 0) {
                return -1;
        }

        ret = SetWatchDogTimeout();
        if (ret < 0) {
                return -1;
        }

        StartWatchDog();

        return ret;
}
