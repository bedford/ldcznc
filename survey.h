#ifndef _SURVEY_H_
#define _SURVEY_H_

#include "ldczn_base.h"

typedef struct _passing_status {
        int vehicle_length;
        int vehicle_speed;
        int event_type;
        int lane;
} PassingStatus;

class Survey {
public:
        static void TrafficStaticsRefresh(void);
        static int TrafficStaticsUpload(void);
        static int PassingStaticsUpdate(PassingStatus * data);
        static int DeviceStatusUpload(void);
        static void Init();
        static void DeInit();

private:
        Survey();
        ~Survey();
        static void TrafficStaticsReset();
};

#endif
