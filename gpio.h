#ifndef _GPIO_H_
#define _GPIO_H_

class GpioCtl {

      public:
        static int MannualSnap();
        static int ResetFpga();
        static int ResetPower();

};

#endif
