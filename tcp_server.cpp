/**
 * @file	TcpServer.cpp
 * @brief	Tcp服务端类实现
 * @author	hrh <huangrh@landuntec.com>
 * @version 	1.0.0
 * @date 	2011-12-07
 *
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#include <unistd.h>
#include <string.h>
#include <signal.h>
#include "socket.h"
#include "tcp_client.h"
#include "tcp_server.h"
#include "sensor.h"
#include "gpio.h"
#include "debug.h"
#include "parameters.h"
#include "uart.h"
#include "util.h"
#include "peripherral_manage.h"
#include "flash_light.h"
#include "upgrade_program.h"

#define RECV_BUF_LENGTH 1024

static inline void fill_std_header(struct header_std* head,
                                   char encoding,
                                   char* auth_code, char msg_type, unsigned int msg_size)
{
        const char magic[] = PROTOCOL_MAGIC;
        memcpy(head->magic, magic, sizeof(magic));

        head->protocol_major    = PROTOCOL_MAJOR;
        head->protocol_minor    = PROTOCOL_MINOR;
        head->encoding          = encoding;
        memcpy(head->auth_code, auth_code, sizeof(head->auth_code));

        head->msg_type = msg_type;
        head->msg_size = msg_size;
}

TcpServer::TcpServer(TcpClient* client)
{
        server_sock     = -1;
        clnt_sock       = -1;
        _tcp_client     = client;
        InitClient();
}

TcpServer::~TcpServer()
{
        server_sock = -1;
        clnt_sock = -1;

        _tcp_client = NULL;
        Debug(DEBUG, "deconstruct tcp server");
}

void TcpServer::InitClient()
{
        UploadParam param = Parameters::GetInstance()->GetUploadParam();
        struct _ClientInfo client_info;
        memcpy(client_info.addr, param.client_addr, sizeof(client_info.addr));
        _tcp_client->SetClient(&client_info);
}

/**
 * @function	void Init()
 * @brief	初始化tcp server，并监听39002端口
 *
 */
void TcpServer::Init()
{
        server_sock = Socket::CreateTcp();
        if (server_sock < 0) {
                Debug(INFO, "Create Nonblock Tcp failed");
                return;
        }

        Socket::SetTcpNonblock(server_sock);
        int ret = Socket::Bind(server_sock, 39002);
        if (ret < 0) {
                Debug(INFO, "Bind port 39002 failed");
                Socket::Close(server_sock);
                return;
        }

        ret = Socket::Listen(server_sock, 5);
        if (ret < 0) {
                Debug(INFO, "Listen port failed");
                Socket::Close(server_sock);
                return;
        }
}

void TcpServer::Run()
{
        Init();

        char buf[RECV_BUF_LENGTH];
        while (!IsTerminated()) {
                clnt_sock = Socket::Accept(server_sock, 2000);
                if (clnt_sock < 0) {
                        usleep(100000);
                        continue;
                }

                int ret = Socket::Read(clnt_sock, buf, RECV_BUF_LENGTH, 5000);
                if (ret <= 0) {
                        Debug(INFO, "remote is not alive");
                } else if (ret < (int)sizeof(PacketRequest)) {
                        Debug(INFO, "got %d packet, buf too less packet", ret);
                } else {
                        ParsePacket(buf, ret);
                }
                Socket::Close(clnt_sock);
                clnt_sock = -1;
                usleep(100000);
        }

        if (clnt_sock >= 0) {
                Socket::Close(clnt_sock);
        }
        Socket::Close(server_sock);
}

static inline int get_auth_code(char *auth_code, int len)
{
        auth_code = auth_code;
        len = len;
        return 0;
}

static inline int match_auth_code(const char *auth_code, int len)
{
        auth_code = auth_code;
        len = len;
        return 0;
}

int TcpServer::SendToClient(char *buf, int len)
{
        if (Socket::Writen(clnt_sock, buf, len, 2000) == len) {
                return 0;
        }

        return -1;
}

int TcpServer::ParsePacket(char *buf, int len)
{
        if ((unsigned int)len < sizeof(struct header_std)) {
                return -1;
        }

        struct header_std *head = (struct header_std *)buf;
        if (ProcessHeader(head, len)) {
                return -1;
        }

        switch (head->msg_type) {
        case MESSAGE_TYPE_REQ:
                ProcessRequest((struct payload_req *)(buf + sizeof(*head)),
                               buf + sizeof(*head) + sizeof(struct payload_req));
                break;
        case MESSAGE_TYPE_ACK:
                ProcessAck((struct payload_ack *)(buf + sizeof(*head)),
                           buf + sizeof(*head) + sizeof(struct payload_ack));
                break;
        default:
                break;
        }

        return len - head->msg_size;
}

int TcpServer::ProcessHeader(struct header_std *head, int packet_len)
{
        const char magic[sizeof(head->magic)] = PROTOCOL_MAGIC;

        if (memcmp(head->magic, magic, sizeof(magic)) != 0) {
                return -1;
        }

        if ((head->protocol_major != PROTOCOL_MAJOR) || (head->protocol_minor != PROTOCOL_MINOR)) {
                return -1;
        }

        if (head->encoding != ENCODING_TYPE_RAW) {
                return -1;
        }

        if (match_auth_code(head->auth_code, sizeof(head->auth_code)) < 0) {
                return -1;
        }

        if (head->msg_size > (unsigned int)packet_len) {
                return -1;
        }

        return 0;
}

int TcpServer::ProcessRequest(struct payload_req *req, char *buf)
{
        switch (req->type & REQ_TYPE_MASK) {

        case REQ_TYPE_HEARTBEAT:
                return ProcessHeartBeat(req, buf);
                break;
        case REQ_TYPE_MANUFACTURE:
                return ProcessMannufacture(req, buf);
                break;
        case REQ_TYPE_CONTROL:
                return ProcessControl(req);
                break;
        case REQ_TYPE_SET_PARAMETER:
                return ProcessSetParameter(req, buf);
                break;
        case REQ_TYPE_GET_PARAMETER:
                return ProcessGetParameter(req);
                break;
        default:
                break;
        }

        return -1;
}

int TcpServer::ProcessAck(struct payload_ack *ack, char *buf)
{
        ack = ack;
        buf = buf;
        return 0;
}

int TcpServer::ProcessHeartBeat(struct payload_req *req, char *buf)
{
        req = req;
        buf = buf;
        return 0;
}

int TcpServer::ProcessControl(struct payload_req *req)
{
        DeviceStatus device_status;
        unsigned int status;

        switch (req->type & REQ_TYPE_SUB_MASK) {
        case CTL_TYPE_VIDEO:
                Sensor::GetInstance()->SetSensorVideo();
                PeripherralManage::DisableRecv();
                break;
        case CTL_TYPE_CAPTURE:
                Sensor::GetInstance()->SetSensorCapture();
                PeripherralManage::EnableRecv();
                break;
        case CTL_TYPE_MANNUAL_SNAP:
                GpioCtl::MannualSnap();
                break;
        case CTL_TYPE_REBOOT:
                device_status   = Parameters::GetInstance()->GetDeviceStatus();
                status          = device_status.status | REBOOT_SYSTEM;
                Parameters::GetInstance()->ChangeDeviceStatus(status);
                break;
        case CTL_TYPE_SIGNAL_MODULE_TESTING:
                device_status   = Parameters::GetInstance()->GetDeviceStatus();
                status          = device_status.status | SIGNAL_MODULE_TESTING;
                Parameters::GetInstance()->ChangeDeviceStatus(status);
                break;
        default:
                break;
        }

        ReturnAck(req);
        return 0;
}

int TcpServer::ProcessSetParameter(struct payload_req *req, char *buf)
{
        switch (req->type & REQ_TYPE_SUB_MASK) {
        case PARAM_TYPE_CAMERA:
                ProcessSetCameraParameter(req, buf);
                break;
        case PARAM_TYPE_NETWORK:
                ProcessSetNetworkParam(buf);
                break;
        case PARAM_TYPE_UPLOAD:
                ProcessSetUploadParam(buf);
                break;
        case PARAM_TYPE_TIME:
                ProcessCalibrateTime(buf);
                break;
        case PARAM_TYPE_FLASH:
                ProcessSetFlashParam(buf);
                break;
        case PARAM_TYPE_PLATE:
                break;
        case PARAM_TYPE_VIDEO_DETECT:
                break;
        case PARAM_TYPE_MAN_PROPERTY:
                break;
        case PARAM_TYPE_DEVICE_INFO:
                ProcessSetDeviceInfo(buf);
                break;
        case PARAM_TYPE_REDLIGHT_ZONE:
                ProcessSetRedlightZone(buf);
                break;
        case PARAM_TYPE_OVERLAY_SETTING:
                ProcessSetOverlaySetting(buf);
                break;
        default:
                break;
        }

        ReturnAck(req);
        return 0;
}

int TcpServer::ProcessSetCameraParameter(struct payload_req *req, char *buf)
{
        CameraParam *setting = (CameraParam *) buf;
        Parameters::GetInstance()->SetCameraParam(setting);
        Debug(DEBUG,"qvalue is %d", setting->jpeg_quality_value);

        switch (req->type & REQ_TYPE_CMD_MASK) {
        case PARAM_CAMERA_DEFAULT_GAIN:
                Sensor::GetInstance()->SetSensorGain(setting->default_gain);
                break;
        case PARAM_CAMERA_MIN_GAIN:
                Sensor::GetInstance()->SetSensorMinGain(setting->min_gain);
                break;
        case PARAM_CAMERA_MAX_GAIN:
                Sensor::GetInstance()->SetSensorMaxGain(setting->max_gain);
                break;
        case PARAM_CAMERA_DEFAULT_EXPOSURE:
                Sensor::GetInstance()->SetSensorExposure(setting->default_exposure);
                break;
        case PARAM_CAMERA_MIN_EXPOSURE:
                Sensor::GetInstance()->SetSensorMinExp(setting->min_exposure);
                break;
        case PARAM_CAMERA_MAX_EXPOSURE:
                Sensor::GetInstance()->SetSensorMaxExp(setting->max_exposure);
                break;
        case PARAM_CAMERA_RED_GAIN:
                Sensor::GetInstance()->SetSensorRgain(setting->red_gain);
                break;
        case PARAM_CAMERA_BLUE_GAIN:
                Sensor::GetInstance()->SetSensorBgain(setting->blue_gain);
                break;
        case PARAM_CAMERA_VIDEO_TARGET:
                Sensor::GetInstance()->SetSensorVideoTargetGray(setting->video_target_gray);
                break;
        case PARAM_CAMERA_CAPTURE_TARGET:
                Sensor::GetInstance()->SetSensorCaptureTargetGray(setting->trigger_target_gray);
                break;
        case PARAM_CAMERA_AD_SAMPLE_BITS:
                Sensor::GetInstance()->SetSensorADSampleBits(setting->ad_sample_bits);
                break;
        case PARAM_CAMERA_SHARPEN_FACTOR:
                Sensor::GetInstance()->SetSensorSharpenFactor(setting->sharpen_factor);
                break;
        case PARAM_CAMERA_AEW_MODE:
                switch (setting->aew_mode) {
                case AEWMODE_DISABLE:
                        Sensor::GetInstance()->SetSensorAEManual();
                        break;
                case AEWMODE_GAIN:
                        Sensor::GetInstance()->SetSensorAEAuto();
                        Sensor::GetInstance()->SetSensorAEMethod(0x01);
                        break;
                case AEWMODE_EXP:
                        Sensor::GetInstance()->SetSensorAEAuto();
                        Sensor::GetInstance()->SetSensorAEMethod(0x02);
                        break;
                case AEWMODE_AUTO:
                        Sensor::GetInstance()->SetSensorAEAuto();
                        Sensor::GetInstance()->SetSensorAEMethod(0x00);
                        break;
                default:
                        break;
                }
                break;
        default:
                switch (setting->aew_mode) {
                case AEWMODE_DISABLE:
                        Sensor::GetInstance()->SetSensorAEManual();
                        break;
                case AEWMODE_GAIN:
                        Sensor::GetInstance()->SetSensorAEAuto();
                        Sensor::GetInstance()->SetSensorAEMethod(0x01);
                        break;
                case AEWMODE_EXP:
                        Sensor::GetInstance()->SetSensorAEAuto();
                        Sensor::GetInstance()->SetSensorAEMethod(0x02);
                        break;
                case AEWMODE_AUTO:
                        Sensor::GetInstance()->SetSensorAEAuto();
                        Sensor::GetInstance()->SetSensorAEMethod(0x00);
                        break;
                default:
                        break;
                }

                Sensor::GetInstance()->SetSensorExposure(setting->default_exposure);
                Sensor::GetInstance()->SetSensorMinExp(setting->min_exposure);
                Sensor::GetInstance()->SetSensorMaxExp(setting->max_exposure);
                Sensor::GetInstance()->SetSensorMinGain(setting->min_gain);
                Sensor::GetInstance()->SetSensorMaxGain(setting->max_gain);
                Sensor::GetInstance()->SetSensorGain(setting->default_gain);
                Sensor::GetInstance()->SetSensorBgain(setting->blue_gain);
                Sensor::GetInstance()->SetSensorRgain(setting->red_gain);
                Sensor::GetInstance()->SetSensorVideoTargetGray(setting->video_target_gray);
                Sensor::GetInstance()->SetSensorCaptureTargetGray(setting->trigger_target_gray);
                Sensor::GetInstance()->SetSensorAEZone(setting->ae_zone);
                Sensor::GetInstance()->SetSensorADSampleBits(setting->ad_sample_bits);
                Sensor::GetInstance()->SetSensorSharpenFactor(setting->sharpen_factor);
                break;
        }

        return 0;
}

int TcpServer::ProcessSetFlashParam(char *buf)
{
        FlashParam *setting = (FlashParam *) buf;
        Parameters::GetInstance()->SetFlashParam(setting);

        if (setting->flash_mode == FlashOn) {
                Sensor::GetInstance()->SetSensorFlashOn();
        } else if (setting->flash_mode == FlashOff) {
                Sensor::GetInstance()->SetSensorFlashOff();
        }

        if (setting->led_mode == FlashOn) {
                Sensor::GetInstance()->SetSensorLedOn();
        } else if (setting->led_mode == FlashOff) {
                Sensor::GetInstance()->SetSensorLedOff();
        }

        if (setting->continuous_light == FlashOn) {
                Sensor::GetInstance()->SetSensorConstantLightOn();
        } else if (setting->continuous_light == FlashOff) {
                Sensor::GetInstance()->SetSensorConstantLightOff();
        }

        if (setting->redlight_sync_mode) {
                Sensor::GetInstance()->SetSensorSyncOn();
        } else {
                Sensor::GetInstance()->SetSensorSyncOff();
        }

        Sensor::GetInstance()->SetSensorFlashDelay(setting->flash_delay);
        Sensor::GetInstance()->SetSensorRedLightDelay(setting->redlight_delay);
        Sensor::GetInstance()->SetSensorRedLightEfficient(setting->redlight_efficient);
        Sensor::GetInstance()->SetSensorLEDMultiple(setting->led_mutiple);

        FlashLightCtl::SetThreshold(setting->flash_on_threshold);
        FlashLightCtl::SetFlashMode(setting->flash_mode);

        return 0;
}

int TcpServer::ProcessSetDeviceInfo(char *buf)
{
        DeviceInfo *setting = (DeviceInfo *) buf;
        Parameters::GetInstance()->SetDeviceInfo(setting);

        return 0;
}

int TcpServer::ProcessSetNetworkParam(char *buf)
{
        NetworkParam *setting = (NetworkParam *) buf;
        Parameters::GetInstance()->SetNetworkParam(setting);
        return 0;
}

int TcpServer::ProcessSetUploadParam(char *buf)
{
        UploadParam *setting = (UploadParam *) buf;
        Parameters::GetInstance()->SetUploadParam(setting);

        InitClient();

        return 0;
}

int TcpServer::ProcessGetParameter(struct payload_req *req)
{
        switch (req->type & REQ_TYPE_SUB_MASK) {
        case PARAM_TYPE_CAMERA:
                ProcessGetCameraParameter(req);
                break;
        case PARAM_TYPE_NETWORK:
                ProcessGetNetworkParameter(req);
                break;
        case PARAM_TYPE_UPLOAD:
                ProcessGetUploadParam(req);
                break;
        case PARAM_TYPE_TIME:
                break;
        case PARAM_TYPE_FLASH:
                ProcessGetFlashParam(req);
                break;
        case PARAM_TYPE_PLATE:
                break;
        case PARAM_TYPE_VIDEO_DETECT:
                break;
        case PARAM_TYPE_MAN_PROPERTY:
                ProcessGetProperty(req);
                break;
        case PARAM_TYPE_DEVICE_INFO:
                ProcessGetDeviceInfo(req);
                break;
        case PARAM_TYPE_TRAFFIC:
                ProcessGetTrafficParam(req);
                break;
        case PARAM_TYPE_REDLIGHT_ZONE:
                ProcessGetRedlightZone(req);
                break;
        case PARAM_TYPE_OVERLAY_SETTING:
                ProcessGetOverlaySetting(req);
                break;
        default:
                break;
        }

        return 0;
}

int TcpServer::ProcessGetProperty(struct payload_req *req)
{
        switch (req->type & REQ_TYPE_CMD_MASK) {
                case PARAM_MAN_PROPERTY_PROGRAM_VERSION:
                ProcessGetProgramVersion(req);
                break;
        default:
                break;
        }

        return 0;
}

int TcpServer::ProcessGetProgramVersion(struct payload_req *req)
{
        ProgramVersion version = Parameters::GetInstance()->GetProgramVersion();

        struct packet_programversion_gparam_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id = 0;
        packet.ack.ack.type = req->type;
        packet.ack.ack.status = ACK_SUCCESS;
        memcpy(&packet.program_version, &version, sizeof(ProgramVersion));

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ReturnAck(struct payload_req *req)
{
        struct packet_img_gparm_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id = req->id;
        packet.ack.ack.type = req->type;
        packet.ack.ack.status = ACK_SUCCESS;

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessGetCameraParameter(struct payload_req *req)
{
        CameraParam params = Parameters::GetInstance()->GetCameraParam();

        struct packet_img_gparm_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id = 0;
        packet.ack.ack.type = req->type;
        packet.ack.ack.status = ACK_SUCCESS;

        packet.parameter.default_exposure       = params.default_exposure;
        packet.parameter.min_exposure           = params.min_exposure;
        packet.parameter.max_exposure           = params.max_exposure;
        packet.parameter.default_gain           = params.default_gain;
        packet.parameter.min_gain               = params.min_gain;
        packet.parameter.max_gain               = params.max_gain;
        packet.parameter.red_gain               = params.red_gain;
        packet.parameter.blue_gain              = params.blue_gain;
        packet.parameter.video_target_gray      = params.video_target_gray;
        packet.parameter.trigger_target_gray    = params.trigger_target_gray;
        packet.parameter.ae_zone                = params.ae_zone;
        packet.parameter.aew_mode               = params.aew_mode;
        packet.parameter.jpeg_quality_value     = params.jpeg_quality_value;
        packet.parameter.ad_sample_bits         = params.ad_sample_bits;
        packet.parameter.sharpen_factor         = params.sharpen_factor;

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessGetDeviceInfo(struct payload_req *req)
{
        DeviceInfo params = Parameters::GetInstance()->GetDeviceInfo();

        struct packet_deviceinfo_gparam_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = req->type;
        packet.ack.ack.status   = ACK_SUCCESS;

        memcpy(&packet.info, &params, sizeof(DeviceInfo));

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessGetNetworkParameter(struct payload_req *req)
{
        NetworkParam params = Parameters::GetInstance()->GetNetworkParam();

        struct packet_networparam_gparam_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = req->type;
        packet.ack.ack.status   = ACK_SUCCESS;

        memcpy(&packet.parameter, &params, sizeof(NetworkParam));

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessGetUploadParam(struct payload_req *req)
{
        UploadParam params = Parameters::GetInstance()->GetUploadParam();

        struct packet_uploadinfo_gparam_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = req->type;
        packet.ack.ack.status   = ACK_SUCCESS;

        memcpy(&packet.parameter, &params, sizeof(UploadParam));

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessGetTrafficParam(struct payload_req *req)
{
        TrafficParam params = Parameters::GetInstance()->GetTrafficParam();

        struct packet_deviceinfo_gparam_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = req->type;
        packet.ack.ack.status   = ACK_SUCCESS;

        memcpy(&packet.info, &params, sizeof(TrafficParam));

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessGetFlashParam(struct payload_req *req)
{
        FlashParam params = Parameters::GetInstance()->GetFlashParam();

        struct packet_flash_gparam_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = req->type;
        packet.ack.ack.status   = ACK_SUCCESS;

        memcpy(&packet.parameter, &params, sizeof(FlashParam));

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessUpgrade(struct payload_req* req, char* buf)
{
        int ret = UpgradeProgram::UpdateProgram(req->type & REQ_TYPE_CMD_MASK, buf);

        struct packet_man_upgrade_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = req->type;

        if (ret < 0) {
                packet.ack.ack.status   = ACK_FAILED;
        } else {       
                packet.ack.ack.status   = ACK_SUCCESS;
        }

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessMannufacture(struct payload_req *req, char *buf)
{
        switch (req->type & REQ_TYPE_SUB_MASK) {
                case REQ_MAN_FMT:
                        break;
                case REQ_MAN_UPG:
                        ProcessUpgrade(req, buf);
                        break;
                case REQ_MAN_RESET:
                        break;
                case REQ_MAN_CLR:
                        break;
        }

        return 0;
}

int TcpServer::ProcessCalibrateTime(char *buf)
{
        CompareTime *time = (CompareTime *) buf;
        char set_time[128];
        sprintf(set_time, "%04d-%02d-%02d %02d:%02d:%02d", time->year, time->mon + 1,
                time->day, time->hour, time->min, time->sec);

        Util::CalibrateTime(set_time);

        return 0;
}

int TcpServer::ProcessGetRedlightZone(struct payload_req *req)
{
        RedLightZone redlight_zone = Parameters::GetInstance()->GetRedlightZone();

        struct packet_redlight_gparam_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = req->type;
        packet.ack.ack.status   = ACK_SUCCESS;
        memcpy(&packet.parameter, &redlight_zone, sizeof(RedLightZone));

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessSetRedlightZone(char *buf)
{
        RedLightZone *zone = (RedLightZone *) buf;
        Parameters::GetInstance()->SetRedlightZone(zone);

        return 0;
}

int TcpServer::ProcessGetOverlaySetting(struct payload_req *req)
{
        Debug(INFO, "Get Overlay Setting");
        OverlaySetting overlay_setting = Parameters::GetInstance()->GetOverlaySetting();

        struct packet_overlaysetting_gparam_ack packet;
        char auth_code[sizeof(packet.ack.head)];
        fill_std_header(&packet.ack.head, ENCODING_TYPE_RAW, auth_code,
                        MESSAGE_TYPE_ACK, sizeof(packet));

        packet.ack.ack.id       = 0;
        packet.ack.ack.type     = req->type;
        packet.ack.ack.status   = ACK_SUCCESS;
        Debug(DEBUG, "device number is %d", overlay_setting.enable_device_number);
        memcpy(&packet.overlay_setting, &overlay_setting, sizeof(OverlaySetting));

        return SendToClient((char *)&packet, sizeof(packet));
}

int TcpServer::ProcessSetOverlaySetting(char *buf)
{
        Debug(INFO, "set overlay");
        OverlaySetting *overlay_setting = (OverlaySetting *)buf;
        Parameters::GetInstance()->SetOverlaySetting(overlay_setting);

        return 0;
}
