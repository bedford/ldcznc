/**
 * @file	camera.h
 * @brief	
 * @author	hrh <huangrh@landuntec.com>
 * @version	1.0.0
 * @date	2011-11-29
 *
 * @verbatim
 * ============================================================================
 * Copyright (c) Shenzhen Landun technology Co.,Ltd. 2011
 * All rights reserved. 
 * 
 * Use of this software is controlled by the terms and conditions found in the
 * license agreenment under which this software has been supplied or provided.
 * ============================================================================
 * 
 * @endverbatim
 * 
 */

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "cecodec.h"

typedef struct {
        int width;
        int height;
        int stride;
        int line;
        unsigned int max_buffer_number;
} CaptureCmemParams;

typedef struct {
        int width;
        int height;
} CaptureSize;

class Camera {

public:
        Camera(int ccd_type);
        ~Camera();
        bool CleanupCamera();
        bool InitCamera();
        bool OpenCamera();
        bool StartCapture();

        bool GetCaptureBuffer(struct cmemBuffer **pCaptureBufs, struct timeval *tv);
        bool PutCaptureBuffer(struct cmemBuffer *pCaptureBufs);

        unsigned int GetSensorStatusOffset();
        bool SetCaptureBufferSize(CaptureCmemParams * params);
        bool SetCaptureSize(CaptureSize * size);
        int GetCameraHandle();

private:
        bool OpenDevice(const char *deviceName);
        bool UserPtrInit();
        bool StopCapture();
        bool SetVideoFormat();
        bool InitDevice();

        bool RequestDriverBuffer();
        bool EnumInputDevice();
        bool QueryAndCheckCap();
        bool QueryAndSetStandard();

        struct cmemBuffer *_pCmemBuffer;
        CaptureCmemParams _capture_cmem_params;
        CaptureSize _capture_size;

        int _CameraFd;
        int _ccd_type;
};

#endif
